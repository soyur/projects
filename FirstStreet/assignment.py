'''
Title: Python Assignment for First Street GIS PYTHON DEVELOPER
Tasks: 
	You can test out some commands in whatever software you prefer, but ultimately all of these steps should be executable in a
	python script (either standalone or run through the ArcGIS pro console)

	1. Calculate zonal statistics with the property sample file (south_florida_properties_samp.shp) as zones and the elevation 
	data (south_florida_elevation_feet_dry_updated_comp2_samp.tif) provided. You should create a table that has statistics associated 
	with each property lot (the individual polygons). This table provides elevation statistics about each property.

	2. Calculate the difference between the elevation raster data (south_florida_elevation_feet_dry_updated_comp2_samp.tif) and the 
	mean higher high water (MHHW) 2017 sample file (sf_mhhw_rp_e-4_center_2017_samp.tif)  (MHHW_2017 - elevation) in order to get 
	water depth raster file (the values correspond to depth of water and note many places will be negative values indicating the land is above the water)

	3. In Python, write a script that takes the output from #2 and makes new files for each of the corresponding years that have the 
	water levels below added to all the values in the raster file. 

	This is like adding sea level rise to the water depths.

	Year	   Depth Increase
	2022	0.2
	2027	0.5
	2032	0.9
	2037	1.4
	2042	2
	2047	2.7
	2052	3.5
	2057	4.4
	2062	5.4
	2067	6.5

	4. Write a script that collects zonal statistics on each of the water depth files (2017 to 2067) using the properties shapefile 
	as zones and create a master table that has the maximum water depth for each property for each year. Also merge the elevation 
	data statistics collated in #1 onto this file. 
	
	5. Provide the script and instructions on how to execute the script in order to make the output.

Author: Sawyer Stippa
email: sawyer.stippa@gmail.com
Modified: 10/24/2017
'''

import arcpy

# Set Defaults
arcpy.env.workspace = '.'

class FS:
	'''
	Initiate Class FS
	'''
	
	def __init__(self,):
		arcpy.CheckOutExtension("SPATIAL")
		self.yearArray = {
						"2022":0.2,
						"2027":0.5,
						"2032":0.9,
						"2037":1.4,
						"2042":2,
						"2047":2.7,
						"2052":3.5,
						"2057":4.4,
						"2062":5.4,
						"2067":6.5
						}
						
		self.zone = '/data/south_florida_properties_samp.shp'
		self.zoneName = 'PARCELID'
			
	def assignment_1(self,inRaster,outTable):
		'''
		Description: Calculate Zonal Statistics and save results as csv
		Inputs:
			(1) self.zone
			(2) self.zoneName (ie PARCELID)
			(3) Raster to Analyze 
		Output:
			(1) Table of Zonal Statistics (.info)
			
		'''
		print "Starting Assignment 1"
		
		# Execute Zonal Stastics for ALL STATISTICS
		arcpy.sa.ZonalStatisticsAsTable(self.zone,self.zoneName, inRaster, outTable, "DATA", "ALL")
		
		print "Assignment 1 Complete"
	
	def assignment_2(self,inRaster1,inRaster2,outRaster):
		'''
		Description: Calculate differennce between two elevation rasters using "Minus Tool." For additional operators use rastor calculator
		Inputs: 
			(1) Raster 1
			(2) Raster2 to subtract from Raster 1
		Output:
			(1) Raster
		'''
		
		print "Starting Assignment 2"
		
		arcpy.Minus_3d(inRaster1,inRaster2,outRaster)
		
		print "Assignment 2 Complete"

	def assignment_3(self,inRaster):
		'''
		Description: Create new rasters for each year and depth increase
		Inputs: 
			(1) self.yearArray (YEAR:DEPTH INCREASE)
			(2) Raster
		Output:
			(n) Raster plus depth 
		'''
		
		print "Starting Assignment 3"
		
		for year in self.yearArray:
			outName  = 'assgn_3_' + year
			arcpy.Plus_3d(inRaster,self.yearArray[year],'/outputs/' + outName)
			print "%s complete" %outName
			
		print "Assignment 3 Complete"
		
		
	def assignment_4(self,inStats):
		'''
		 Write a script that collects zonal statistics on each of the water depth files (2017 to 2067) using the properties shapefile 
	as zones and create a master table that has the maximum water depth for each property for each year. Also merge the elevation 
	data statistics collated in #1 onto this file. 
		Description: Create zonal stats for each depth.
		Inputs: 
			(1) self.zone
			(2) self.zoneName
			(3) Zonal Stats
			(4) Raster Depths
		Output:
			(1) Max water depths Master Table
			(2) Merged Zonal Stats table
		'''
		
		print "Starting Assignment 4"
		
		init = True
		global master
		
		for year in self.yearArray:
			raster = '/outputs/assgn_3_' + year
			outStat = '/outputs/Stat_' + year
			outTable = outStat + '_MAX'
			
			arcpy.sa.ZonalStatisticsAsTable(self.zone,self.zoneName, raster, outStat, "DATA", "MAXIMUM")
			arcpy.TableToTable_conversion(outStat,'/outputs/',outTable,['PARCELID',{'MAX':'MAX_'+year}])
			
			if init:
				master = outTable
				init = False
			else:
				arcpy.JoinField_management(master,self.zoneName,outTable,self.zoneName,'MAX_'+year)
		
		arcpy.JoinField_management(master,self.zoneName,inStats,self.zoneName)
			
		print "Assignment 4 Complete"

if __name__ == "__main__": 
	import assignment as at
	at.FS().assignment_1('/data/south_florida_elevation_feet_dry_updated_comp2_samp.tif','outputs/assignment_1.info')
	
	at.FS().assignment_2('/data/sf_mhhw_rp_e-4_center_2017_samp.tif','/data/south_florida_elevation_feet_dry_updated_comp2_samp.tif','outputs/assignment_2')
	
	at.FS().assignment_3('/outputs/assignment_2')
	
	at.FS().assignment_4('outputs/assignment_1.info')