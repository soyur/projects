You can test out some commands in whatever software you prefer, but ultimately all of these steps should be executable in a python script (either standalone or run through the ArcGIS pro console)

1. Calculate zonal statistics with the property sample file (south_florida_properties_samp.shp) as zones and the elevation data (south_florida_elevation_feet_dry_updated_comp2_samp.tif) provided. You should create a table that has statistics associated with each property lot (the individual polygons). This table provides elevation statistics about each property.

2. Calculate the difference between the elevation raster data (south_florida_elevation_feet_dry_updated_comp2_samp.tif) and the mean higher high water (MHHW) 2017 sample file (sf_mhhw_rp_e-4_center_2017_samp.tif)  (MHHW_2017 - elevation)
in order to get water depth raster file (the values correspond to depth of water and note many places will be negative values indicating the land is above the water)

3. In Python, write a script that takes the output from #1 and makes new files for each of the corresponding years that have the water levels below added to all the values in the raster file. 

This is like adding sea level rise to the water depths.

Year	   Depth Increase
2022	0.2
2027	0.5
2032	0.9
2037	1.4
2042	2
2047	2.7
2052	3.5
2057	4.4
2062	5.4
2067	6.5

4. Write a script that collects zonal statistics on each of the water depth files (2017 to 2067) using the properties shapefile as zones and create a master table that has the maximum water 
depth for each property for each year. Also merge the elevation data statistics collated in #2 onto this file. 

5. Provide the script and instructions on how to execute the script in order to make the output.