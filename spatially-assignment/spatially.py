#http://geoffboeing.com/2014/08/clustering-to-reduce-spatial-data-set-size/
'''
import pandas as pd, numpy as np, matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from geopy.distance import great_circle
from shapely.geometry import MultiPoint
df = pd.read_csv('bosbiz.csv')
coords = df.as_matrix(columns=['lat', 'lon'])


kms_per_radian = 6371.0088
epsilon = 0.01 / kms_per_radian
db = DBSCAN(eps=epsilon, min_samples=1, algorithm='ball_tree', metric='haversine').fit(np.radians(coords))
cluster_labels = db.labels_
num_clusters = len(set(cluster_labels))
clusters = pd.Series([coords[cluster_labels == n] for n in range(num_clusters)])
print('Number of clusters: {}'.format(num_clusters))

def get_centermost_point(cluster):
	centroid = (MultiPoint(cluster).centroid.x, MultiPoint(cluster).centroid.y)
	centermost_point = min(cluster, key=lambda point: great_circle(point, centroid).m)
	return tuple(centermost_point)
centermost_points = clusters.map(get_centermost_point)


lats, lons = zip(*centermost_points)
rep_points = pd.DataFrame({'lon':lons, 'lat':lats})

rs = rep_points.apply(lambda row: df[(df['lat']==row['lat'])&(df['lon']==row['lon'])].iloc[0], axis=1)


fig, ax = plt.subplots(figsize=[10, 6])
rs_scatter = ax.scatter(rs['lon'], rs['lat'], c='#99cc99', edgecolor='None', alpha=0.7, s=120)
df_scatter = ax.scatter(df['lon'], df['lat'], c='k', alpha=0.9, s=3)
ax.set_title('Full data set vs DBSCAN reduced set')
ax.set_xlabel('Longitude')
ax.set_ylabel('Latitude')
ax.legend([df_scatter, rs_scatter], ['Full set', 'Reduced set'], loc='upper right')
plt.show()

'''



'''
import pandas as pd
import numpy as np

from sklearn.cluster import MeanShift, estimate_bandwidth

# #############################################################################
# Generate sample data

df = pd.read_csv('bosbiz.csv')
coords = df.as_matrix(columns=['lon', 'lat'])


# #############################################################################
# Compute clustering with MeanShift

# The following bandwidth can be automatically detected using
bandwidth = estimate_bandwidth(coords, quantile=0.2, n_samples=500)
print bandwidth

ms = MeanShift(bandwidth=0.009, bin_seeding=True)
ms.fit(coords)
labels = ms.labels_
cluster_centers = ms.cluster_centers_

labels_unique = np.unique(labels)
n_clusters_ = len(labels_unique)

print("number of estimated clusters : %d" % n_clusters_)

# #############################################################################
# Plot result
import matplotlib.pyplot as plt
from itertools import cycle

plt.figure(1)
plt.clf()

colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
for k, col in zip(range(n_clusters_), colors):
	my_members = labels == k
	cluster_center = cluster_centers[k]
	plt.plot(coords[my_members, 0], coords[my_members, 1], col + '.')
	plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
			 markeredgecolor='k', markersize=14)
plt.title('Estimated number of clusters: %d' % n_clusters_)
plt.show()


'''

import pandas as pd
import numpy as np
import hdbscan
import matplotlib.pyplot as plt
import time
import seaborn as sns


df = pd.read_csv('bosbiz.csv')
coords = df.as_matrix(columns=['lon', 'lat'])
#kms_per_radian = 6371.0088
#epsilon = 0.01 / kms_per_radian

#clusterer = hdbscan.HDBSCAN(metric='haversine').fit(np.radians(coords))
#clusterer.fit(coords)
#print clusterer.labels_.max()

def plot_clusters(data, algorithm, args, kwds):
	plot_kwds = {'alpha' : 0.25, 's' : 80, 'linewidths':0}
	start_time = time.time()
	labels = algorithm(*args, **kwds).fit_predict(data)
	end_time = time.time()
	palette = sns.color_palette('deep', np.unique(labels).max() + 1)
	colors = [palette[x] if x >= 0 else (0.0, 0.0, 0.0) for x in labels]
	plt.scatter(data.T[0], data.T[1], c=colors, **plot_kwds)
	#frame = plt.gca()
	#frame.axes.get_xaxis().set_visible(False)
	#frame.axes.get_yaxis().set_visible(False)
	plt.title('Clusters found by {}'.format(str(algorithm.__name__)), fontsize=24)
	plt.text(-0.5, 0.7, 'Clustering took {:.2f} s'.format(end_time - start_time), fontsize=14)
	plt.show()

	
plot_clusters(coords, hdbscan.HDBSCAN, (), {'min_cluster_size':130,'min_samples':15})
	