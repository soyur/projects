var serverURL = "https://gis.dutchessny.gov/wa/rest/services/AerialAccess/MapServer";
var cacheRootURL = "https://gis.dutchessny.gov/wa/rest/services/Tiles/Cache_";
var geometryServer = "https://gis.dutchessny.gov/wa/rest/services/Utilities/Geometry/GeometryServer";

var cacheList = ['2016_tc', '2016_cir', '2014_tc', '2014_cir', '2013_tc', '2013_cir', '2009_tc', '2009_cir', '2004_tc', '2004_cir', '2000_tc', '1995_cir', '1990_bw', '1980_bw', '1970_bw', '1955_bw', '1940s_bw', '1936_bw']

var thumbList = [];
var map;
var watchId;
var controldId;
var locateGraphic;
var mapHeight, contentHeight;
var mapLayer;
var swipeLayer;
var swipeWidget;
var nextSwipeLayer;
var previousMapLayer;
var tempLayer;
var first = true;
var refresh = true;
var delayer = true;
var cycle = false;
var xmin = 444125.68055555545,
    ymin = 939484.1388888886,
    xmax = 954542.347222222,
    ymax = 1189527.5416666665;
var spr = 2260; // Spatial Reference ID
var sprRef;
var bboxCoords;
var zoomInFactor = 0.65,
    zoomOutFactor = 1.55;
var searchScale = 1253.4820393975292; // Search zoom to scale.
var searchGraphic;
var timelinyBoundaries;
var timeLineYears = ['1936', '1940', '1955', '1970', '1980', '1990', '1995', '2000', '2004', '2009', '2013', '2014', '2016'];
var yearTop;
// REST IDs
var municipalitiesID = 0,
    bikeParkingID = 1, // [2,3]
    poiID = 4,
    POI_APF_View = 8;
var icon;
var termDict = {
    mosaic: "A series of overlapping air photos that have been rectified and aligned with ground control points, to allow planimetrically correct measurements.",
    ortho: "These combine the planimetric precision of a map with the detail available in an aerial photograph. Accurate measurements of distance, area and directions can be made. However the resolution of the aerial photograph should be accounted for to account for precision.",
    res: "Digital photography resolution is generally expressed as the amount of area covered by one pixel. For example imagery with a one foot pixel resolution will cover one foot of ground in each pixel.",
    bw: "Self explanatory. These have been in use since the 1930's.",
    tc: "They show the 'natural' color of the landscape. They tend to be good for water penetration and discrimination of underwater features.",
    cir: "Also known as false color, these were first developed to detect camouflage. They are sensitive to near infrared reflective light (not heat). This results in 'seeing' healthy vegetation as red. Vegetation with very active photosynthesis will appear the brightest red. Color infrared images are superior for mapping the land-water interface as well as for wetlands mapping. It also shows water in light blue/green to dark blue/black hues, depending on the amount of particulates suspended in the water body. Clear, clean water will appear very dark, close to a black tone. As sediment content increases the shades shift to blue color tones. The darkness of the soil tones provides information regarding the moisture content of the soil, darker areas contain more moisture."
}


require(["esri/map", "esri/geometry/Extent", "esri/SpatialReference", "esri/layers/ArcGISDynamicMapServiceLayer", "esri/layers/ArcGISTiledMapServiceLayer", "esri/tasks/query", "esri/tasks/QueryTask", "esri/tasks/IdentifyTask", "esri/tasks/IdentifyParameters", "esri/symbols/Symbol", "esri/graphic", "esri/symbols/SimpleMarkerSymbol", "esri/Color", "esri/symbols/PictureMarkerSymbol", "dojo/_base/array", "esri/geometry/Point", "esri/geometry/Multipoint", "esri/dijit/Scalebar", "esri/dijit/Popup", "dojo/dom-construct", "dojo/_base/array", "esri/InfoTemplate", "dojo/_base/connect", "dojo/dom", "esri/geometry/Polyline", "esri/tasks/GeometryService", "esri/tasks/ProjectParameters", "esri/symbols/SimpleLineSymbol", "esri/dijit/LocateButton", "dojo/on", "dojo/query", "esri/dijit/LayerSwipe", "dojo/domReady!"], function(Map, Extent, SpatialReference, ArcGISDynamicMapServiceLayer, ArcGISTiledMapServiceLayer, Query, QueryTask, IdentifyTask, IdentifyParameters, Symbol, Graphic, SimpleMarkerSymbol, Color, PictureMarkerSymbol, arrayUtils, Point, Multipoint, Scalebar, Popup, domConstruct, arrayUtils, InfoTemplate, connect, dom, Polyline, GeometryService, ProjectParameters, SimpleLineSymbol, LocateButton, on, query, LayerSwipe) {



        /*===================================
         =            Set Content            =
         ===================================*/

        setContent()

        $(window).resize(function() {
            setContent()
        });

        function setContent() {

            if ($(window).width() < 768) {
                $('#controlsContainer').css({
                    'bottom': ($('#tabContainer').height() + 5) + 'px',
                })

                timelinyBoundaries = 0;
                $('#tabContainer').css('height', '45%')

            } else {
                $('#tabContainer').css({
                    'height': '33%',
                    'min-height': '300px'
                })

                $('#controlsContainer').css({
                    'bottom': 'auto',
                })

                timelinyBoundaries = 6;
            }


            contentHeight = $('body').height() - $('#navbar-container').height() - $('#tabContainer').height() - 10
            $('#mapContainer').height(contentHeight);
            mapHeight = $('#mapContainer').height();

            $('.controlContainer').css({
                'top': 10 + 'px'
            })

            yearTop = $('#playTimeLapse').offset().top - 26
            $('#playYear').offset({
                top: yearTop,
                left: $('#playTimeLapse').offset().left + 14
            })

            if (map) {
                map.reposition();
                map.resize();
            }

            swipeWidget ? null : $('#swipeDiv').height(contentHeight);

        }

        function viewBar() {
            $('.nav-tabs').on('click', function(e) {
                if (e.target === e.currentTarget) {
                    collapseContent()
                }
            });

            $(".close-mobile").click(collapseContent)
        }

        $(document).ready(function() {
            viewBar();
            initTimeline();
        })

        /*=====  End of Set Content  ======*/



        sprRef = new SpatialReference({
            wkid: spr
        })

        //function to get url variables out of url
        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
            return (false);
        }

        var xmini = getQueryVariable("xmin");
        if (xmini != false) {
            var xmini = parseInt(getQueryVariable("xmin"));
            var ymini = parseInt(getQueryVariable("ymin"));
            var xmaxi = parseInt(getQueryVariable("xmax"));
            var ymaxi = parseInt(getQueryVariable("ymax"));

            startExtent = new Extent(xmini, ymini, xmaxi, ymaxi, sprRef);
        } else {
            startExtent = new Extent(xmin, ymin, xmax, ymax, sprRef);
        }

        gsvc = new GeometryService(geometryServer);
        esriConfig.defaults.geometryService = gsvc;

        // Location symbol
        var outline = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([239, 236, 225]), 2);
        var locateMarker = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 20, outline, new Color([61, 85, 119, 0.8]));

        var popup = new Popup({}, domConstruct.create("div"));
        popup.on('set-features', function() {
            setProfile();
            setComplete = true
        })

        popup.on('selection-change', function() {
            if (setComplete) {
                setProfile()
            }
        })

        map = new Map("mapContainer", {
            logo: false,
            slider: false,
            infoWindow: popup,
            minZoom: 2,
            maxZoom: 13
        });

        map.setExtent(startExtent)
            // currentExtent = map.extent

        map.on('load', function() {
            $('.scalebar_bottom-left').css("bottom", '5px')
            aerials();

            collapseContent();

        });

        function refreshExtent(ext) {
            map.setExtent(ext)
        }

        $('.nav-tabs a').click(function(e) {
            e.preventDefault()
            $(this).tab('show')
        })

        $('.panel').hover(function() {
            $(this).css({
                'border-color': '#e49c2d'
            })
            $(this).children('.panel-heading').css({
                'background-color': '#f9bb56',
                'border-color': '#e49c2d'
            })

        }, function() {
            if (!$(this).hasClass('selectedPanel')) {
                $(this).css({
                    'border-color': '#ddd'
                })
                $(this).children('.panel-heading').css({
                    'background-color': '#f5f5f5',
                    'border-color': '#ddd'
                })
            }
        })


        scalebar = new Scalebar({
            map: map,
            attachTo: "bottom-left",
            scalebarStyle: "line"
        }, dom.byId("scalebarContainer"));


        locate = new LocateButton({
            map: map
        }, dom.byId("zoomLoc"))
        locate.startup();

        // Create tile layers
        $.each(cacheList, function(i, item) {
            window['cache_' + item] = new ArcGISTiledMapServiceLayer(cacheRootURL + item + "/MapServer", {
                id: item
            })
        })

        turfObj = {
            extent: new Extent(653850.4024257273, 1038936.5544320114, 656767.0690923941, 1039913.984987567, sprRef),
            lYear: 'cache_2014_cir',
            rYear: 'cache_2014_tc',
            lYearShort: '2014 CIR',
            rYearShort: '2014 TC'
        }

        castleObj = {
            extent: new Extent(630817.6556884167, 955004.2601427274, 633734.3223550834, 955981.6906982829, sprRef),
            lYear: 'cache_1936_bw',
            rYear: 'cache_2016_tc',
            lYearShort: '1936 BW',
            rYearShort: '2016 TC'
        }

        suburbanObj = {
            extent: new Extent(641006.3557177, 1070860.7064804072, 651974.3235624277, 1076494.8471054072, sprRef),
            lYear: 'cache_1955_bw',
            rYear: 'cache_1940s_bw',
            lYearShort: '1955 BW',
            rYearShort: '1940s BW'
        }

        damObj = {
            extent: new Extent(651521.0578814818, 1105249.8580583984, 663187.7245481486, 1109159.5802806204, sprRef),
            lYear: 'cache_2014_tc',
            rYear: 'cache_1955_bw',
            lYearShort: '2014 TC',
            rYearShort: '1955 BW'
        }

        airportObj = {
            extent: new Extent(654421.8558561965, 1016137.8375186264, 666088.5225228632, 1020047.5597408486, sprRef),
            lYear: 'cache_2016_tc',
            rYear: 'cache_1940s_bw',
            lYearShort: '2016 TC',
            rYearShort: '1940s BW'
        }

        bridgeObj = {
            extent: new Extent(641301.2931625718, 1144405.129202729, 647134.6264959051, 1146359.99031384, sprRef),
            lYear: 'cache_2014_tc',
            rYear: 'cache_1955_bw',
            lYearShort: '2014 TC',
            rYearShort: '1955 BW'
        }

        shadowObj = {
            extent: new Extent(673575.5209506486, 1047513.7263371802, 674147.9300126954, 1047807.7674208388, sprRef),
            lYear: 'cache_2009_cir',
            rYear: 'cache_2014_cir',
            lYearShort: '2009 CIR',
            rYearShort: '2014 CIR'
        }

        quarryObj = {
            extent: new Extent(673762.4126629179, 1067280.923230998, 685429.0793295846, 1071190.64545322, sprRef),
            lYear: 'cache_2016_tc',
            rYear: 'cache_1955_bw',
            lYearShort: '2016 TC',
            rYearShort: '1955 BW'
        }


        mapLayer = window['cache_2016_tc']
        swipeLayer = window['cache_1936_bw']
        map.addLayer(mapLayer);
        map.addLayer(swipeLayer);


        defaultLayers = new ArcGISDynamicMapServiceLayer(serverURL);
        map.addLayer(defaultLayers)


        // --->>> Zoom in <<<--- //
        $('#zoomIn').click(function() {
            map.centerAndZoom(map.extent.getCenter(), map.getLevel() + 1)
        })

        // --->>> Zoom out <<<--- //
        $('#zoomOut').click(function() {
            map.centerAndZoom(map.extent.getCenter(), map.getLevel() - 1)
        })

        // --->>> Zoom to extent <<<--- // 
        $('#zoomToExtent').click(function() {
            map.setExtent(startExtent)
        })

        // --->>> Zoom to extent <<<--- // 
        $('#toggleDefaults').click(function() {
            defaultLayers.visible === true ? defaultLayers.hide() : defaultLayers.show();

            $('#toggleDefaults i').text() == "layers" ? $('#toggleDefaults i').text('layers_clear') : $('#toggleDefaults i').text('layers');
        })


        var mid = ($('body').width() / 2) - 5

        map.on('load', function() {
            swipeWidget = new LayerSwipe({
                type: "vertical", //"scope" or "horizontal"
                map: map,
                left: mid,
                layers: [swipeLayer]
            }, "swipeDiv");
            swipeWidget.startup();
            swipeWidget.on('swipe', setYearLocation);
        })

        $('#swipeLayerControlContainer').hover(function() {
            $('#yearLeftDIV').toggle();
        })

        $('#mapLayerControlContainer').hover(function() {
            $('#yearRightDIV').toggle();
        })

        $('#swipeLayerControlContainer .selectedYear').click(function() {
            swipeLayerChange($(this))
        })

        $('#mapLayerControlContainer .selectedYear').click(function() {
            mapLayerchange($(this))
        })

        function changeSwipeLayer(dl) {
            $('.nav-tabs .active').is('#timelapseTab') && map.layerIds.indexOf(mapLayer) < 0 && !first ? null : map.removeLayer(swipeLayer);
            swipeLayer = window[dl];
            swipeWidget.layers = [swipeLayer];
            map.addLayer(swipeLayer);
        }

        function changeMapLayer(dl) {
            map.removeLayer(mapLayer);
            mapLayer = window[dl];
            map.addLayer(mapLayer, 0);
        }

        // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
        $('.dropdown').on('show.bs.dropdown', function(e) {
            $(this).find('.dropdown-menu').first().stop(true, true).slideDown({
                "duration": 250
            });
        });

        // ADD SLIDEUP ANIMATION TO DROPDOWN //
        $('.dropdown').on('hide.bs.dropdown', function(e) {
            $(this).find('.dropdown-menu').first().stop(true, true).slideUp({
                "duration": 250
            });
        });


        function changeLayerSelect(side, item, layer) {
            var text, other;
            if (typeof item === 'string') {
                var $leftLayer = $('#' + side + ' .selectedYear[data-layer="' + layer + '"]')
                text = $leftLayer.children('a').text()
            } else {
                if (item.is('.panel') && side === 'swipeLayerControlContainer') {
                    text = window[item.attr('data-layer')].lYearShort
                } else if (item.is('.panel') && side === 'mapLayerControlContainer') {
                    text = window[item.attr('data-layer')].rYearShort
                } else {
                    text = $(item).children('a').text()
                }
            }

            $('#' + side).find('.activeYear').text(text)

            side.indexOf('map') > -1 ? other = 'swipeLayerControlContainer' : other = 'mapLayerControlContainer';
            $('#' + other + ' .selectedYear')
                .removeClass('disabled')
                .filter("[data-layer='" + layer + "']").addClass('disabled')
        }

        function swipeLayerChange(item) {
            if (!item.hasClass('disabled')) {
                var layer;
                item.is('.panel') ? layer = window[item.attr('data-layer')].lYear : layer = item.attr('data-layer');
                changeSwipeLayer(layer)
                changeLayerSelect('swipeLayerControlContainer', item, layer)
                map.reorderLayer(defaultLayers, 2)
            }
        }

        function mapLayerchange(item) {
            if (!item.hasClass('disabled')) {
                var layer;
                item.is('.panel') ? layer = window[item.attr('data-layer')].rYear : layer = item.attr('data-layer');
                changeMapLayer(layer)
                changeLayerSelect('mapLayerControlContainer', item, layer)
                map.reorderLayer(defaultLayers, 2)
            }
        }

        //--->>> Get map extent coordinates <<<---//
        map.on('extent-change', getMapCoord)

        function getMapCoord() {
            bboxCoords = map.extent.xmin + "%2C" + map.extent.ymin + "%2C" + map.extent.xmax + "%2C" + map.extent.ymax;
        }


        function aerials() {
            if ($('#mapContainer').outerWidth() > 768) { // Desktop
                // Generate thumb urls
                function getThumbs() {
                    for (var i = 0; i < cacheList.length; i++) {
                        thumbList[i] = cacheRootURL + cacheList[i] + '/MapServer/export?transparent=false&format=jpg&bbox=' + bboxCoords + '&bboxSR=2260&imageSR=2260&size=100%2C100&f=image';
                    }
                    preloadImages(thumbList);
                }

                // Cache Thumbs
                function preloadImages(srcs) {
                    if (!preloadImages.cache) {
                        preloadImages.cache = [];
                    }
                    var img;
                    for (var i = 0; i < srcs.length; i++) {
                        img = new Image();
                        img.src = srcs[i];
                        preloadImages.cache.push(img);
                    }
                }

                map.on('update-start', getThumbs)
                map.on('extent-change', getThumbs)

                // Display thumbs on hover
                $('.dropdown-menu li').hover(function() {
                    var val = parseInt($(this).attr('value'));
                    var thumbTop = ($(this).offset().top - 70) + 'px';
                    var thumb = $(this).closest('.yearDropDown').find('.yearThumb');
                    var thumbURL = 'url("' + thumbList[val] + '")'
                    thumb.css({
                        'top': thumbTop,
                        'background-image': thumbURL
                    });
                    thumb.toggle();
                })
            }

            // Toggle scrollbars for dropdown-menus
            var dropdownHeight = $('.dropdown-menu').height();
            var mapHeight = $('#mapContainer').height();
            var headerHeight = $('#infoHeader').height();
            if (dropdownHeight > (mapHeight - headerHeight)) {
                $('.dropdown-menu').css({
                    'max-height': '200px',
                    'overflow-y': 'auto'
                });
            }
        }


        function setYearLocation() {
            var vertLoc = parseInt($('.vertical').css('left'));
            var mapWidth = $('#mapContainer').outerWidth();

            $('#swipeLayerControlContainer').css('left', (vertLoc - ($('#swipeLayerControlContainer').outerWidth() + 25)) + 'px')
            $('#mapLayerControlContainer').css('left', (vertLoc + 36) + 'px')

            $('#yearLeftDIV').width(vertLoc)
            $('#yearRightDIV').width(mapWidth - vertLoc - 10)
        }


        $('.panel').click(function() {

            $('.panel').removeClass('selectedPanel')
            $(this).addClass('selectedPanel')
            $('.panel').css({
                'border-color': '#ddd'
            })
            $('.panel').children('.panel-heading').css({
                'background-color': '#f5f5f5',
                'border-color': '#ddd'
            })


            $(this).css({
                'border-color': '#e49c2d'
            })
            $(this).children('.panel-heading').css({
                'background-color': '#f9bb56',
                'border-color': '#e49c2d'
            })

            var currentSwipeLayer = 'cache_' + swipeLayer.id;
            var currentMapLayer = 'cache_' + mapLayer.id;
            var gotoSwipeLayer = window[$(this).attr('data-layer')].lYear
            var gotoMapLayer = window[$(this).attr('data-layer')].rYear

            if (currentSwipeLayer == gotoSwipeLayer && currentMapLayer == gotoMapLayer) {
                console.log('all layers are the same, do not change')
            } else if (currentSwipeLayer == gotoSwipeLayer) {
                mapLayerchange($(this))
            } else if (currentMapLayer == gotoMapLayer) {
                swipeLayerChange($(this))
            } else if (currentMapLayer == gotoSwipeLayer) {
                mapLayerchange($(this))
                swipeLayerChange($(this))

            } else {
                swipeLayerChange($(this))
                mapLayerchange($(this))

            }

            map.setExtent(window[$(this).attr('data-layer')].extent);
        })


        function collapseContent() {
            $(".close-mobile").text() === 'keyboard_arrow_down' ? $(".close-mobile").text('keyboard_arrow_up') : $(".close-mobile").text('keyboard_arrow_down');
            var currExt = map.extent;
            var hc = Math.round($('#mapContainer').height()) === Math.round(contentHeight);

            if ($(window).width() < 768) {
                $('#controlsContainer').animate({
                    bottom: hc ? ($('.nav-tabs').height() + 10) + 'px' : ($('#tabContainer').height() + 5) + 'px',
                }, 500)
            }
            $('#mapContainer').animate({
                height: hc ? contentHeight + $('.tab-content').height() - 17 : contentHeight + 10
            }, 500, function() {
                swipeWidget.left = '105%';
                refresh ? refreshExtent(currExt) : null;
                refresh = false;
            })

            hc === Math.round(contentHeight) && $('#playTimeLapse').hasClass('playing') ? $('#playYear').show() : $('#playYear').hide()

            $('#playYear').animate({
                top: hc ? contentHeight + $('.tab-content').height() + 5 : yearTop

            }, 500, function() {

            });

        }

        $('.nav-tabs li>a').click(function() {
            Math.round($('#mapContainer').height()) === Math.round(contentHeight) ? null : collapseContent();
        })

        $('#timelapseTab').click(function() {

            timeLapseControls.resume();

            $('.time-lapse-controls').addClass('on')
            first = true

            var year;

            if (mapLayer.id == '1936_bw') {
                changeSwipeLayer('cache_2016_tc')
                year = "2016"
            } else {
                changeSwipeLayer('cache_1936_bw')
                year = "1936"
            }

            map.reorderLayer(defaultLayers, 2)

            $('#timeline').timeliny('goToYear', year);

            $('.vertical ').animate({
                left: '105%'
            }, 600, function() {
                swipeWidget.left = '105%'
                swipeWidget.swipe()
            });

            $('.controlContainer').fadeOut();
        })

        $('#exploreTab').click(function() {

            timeLapseControls.pause();

            $('.time-lapse-controls').removeClass('on');

            var currentSwipeLayer = 'cache_' + swipeLayer.id;
            var gotoSwipeLayer = 'cache_1936_bw'
            var gotoMapLayer;

            currentSwipeLayer == 'cache_2016_tc' ? gotoMapLayer = 'cache_1936_bw' : gotoMapLayer = 'cache_2016_tc';
            changeMapLayer(gotoMapLayer)
            var currentMapLayer = 'cache_' + mapLayer.id;
            tempLayer = "";
            changeLayerSelect('mapLayerControlContainer', 'exploreTab', currentMapLayer)
            changeLayerSelect('swipeLayerControlContainer', 'exploreTab', currentSwipeLayer)

            $('#mapLayerControlContainer button').switchClass('btn-warning', 'btn-default')
            $('#mapLayerControlContainer .btn-group button').eq(0).switchClass('btn-default', 'btn-warning')
            $('#swipeLayerControlContainer button').switchClass('btn-warning', 'btn-default')
            $('#swipeLayerControlContainer button').eq(10).switchClass('btn-default', 'btn-warning')

            $('.LayerSwipe').width()

            $('.vertical ').animate({
                left: $('.LayerSwipe').width() / 2 + 'px'
            }, 600, function() {
                swipeWidget.left = $('.LayerSwipe').width() / 2 + 'px'
                swipeWidget.swipe()
            });

            $('.controlContainer').fadeIn();
        })

        function animateOpaque() {
            if (swipeLayer.opacity <= 0) {
                /* At this point, mapLayer is visible at current year and swipeLayer is invisible at old year. In order to change swipeLayer to current year, you must first remove mapLayer */
                mapLayer = "";
                tempLayer = window[nextSwipeLayer];
                map.addLayer(tempLayer);
                map.reorderLayer(tempLayer, 2);
                changeSwipeLayer(nextSwipeLayer);
                map.removeLayer(window[previousMapLayer]);
                map.reorderLayer(defaultLayers, 2);
                window[previousMapLayer].setOpacity(1); // reset opacity of previous layer, or else won't show later

                cycle ? setTimeout(function() {
                    playYear()
                }, 500) : null;

                delayer = true;
                controldId === "playTimeLapse" ? null : timeLapseControls.resume();

            } else {
                var op = ((swipeLayer.opacity * 100) - 10) / 100 // To avoid floating numbers
                swipeLayer.setOpacity(op)
                setTimeout(function() {
                    animateOpaque()
                }, 110)
            }
        }

        function getLayer(year) {
            if (year > 1990) {
                if (year == 1995) {
                    return 'cache_' + year + '_cir'
                } else {
                    return 'cache_' + year + '_tc'
                }
            } else {
                if (year == 1940) {
                    return 'cache_' + year + 's_bw'
                } else {
                    return 'cache_' + year + '_bw'
                }
            }
        }

        function orderLayers(currLayer, nextLayer) {
            changeMapLayer(nextLayer);
            previousMapLayer = currLayer;
            nextSwipeLayer = nextLayer;
            controldId === "playTimeLapse" ? null : timeLapseControls.pause()
            animateOpaque()
        }


        /*================================
        =            Timeliny            =
        ================================*/

        function initTimeline() {
            $('#timeline').timeliny({
                order: 'asc',
                className: 'timeliny',
                wrapper: '<div class="timeliny-wrapper"></div>',
                boundaries: timelinyBoundaries,
                animationSpeed: 200,
                hideBlankYears: true,
                onInit: function() {},
                onDestroy: function() {},
                afterLoad: function(currYear) {},
                onLeave: function(currYear, nextYear) {
                    if (!first && delayer) {
                        orderLayers(getLayer(currYear), getLayer(nextYear))
                        delayer = false
                    } else if (delayer === false) {
                        setTimeout(function() {
                            orderLayers(getLayer(currYear), getLayer(nextYear))
                        }, 1000)
                    }
                },
                afterChange: function(currYear) {
                    first = false
                },
                afterResize: function() {}
            });
        };


        tlc = query('.time-lapse-controls')
        timeLapseControls = on.pausable(tlc, 'click', function(evt) {
            controldId = $(this).attr('id')
            if (controldId === 'previousYear') {
                $('#playTimeLapse').removeClass('playing');
                //playYear()
                goToPreviousYear()
            } else if (controldId === 'playTimeLapse') {
                $('#playTimeLapse').toggleClass('playing');
                playYear()
            } else {
                $('#playTimeLapse').removeClass('playing');
                // playYear()
                goToFollowingYear()
            }
        })

        function goToPreviousYear() {
            var currYear = $('#timeline .active').attr('data-year');
            var prevYear;
            id = timeLineYears.indexOf(currYear)
            id === 0 ? prevYear = timeLineYears[timeLineYears.length - 1] : prevYear = timeLineYears[id - 1];
            $('#timeline').timeliny('goToYear', prevYear);
        }


        function goToFollowingYear() {
            var currYear = $('#timeline .active').attr('data-year');
            var nextYear;
            id = timeLineYears.indexOf(currYear)
            id === timeLineYears.length - 1 ? nextYear = timeLineYears[0] : nextYear = timeLineYears[id + 1];
            $('#playYear').is(':visible') ? $('#playYear').text(nextYear) : null;
            $('#timeline').timeliny('goToYear', nextYear);
        }

        function playYear() {
            if ($('#playTimeLapse').hasClass('playing')) {
                Math.round($('#mapContainer').height()) !== Math.round(contentHeight) ? $('#playYear').show() : null;
                cycle = true;
                $('#playTimeLapse i').text('pause')
                goToFollowingYear()
            } else {
                $('#playYear').hide();
                $('#playTimeLapse i').text('play_arrow')
                cycle = false;
            }
        }


        /*=====  End of Timeliny  ======*/

        /*===================================
        =            Terminology            =
        ===================================*/

        $('#termList p').click(function() {
            $('#termList p').removeClass('active');
            $(this).addClass('active')
            var term = $(this).attr('data-term')
            $('#termListDesc').fadeOut(function() {
                $(this).html("<img style='float:left;margin-right:20px;box-shadow:0px 0px 5px 0px black;' height='200px;' src='images/term_" + term + ".jpg'>" + termDict[term]).fadeIn()
            })
        })

        /*=====  End of Terminology  ======*/

        /*====================================
        =            Autocomplete            =
        ====================================*/

        function search(request, response) {
            $.ajax({
                url: serverURL + "/" + POI_APF_View + "/query",
                dataType: "jsonp", // with this dataType, cache is set to false by default
                data: {
                    where: "TopItem LIKE '%" + request.term.replace(/\'/g, '\'\'') + "%' OR BottomItem LIKE  '%" + request.term.replace(/\'/g, '\'\'') + "%'", //makes single quotes into double for sql  
                    outFields: "TopItem, BottomItem,Feature_ID",
                    returnGeometry: false,
                    f: "pjson"
                },
                success: function(data) {

                    //$('#autocomplete').removeClass('ui-autocomplete-loading');
                    if (data.features) {
                        response($.map(data.features.slice(0, 100), function(item) { //only display first 100 

                            console.log(item.attributes.Feature_ID)
                            item.attributes.Feature_ID.length == 0 ? icon = './graphics/mapIcons/pin-APF.png' : icon = './graphics/mapIcons/pin-' + item.attributes.BottomItem.toLowerCase() + '.png';


                            return {
                                label: '<img height="30px" src="' + icon + '"> ' + item.attributes.TopItem + ', ' + '<i>' + item.attributes.BottomItem + '</i>',
                                value: item.attributes.TopItem + ', ' + item.attributes.BottomItem + ', ' + icon
                            }
                        }));
                    }
                }
            })
        }

        //--->>> Zoom to focus <<<---// 
        function getSearchLocation(event, ui) {
            var street = ui.item.value.split(', ')[0]
            var city = ui.item.value.split(', ')[1]
            var iconURL = ui.item.value.split(', ')[2]

            var addressMarker = new PictureMarkerSymbol(iconURL, 35, 43);
            var sql = "TopItem = '" + street.replace(/\'/g, '\'\'') + "' AND BottomItem = '" + city.replace(/\'/g, '\'\'') + "'";
            var queryTask = new QueryTask(serverURL + "/8");
            var query = new Query();
            query.returnGeometry = false; // table has no inherit geometry
            query.outFields = ["northing", "easting"]
            query.where = sql;
            queryTask.execute(query, function(result) {
                if (searchGraphic) {
                    map.graphics.remove(searchGraphic);
                }
                var easting = result.features[0].attributes.easting
                var northing = result.features[0].attributes.northing
                var searchPoint = new Point(easting, northing, sprRef);
                searchGraphic = new Graphic(searchPoint, addressMarker);
                map.graphics.add(searchGraphic);
                if (event.type === "autocompleteselect") {
                    var factor = searchScale / map.getScale();
                    map.centerAndZoom(searchPoint, 9);

                }
            });
        }


        // Search results
        $('#autocomplete').autocomplete({
            source: search,
            minLength: 1, /// Minimum search value
            focus: function(event, ui) { // Results focus event
                event.preventDefault();
                $(this).val(ui.item.value.split(', ')[0] + ', ' + ui.item.value.split(', ')[1]);
                getSearchLocation(event, ui);
            },
            select: function(event, ui) { // Results select event
                event.preventDefault();
                $(this).val(ui.item.value.split(', ')[0] + ', ' + ui.item.value.split(', ')[1]);
                this.blur();
                map.graphics.clear();
                $('#zoomToLocation i').text('location_searching');
                getSearchLocation(event, ui);
            },
            change: function(event, ui) { // Results change event
                map.graphics.clear();
                $('#zoomToLocation i').text('location_searching');
            },
            error: function(error) { // error log
                console.log(error)
            }
        });

        // Match Suggestions String and add icon
        $.ui.autocomplete.prototype._renderItem = function(ul, item) {

            item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<li style='text-align:left'>" + item.label + "</a>")
                .appendTo(ul);
        };



        /*=====  End of Autocomplete  ======*/


        /*====================================
        =            geolocations            =
        ====================================*/


        $('#zoomToLocation').click(function() {
            location()
        })


        function location() {
            navigator.geolocation.clearWatch(watchId);
            map.graphics.clear()
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(zoomToLocation, locationError);
                watchId = navigator.geolocation.watchPosition(showLocation, locationError);
            } else {
                alert("Browser doesn't support Geolocation. Visit http://caniuse.com to see browser support for the Geolocation API.");
            }
        }

        function locationError(error) {
            //error occurred so stop watchPosition
            if (navigator.geolocation) {
                navigator.geolocation.clearWatch(watchId);
            }
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    alert("Location not provided");
                    break;

                case error.POSITION_UNAVAILABLE:
                    alert("Current location not available");
                    break;

                case error.TIMEOUT:
                    alert("Timeout");
                    break;

                default:
                    alert("unknown error");
                    break;
            }
        }

        //initial zoom to the users location 
        function zoomToLocation(location) {
            var point = new Point(location.coords.longitude, location.coords.latitude);
            gsvc.project([point], new SpatialReference({
                wkid: spr
            }), function(projectedPoints) {
                var pt = projectedPoints[0];
                addGraphic(pt);
                map.centerAndZoom(pt, 10);
                $('#zoomToLocation i').text('my_location');
            })
        }

        // existing zoom to the users location
        function showLocation(location) {

            var point = new Point(location.coords.longitude, location.coords.latitude);
            gsvc.project([point], new SpatialReference({
                wkid: spr
            }), function(projectedPoints) {
                var pt = projectedPoints[0];
                if (!locateGraphic) {
                    addGraphic(pt);
                } else { // move the graphic if it already exists
                    locateGraphic.setGeometry(pt);
                }
                //map.centerAt(pt);
            })
        }

        // Add location graphic
        function addGraphic(pt) {
            locateGraphic = new Graphic(pt, locateMarker);
            map.graphics.add(locateGraphic);
        }

        // Clear location when user searches
        function stopLocation() {
            navigator.geolocation.clearWatch(watchId);
            map.graphics.clear();
            $('#zoomToLocation i').text('location_searching');
        }


        /*=====  End of geolocations  ======*/

        /*=============================
        =            Print            =
        =============================*/

        $('#print').click(print)

        function print() {
            var imageList = [];
            var yearList = [];
            var images;
            if ($('.controlContainer').is(':visible')) { // Print Aerials
                images = [swipeLayer.id, mapLayer.id]
            } else {
                images = cacheList.slice().reverse();
            }



            for (var i = 0; i < images.length; i++) {
                imageList[i] = cacheRootURL + images[i] + '/MapServer/export?transparent=true&format=png8&bbox=' + bboxCoords + '&bboxSR=2260&imageSR=2260&size=270%2C270&f=image';

                yearList[i] = images[i].toUpperCase().replace("_", " ")
            }


            printWindow2(imageList, yearList);

        }


        function prinwWindow(printList) {
            newwindow2 = window.open('', 'Aerial Access Export', 'height=800,width=900,scrollbars=yes,resizable=yes');
            var tmp = newwindow2.document;
            tmp.write('<html><head><title>Aerial Access</title> </head><body>');
            for (var i = 0; i < printList.length; i++) {
                tmp.write('<div class="printYear">')
                tmp.write('<img src="' + printList[i] + '"></div>');
            }
            tmp.write('</body></html> ');
            tmp.close();
            newwindow2.focus();
        }




        function printWindow2(printList, printYears) {

            newwindow2 = window.open('', 'Aerial Access Export', 'height=800,width=900,scrollbars=yes,resizable=yes');
            var tmp = newwindow2.document;
            tmp.write('<html><head><title>Aerial Access</title>');
            tmp.write('<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>');
            tmp.write('<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>');
            tmp.write('<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">')
            tmp.write('<style>body{width: 800px;margin: auto;}')
            tmp.write('.closeOut{position:absolute; top: -11px; right: -11px; z-index:9999999; color:gray; cursor:pointer; border-radius:12px; background-color:white; padding:3px; text-align:center;line-height:50%;}')
            tmp.write('.closeOut:hover{color: black;}')
            tmp.write('h3{margin-top:2px;margin-bottom:3px;page-break-after:avoid;}')
            tmp.write('h6{float:left;margin-top:6px;margin-bottom:0px;}')
            tmp.write('@media print {.closeOut{display:none;}#printIMG{display:none;}}')
            tmp.write('#printIMG{position:absolute;top:5px;right:25px;}')
            tmp.write('.printYear{display:inline-block;position:relative;margin-left:15px;margin-bottom:5px;}')
            tmp.write('#footer{background-color: #3d5577;text-align: right; padding:2px; color:white; width:100%;}')
            tmp.write('</style></head><body>')
            tmp.write('<div style="position:relative; background-image:' + 'url("graphics/aerialaccessbanner_extend.png");' + '">')
            tmp.write('<img valign=top src="graphics/aerialaccessbanner.png" style="float:left;margin-bottom:25px">');
            tmp.write('<img id="printIMG" style="z-index:99999" border=0 src="graphics/print_panel.png" onclick="window.print()" ></div>');
            for (var i = 0; i < printList.length; i++) {
                tmp.write('<div class="printYear" data-year="' + printYears[i] + '">')
                tmp.write('<div class="closeOut" onclick="$(this).parent().remove();onlyOne()">' +
                    '<span class="glyphicon glyphicon-remove" style="top: 0 px;"></span></div>');
                tmp.write('<img src="' + printList[i] + '">');
                tmp.write('<h3 align=center>' + printYears[i] + ' </h3></div>')
            }

            tmp.write('<div id="footer"><h6>BW=Black and White, TC=True Color, CIR=Color Infrared</h6>  Aerial Access is provided by Dutchess County, NY.</div>');
            tmp.write('<script>function onlyOne() { var yearList =' + JSON.stringify(printYears) + '; var $pic1 = $(".printYear");var $picURL = $pic1.attr("data-year").toLowerCase().replace(" ","_");console.log($picURL); if( $pic1.length === 1){ var name = $.trim($pic1.children("h3").html()); var picLarge = "' + cacheRootURL + '"+$picURL+"/MapServer/export?dpi=96&transparent=true&format=png8&bbox=' + bboxCoords + '&bboxSR=2260&imageSR=2260&size=760%2C520&f=image";$pic1.children("img").attr("src",picLarge); $pic1.css({"display":"block","height":"auto"});$(".closeOut").remove();}} </script>');
            tmp.write('</body></html> ');
            tmp.close();
            newwindow2.focus();
        }

        /*=====  End of Print  ======*/

    }) // End require