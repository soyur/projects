$(document).ready(function() {
    App.init();
    setDateRange()
    initApps()
    getNews()
    initEvents()
});

var server = 'GISAPPOUT'
var app = "All";
var startDate;
var endDate;
var startDateLong;
var endDateLong;
var appSelect;
var dayMilli = 86400000;

var app_start_dates = {
    'GISAPPOUT': {
        'All': '2014-04-04',
        'AddressInfoFinder': '2014-04-04',
        'AerialAccess': '2014-04-04',
        'BicycleParking': '2017-03-03',
        'InfoAccess': '2014-04-11',
        'HistoricResourceSurvey': '2014-04-04',
        'other': '2014-04-04',
        'Parking': '2014-04-04',
        'ParksReservations': '2014-04-04',
        'ParcelAccess': '2014-04-04',
        'ReferralIdentifier': '2015-10-22',
        'Shared': '2014-04-04',
        'TIP': '2014-04-04'
    },
    'GISPROD2': {
        'All': '2015-02-24',
        'AddressInfoFinder': '2015-02-24',
        'AerialAccess': '2015-02-24',
        'ArcStudio': '2015-02-24',
        'HATS': '2015-02-24',
        'Metadata': '2015-02-24',
        'other': '2015-02-24',
        'ParcelAccess': '2015-02-24',
        'RIMS': '2015-02-24',
        'Root': '2015-02-24',
        'SnowAndIce': '2015-02-24',
        'WaterSystemViewer': '2015-02-26'
    }
}

function initEvents() {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        if ($(e.target)[0].hash == '#stats') {
            $('#stats-form select').val('GISAPPOUT')
            app = 'All'
            initStats('GISAPPOUT')
            initServerApps()
        }
        e.relatedTarget // previous active tab
    })

    $('.list-group-item').click(function(event) {
        $('.list-group-item').removeClass('active')
        $(this).addClass('active')
        toggleNews($(this).index())
    })

    $('.app').click(function(event) {
        $(".app-wrapper").removeClass('appActive')
        $(this).find(".app-wrapper").addClass('appActive')
        var serv = $(this).attr('data-server')
        toggleApps($(this).attr('data-name'), serv)
    })

    $('.apps-full .close').click(function(event) {
        $(this).parent('.apps-full').hide()
    })

    $('.app-ctr').click(function(event) {
        var index = $(this).parent('.apps-full').index()
        var dir;
        $(this).hasClass('app-left') ? dir = -1 : dir = 1;
        switchApps(index, dir)
    })

    $('.apps-full-link').click(function() {
        $('a[href="#stats"] ').tab('show')
    })

    $('img[data-toggle="modal"]').click(function() {
        var ind = $(this).attr('data-slide-to')
        $('.carousel-indicators li').removeClass('active')
        $('.carousel-indicators li[data-slide-to="' + ind + '"]').addClass('active')
        $('.carousel-inner .item').removeClass('active')
        $('.carousel-inner .item').eq(ind).addClass('active')
    })

    $("select[name='select-server']").change(initServerApps)

    $('#changeStats').click(function(evt) {
        app = $("select[name='select-app']").val()
        server = $("select[name='select-server']").val()
        startDateLong = new Date(app_start_dates[server][app]).getTime() + dayMilli

        if (app == 'All') {
            startDate = app_start_dates[server][app]
            initStats(server)
        } else {
            app == 'AerialAccess' ? $('#aa-note').show() : $('#aa-note').hide();
            getTables(app_start_dates[server][app], endDate, app, 'chart-GISAPPOUT', true, server)
        }
    })
}

/*=================================
=            Functions            =
=================================*/


function setDateRange() {
    startDate = app_start_dates[server][app]
    var d = new Date();

    d.setDate(d.getDate() - 1);

    var year = d.getFullYear();
    var month = d.getMonth() + 1 < 10 ? '0' + (d.getMonth() + 1) : d.getMonth() + 1;
    var day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate()

    var yesterday = year + '-' + month + '-' + day

    endDate = yesterday

    $("input[name='start']").val(startDate)
    $("input[name='end-date']").val(endDate)

    endDateLong = new Date(endDate).getTime() + dayMilli
    startDateLong = new Date(startDate).getTime() + dayMilli;
}


function initApps() {
    $('.app').each(function(i, item) {
        $(item).delay(100 * i).fadeTo(500, 1)
    })
    toggleApps('ParcelAccess', 'GISAPPOUT')
}


function toggleApps(name, server) {
    $('.apps-full').hide()
    var sel = '.apps-full[data-name="' + name + '"]'
    $(sel).show();
    name == 'SAT' ? null : getTables(app_start_dates[server][name], endDate, name, null, false, server);
}

function switchApps(currIndex, direction) {
    console.log(currIndex)
    toggleApps($('.apps-full').eq(currIndex + direction).attr('data-name'))
}


function toggleNews(index) {
    $('.news-story-full-default').hide();
    $('.news-story-full').addClass('hidden')
    $('.news-story-full').eq(index).fadeIn().removeClass('hidden')
}

function initStats(server) {
    var data_gisappout, data_clients, data_os, data_all;

    data_gisappout = getTables(startDate, endDate, 'SERVER', null, false, server);
    data_clients = getTables(startDate, endDate, 'Client', null, false, server);
    data_os = getTables(startDate, endDate, 'OS', null, false, server);
    data_all = getTables(startDate, endDate, 'ALL', null, false, server);

    $.when(data_gisappout, data_clients, data_os, data_all).done(function() {
        zingchart.render({
            id: 'chart-GISAPPOUT',
            data: chartData([JSON.parse(data_gisappout.responseText, false), JSON.parse(data_clients.responseText), JSON.parse(data_os.responseText), JSON.parse(data_all.responseText)], true),
            height: '100%',
            width: '100%',
        });
    })
}

function initServerApps() {
    var server = $("select[name='select-server']").val()
    $("select[name='select-app'] option").each(function(i, item) {
        $(item).hasClass(server) ? $(item).show() : $(item).hide();
    })

    $("select[name='select-app']").val('All')
}

function getTables(s, e, a, id, zoom, sv) {
    var sd = s
    return $.ajax({
        type: "POST",
        url: "stats.asp",
        data: {
            starting: s,
            ending: e,
            site: a,
            serverName: sv
        },

        success: function(result) {
            var data = JSON.parse(result);
            var totals = getTotals(data)

            if (a == 'GISAPPOUT' || a == 'GISPROD2') {
                return data
            } else {
                if ($('#apps').hasClass('active')) {
                    //var r = '#' + 'total-requests-' + a
                    //$(r).text(totals[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
                    var p = '#' + 'total-pages-' + a
                    $(p).text(totals[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
                    $(p).next('span').text(sd)
                }

                if (id == 'chart-GISAPPOUT') {
                    $('#stats-total-requests').text(totals[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
                    $('#stats-total-pages').text(totals[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))

                    zingchart.render({
                        id: id,
                        data: chartData(data, zoom),
                        height: '100%',
                        width: '100%',
                    });
                }
            }
        }
    })
}

function chartData(json, full) {
    var zoom;
    if (app != 'All') {
        $('#chart-GISAPPOUT').height('700')
        var requests = [];
        var pages = [];

        $.each(json, function(i, item) {
            requests.push(item['Requests']);
            pages.push(item['Pages'])
        })

        full ? zoom = Math.round(requests.length / 4) : zoom = 10;

        chart = buildBar(startDateLong, endDateLong, requests, pages, zoom, app, getTotals(json)[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","), getTotals(json)[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
    } else {
        $('#chart-GISAPPOUT').height('1200')
        var pie1 = [];
        var pie2 = [];
        var pie3 = [];
        var pie4 = [];
        var requests = [];
        var pages = [];
        var visitors = []

        $.each(json[0], function(i, item) {
            requests.push(item['Requests']);
            pages.push(item['Pages'])
            visitors.push(item['Visitors'])
        })

        var blueScales = ["#4668a0", "#4e74b1", "#5f82b9", "#7190c1", "#839ec9", "#95acd0", "#a6b9d8", "#b8c7e0", "#cad5e8", "#dce3ef", "#dce3ef", "#dce3ef", "#dce3ef", "#dce3ef", "#dce3ef", "#dce3ef", "#dce3ef", "#dce3ef", "#dce3ef", "#dce3ef"];

        var yellowScales = ["#f9bb56", "#fac36b", "#fac36b", "#fbcd84", "#fbcd84", "#fbd79d", "#fbd79d", "#fce1b5", "#fce1b5"];

        $.each(json[1], function(i, item) {
            pie1.push({
                "values": [item['Totals']],
                "text": item['Stat'],
            });
        })


        $.each(json[2], function(i, item) {
            pie2.push({
                "values": [item['Totals']],
                "text": item['Stat'],
            });
        })

        var tr = 0;
        $.each(json[3], function(i, item) {
            if (item['Site'] != 'other' && item['Site'] != 'Shared') {
                tr = tr + item['Requests']
                pie3.push({
                    "values": [item['Requests']],
                    "text": item['Site'],
                });
            }
        })

        $.each(pie3, function(i, item) {
            var perc = (item['values'] / tr) * 100
            if (perc <= 5) {
                pie4.push(item)
            }
        })


        $.each([pie1, pie2, pie3, pie4], function(i, item) {
            item.sort(function(a, b) {
                return b.values - a.values
            })

            if (i == 3) {
                $.each(item, function(i, item) {
                    item["background-color"] = yellowScales[i]
                })
            } else {
                $.each(item, function(i, item) {
                    item["background-color"] = blueScales[i]
                })
            }
        })

        full ? zoom = Math.round(requests.length / 4) : zoom = 10;

        chart = buildDash(pie1, pie2, pie3, pie4, requests, pages, visitors, startDateLong, endDateLong, getTotals(json[0])[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","), getTotals(json[0])[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","), zoom, server)
    }
    return chart;
}

function getTotals(json) {
    var requests = 0;
    var pages = 0

    $.each(json, function(i, item) {
        requests += item.Requests;
        pages += item.Pages;
    });

    return [requests, pages]
}


function getNews() {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.open("GET", "news.xml", false);
    xmlhttp.send();
    xmlDoc = xmlhttp.responseXML;

    var x = xmlDoc.getElementsByTagName("item");
    var yearList = []
    for (i = 0; i < x.length; i++) {

        var date = x[i].getElementsByTagName('pubDate')[0].childNodes[0].nodeValue;
        var year = date.slice(-4)
        var id;

        if (yearList.indexOf(year) < 0) {
            yearList.push(year);
            id = 'id="year-' + year + '"'
        } else {
            id = ''
        }

        $('#articles').append("<div " + id + " class='row year-" + year + "'><div class='col-md-12'><h3>" + x[i].getElementsByTagName('title')[0].childNodes[0].nodeValue + "<small>" + date + "</small></h3><p>" + x[i].getElementsByTagName('description')[0].childNodes[0].nodeValue + "</p></div></div>")
    }
}

/*=====  End of Functions  ======*/