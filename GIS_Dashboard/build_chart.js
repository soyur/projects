function buildBar(startDateLong, endDateLong, requests, pages, zoom, appName, total_requests, total_pages) {

    var originalConfig = {
            "type": "bar",
            "background-color": "transparent",
            "options": {
                mobile: true
            },
            "gui": {
                "watermark": {
                    "position": "br"
                },
                contextMenu: {
                    button: {
                        visible: true
                    }
                }
            },
            "legend": {
                "visible": true,
                "text": "%t<br>",
                "layout": "1x2",

                "verticalAlign": "right",
                "align": "right",
                "borderWidth": 0,
                "toggleAction": "hide",

                "background-color": "transparent",
                "item": {
                    y: '22px',
                    "padding": 5,
                    "borderRadius": 3,
                    "fontColor": "#fff",
                    "align": "right",
                    "width": 100
                }
            },
            "labels": [{
                "text": "Requests:",
                fontFamily: "Open Sans",
                "font-weight": "bold",
                "font-size": "16",
                "x": "0%",
                "y": "90%",
                "width": "15%"
            }, {
                "text": "text",
                "id": "labelRequests",
                fontFamily: "Open Sans",
                "font-size": "16",
                "x": "15%",
                "y": "90%",
                "width": "15%",
                "color": "white",
                "background-color": "#f9bb56"
            }, {
                "text": "Pages:",
                fontFamily: "Open Sans",
                "font-weight": "bold",
                "font-size": "16",
                "x": "30%",
                "y": "90%",
                "width": "15%"
            }, {
                "text": "text",
                "id": "labelPages",
                fontFamily: "Open Sans",
                "font-size": "16",
                "background-color": "#273b5f",
                "color": "white",
                "x": "45%",
                "y": "90%",
                "width": "15%"
            }, {
                "text": "Date Range:",
                fontFamily: "Open Sans",
                "font-weight": "bold",
                "font-size": "16",
                "x": "60%",
                "y": "90%",
                "width": "15%"
            }, {
                "text": "text",
                "id": "labelStartDate",
                fontFamily: "Open Sans",
                "font-size": "16",
                "x": "75%",
                "y": "90%",
                "width": "12.5%"
            }, {
                "text": "text",
                "id": "labelEndDate",
                fontFamily: "Open Sans",
                "font-size": "16",
                "x": "83%",
                "y": "90%",
                "width": "12.5%"
            }, {
                "text": "Total Requests:",
                fontFamily: "Open Sans",
                "font-weight": "bold",
                "font-size": "16",
                "x": "0%",
                "y": "95%",
                "width": "15%"
            }, {
                "text": total_requests,
                fontFamily: "Open Sans",
                "font-size": "16",
                "x": "15%",
                "y": "95%",
                "width": "15%",
                "color": "white",
                "background-color": "#f9bb56"
            }, {
                "text": "Total Pages:",
                fontFamily: "Open Sans",
                "font-weight": "bold",
                "font-size": "16",
                "x": "30%",
                "y": "95%",
                "width": "15%"
            }, {
                "text": total_pages,
                fontFamily: "Open Sans",
                "font-size": "16",
                "background-color": "#273b5f",
                "color": "white",
                "x": "45%",
                "y": "95%",
                "width": "15%"
            }, {
                "text": "Date Range:",
                fontFamily: "Open Sans",
                "font-weight": "bold",
                "font-size": "16",
                "x": "60%",
                "y": "95%",
                "width": "15%"
            }, {
                "text": "text",
                "id": "labelStartDateFull",
                fontFamily: "Open Sans",
                "font-size": "16",
                "x": "75%",
                "y": "95%",
                "width": "12.5%"
            }, {
                "text": "text",
                "id": "labelEndDateFull",
                fontFamily: "Open Sans",
                "font-size": "16",
                "x": "83%",
                "y": "95%",
                "width": "12.5%"
            }],
            "header": {
                "text": "Application",
                "align": "right",
                "fontSize": 13,
                "bold": true,
                "fontColor": "#616161"
            },
            height: "90%",
            "plot": {
                "detach": false,
                "bar-space": "0px",
                "barsOverlap": "0%",
                "cursor": "normal",
                "shadow": false,
            },
            title: {
                text: 'Daily Total Traffic for ' + appName,
                align: "center",
                y: "-10px",
                fontFamily: "Open Sans",
                fontSize: 22
            },
            "plotarea": {
                margin: '70px dynamic dynamic dynamic',
            },
            "scale-x": {
                "line-color": "#55717c",
                "tick": {
                    "size": "5px",
                    "line-color": "#55717c",
                    "line-width": "1px",
                    "visible": false
                },
                "guide": {
                    "visible": false
                },
                "item": {
                    "font-size": "10px",
                    "font-family": "Arial",
                    "font-color": "gray",
                    "font-angle": -35,
                },
                "minValue": startDateLong,
                "maxValue": endDateLong,
                "step": 86400000, // Day in milliseconds,  or 'day'
                "transform": {
                    "type": 'date',
                    "all": '%m/%d/%y'
                },
                "zooming": true,
                "zoom-to-values": [endDateLong - (86400000 * zoom), endDateLong],
            },
            "scale-y": {
                "line-color": "none",
                "auto-fit": true,
                "multiplier": true,
                "guide": {
                    "line-style": "solid",
                    "line-color": "lightgray",
                    "alpha": 0.5
                },
                "tick": {
                    "visible": false
                },
                "label": {
                    "offset-x": "-5px",
                    "font-size": "11px",
                    "font-family": "Arial",
                    "font-color": "#ffffff",
                    "font-weight": "normal"
                },
                "item": {
                    "padding-left": "2px",
                    "font-size": "10px",
                    "font-family": "Arial",
                    "font-color": "gray"
                },
                "ref-line": {
                    "visible": true,
                    "line-color": "#c7c9c9",
                    "alpha": 0.25
                }
            },
            "scroll-x": { // Blank to activate
            },
            "crosshair-x": {
                "plot-label": {
                    "multiple": true,
                    "border-radius": 3,
                    "padding": "4px"
                },
                "marker": {
                    "type": "circle",
                    "size": 4,
                    "background-color": "white",
                    "border-width": 1,
                    "border-color": "gray"
                },
                scaleLabel: {
                    "background-color": "gray",
                    "color": 'white',
                    text: "%scale-label"
                }
            },
            "tooltip": {
                visible: false,
                "text": "%t: %v",
                "border-radius": 5
            },
            "series": [{
                "values": requests,
                "background-color": "#f9bb56",
                "borderWidth": 0,
                "border-color": "white",
                "guide-label": {
                    "text": '%v',
                    "font-color": "white",
                    "background-color": "#f9bb56",
                    "border-color": "white",
                },
                "text": "Requests",
                "legendText": "Requests",
                "legendMarker": {
                    "type": "circle",
                    "size": 7,
                    "borderColor": "#f9bb56",
                    "borderWidth": 4,
                    "backgroundColor": "#f9bb56"
                },
                "legendItem": {
                    "backgroundColor": "#f9bb56"
                }
            }, {
                "values": pages,
                "background-color": "#273b5f",
                "borderWidth": 0,
                "border-color": "white",
                "guide-label": {
                    "text": '%v',
                    "font-color": "white",
                    "background-color": "#273b5f",
                    "border-color": "white",
                },
                "text": "Pages",
                "legendText": "Pages",
                "legendMarker": {
                    "type": "circle",
                    "size": 7,
                    "borderColor": "#273b5f",
                    "borderWidth": 4,
                    "backgroundColor": "#273b5f"
                },
                "legendItem": {
                    "backgroundColor": "#273b5f"
                }
            }]
        } // End Config

    if (zoom > 10) {

        originalConfig['preview'] = {
            "adjustLayout": true,
            "live": true,
            preserveZoom: true,
            y: "80%",
            "handle-left": {
                "background-color": "white",
                "border-color": "gray",
                "border-radius": "6px",
                "height": "50px",
                "width": "12px"
            },
            "handle-right": {
                "background-color": "white",
                "border-color": "gray",
                "border-radius": "6px",
                "height": "50px",
                "width": "12px"
            },
            "mask": {
                "alpha": 0.7,
            }
        }
    }
    return originalConfig
}

function buildClient(clients, name) {

    var totals = 0;
    var others = 0;
    var indToRemove = [];

    $.each(clients, function(i, item) {
        totals = totals + parseInt(item['values'])
    })

    var clientList = [{
        "values": 0,
        "text": "other"
    }]

    $.each(clients, function(i, item) {
        if (item['text'] == 'other') {
            others = others + parseInt(item['values'])
        }
        if (((parseInt(item['values']) / totals) * 100) < 1) {
            others = others + parseInt(item['values'])
            if (item['text'] != 'other') {
                indToRemove.push(i)
            }
        }
    })

    for (var i = indToRemove.length - 1; i >= 0; i--) {
        clients.splice(indToRemove[i], 1)
    }

    var myConfig = {
        type: "pie",
        plot: {
            "detach": true,
            layout: "auto",
            "ref-angle": 35,
            borderColor: "white",
            borderWidth: 1,
            valueBox: {
                placement: 'out',
                text: '%t\n%v',
                fontFamily: "Open Sans",
                "thousands-separator": ",",
            },
            tooltip: {
                fontSize: '18',
                fontFamily: "Open Sans",
                padding: "5 10",
                decimals: 2,
                text: "%npv%"
            },
            animation: {
                effect: 2,
                method: 5,
                speed: 500,
                sequence: 1
            },
        },
        title: {
            text: name + ' Requests',
            align: "center",
            offsetX: 10,
            fontFamily: "Open Sans",
            fontSize: 25
        },
        series: clients
    };

    return myConfig
}


function mergeOther(clients) {

    var totals = 0;
    var others = 0;
    var indToRemove = [];

    $.each(clients, function(i, item) {

        totals = totals + parseInt(item['values'])

    })

    $.each(clients, function(i, item) {
        if (item['text'] == 'other') {
            others = others + parseInt(item['values'])
        }
        if (((parseInt(item['values']) / totals) * 100) < 1) {
            others = others + parseInt(item['values'])
            if (item['text'] != 'other') {
                indToRemove.push(i)
            }
        }
    })

    for (var i = indToRemove.length - 1; i >= 0; i--) {
        clients.splice(indToRemove[i], 1)
    }

    return clients
}

function lessThanOne(json) {
    var totals = 0;
    var less = 0;
    var indToRemove = [];
    $.each(json, function(i, item) {

        totals = totals + parseFloat(item['values'])

    })

    $.each(json, function(i, item) {
        if (((parseFloat(item['values']) / totals) * 100.00) < 5.00) {
            less = less + parseFloat(item['values'])
            indToRemove.push(i)
        }

    })

    for (var i = indToRemove.length - 1; i >= 0; i--) {
        json.splice(indToRemove[i], 1)
    }

    var lessA = {
        'values': [less],
        'text': 'Other Apps',
        'background-color': "#f9bb56",
        "detached": true
    }

    json.push(lessA)

    return json
}



function buildDash(pie1, pie2, pie3, pie4, requests, pages, visitors, startDateLong, endDateLong, total_requests, total_pages, zoom, server) {

    var serverType;
    server == 'GISAPPOUT' ? serverType = 'Public' : serverType = 'Internal';

    var myConfig = {
        layout: "2x4",
        "gui": {
            "watermark": {
                "position": "br", //br (default), bl, tr, tl
            },
            contextMenu: {
                button: {
                    visible: true
                }
            }
        },
        graphset: [{
                type: "pie",
                x: 0, //position from left of chart edge
                y: 0, //position top of chart
                height: "25%",
                width: "50%",
                "background-color": "transparent",
                plot: {
                    "detach": true,
                    layout: "auto",
                    "ref-angle": 0,

                    borderColor: "white",
                    borderWidth: 1,
                    // slice: 90,
                    valueBox: {
                        decimals: 2,

                        text: '%t\n%npv%',
                        fontFamily: "Open Sans",

                        fontSize: '14',
                        placement: 'out',
                        rules: [{
                            rule: '%npv > 10',
                            placement: 'in',
                            "font-color": "white"
                        }]
                    },
                    tooltip: {
                        fontSize: '14',
                        fontFamily: "Open Sans",
                        padding: "5 10",
                        text: "%v",
                        "thousands-separator": ",",
                    },
                    animation: {
                        effect: 2,
                        method: 5,
                        speed: 500,
                        sequence: 1
                    },
                    // rules: [{
                    //     "rule": "%npv < 1",
                    //     visible: false

                    // }]
                },
                scale: {
                    sizeFactor: 0.8
                },
                title: {
                    text: 'Server Requests by App',
                    align: "center",

                    fontFamily: "Open Sans",
                    fontSize: 18
                },
                series: lessThanOne(pie3)
            }, {
                type: "pie",
                x: "50%", //position from left of chart edge
                y: 0, //position top of chart
                height: "25%",
                width: "50%",
                "background-color": "transparent",
                plot: {
                    "detach": true,
                    layout: "auto",
                    "ref-angle": 35,

                    borderColor: "white",
                    borderWidth: 1,
                    // slice: 90,
                    valueBox: {
                        placement: 'out',
                        text: '%t',
                        fontFamily: "Open Sans",

                        fontSize: '14',
                        decimals: 2,
                        padding: "0 0"

                    },
                    tooltip: {
                        fontSize: '14',
                        fontFamily: "Open Sans",
                        padding: "5 10",
                        text: "%v",
                        "thousands-separator": ",",
                    },
                    animation: {
                        effect: 2,
                        method: 5,
                        speed: 500,
                        sequence: 1
                    },
                    // rules: [{
                    //     "rule": "%npv < 1",
                    //     visible: false

                    // }]
                },
                scale: {
                    sizeFactor: 0.5
                },
                title: {
                    text: 'Server Requests by App (Other)',
                    align: "center",
                    fontFamily: "Open Sans",
                    fontSize: 18
                },
                series: pie4
            }, {
                type: "pie",
                x: 0, //position from left of chart edge
                y: "25%", //position top of chart
                height: "25%",
                width: "50%",
                "background-color": "transparent",
                plot: {
                    "detach": true,
                    layout: "auto",
                    "ref-angle": 40,

                    borderColor: "white",
                    borderWidth: 1,
                    // slice: 90,
                    valueBox: {
                        placement: 'out',
                        text: '%t\n%npv%',
                        fontFamily: "Open Sans",
                        decimals: 2,
                        fontSize: '14',
                        rules: [{
                            rule: '%npv > 14',
                            placement: 'in',
                            "font-color": "white"
                        }]
                    },
                    tooltip: {
                        fontSize: '14',
                        fontFamily: "Open Sans",
                        padding: "5 10",

                        "thousands-separator": ",",
                        text: "%v"
                    },
                    animation: {
                        effect: 2,
                        method: 5,
                        speed: 500,
                        sequence: 1
                    },
                    // rules: [{
                    //     "rule": "%npv < 1",
                    //     visible: false

                    // }]
                },
                scale: {
                    sizeFactor: 0.8
                },
                title: {
                    text: 'Server Requests by Browser',
                    align: "center",

                    fontFamily: "Open Sans",
                    fontSize: 18
                },
                series: mergeOther(pie1)
            }, {
                type: "pie",
                x: "50%", //position from left of chart edge
                y: "25%", //position top of chart
                height: "25%",
                width: "50%",
                // "scale": {
                //     "size-factor": 1
                // },
                "background-color": "transparent",
                plot: {
                    "detach": true,
                    layout: "auto",
                    "ref-angle": 35,

                    borderColor: "white",
                    borderWidth: 1,
                    // slice: 90,
                    valueBox: {
                        placement: 'out',
                        text: '%t\n%npv%',
                        fontFamily: "Open Sans",
                        decimals: 2,
                        fontSize: '14',
                        rules: [{
                            rule: '%npv > 20',
                            placement: 'in',
                            "font-color": "white"
                        }]
                    },
                    tooltip: {
                        fontSize: '14',
                        fontFamily: "Open Sans",
                        padding: "5 10",
                        decimals: 0,
                        "thousands-separator": ",",
                        text: "%v"
                    },
                    animation: {
                        effect: 2,
                        method: 5,
                        speed: 500,
                        sequence: 1
                    },
                    // rules: [{
                    //     "rule": "%npv < 1",
                    //     visible: false

                    // }]
                },
                scale: {
                    sizeFactor: 0.8
                },
                title: {
                    text: 'Server Requests by OS',
                    align: "center",
                    fontFamily: "Open Sans",
                    fontSize: 18
                },
                series: mergeOther(pie2)
            }, {
                type: "mixed",
                x: 0, //position from left of chart edge
                y: "50%", //position top of chart
                height: "40%",
                width: "100%",
                "labels": [{
                    "text": "Requests:",
                    fontFamily: "Open Sans",
                    "font-weight": "bold",
                    "font-size": "16",
                    "x": "0%",
                    "y": "102%",
                    "width": "15%"
                }, {
                    "text": "text",
                    "id": "labelRequests",
                    fontFamily: "Open Sans",
                    "font-size": "16",
                    "x": "15%",
                    "y": "102%",
                    "width": "15%",
                    "color": "white",
                    "background-color": "#f9bb56"
                }, { //Label Two
                    "text": "Pages:",
                    fontFamily: "Open Sans",
                    "font-weight": "bold",
                    "font-size": "16",
                    "x": "30%",
                    "y": "102%",
                    "width": "15%"
                }, {
                    "text": "text",
                    "id": "labelPages",
                    fontFamily: "Open Sans",
                    "font-size": "16",
                    "background-color": "#273b5f",
                    "color": "white",
                    "x": "45%",
                    "y": "102%",
                    "width": "15%"
                }, { //Label Two
                    "text": "Date Range:",
                    fontFamily: "Open Sans",
                    "font-weight": "bold",
                    "font-size": "16",
                    "x": "60%",
                    "y": "102%",
                    "width": "15%"
                }, {
                    "text": "text",
                    "id": "labelStartDate",
                    fontFamily: "Open Sans",
                    "font-size": "16",
                    "x": "75%",
                    "y": "102%",
                    "width": "12.5%"

                }, {
                    "text": "text",
                    "id": "labelEndDate",
                    fontFamily: "Open Sans",
                    "font-size": "16",
                    "x": "83%",
                    "y": "102%",
                    "width": "12.5%"
                }, {
                    "text": "Total Requests:",
                    fontFamily: "Open Sans",
                    "font-weight": "bold",
                    "font-size": "16",
                    "x": "0%",
                    "y": "109%",
                    "width": "15%"
                }, {
                    "text": total_requests,
                    fontFamily: "Open Sans",
                    "font-size": "16",
                    "x": "15%",
                    "y": "109%",
                    "width": "15%",
                    "color": "white",
                    "background-color": "#f9bb56"
                }, { //Label Two
                    "text": "Total Pages:",
                    fontFamily: "Open Sans",
                    "font-weight": "bold",
                    "font-size": "16",
                    "x": "30%",
                    "y": "109%",
                    "width": "15%"
                }, {
                    "text": total_pages,
                    fontFamily: "Open Sans",
                    "font-size": "16",
                    "background-color": "#273b5f",
                    "color": "white",
                    "x": "45%",
                    "y": "109%",
                    "width": "15%"
                }, { //Label Two
                    "text": "Date Range:",
                    fontFamily: "Open Sans",
                    "font-weight": "bold",
                    "font-size": "16",
                    "x": "60%",
                    "y": "109%",
                    "width": "15%"
                }, {
                    "text": "text",
                    "id": "labelStartDateFull",
                    fontFamily: "Open Sans",
                    "font-size": "16",
                    "x": "75%",
                    "y": "109%",
                    "width": "12.5%"

                }, {
                    "text": "text",
                    "id": "labelEndDateFull",
                    fontFamily: "Open Sans",
                    "font-size": "16",
                    "x": "83%",
                    "y": "109%",
                    "width": "12.5%"
                }],
                "background-color": "transparent",
                // "title": {
                //     "text": site + " Total Monthly Requests"
                // },
                'preview': {
                    "adjustLayout": false,
                    "live": true,
                    y: "90%",
                    preserveZoom: true,
                    "handle-left": {
                        "background-color": "white",
                        "border-color": "gray",
                        // "alpha": 1,
                        "border-radius": "6px",
                        // "border-width": 1,
                        "height": "50px",
                        "width": "12px"
                    },
                    "handle-right": {
                        "background-color": "white",
                        "border-color": "gray",
                        // "alpha": 1,
                        "border-radius": "6px",
                        // "border-width": 0,
                        "height": "50px",
                        "width": "12px"
                    },
                    "mask": {
                        "alpha": 0.7,
                        // "background-color": "#ff6666"
                    }
                },
                "options": {
                    mobile: true
                },

                "legend": {
                    "background-color": "transparent",
                    "visible": true,
                    "text": "%t",
                    // "width": 120,
                    "offset-y": 30,
                    "layout": "1x3",
                    "verticalAlign": "top",
                    "align": "center",
                    "borderWidth": 0,
                    "toggleAction": "hide",
                    "item": {
                        "padding": 5,
                        "borderRadius": 3,
                        "fontColor": "#fff",
                        "align": "right",
                        "width": 100
                    }
                },
                "header": {
                    "text": "Application",
                    "align": "right",
                    "fontSize": 13,
                    "bold": true,
                    "fontColor": "#616161"
                },
                "plot": {
                    "detach": false,
                    "bar-space": "0px",
                    "barsOverlap": "0%",
                    "cursor": "normal",
                    "shadow": false,
                    // animation: {
                    //     effect: 4,
                    //     method: 4,
                    //     speed: 1000,
                    //     sequence: 1
                    // }

                },
                title: {
                    text: 'Daily Total Requests, Pages, and Visitors for the ' + serverType + ' GIS Server',
                    align: "center",

                    fontFamily: "Open Sans",
                    fontSize: 18
                },
                "scroll-x": {

                },

                "scale-x": {
                    "line-color": "#55717c",
                    "tick": {
                        "size": "5px",
                        "line-color": "#55717c",
                        "line-width": "1px",
                        "visible": false
                    },
                    "guide": {
                        "visible": false
                    },
                    "item": {
                        "font-size": "10px",
                        "font-family": "Arial",
                        "font-color": "#c0c0c0",
                        "font-angle": -35,

                    },
                    "minValue": startDateLong,
                    "maxValue": endDateLong,
                    "step": 86400000, // Day in milliseconds,  or 'day'
                    "transform": {
                        "type": 'date',
                        "all": '%m/%d/%y'
                    },
                    "zooming": true,
                    "zoom-to-values": [endDateLong - (86400000 * zoom), endDateLong],
                    itemsOverlap: false,

                },
                "scale-y": {
                    "line-color": "none",
                    "auto-fit": true,
                    "multiplier": true,
                    "guide": {
                        "line-style": "solid",
                        "line-color": "#e0e0e0",
                        "alpha": 1
                    },
                    "tick": {
                        "visible": false
                    },
                    "label": {
                        "text": "Requests and Pages",
                        "offset-x": "-5px",
                        "font-size": "12px",
                        "font-family": "Arial",
                        "font-color": "gray",
                        "font-weight": "normal",
                    },
                    "item": {
                        "padding-left": "2px",
                        "font-size": "10px",
                        "font-family": "Arial",
                        "font-color": "#c0c0c0"
                    },
                    "ref-line": {
                        "visible": true,
                        "line-color": "#c7c9c9",
                        "alpha": 0.25
                    }
                },
                "scale-y-2": {
                    "line-color": "none",
                    "auto-fit": true,
                    "multiplier": true,
                    "guide": {
                        "visible": false,
                        "line-style": "solid",
                        "line-color": "lightgray",
                        "alpha": 1
                    },
                    "tick": {
                        "visible": false
                    },
                    "label": {
                        "text": "Visitors",
                        "offset-x": "5px",
                        "font-size": "12px",
                        "font-family": "Arial",
                        "font-color": "green",
                        "font-weight": "normal"
                    },
                    "item": {
                        "padding-left": "2px",
                        "font-size": "12px",
                        "font-family": "Arial",
                        "font-color": "green"
                    },
                    "ref-line": {
                        "visible": true,
                        "line-color": "#c7c9c9",
                        "alpha": 0.25
                    }
                },
                "crosshair-x": {
                    "plot-label": {
                        "multiple": true,
                        "border-radius": 3,
                        "padding": "4px"
                    },
                    "marker": {
                        "type": "circle",
                        "size": 4, //Make sure to specify size.
                        "background-color": "white",
                        "border-width": 1,
                        "border-color": "gray"
                    },
                    scaleLabel: {
                        "background-color": "gray",
                        "color": 'white',
                        text: "%scale-label"
                    }
                },
                "tooltip": {
                    visible: false,
                    "text": "%t: %v",
                    "border-radius": 5
                },
                "series": [{
                    "type": "bar",
                    "scales": "scale-x,scale-y",
                    "values": requests,
                    "background-color": "#f9bb56",
                    "borderWidth": 0,
                    "border-color": "white",
                    "guide-label": {
                        "text": '%v',
                        "font-color": "white",
                        "background-color": "#f9bb56",
                        "border-color": "white",
                    },
                    "text": "Requests",
                    "legendText": "Requests",
                    "legendMarker": {
                        "type": "circle",
                        "size": 7,
                        "borderColor": "#f9bb56",
                        "borderWidth": 4,
                        "backgroundColor": "#f9bb56"
                    },
                    "legendItem": {
                        "backgroundColor": "#f9bb56"
                    }
                }, {
                    "type": "bar",
                    "scales": "scale-x,scale-y",
                    "values": pages,
                    "background-color": "#273b5f",
                    "borderWidth": 0,
                    "border-color": "white",
                    "guide-label": {
                        "text": '%v',
                        "font-color": "white",
                        "background-color": "#273b5f",
                        "border-color": "white",
                    },
                    "text": "Pages",
                    "legendText": "Pages",
                    "legendMarker": {
                        "type": "circle",
                        "size": 7,
                        "borderColor": "#273b5f",
                        "borderWidth": 4,
                        "backgroundColor": "#273b5f"
                    },
                    "legendItem": {
                        "backgroundColor": "#273b5f"
                    }
                }, {
                    "type": "line",
                    "scales": "scale-x,scale-y-2",
                    "line-color": "green",
                    "values": visitors,
                    // "visible": false,
                    "guide-label": {
                        "text": '%v',
                        "font-color": "white",
                        "background-color": "green",
                        "border-color": "white",
                    },
                    "text": "Visitors",
                    "legendText": "Visitors",
                    "legendMarker": {
                        "type": "circle",
                        "size": 7,
                        "borderColor": "green",
                        "borderWidth": 4,
                        "backgroundColor": "green"
                    },
                    "legendItem": {
                        "backgroundColor": "green"
                    },
                    "marker": {
                        "background-color": "green",
                        "border-width": 1,
                        "shadow": 0,
                        "border-color": "green"
                    },
                    "highlight-marker": {
                        "size": 7,
                        "background-color": "green",
                    }
                }]

            }



        ]
    };


    return myConfig
}


zingchart.load = function(p) {
    var grID
    p.height == 700 ? grID = 0 : grID = 4;
    zingchart.exec('chart-GISAPPOUT', 'updateobject', {
        'type': 'label',
        graphid: grID,
        'data': {
            'id': 'labelStartDateFull',
            'text': convertDate(startDateLong) + " - "
        }
    });

    zingchart.exec('chart-GISAPPOUT', 'updateobject', {
        'type': 'label',
        graphid: grID,
        'data': {
            'id': 'labelEndDateFull',
            'text': convertDate(endDateLong)
        }
    });
}

zingchart.zoom = function(p) {

    //displayZoomValues(p.kmin, p.kmax);
    // console.log(p)
    var imin = p.xmin
    var imax = p.xmax
    var zoomRequests = 0;
    var zoomPages = 0;
    var grID
    p.graphid.indexOf('id0') > -1 ? grID = 0 : grID = 4;
    // get totals for chart
    var totals = zingchart.exec('chart-GISAPPOUT', 'getseriesvalues', {
        graphid: 4,
        plotindex: 0
    });

    for (var i = imin; i <= imax; i++) {
        zoomRequests = zoomRequests + zingchart.exec('chart-GISAPPOUT', 'getnodevalue', {
            graphid: grID,
            plotindex: 0,
            nodeindex: i
        });

        zoomPages = zoomPages + zingchart.exec('chart-GISAPPOUT', 'getnodevalue', {
            graphid: grID,
            plotindex: 1,
            nodeindex: i
        });
    }

    zingchart.exec('chart-GISAPPOUT', 'updateobject', {
        'type': 'label',
        graphid: grID,
        'data': {
            'id': 'labelRequests',
            'text': zoomRequests.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }
    });

    zingchart.exec('chart-GISAPPOUT', 'updateobject', {
        'type': 'label',
        graphid: grID,
        'data': {
            'id': 'labelPages',
            'text': zoomPages.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }
    });


    zingchart.exec('chart-GISAPPOUT', 'updateobject', {
        'type': 'label',
        graphid: grID,
        'data': {
            'id': 'labelStartDate',
            'text': convertDate(p.kmin) + " - "
        }
    });


    zingchart.exec('chart-GISAPPOUT', 'updateobject', {
        'type': 'label',
        graphid: grID,
        'data': {
            'id': 'labelEndDate',
            'text': convertDate(p.kmax)
        }
    });

}

function convertDate(ts) {
    var d = new Date(ts);
    var year = d.getFullYear();
    var month = d.getMonth() + 1 < 10 ? '0' + (d.getMonth() + 1) : d.getMonth() + 1;
    var day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate()

    // Will display time in 10:30:23 format
    var formattedTime = month + '/' + day + '/' + year.toString().substr(-2);

    return formattedTime
}