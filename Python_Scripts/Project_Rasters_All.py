import arcpy, time
import numpy as np

arcpy.env.workspace = r'G:\CRM.gdb'

incoord = arcpy.SpatialReference("WGS 1984")
sr =  r"G:\fromMatlab\shapefiles\NAD_1983_Albers.prj" # Path to NAD_1983_Albers
outcoord = arcpy.SpatialReference(sr)
transform = "WGS_1984_(ITRF00)_To_NAD_1983"

fishnet = np.arange(1,11,1) # can be adjusted to do only certain groups (1,2,3,4,5,6,7,8,9,10)
years = [20,30,50,80] 

for f in fishnet: # For each fishnet 1-10
    wild = "f%sg*" %f
    groups =  arcpy.ListFeatureClasses(wild)
    length = len(groups)

    for group in groups: # For each group within each fishnet 
            start = time.clock()
            print "Doing group %s of %s in fishnet %s" %(group,length,f)
            regionIn = r'G:\fromMatlab\shapefiles\%s.shp' %group
            regionOut = r'G:\CRM.gdb\%s' %group
            
            # Project 
            arcpy.Project_management(regionIn,regionOut,outcoord,transform,incoord)

            # Point to Raster

            for year in years:
                fields = ['B1y%s'%year,'B2y%s'%year,'B3y%s'%year,'B4y%s'%year,'B5y%s'%year,'D%s'%year,'I%s'%year,'PH%s'%year,'BH%s'%year] 
                outRas = r'G:\CRM.gdb\%s' %group
                for field in fields:
                    outRaster = outRas + "_" + field
                    arcpy.PointToRaster_conversion(regionOut, field, outRaster, "", "", 30)
                    print "Field %s Raster for group %s Complete" %(field,group)

            end = time.clock()
            duration = end - start
            hours, remainder = divmod(duration, 3600)
            minutes, seconds = divmod(remainder, 60)
            print "Group %s Completed in %dh:%dm:%fs" % (group,hours, minutes, seconds)

        print "Fishnet %s complete" %f
        

# Combine rasters by ouput and year
outLoc = r'G:\CRM.gdb'
for year in years:
    wildD = "*_D%s" %year
    wildBH = "*_BH%s" %year
    wildPH = "*_PH%s" %year
    for dynamic in arcpy.ListRaster(wildD)
        arcpy.MosaicToNewRaster_management(dynamic,outLoc, 'Dynamic_%s'%year, "","32_BIT_FLOAT", "30", "1", "LAST","FIRST")
    
    for binHigh in arcpy.ListRasterss(wildBH)
        arcpy.MosaicToNewRaster_management(binHigh,outLoc, 'BinHigh_%s'%year, "","32_BIT_FLOAT", "30", "1", "LAST","FIRST")
    
    for probHgh in arcpy.ListRasters(wildPH)
        arcpy.MosaicToNewRaster_management(probHigh,outLoc,'ProbHigh_%s'%year, "","32_BIT_FLOAT", "30", "1", "LAST","FIRST")
        
