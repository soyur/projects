#######################################################
## Import iPlover nest sites as a new point feature class and enable images - requires SCP shell script to get images
## Created: September 11, 2014
## Author: Sawyer Stippa
## Email: sstippa@usgs.gov
#######################################################
## Not a stand alone python script, needs to be run within ArcGIS python window.
import arcpy, datetime, subprocess

arcpy.env.workspace = "Database Connections/iplover.sde"
sdeCount = int(arcpy.GetCount_management('iplover.public.entries').getOutput(0))

arcpy.env.workspace = r'G:\iPlover\iplover.gdb'
pList = arcpy.ListFeatureClasses('*')

for fc in pList:
	pCount = int(arcpy.GetCount_management(fc).getOutput(0))
	if pCount == sdeCount:
		stop = 1
	else:
		print 'Searching for new entries'
		stop = 0
		
if stop == 0:
	print 'New entries found'
	# Location of OLE Database Connection file
	arcpy.env.workspace = "C:\Users\SSTIPPA\AppData\Roaming\ESRI\Desktop10.2\ArcCatalog\iplover.sde"
	#arcpy.env.overwrite = True

	print "Retrieve images? Download may take several minutes..."
	person = raw_input("Enter yes or no: ")
	
	if person.lower() == 'yes':
		# Run shell script to import iPlover images
		print 'Retrieving images'
		#subprocess.call(["sh", 'G:/iPlover/scp.sh'])
	else:
		print "Skipping images, moving on"
	
	# Timestamp for filename
	format = "%Y%m%d"
	today = datetime.date.today()
	date = today.strftime(format)

	try:
		# Postgres table
		in_Table = "iplover.public.entries"
		x_coords = "longitude"
		y_coords = "latitude"
		
		# Temporary layer
		out_Layer = 'iplover_dump_layer_' + date
		
		# Output feature class
		saved_Layer = r'G:\iPlover\iplover.gdb\iplover_' + date 

		# Set the spatial reference
		spRef = arcpy.SpatialReference("WGS 1984")

		# Make the XY event layer...
		arcpy.MakeXYEventLayer_management(in_Table, x_coords, y_coords, out_Layer, spRef)

		# Print the total rows
		count = arcpy.GetCount_management(out_Layer)
		print "New iplover dumb has %s records" %count
		
		# Save to feature class
		arcpy.CopyFeatures_management(out_Layer,saved_Layer)
		
		# Delete temp layer
		arcpy.Delete_management(out_Layer)
		
		# Add images
		arcpy.env.workspace = r'G:\iPlover\iplover.gdb'
		arcpy.EnableAttachments_management(saved_Layer)
		arcpy.AddAttachments_management(saved_Layer, "OBJECTID", saved_Layer, "OBJECTID","imagekey",r"G:/iPlover/iplover_images")
		
	except:
		print arcpy.GetMessages()
else:
	print 'No new entries found'
