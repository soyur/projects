# Extract rasters at 5m grids around iPlover nest locations
import arcpy
from math import radians, cos, asin, sin, atan2, sqrt, degrees

arcpy.env.workspace = r'G:\path\to\workingDir' # Working directory 

#########################--->>> Inputs <<<---###########################
nests = 'iPlover_021015' 			# iPlover nest locations 
rasters = ['elev_2012','slope_2012'] # List of rasters to be extracted
stat = 'ALL' 						# ALL, MEAN, MAJORITY, MAXIMUM, MEDIAN, MINIMUM, MINORITY, ...
									# ...  RANGE, STD , SUM , VARIETY , MIN_MAX , MEAN_STD , MIN_MAX_MEAN
fieldList = ["MEAN", "STD"]  		# Pick from above list of statistics

############ Beach width inputs - make sure coordinate systems is lat/lon
getBeachWidth = False # doing beach width?

DLow = "DuneToe" 					# or path to Dune Toe
DHigh = "DuneCrest" 				# or path to Dune Crest
shoreline = 'shoreline' 			# or path to shoreline
widths = [DHigh,DLow,shoreline] 	# list of above

################# Start 

arcpy.Buffer_analysis(nests, "nests_buffer", "2.5 Meters") # Buffer each nest location with diameter of 5m (r = 2.5)
arcpy.MinimumBoundingGeometry_management("nests_buffer", "nest_5m", "CONVEX_HULL" ) # turn that circle into 5m square around each nest

# Loop through rasters calculating stats and appending to nest locations
for raster in rasters:
	tableOut = raster + '_table'
	arcpy.sa.ZonalStatisticsAsTable("nest_5m", "id", raster,tableOut, "DATA", stat)
	arcpy.JoinField_management (nests, "id", tableOUt,"id", fieldList)
	for field in fieldList:
		arcpy.AddField_management(nests,field+'_'+raster,'LONG')
	arcpy.DeleteField_management(nests,fieldList)

# Get Beach Width if True
if (getBeachWidth):
	arcpy.GenerateNEARTable_analysis(nests, widths, "NEAR_nests","LOCATION","ANGLE")
	arcpy.JoinField_management (nests, "id", "NEAR_nests","id")

	arcpy.AddField_management(nests,'BeachWidth_DuneCrest','DOUBLE')
	arcpy.AddField_management(nests,'BeachWidth_DuneToe','DOUBLE')
	
	# Calculate BeachWidth using haversine formula
	r = 6371 # Radius of earth in meters
	cursor = arcpy.UpdateCursor(nests)
	for row in cursor:
		for loc in widths:
			lon1 = radians(row.getValue("NEAR_X_"+loc))
			lat1 = radians(row.getValue("NEAR_Y_"+loc))
			lon2 = radians(row.getValue("NEAR_X_shoreline"))
			lat2 = radians(row.getValue("NEAR_Y_shoreline"))
			dlon = radians(row.getValue("NEAR_X_shoreline") - row.getValue("NEAR_X_"+loc))
			dlat = radians(row.getValue("NEAR_Y_shoreline") - row.getValue("NEAR_Y_"+loc))
			a = (sin(dlat/2) * sin(dlat/2)) + (cos(lat1) * cos(lat2) * (sin(dlon/2) * sin(dlon/2)))
			c = 2 * atan2(sqrt(a), sqrt(1-a)) # Angular distance in radians
			width = r * c  # Distance (m) between dune and MLW
			row.setValue("BeachWidth_" + loc, width*1000)
			cursor.updateRow(row)