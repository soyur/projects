"""
File name: mapImages.py
Description:
    - Generates table (.csv) of Ricoh Theta S images
    - Generates Feature Class of image locations using CSV
    - Apply DCNY Logo to images
    - Rename images with place name (ie Bowdoin1, Bowdoin2, ....)
    - Generates "Sphere" icons
Requirements:
    (1) Python Imaging Library (PIL) module v1.1.7
    (2) Arcpy module (including matplotlib)
    (3) Directory of Images with ".JPG" extension
Inputs:
    (1) Path to image directory
    (2) Path to output directory
    (3) Path to DCNY logo
    (4) Place Name
    (5) Server type
Outputs:
    (1) Image table (.csv)
    (2) Point Feature Class of image locations
    (3) Images renamed with place name and stamped with DCNY Logo (.JPG)
    (4) One Sphere icon per image (.png)
Author: Sawyer Stippa sstippa@dutchessny.gov
Date created: 5/12/16
Edited: 7/13/2016
"""

import os, sys, csv, arcpy
from datetime import datetime as dt
import matplotlib.pyplot as plt
from ExifUtils import GetExifMetadata as exif
try:
    from PIL import Image, ImageOps, ImageDraw
except ImportError:
    print '!!!! Unable to Find PIL module !!!!'
    print 'Please Install Python Imaging Library (PIL) http://www.pythonware.com/products/pil/'
    while True:
        stopScript = raw_input('press q to quit')
        if stopScript == 'q':
            sys.exit()
        else:
            pass

#----------------------------------------------------------------------------#
#                         --->>> Functions <<<---                            #
#----------------------------------------------------------------------------#


def createCSV(imageDir, outDir, placeName, server, csvOut):
    """ Create table of image data """

    # List of JPGs
    images = [f for f in os.listdir(imageDir) if os.path.isfile(os.path.join(imageDir, f)) if f.endswith('.JPG')]

    # Write CSV
    print "*** Extracting coordinates and writing to file ***"
    lon = []
    lat = []

    with open(csvOut, 'wb') as csvfile:
        row = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)
        row.writerow(['ID'] + ['MEDIA'] + ['PLACE'] + ['DESCRIPTION'] + ['PURPOSE'] + ['CREATED'] + ['CODEBLOCK'] + ['LATITUDE'] + ['LONGITUDE'] + ['EASTING'] + ['NORTHING'])
        count = 1
        for image in images:
            imagePath = os.path.join(imageDir, image)
            # Populate fields
            ID = ''
            MEDIA = "%s%s.JPG" % (placeName, count)
            PLACE = placeName
            DESCRIPTION = ''
            PURPOSE = ''
            DATE = dt.strptime(exif(imagePath).m, '%Y:%m:%d %H:%M:%S')
            CODEBLOCK = '<div id="containerVR" style="width:100%;height:100%"><link rel="stylesheet" href="' + server + 'vr/pannellum/pannellum.css"/></div><script type="text/javascript" src="'+ server + 'vr/pannellum/libpannellum.js"></script><script type="text/javascript" src="'+ server + 'vr/pannellum/pannellum.js"></script><script>pannellum.viewer("containerVR", {"autoLoad": true,autoRotate:0.5,"panorama": "' + server + 'vr/images/' + MEDIA + '"})</script>'
            LATITUDE = exif(imagePath).y
            LONGITUDE = exif(imagePath).x
            EASTING = 0
            NORTHING = 0

            # Write fields to file
            row.writerow([ID] + [MEDIA] + [PLACE] + [DESCRIPTION] + [PURPOSE] + [DATE] + [CODEBLOCK] + [LATITUDE] + [LONGITUDE] + [EASTING] + [NORTHING])

            # Append lat/lon lists for plotting
            lon.append(exif(imagePath).x)
            lat.append(exif(imagePath).y)

            count += 1

    csvfile.close()

    return lon, lat


def plotCoords(lon, lat):
    """ Plot image locations """

    print "*** Plotting Coordinates ***"

    plt.plot(lon, lat, 'ro')
    # plt.xticks(np.arange(min(lon), max(lon)+1, 1))
    # plt.xticks(np.arange(min(lat), max(lat)+1, 1))
    plt.title('Coordinates of Panoramas')
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.ticklabel_format(style='plain')
    plt.gca().get_xaxis().get_major_formatter().set_useOffset(False)
    plt.gca().get_yaxis().get_major_formatter().set_useOffset(False)
    plt.show(block=False)


def createFC(placeName, outDir, csvOut):
    """ Create Feature Class from image locations """

    print "*** Creating Feature Class ***"

    # File geodatabase
    FGDB = placeName + '.gdb'
    if not arcpy.Exists(outDir + '/' + FGDB):
        arcpy.CreateFileGDB_management(outDir, FGDB)
    arcpy.env.workspace = outDir + '/' + FGDB  #  make FGDB working directory for arcpy
    arcpy.env.overwriteOutput = True #  overwrite FC

    # Make featuere layer
    arcpy.MakeXYEventLayer_management(csvOut, "LONGITUDE", "LATITUDE", 'temp', arcpy.SpatialReference(4326), "")

    # Project to State plane
    arcpy.Project_management('temp', placeName, arcpy.SpatialReference(2260))

    # Parse XY Fields
    arcpy.CalculateField_management(placeName,"EASTING","!SHAPE.CENTROID.X!","PYTHON_9.3","#")
    arcpy.CalculateField_management(placeName,"NORTHING","!SHAPE.CENTROID.Y!","PYTHON_9.3","#")

    # Convert to shapefile
    # shapefile = 'theta_' + time.strftime("%Y%m%d%I%M")
    # arcpy.FeatureClassToFeatureClass_conversion('temp', outDir, shapefile)


def addLogo(outDir, imagesDir, logoPath, placeName):
    """ Add logo to images and create icons """

    print "*** Pasting Logo and Creating Icons ***"

    # icons
    iconDir = outDir + "/icons/"
    if not os.path.isdir(iconDir):
        os.mkdir(iconDir)

    # images with logo
    imagesOutDir = outDir + "/images/"
    if not os.path.isdir(imagesOutDir):
        os.mkdir(imagesOutDir)

    # List of JPGs
    images = [f for f in os.listdir(imagesDir) if os.path.isfile(os.path.join(imagesDir, f)) if f.endswith('.JPG')]

    count = 1
    logo = Image.open(logoPath)
    logoWidth, logoHeight = logo.size
    for image in images:
        imagePath = os.path.join(imagesDir, image)
        im = Image.open(imagePath)
        imWidth, imHeight = im.size

        # Create Icon
        size = (128, 128)
        mask = Image.new('L', size, 0)
        draw = ImageDraw.Draw(mask)
        draw.ellipse((0, 0) + size, fill=255)
        thumb = ImageOps.fit(im, mask.size, centering=(0.5, 0.5))
        thumb.putalpha(mask)
        thumbOut = "%s%s%s.png" % (iconDir, placeName, count)
        thumb.save(thumbOut)

        # Paste DCNY logo
        x = 0
        y = imHeight - logoHeight
        im.paste(logo, (x, y))  # Location of logo - xy top left - on image
        imOut = "%s%s%s.JPG" % (imagesOutDir, placeName, count)
        im.save(imOut)
        print "%s ---> %s%s.JPG" % (image, placeName, count)
        count += 1


def main():
    """ Process Ricoh Theta S Imagery for ArcGIS """

    # Parameters
    #imagesDir = sys.argv[1]
    #outDir = sys.argv[2]
    #logoPath = sys.argv[3]
    #placeName = sys.argv[4]
    #erverType = sys.argv[5]

    # Parameters
    imagesDir = "K:/PROJECTS/OCIS/360Camera/vr/quietcove/qc/raw"
    outDir = "K:/PROJECTS/OCIS/360Camera/vr/quietcove/qc"
    logoPath = "K:/PROJECTS/OCIS/360Camera/vr/vrSEAL.png"
    placeName = "QUIETCOVE"
    serverType = "pub"

    # Test Parameters
    while True:
        if not os.path.isdir(imagesDir):
            print imagesDir + " does not exist"
            imagesDir = raw_input('Path to image directory: ')
        else:
            break
    while True:
        if not os.path.isdir(outDir):
            print outDir + " does not exist"
            outDir = raw_input('Path to output folder: ')
        else:
            break
    while True:
        if not os.path.isfile(logoPath):
            print logoPath + " does not exist"
            logoPath = raw_input('Path to logo: ')
        else:
            break
    while True:
        if placeName is None:
            placeName = raw_input('Place Name: ')
        else:
            break
    while True:
        if serverType == 'test':
            server = 'http://gistest2.dcny.gov/'
            break
        elif serverType == 'prod':
            server = 'http://gis.dcny.gov/'
            break
        elif serverType == 'pub':
            server = 'http://geoaccess.co.dutchess.ny.us/'
            break
        else:
            print 'Please type "test","prod", or "pub"'
            serverType = raw_input('Server (test,prod,pub):')

    # csv file
    csvOut = outDir + '/' + placeName + '.csv'

    latlon = createCSV(imagesDir, outDir, placeName, server, csvOut)
    plotCoords(latlon[0], latlon[1])
    createFC(placeName, outDir, csvOut)
    addLogo(outDir, imagesDir, logoPath, placeName)
    print '*** Process Complete ***'
    plt.show()  # Keep graph displayed

if __name__ == "__main__":
    main()
