## Jumbled Compass
import sys

data = []

def jc(r1,r2):
	d = r1-r2
	if abs(d) == 180:
		print abs(d)
	elif  d > 0 and d < 180:
		print d * -1
	elif d > -180 and d < 180:
		print abs(d)
	else:
		if d < 0: 
			print (360-abs(d)) * -1
		else: 
			print 360-abs(d)

for line in sys.stdin:
	data.append(int(line))
	
jc(data[0],data[1])
