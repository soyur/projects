import sys
import math

ind = 0
countLine = False
pairCount = 0
points = []
segLenths = []

def segLeg(p1,p2):
	x2 = p2[0]
	x1 = p1[0]
	y2 = p2[1]
	y1 = p1[1]
	dx = x2 - x1
	dy = y2 - y1
	seg = math.sqrt(dx**2 + dy**2)
	return seg


def countHigher(points):
	
	for point in points:
		


for line in sys.stdin:
	
	if ind == 0:
		countLine = True
		ind += 1
	elif countLine:
		pairCount = int(line)
		countLine = False
		ind += 1
	elif ind == pairCount + 1:
		x = int(line.split()[0])
		y = int(line.split()[1])
		points.append([x,y])
		i = 0
		while i < len(points)-1:
			segLenths.append(segLeg(points[i],points[i+1]))
			i+=1
			
		
		# Reset for new test case
		ind = 1
		pairCount = 0
		points = []
		segLenths = []
		countLine = True
	else:
		x = int(line.split()[0])
		y = int(line.split()[1])
		points.append([x,y])
		ind += 1
	
