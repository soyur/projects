import exifread
import os, glob

dir = 'G:/Photoscan/Phantom/6-12-2015 Peterson farm'

os.chdir(dir)

files = glob.glob('*.jpg')

geoTags = open(dir+'/'+ 'geoTags.txt','a')

geoTags.write('Name,lon,lat\n')

def convert_to_degrees(value):
    """Helper function to convert the GPS coordinates stored in the EXIF to degress in float format"""
    d0 = value[0].num
    d1 = value[0].den
    d = float(d0) / float(d1)

    m0 = value[1].num
    m1 = value[1].den
    m = float(m0) / float(m1)

    s0 = value[2].num
    s1 = value[2].den
    s = float(s0) / float(s1)

    return d + (m / 60.0) + (s / 3600.0)

for file in files:

	path = dir + '/' + file
	
	f = open(path,'rb')

	tags = exifread.process_file(f)

	lon = convert_to_degrees(tags['GPS GPSLongitude'].values)
	lat = convert_to_degrees(tags['GPS GPSLatitude'].values)

	f.close()
	
	geoTags.write(file.strip('.JPG') + ',')
	geoTags.write('-' + str(lon) + ',')
	geoTags.write(str(lat) + '\n')
	
geoTags.close()

print "Process Complete"
	
