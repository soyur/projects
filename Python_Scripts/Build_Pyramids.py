import arcpy
import time

start = time.clock()
print "Building Pyramids"
arcpy.env.workspace = r"C:\Users\sstippa\AppData\Roaming\ESRI\Desktop10.3\ArcCatalog\gis_prod.gis_raster.sde"

inras = "AERIALS2016"
pylevel = "-1"
skipfirst = "NONE"
resample = "BILINEAR"
compress = "JPEG"
quality = "75"
skipexist = "OVERWRITE"

arcpy.BuildPyramids_management(inras, pylevel, skipfirst, resample,compress, quality, skipexist)

end = time.clock()
duration = end - start
hours, remainder = divmod(duration, 3600)
minutes, seconds = divmod(remainder, 60)
print "Process Completed in %dh:%dm:%fs" % (hours, minutes, seconds)