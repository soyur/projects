# Name: Split CRM 
# Description: Split CRM into groups by OID
 
# Import system modules
import arcpy , time
arcpy.env.overwriteOutput = True

perG = 500000 # Number of points per group

# Set the workspace
arcpy.env.workspace  = r'G:\NECSC.gdb\ESMPlus_050514_Clip2_CRM_Points_v2'

zones = arcpy.ListFeatureClasses('*')

for zone in zones:
        # Make a layer from the feature class
        arcpy.MakeFeatureLayer_management(zone, "lyr") 
        
        # Get number of rows
        total = int(arcpy.GetCount_management("lyr").getOutput(0))
        print "Total for %s is %s" %(zone,total)

        if total > 1000000:
                groups = round(((float(total/perG)))+0.5) # Number of Groups for each zone
                x = 0 # start of group
                y = perG # end of group
                i = 1 #group counter
                
                while y <= (perG*groups): # Make sure to include all points
                        exp = '"OBJECTID" <= %s and "OBJECTID" > %s' %(y,x) 
                        start = time.clock()
                        if (y - total) < perG and (y - total) > 0: # If remaining number of rows is less than 500,000, add to previous group
                                e = i-1
                                print "Additional points added to %s group %s" %(zone,e) 
                                arcpy.SelectLayerByAttribute_management("lyr", "ADD_TO_SELECTION", exp)

                                 # Add selected features to previous selection
                                outName = r'G:\NECSC.gdb\CRM_groups_v2\%s_g%s' %(zone,e)
                                arcpy.CopyFeatures_management("lyr", outName)
                                
                        else:
                                # Select by groups of 500,000
                                arcpy.SelectLayerByAttribute_management("lyr", "New_SELECTION", exp)
                                        
                                # Write the selected features to a new featureclass
                                outName = r'G:\NECSC.gdb\CRM_groups_v2\%s_g%s' %(zone,i)
                                print "%s < %s group %s < %s" %(x,zone,i,y)
                                arcpy.CopyFeatures_management("lyr", outName)
                        
                        end = time.clock()
                        duration = end - start
                        hours, remainder = divmod(duration, 3600)
                        minutes, seconds = divmod(remainder, 60)
                        print "Section %s_g%s completed in %dh:%dm:%fs" % (zone,i,hours, minutes, seconds)

						# Proceed to next group of 500000
						x = x + perG
						y = y + perG
						i = i + 1
		else:
				print "%s is less than 1000000, moving on" %zone

