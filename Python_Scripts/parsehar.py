"""Reads a har file from the filesystem, converts to CSV, then dumps to
stdout.
edited by sawyer.
"""
import argparse
import json
from urlparse import urlparse

def main(harfile_path):
	"""Reads a har file from the filesystem, converts to CSV, then dumps to
	stdout.
	"""
	harfile = open(harfile_path)
	harfile_json = json.loads(harfile.read())
	i = 0
	
	f = open(harfile.name + '.csv','w')

	for entry in harfile_json['log']['entries']:
		i = i + 1
		urlLong = entry['request']['url']
		#"url": "http://gistest2:6080/arcgis/rest/services/test_tiled/MapServer/tile/7/19872/20933",
		if 'dynamic' in urlLong:
			urlShort = 'dynamic'
		elif 'tiled' in urlLong:
			urlShort = 'tiled'
		else: 
			urlShort = 'arcgis.com'
		urlparts = urlparse(entry['request']['url'])
		#size_bytes = entry['response']['bodySize']
		size_kilobytes = float(entry['response']['bodySize'])/1024
		timing = entry['time']
		mimetype = 'unknown'
		if 'mimeType' in entry['response']['content']:
			mimetype = entry['response']['content']['mimeType']

		#print '%s,"%s",%s,%s,%s,%s' % (i, urlShort, urlparts.hostname, size_kilobytes,timing, mimetype)
		line = '%s,"%s",%s,%s,%s,%s\n' % (i, urlShort, urlparts.hostname, size_kilobytes,timing, mimetype)
		
		f.write(line)
	
	print '%s complete' %harfile
	f.close()

if __name__ == '__main__':
	argparser = argparse.ArgumentParser(
		prog='parsehar',
		description='Parse .har files into comma separated values (csv).')
	argparser.add_argument('harfile', type=str, nargs=1,help='path to harfile to be processed.')
	args = argparser.parse_args()

	main(args.harfile[0])