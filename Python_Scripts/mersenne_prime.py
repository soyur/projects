'''
Finding the first Mersenne Prime

author: Sawyer Stippa
last modified: 2/6/16
'''

# Test if number is prime
def isPrime(y):
    x = 1.0
    divisors = []
    while x <= y:
        if (y/x).is_integer():
            divisors.append(x)
        else:
            pass
        x = x + 1

    if len(divisors) > 2:
        return False
    else:
        return True


# Find where n is prime and M is not
search = True
n = 2
while search:
    if isPrime(n):
        M = (2**n) - 1
        if isPrime(M):
            n = n + 1
        else:
            print "%s" %n
            search = False
    else:
        n = n + 1



