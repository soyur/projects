## Polygon Area

import sys,math

point = []
points = []
polygon = []
polygons = []
first = True
# Generate list of polygons
f = open('point_in_polygon.txt','r')
count = 0
odd = 1
for line in f:
	if line == '0':
		polygons.append(polygon)
		points.append(point)
		break
	try:
		group = count + int(line)
		
		if first:
			pass
		else:
			if (odd % 2 == 0): #polygons
				polygons.append(polygon)
				global polygon
				polygon = []
			else:
				points.append(point)
				global point
				point = []
		
		odd = odd + 1
		global first
		first = False
	except:
		if (odd % 2 == 0): #polygons
			x = int(line.split(' ')[0])
			y = int(line.rstrip('\n').split(' ')[1])
			polygon.append((x,y))
		else: #points
			x = int(line.split(' ')[0])
			y = int(line.rstrip('\n').split(' ')[1])
			point.append((x,y))
			

	count = count + 1

def inside_polygon(x, y, points):
	"""
	Return True if a coordinate (x, y) is inside a polygon defined by
	a list of verticies [(x1, y1), (x2, x2), ... , (xN, yN)].

	Reference: http://www.ariel.com.au/a/python-point-int-poly.html
	"""
	n = len(points)
	inside = False
	p1x, p1y = points[0]
	for i in range(1, n + 1):
		p2x, p2y = points[i % n]
		if y > min(p1y, p2y):
			if y <= max(p1y, p2y):
				if x <= max(p1x, p2x):
					if p1y != p2y:
						xinters = float((y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x)
					if p1x == p2x or x <= xinters:
						print p1x,p2x,xinters
						inside = not inside
		p1x, p1y = p2x, p2y
	return inside

def distance(a,b):
	return float(math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2))

def is_between(a,c,b):
	return distance(a,c) + distance(c,b) == distance(a,b)
	
#polygons = [ [(x,y),(x,y),(x,y)], [(x,y),(x,y),(x,y),(x,y)] ]
#points = [ [(x,y),(x,y),(x,y),(x,y)], [(x,y),(x,y)] ]

# for each group of vertices
for p in range(0,len(points)-1):
	# for each vertex in groups
	for v in range(0,len(points[p])-1):
		if inside_polygon(points[p][v][0],points[p][v][1],polygons[p]):
			print 'in'
	


	

	
	

#inside_polygon(points[0][0][0],points[0][0][1],polygons[0])
#inside_polygon(6,0,[(0,0),(10,0),(10,10),(0,10)])