
import datetime
from dateutil.relativedelta import relativedelta
import glob
from ua_parser import user_agent_parser
import numpy as np
import math
import matplotlib.pyplot as plt
import pypyodbc
import json
import inspect


class Logs:
	"""Parse IIS Log Files
		example Header:
			#Software: Microsoft Internet Information Services 7.5
			#Version: 1.0
			#Date: 2017-05-31 00:00:00
			#Fields: date time s-ip cs-method cs-uri-stem cs-uri-query s-port cs-username c-ip cs(User-Agent) sc-status sc-substatus sc-win32-status time-taken
		Attributes:
			Log: Log file (.log)
	"""

	def __init__(self):
		# Site and keywords
		sites = {
			'ParcelAccess':['parcelaccess','ParcelAccess','parcelpics','Parcel','PARCELACCESS','/PrintServiceDutchess/'],
			'AerialAccess':['aerialaccess','/Aerials_','AA_reference'],
			'InfoAccess':['infoaccess','InfoAccess'],
			'ArcStudio':['ArcStudio'],
			'TIP':['TIP','tip'],
			'AddressInfoFinder':['addressinfofinder','addressfinder'],
			'BicycleParking':['bicycleparking'],
			'HistoricResourceSurvey':['hrs','Historic_Survey'],
			'ReferralIdentifier':['/RI/','/ri/'],
			'Parking':['/parking/'],
			'ParksReservations':['/ParksReservations/'],
			'ArcGISJavascriptAPI':['arcgis_js_api'],
			'Other':['apple-touch-icon'],
		}

		self.sites=sites

		# List of web clients to generate stats for
		not_bot_list = ['Chrome','Firefox Mobile','Apple Mail','Safari','Mobile Safari','Facebook','Chrome Mobile iOS','Edge','IE','Firefox','Firefox iOS','Samsung Internet','Opera','Vivaldi','Amazon Silk']

		self.not_bot_list=not_bot_list

		extensions = ['.htm','.html','.shtm','.asp','.aspx','.php','.cgi','.pl','.shtml','.xhtml','.jsp','.cfm','/export','/query']

		self.extensions=extensions

		#files = ['.pdf','.png','.jpg','.gif']

	def read_Log(self,log):
		# Main stat array
		stats={
			'unknown':{
				'requests':0,
				'pages':0,
				'visitors':0,
				'clients':{'other':0},
				'os':{}
			}
		} 
		site_IP_List = {} # Unique ip array per site
		# For each site, populate stat arrays and generate IP List
		for site in self.sites:
			stats[site] = {
				'requests':0,
				'pages':0,
				'visitors':0,
				'clients':{'other':0},
				'os':{}
			}

			site_IP_List[site] = []

		# Lists of Uniques
		request_List = []
		client_List = []
		os_List = []
		unknown_IP_List = []
		unknown_request_List= []

		# Open file
		f = open(log, 'r')
		# Read and ignore header lines
		header_software = f.readline()
		header_version = f.readline()
		header_date = f.readline()
		header_fields = f.readline()

		# Loop over lines and extract variables of interest
		for line in f:
			# test for new headers
			if line[0] != '#': 
				global unknown # boolean for testing site keyword match
				line = line.strip() # remove newline characters
				columns = line.split() # get fields
				
				
				time = columns[0] + ' ' + columns[1]  # YY-mm-dd HH:MM:SS
				request = columns[4] # cs-uri-stem 
				ip = columns[8] # c-ip
				agent =  user_agent_parser.Parse(columns[9]) # cs(User-Agent)
				client = agent['user_agent']['family'] # Device client
				os = agent['os']['family'] # Device operating system

				# Generate List of Unique Requests
				if request not in request_List:
					request_List.append(request)

				# Generate List of Unique Clients
				if client not in client_List:
					client_List.append(client)

				# Generate List of Unique Operating Systems
				if os not in os_List:
					os_List.append(os)

				# If no site keywords in found request Boolean 
				unknown = True
				
				# if site string found in request
				for site in self.sites:

					for keyword in self.sites[site]:
						if keyword in request:
							# Add to request total
							stats[site]['requests'] = stats[site]['requests'] + 1

							#timeout = 20 # session timeout in minutes
							#(((datetime.datetime.strptime(time, "%Y-%m-%d %H:%M:%S") - datetime.datetime.strptime(site_IP_List[site][ip], "%Y-%m-%d %H:%M:%S")).total_seconds()/60) > timeout):
							#site_IP_List[site][ip]= time # most recent session

							# Generate List of Unique IPs 
							timeout = 20 # session timeout in minutes
							
							if ip not in site_IP_List[site]:
								site_IP_List[site].append(ip)
								stats[site]['visitors'] = stats[site]['visitors'] + 1
							
							# add to page totals
							if any(x in request for x in self.extensions) or request[-1:] =='/':
								stats[site]['pages'] = stats[site]['pages'] + 1  

							# Generate List of Unique clients and add to site client totals
							if client not in stats[site]['clients']:
								if client in self.not_bot_list:
									stats[site]['clients'][client] = 1
								else:
									stats[site]['clients']['other'] = stats[site]['clients']['other'] + 1
							else:
								stats[site]['clients'][client] = stats[site]['clients'][client] + 1
							
							# Generate List of Unique operating systems and add site os totals
							if os not in stats[site]['os']:
								stats[site]['os'][os] = 1
							else:
								stats[site]['os'][os] = stats[site]['os'][os] + 1

							
							unknown = False # site keyword match found

							# reset site loop and go to next line
							break 
					else:
						continue
					break
				# add unknown requests and pages to 'unknown' totals
				if unknown:
					stats['unknown']['requests'] = stats['unknown']['requests'] + 1
					if any(x in request for x in self.extensions) or request[-1:] =='/':
						stats['unknown']['pages'] = stats['unknown']['pages'] + 1  
					# Generate List of Unique clients and add to site client totals
					if client not in stats['unknown']['clients']:
						if client in self.not_bot_list:
							stats['unknown']['clients'][client] = 1
						else:
							stats['unknown']['clients']['other'] = stats['unknown']['clients']['other'] + 1
					else:
						stats['unknown']['clients'][client] = stats['unknown']['clients'][client] + 1

					# Generate List of Unique operating systems and add site os totals
					if os not in stats['unknown']['os']:
						stats['unknown']['os'][os] = 1
					else:
						stats['unknown'][os] = stats['unknown']['os'][os] + 1
					
					if ip not in unknown_IP_List:
						unknown_IP_List.append(ip)
						stats['unknown']['visitors'] = stats['unknown']['visitors'] + 1  

		f.close()

		#print 'Total Server Requests: %s' %stats['server']['requests']
		#print 'Total Server Pages: %s' %stats['server']['pages']
		print 'Total Unknown Requests: %s' %stats['unknown']['requests']
		#print 'Total Unknown Pages: %s' %stats['unknown']['pages']
		return stats
		
	# Return list of non-site requests
	def non_Sites(self,log):
		nonsites_List = []

		# Open file
		f = open(log, 'r')

		# Read and ignore header lines
		header_software = f.readline()
		header_version = f.readline()
		header_date = f.readline()
		header_fields = f.readline()

		# Loop over lines and extract variables of interest
		for line in f:
			# test for new headers
			if line[0] != '#': 
				counter = 0
				line = line.strip() # remove newline chaaracters
				columns = line.split() # get fields
				request = columns[4] # cs-uri-stem 

				# If request doesn't contain key words (sites)
				if not any(word in request for word in sites):
					nonsites_List.append(request)
		
		return len(nonsites_List)
		f.close()

	def total_requests_pages(self,statistics,statistic):
		count = 0
		for site in statistics:
			count  = count + statistics[site][statistic]
		return count;

	def total_clients_os(self,statistics,statistic):
		user_agent = {}
		for site in statistics:
			for ua in statistics[site][statistic]:
				if ua not in user_agent:
					user_agent[ua] = 1
				else:
					user_agent[ua] = user_agent[ua] + statistics[site][statistic][ua] 

		return user_agent

	# Parse all logs in server directory  
	def log_Year(self,year,server):
		dir = '../'+server+'/'+year+'/*.log'
		logs = glob.glob(dir)
		count = len(logs)
		i = 1
		days = {}
		for log in logs:
			year = log.split('ex')[1].split('.log')[0]
			days[year] = Logs().read_Log(log)
			print '%s complete: %s / %s complete' %(log,i,count)
			i = i+1

		return days

	# Parse User Agent String (ua)
	def parse_UA(self,ua):
		parsed_string = user_agent_parser.Parse(ua)
		os = parsed_string.os.family
		client = parsed_string.user_agent.family
		return os,client

	# Plot page requests (Data Array,"requests" or "pages")
	def plot_requests_pages(self,data,statistic):
		x = np.arange(len(data))
		y = []
		labels = []
		
		for site in data:
			labels.append(site)
			y.append(data[site][statistic])

		plt.bar(x, y, align='center')
		plt.xticks(x,labels,rotation=20)
		plt.ylabel(statistic)
		plt.title('Geoaccess Totals by Site')
		 
		plt.show()

	# Plot client or operating system usage (array of clients or array of operating systems)
	def plot_clients_os(self,site,statistic):
		x = np.arange(len(site[statistic]))
		y = []
		labels = []
		
		for i in site[statistic]:
			labels.append(i)
			y.append(site[statistic][i])

		plt.bar(x, y, align='center')
		plt.xticks(x,labels,rotation=20)
		plt.ylabel(statistic)
		plt.title('Geoaccess Totals by Device')
		 
		plt.show()

	# Add total Requests and Pages to SQL Table
	def update_requests(self,stats):
		connection = pypyodbc.connect('Driver={SQL Server};Server=DBSVRTEST64\SQLTEST;Database=########;uid=######;pwd=#######')

		cursor = connection.cursor()
		table = "server_logs_requests"
		columns = "Site,Requests,Pages,Date"

		for day in stats:
			for stat in stats[day]:
				date = '20' + day[0:2] + '-' + day[2:4] + '-' + day[4:6]
				SQL = ("INSERT INTO "+ table +" ("+columns+") VALUES (?,?,?,?)")
				Values = [stat,stats[day][stat]['requests'],stats[day][stat]['pages'],date] 
				cursor.execute(SQL,Values)

		connection.commit()
		connection.close()

	
		
class Charts:
	""" Build json files for chart inputs

	Attributes:
		site: List of sites to evaluate. Use 'all' for all sites.
		start_date: Start date yyyy-mm-dd.
		end_date: End date yyyy-mm-dd.
		statistic: Statistic (Requests, Pages) to evaluate. Use 'all' for all stats.
	"""

	# def __init__(self,site,start_date,end_date,statistic):
	# 	self.site = site
	# 	self.start_date = start_date
	# 	self.end_date = end_date
	# 	self.statistic = statistic

	# 'ParcelAccess' '2015-01-01' '2017-05-31' 'Requests'
	def get_totals_from_sql(self,site,start_date,end_date,statistic):
		
		#dateS = '2015-01-01'
		#dateE = datetime.date.today().strftime("%Y-%m-%d")

		# If site is null, do all sites
		#if site is not None:
		#	siteT = site
			
		# If dates are null, do all time
		#if date_start is not None: 
		#	dateS = self.start_date
		#if date_end is not None: 
		#	dateE = self.end_date
			
		connection = pypyodbc.connect('Driver={SQL Server};Server=DBSVRTEST64\SQLTEST;Database=#######;uid=#####;pwd=######',readonly=True)
		cursor = connection.cursor()
		table = "server_logs_requests"

		SQL = ("SELECT SUM("+statistic+") FROM "+table+ " WHERE Site LIKE '"+site+"' AND Date BETWEEN '" + start_date + "' AND '" + end_date + "'")

	
		cursor.execute(SQL)
		totals = cursor.fetchone()
	
		connection.commit()

		if totals[0] is not None:
			return int(totals[0])
		else:
			return None
	
		connection.close()

	def build_json(self,site,start_date,end_date,time,count):
		data = {}
		data[site] = {}
		data[site][start_date] = {}

		#timing = inspect.getargspec(Charts().build_json)[0][4]
		timing = time
		#value = inspect.getargspec(Charts().build_json)[3][0]
		value = count
	
		
		date1 = datetime.datetime.strptime(start_date, '%Y-%m-%d').date()
		date2 = datetime.datetime.strptime(end_date, '%Y-%m-%d').date()

		cur_date = date1

		dates = []
		while cur_date < date2:
			dates.append(cur_date)
			cur_date += eval("relativedelta("+timing+"="+str(value)+")")
			
			# while sd != end_date:
			# 	date_after_month = datetime.datetime.strptime(sd, "%Y-%m-%d") + relativedelta(months=1)
			# 	months.append(date_after_month.strftime("%Y-%m-%d"))
			# 	sd = date_after_month

			# print months


		# if self.site == 'all':
		# 	site_request_total = get_totals_from_sql('ParcelAccess','2017-01-01','2017-05-31')
		# 	all_requests = get_totals_from_sql(None,'2017-01-01','2017-05-31')
		# 	pa_perc = int((float(site_request_total)/float(all_requests))*100)
		# 	notPA_perc = 100 - pa_perc
		
		# 	print site_request_total
		# 	print all_requests
		# 	print 'PA Precent: %d Not PA: %d' %(pa_perc,notPA_perc)
			
		# 	chart = {
		# 		'ParcelAccess':pa_perc,
		# 		'not_ParcelAccess':notPA_perc
		# 	}d

		# 	with open('P:\gisHOME\charts\parent.json', 'w') as outfile:
		# 		json.dump(chart, outfile)

		
		count = 0;
		for date in dates:
			d1 = date.strftime('%Y-%m-%d')
			if count < len(dates) -1:
				d2 = dates[count+1].strftime('%Y-%m-%d')
			else:
				d2 = end_date

			request_total = Charts().get_totals_from_sql('%',d1,d2,'Requests')
			page_total = Charts().get_totals_from_sql('%',d1,d2,'Pages')
			if site == 'Geoaccess':
				if request_total is not None:
					data[site][d1] = {}
					data[site][d1]["requests"] = request_total
					data[site][d1]["pages"] = page_total
					data[site][d1]["end_date"] = d2
			else:

				site_request_total = Charts().get_totals_from_sql(site,d1,d2,'Requests')
				site_page_total = Charts().get_totals_from_sql(site,d1,d2,'Pages')
				if site_request_total is not None:
				
					api_request_total = Charts().get_totals_from_sql('ArcGISJavascriptAPI',d1,d2,'Requests')
					api_page_total = Charts().get_totals_from_sql('ArcGISJavascriptAPI',d1,d2,'Pages')

					unknown_request_total = Charts().get_totals_from_sql('unknown',d1,d2,'Requests')
					unknown_page_total = Charts().get_totals_from_sql('unknown',d1,d2,'Pages')

					other_request_total = Charts().get_totals_from_sql('Other',d1,d2,'Requests')
					other_page_total = Charts().get_totals_from_sql('Other',d1,d2,'Pages')
					
					percR = round((float(site_request_total)/float(request_total-api_request_total-unknown_request_total-other_request_total))*100,2)
					percP = round((float(site_page_total)/float(page_total-api_page_total-unknown_page_total-other_page_total))*100,2)
				
					data[site][d1] = {}
					data[site][d1]["requests"] = site_request_total
					data[site][d1]["percent_requests"] = percR
					data[site][d1]["pages"] = site_page_total
					data[site][d1]["percent_pages"] = percP
					data[site][d1]["end_date"] = d2

			count = count + 1

		outname = 'P:\\gisHOME\\charts\\'+site.lower()+'\\' + site.lower() + '_'+ start_date + '_'+ end_date + '_' + timing + '_' +'.json'

		with open(outname, 'w') as outfile:
			json.dump(data, outfile)


	def build_charts_year(self):
		years = {}
		for y in range(2014,2018,1):
			years[str(y)] = {}
			for m in range(1,13):
				years[str(y)][datetime.date(y, m, 1).strftime('%B')] = {'Request':0,'Pages':0}
			

 		print years

if __name__ == '__main__':
	#logFile = '../geoaccess/2017/u_ex170208.log'
	#statistics = read_Log(logFile)
	#plot_clients_ips(statistics['parcelaccess'],'clients')
	#plot_requests_pages(statistics,'requests')
	update_requests(log_Year('2016','geoaccess'))
	update_requests(log_Year('2015','geoaccess'))
	update_requests(log_Year('2014','geoaccess'))
	update_requests(log_Year('2013','geoaccess'))
	#site_list = ['ParcelAccess','AerialAccess','InfoAccess','ReferralIdentifier','ArcStudio','AddressInfoFinder','TIP','BicycleParking','HistoricResourceSurvey']
	#build_chart(site_list)