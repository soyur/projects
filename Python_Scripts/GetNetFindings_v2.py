######################################################################
## Query ArcMap using Netica findings
## Requires netica.py module - with addional "getnodefinding" class
##    path to module:  C:\Python27\ArcGISx6410.2\Lib\site-packages\Netica-0.0.1-py2.7.egg\netica
##       ~\Netica folder must include lib\netica.dll (64bit or 32bit) and this script
##
## Created by Sawyer Stippa
## sstippa@usgs.gov
## 09/23/14
######################################################################

from netica import Netica
#import numpy as np

# initialize class
ntc = Netica()
# create new environment
env = ntc.newenv()
# initialize environment
ntc.initenv(env)
# open net
netName = 'G:\iPlover\ArcNet\PIPL_iPlover_SimpleModel_21Nov2014.neta'
net = ntc.opennet(env,netName)
#strm = ntc._newstream(env,netName)
#net = ntc._readnet(strm)
#ntc._writenet(net,netName)

# compile net
#ntc.compilenet(net)
 
# set autupdate - update beliefs when new findings entered
#ntc.setautoupdate(net, auto_update = 1)5

# nodes (in future loop through all nodes)
vegtype = ntc.getnodenamed('VegetationType',net)
subtype = ntc.getnodenamed('SubstrateType',net)
vegden = ntc.getnodenamed('VegetationDensity',net)
geoset = ntc.getnodenamed('GeomorphicSetting',net)
nest = ntc.getnodenamed('NestAttempt',net)
nodes = [vegtype,subtype,vegden,geoset,nest]

# get findings - Outputs state index if finding is true, negative number is false
vegtypeFind = ntc.getnodefinding(vegtype) 
subtypeFind = ntc.getnodefinding(subtype) 
vegdenFind = ntc.getnodefinding(vegden) 
geosetFind = ntc.getnodefinding(geoset)
nestFind = ntc.getnodefinding(nest)
findings = [vegtypeFind,subtypeFind,vegdenFind,geosetFind,nestFind]

flen = len(findings)
x = 0
while x <= flen-1:
	print "%s-%s;" % (ntc.getnodename(nodes[x]),ntc.getnodestatename(nodes[x],findings[x]))
	x =  x + 1

# clear findings
#ntc.retractnetfindings(net)

# Save net
#ntc.savenet(env,net,netName)
#ntc._writenet(net,netName)

# close net
ntc.closeenv(env)

# Delete net
#ntc.deletenet(net)







