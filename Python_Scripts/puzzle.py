‘’’
Solving Director GCHQ's Christmas Puzzle - Part 2
The puzzle answer had a URL ending in a combination of letters ABCDEF (6 separate puzzle answers). This script simply tries all the url links until it doesn’t display “Sorry - you did not get all the questions correct”
https://www.gchq.gov.uk/features/director-gchqs-christmas-puzzle-part-2
‘’’

import urllib2
import re
import itertools


list= ['A','B','C','D','E','F']

count = 0
for subset in itertools.product(list,repeat=6):
    #print subset
    part = "".join(subset)
    #print part

    url = "http://s3-eu-west-1.amazonaws.com/puzzleinabucket/%s.html" %part

    html_content = urllib2.urlopen(url).read()

    #print html_content

    matches = re.findall('Sorry - you did not get all the questions correct', html_content);

    if len(matches) == 0: 
        print url
        
    else:
       pass

    count = count + 1

print "complete"

#http://s3-eu-west-1.amazonaws.com/puzzleinabucket/DEFACE.html
