###################################################################
## Query ArcMap using Netica findings
## Requires GetNetFindings.py
##
## Created by Sawyer Stippa
## sstippa@usgs.gov
## 11/29/14
##
####################################################################

# Not a completely stand alone script - to be run in ArcMap (32 bit) python window

import arcpy, subprocess

# Get PloverNet findings. Python.exe (64 bit) must be in path.
p = subprocess.Popen(['G:/iPlover/ArcNet/GetNetFindings_v2.py'],stdout=subprocess.PIPE,shell=True)
result = p.communicate()[0]
print "---From ploverNet---"
print result

# Split Nodes by finding
vegtype = result.split(';')[0].split('-')[1]
subtype = result.split(';')[1].split('-')[1]
vegden = result.split(';')[2].split('-')[1]
geoset = result.split(';')[3].split('-')[1]
nest2 = result.split(';')[4].split('-')[1]
if nest2 == 'Absent':
    nest = '0'
elif nest2 == 'Present':
    nest = '1'
else:
    nest = 'error'


findings = [vegtype,subtype,vegden,geoset,nest]
nodes = ['Vegetation','SubstrateT','Vegetati_1','Geomorphic','NestAttemp'] # Node names in table (shortened)

defQ = [] # list of selected findings (without error)
elist = [] # location of selected findings in list
x = 0
for finding in findings:
    if 'error' not in finding:
        defQ.append(finding)
        elist.append(x)
        x = x + 1
    else:
        x = x + 1
        
x = 0
strings = [] # list of findings (strings) as definition queries 
while x <=  len(elist) - 1:
    if defQ[x] == '0' or defQ[x] == '1':
        string = ''' "%s" = %s ''' %(nodes[elist[x]],defQ[x])
        strings.append(string)
        x = x + 1
    else:
        string = ''' "%s" = '%s' ''' %(nodes[elist[x]],defQ[x])
        strings.append(string)
        x = x + 1

defQuery = ' AND '.join(strings) # join each list item to create complete definition query

#################### Requires ArcMap to be open ######################

mxd = arcpy.mapping.MapDocument("CURRENT") 
df = arcpy.mapping.ListDataFrames(mxd)[0]

#fc = 'Grid_example'
fc = 'iPlover_112114'


for layer in arcpy.mapping.ListLayers(mxd,fc):
# Update iPlover mxd to display only selected findings
    layer.definitionQuery = defQuery 
    
arcpy.RefreshActiveView()
df.extent = layer.getExtent()
df.extent = layer.getExtent()
arcpy.RefreshActiveView()

'''
if 'no findings in net' in result:
	print "---Definition Query---"
	print 'no findings in net'

	# Must be open in ArcMap
	mxd = arcpy.mapping.MapDocument("CURRENT") 
	df = arcpy.mapping.ListDataFrames(mxd)[0]
	for layer in arcpy.mapping.ListLayers(mxd,'iplover_*'):
		layer.definitionQuery = """ "substrate" LIKE '%' AND "vegtype" LIKE '%'  """ 

	df.extent = layer.getExtent()
	arcpy.RefreshActiveView()
	
	stop = 1

# Import node names and state names from Netica. Alter to match iPlover fields.
# Substrate type (VegRech) and Vegetation Type (Vegetation Refined) only for now
elif 'no findings for vegetation type ' in result:
	nodes = result.split(';')
	sub = nodes[0].split('-')
	if ('shrub' or 'forest') in sub[1].lower(): 
		subtype = 'forestshrub'
	elif 'sandy' in sub[1].lower():
		subtype = 'sandy'
	else:
		subtype = sub[1].lower()

	vegtype = 'no findings entered'

	print "---Definition Query---"
	print 'Substrate Type: %s\nVegetation Type: %s' %(subtype,vegtype)

	stop = 0
	
elif 'no findings for substrate type ' in result:
	nodes = result.split(';')
	veg = nodes[0].split('-')
	if 'woody' in veg[1].lower():    
		vegtype = 'woodyshrub'
	else:
		vegtype = veg[1].lower()

	subtype = 'no findings entered'

	print "---Definition Query---"
	print 'Substrate Type: %s\nVegetation Type: %s' %(subtype,vegtype)

	stop = 0

else:

	nodes = result.split(';')
	veg = nodes[0].split('-')
	if 'woody' in veg[1].lower():    
		vegtype = 'woodyshrub'
	else:
		vegtype = veg[1].lower()

	sub = nodes[1].split('-')
	if ('shrub' or 'forest') in sub[1].lower(): 
		subtype = 'forestshrub'
	elif 'sandy' in sub[1].lower():
		subtype = 'sandy'
	else:
		subtype = sub[1].lower()

	print "---Definition Query---"
	print 'Substrate Type: %s\nVegetation Type: %s' %(subtype,vegtype)

	stop = 0

if stop == 0:
	# Must be open in ArcMap
	mxd = arcpy.mapping.MapDocument("CURRENT") 
	df = arcpy.mapping.ListDataFrames(mxd)[0]

	# Possible field types
	sublist = 'forestshrub sandy wetland'
	veglist = 'herbaceous shellbed water woodyshrub'
	for layer in arcpy.mapping.ListLayers(mxd,'iplover_*'):
		# Update iPlover mxd to display only selected findings
		if (subtype in sublist) and (vegtype in veglist):
			layer.definitionQuery = """ "substrate" = '%s' AND "vegtype" = '%s'  """ %(subtype,vegtype)
		elif subtype in sublist:
			layer.definitionQuery = """ "substrate" = '%s' """ %subtype
			print "\nWarning: No finding for vegtype or finding does not have corrresponding iPlover value"
			print "Only showing nest locations with substrate type: %s" %subtype
		elif vegtype in veglist:
			layer.definitionQuery = """ "vegtype" = '%s' """ %vegtype
			print "\nWarning: No finding for subtype or finding does not have corrresponding iPlover value." 
			print "Only showing nest locations with substrate type: %s" %vegtype
		else:
			print "PloverNet findings do not have corresponding iPlover values"

	arcpy.RefreshActiveView()
	df.extent = layer.getExtent()
	df.extent = layer.getExtent()
	arcpy.RefreshActiveView()

else:
	print "no findings entered"

'''
