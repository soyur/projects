
import os
from PIL import Image

imagesDir = r"K:\PROJECTS\OCIS\360Camera\vr\bowdoin\images_old"
logoPath = r"K:\PROJECTS\OCIS\360Camera\Logo\Final\SEAL_warped.png"
# List of JPGs
images = [f for f in os.listdir(imagesDir) if os.path.isfile(os.path.join(imagesDir, f)) if f.endswith('.JPG')]
logo = Image.open(logoPath)
logoWidth, logoHeight = logo.size
for image in images:
	imagePath = os.path.join(imagesDir, image)
	im = Image.open(imagePath)
	imWidth, imHeight = logo.size
	x = 0
	y = imHeight - logoHeight
	# Paste DCNY logo
	im.paste(logo, (x, y))  # Location of logo - xy top left - on image
	imOut = "%s" % (image)
	im.save(imOut)
	print "%s complete" % (image)



