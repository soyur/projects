'''
Convert P_pres points to kilometer percentages.
'''

import arcpy, math
###########################################################
arcpy.env.workspace = r'G:\ASIS_PIplFiles_6_2015\ASISkm.gdb' 
arcpy.env.overwriteOutput = True # Overwrite output?		 
DEL = True # Delete temp files?								 
points = 'PpresPredALL_1_27_3Bmmyr'
tranSort = 'PpresPredALL_1_27_3Bmmyr_tranSORT'
###########################################################

## Start processing

## Point to line - converts Ppres points to line according to transOrder
print "Converting points to line"
arcpy.PointsToLine_management(points, 'transects_TEMP',"TransOrder")

## Add start/end geometry -  defines start/mid/end points for line
print "Adding start and end point geometry"
arcpy.AddGeometryAttributes_management('transects_TEMP', "LINE_START_MID_END")

## Sort according to Ocean side Y
for row in arcpy.SearchCursor('transects_TEMP',"OBJECTID=1"):
	if row.getValue('START_Y') > row.getValue('END_Y'):
		sort = 'END'
	else:
		sort = 'START'

## Sort descending - sorts lines north to south, according to position of line startpoint (oceanside)
print "Sorting North to South"
#arcpy.Sort_management('transects_TEMP', tranSort, [[sort+"_Y", "DESCENDING"]])
arcpy.Sort_management('transects_TEMP', tranSort, [['transOrder', "DESCENDING"]])
# add km field
print "adding km field"
arcpy.AddField_management(tranSort,'km','SHORT')

## Calculate distance between line start points - assign km value
print "Calculating distance"
cursor = arcpy.UpdateCursor(tranSort)
dz = 0 # distance traveled in meters from start of km
km = 1 
firstRow = True
for row in cursor:
	while firstRow:
		x1 = row.getValue(sort+'_X')
		y1 = row.getValue(sort+'_Y')
		firstRow = False
	x2 = row.getValue(sort+'_X')
	y2 = row.getValue(sort+'_Y')
	dx = abs(x1 - x2)
	dy = abs(y1 - y2)
	z = math.sqrt((dx*dx)+(dy*dy))
	dz = dz + z
	if dz <= (1000):
		row.setValue('km',km) 
	else:
		row.setValue('km', km+1)
		km = km + 1
		dz = 0

	x1 = x2
	y1 = y2
	cursor.updateRow(row)

## Create points from vertices
#arcpy.FeatureVerticesToPoints_management(tranSort, 'start_end_pts', "BOTH_ENDS")
arcpy.FeatureVerticesToPoints_management(tranSort, 'start_pts_TEMP', "START")
arcpy.FeatureVerticesToPoints_management(tranSort, 'end_pts_TEMP', "END")

## Create KM polygons
print "Creating km polygons" 
count = 1
while count <= km-1:
	query = '"km" = %s' %count
	arcpy.Select_analysis('start_pts_TEMP','outStart_TEMP',query)
	arcpy.PointsToLine_management('outStart_TEMP', 'outStartLine_TEMP')

	arcpy.Select_analysis('end_pts_TEMP','outEnd_TEMP',query)
	arcpy.PointsToLine_management('outEnd_TEMP', 'outEndLine_TEMP')
	
	try:
		arcpy.FeatureVerticesToPoints_management('outStartLine_TEMP', 'startStart_TEMP', "START")
		arcpy.FeatureVerticesToPoints_management('outStartLine_TEMP', 'startEnd_TEMP', "END")
		arcpy.FeatureVerticesToPoints_management('outEndLine_TEMP', 'endStart_TEMP', "START")
		arcpy.FeatureVerticesToPoints_management('outEndLine_TEMP', 'endEnd_TEMP', "END")

		arcpy.Merge_management(['startStart_TEMP','endStart_TEMP'],'startPts_TEMP')
		arcpy.Merge_management(['startEnd_TEMP','endEnd_TEMP'],'endPts_TEMP')

		arcpy.PointsToLine_management('startPts_TEMP', 'startLines_TEMP')
		arcpy.PointsToLine_management('endPts_TEMP', 'endLines_TEMP')

		arcpy.Merge_management(['outStartLine_TEMP','outEndLine_TEMP','startLines_TEMP','endLines_TEMP'],'outMerge_TEMP')
		outPoly = 'km%s_TEMP' %count
	
		arcpy.FeatureToPolygon_management('outMerge_TEMP',outPoly)
		arcpy.AddField_management(outPoly,'km','SHORT')
		arcpy.CalculateField_management(outPoly,'km',count,"PYTHON_9.3")
		print '%s-km complete' %count
	except:
		print "not enough segments to create polygon, skipping %skm" %count

	count = count + 1
	

## Merge/buffer/dissolve km polygons
print 'Merging polygons'
kms = arcpy.ListFeatureClasses('km*')
arcpy.Merge_management(kms,'Merge_TEMP')
print"Buffering polygons"
arcpy.Buffer_analysis('Merge_TEMP','buffer_TEMP','1 Meter','FULL') # Buffer 1m
print"Dissolving polygons"
arcpy.Dissolve_management("buffer_TEMP", 'kmOut_TEMP','km')
print "%s complete" %points

## Ppres > 0.5? Herb?
arcpy.AddField_management(points,'above5','SHORT')
if 'mmyr' in points:
	arcpy.AddField_management(points,'herb','SHORT')
	
cursor = arcpy.UpdateCursor(points)
for row in cursor:
	if 'mmyr' in points:
		if row.getValue('Pres') > 0.5:
			row.setValue('above5',1)
		else:
			row.setValue('above5',0)
		
		if row.getValue('HabOut') == 33:
			row.setValue('herb',1)
		else:
			row.setValue('herb',0)	
	else: 
		if row.getValue('Ppres') > 0.5:
			row.setValue('above5',1)
		else:
			row.setValue('above5',0)
	cursor.updateRow(row)

## Spatial Join Ppres points with KM polygon - Count and SUM of Ppres
fieldmappings = arcpy.FieldMappings()
fieldmappings.addTable('kmOut_TEMP')
fieldmappings.addTable(points)
Ppres_Above5 = fieldmappings.findFieldMapIndex("above5")	
fieldmap = fieldmappings.getFieldMap(Ppres_Above5)
field = fieldmap.outputField
fieldmap.outputField = field
fieldmap.mergeRule = "Count"
fieldmappings.replaceFieldMap(Ppres_Above5, fieldmap)
fieldmap = fieldmappings.getFieldMap(Ppres_Above5)
field = fieldmap.outputField
fieldmap.outputField = field
fieldmap.mergeRule = "Sum"
fieldmappings.replaceFieldMap(Ppres_Above5, fieldmap)

if 'mmyr' in points:
	Herb_Above2 = fieldmappings.findFieldMapIndex("herb")
	fieldmap = fieldmappings.getFieldMap(Herb_Above2)
	field = fieldmap.outputField
	fieldmap.outputField = field
	fieldmap.mergeRule = "Sum"
	fieldmappings.replaceFieldMap(Herb_Above2, fieldmap)
	
output = points+'_Perc'
arcpy.SpatialJoin_analysis('kmOut_TEMP', points, output, "#", "#", fieldmappings)

## Add field perc and calculate
arcpy.AddField_management(output,'PercAbove5','FLOAT')
cursor = arcpy.UpdateCursor(output)
for row in cursor:
	total = row.getValue('Join_Count')
	above5 = row.getValue('above5')
	math = float(float(above5)/float(total))*100
	row.setValue('PercAbove5',math)
	cursor.updateRow(row)
	
if 'mmyr' in points:	
	arcpy.AddField_management(output,'PercHerb','FLOAT')
	cursor = arcpy.UpdateCursor(output)
	for row in cursor:
		total = row.getValue('Join_Count')
		above2 = row.getValue('herb')
		math = float(float(above2)/float(total))*100
		row.setValue('PercHerb',math)
		cursor.updateRow(row)


## Clip
clipOut = points + "_clip"
arcpy.Clip_analysis(output, "ASIS_BND", clipOut)

## Delete temps
if DEL:
	print "Deleting TEMPs"
	for fc in arcpy.ListFeatureClasses('*TEMP'):
		arcpy.Delete_management(fc)

print 'Processing complete'	

