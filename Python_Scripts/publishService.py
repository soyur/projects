# Publishes a Map service 
# A connection to ArcGIS Server must be established in the
# Catalog window of ArcMap before running this script
import arcpy

# Define local variables
staging = "C:/Users/sstippa/AppData/Local/Temp/arcABC4/Staging/" # Default Temp
mapDoc = arcpy.mapping.MapDocument("K:/ARCGISSERVER/rims_v2/mxd/Rims.mxd") # Current, open MXD
servName = "arcgis on gistest2_6080 (publisher).ags" # test, prod or public

# Provide path to connection file
# To create this file, right-click a folder in the Catalog window and
#  click New > ArcGIS Server Connection
con = r"C:\Users\sstippa\AppData\Roaming\ESRI\Desktop10.3\ArcCatalog\\" + servName

# Provide other service details
service = 'Rims_v2'
sddraft = staging + service + '.sddraft'
sd = staging + service + '.sd'

# Create service definition draft
arcpy.mapping.CreateMapSDDraft(mapDoc, sddraft, service, 'FROM_CONNECTION_FILE', con)

# Analyze the service definition draft
analysis = arcpy.mapping.AnalyzeForSD(sddraft)

# Print errors, warnings, and messages returned from the analysis
print "The following information was returned during analysis of the MXD:"
for key in ('messages', 'warnings', 'errors'):
  print '----' + key.upper() + '---'
  vars = analysis[key]
  for ((message, code), layerlist) in vars.iteritems():
    print '    ', message, ' (CODE %i)' % code
    print '       applies to:',
    for layer in layerlist:
        print layer.name,
    print

# Stage and upload the service if the sddraft analysis did not contain errors
if analysis['errors'] == {}:
    # Execute StageService. This creates the service definition.
    arcpy.StageService_server(sddraft, sd)

    # Execute UploadServiceDefinition. This uploads the service definition and publishes the service.
    arcpy.UploadServiceDefinition_server(sd, con)
    print "Service successfully published"
else: 
    print "Service could not be published because errors were found during analysis."

print arcpy.GetMessages()