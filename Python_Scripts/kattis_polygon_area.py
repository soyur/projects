## Polygon Area

import sys,math

polygon = []
polygons = []
first = True

# Generate list of polygons
for line in sys.stdin:
	if line == '0':
		polygons.append(polygon)
		break
	else:
		try:
			int(line)
		
			#skip first
			if first:
				pass
			else:
				polygons.append(polygon)
			global polygon
			polygon = []
			global first
			first = False
		except:
			x = int(line.split(' ')[0])
			y = int(line.split(' ')[1])
			polygon.append((x,y))
			


# Calculate area and get direction
def getArea(vertices):
	c = 0
	adds = []
	while c < len(vertices):
		if c == len(vertices)-1:
			v2y = vertices[0][1]
			v1y = vertices[c][1]
			v2x = vertices[0][0]
			v1x = vertices[c][0]
			top = float(v1x*v2y-v1y*v2x)
			adds.append(top)
		else:
			v2y = vertices[c+1][1]
			v1y = vertices[c][1]
			v2x = vertices[c+1][0]
			v1x = vertices[c][0]
			top = float(v1x*v2y-v1y*v2x)
			adds.append(top)
			
		c = c + 1
		
	
	if 	sum(adds)/2 > 0:
		d = 'CCW'
	else:
		d = 'CW'
	
	area = abs(sum(adds)/2)
	print "%s %.1f" %(d,area)
	
for poly in polygons:
	getArea(poly)


