######################################################################
## Query ArcMap using Netica findings
## Requires netica.py module - with addional "getnodefinding" class
##    path to module:  C:\Python27\ArcGISx6410.2\Lib\site-packages\Netica-0.0.1-py2.7.egg\netica
##       ~\Netica folder must include lib\netica.dll (64bit or 32bit) and this script
##
## Created by Sawyer Stippa
## sstippa@usgs.gov
## 09/23/14
######################################################################

from netica import Netica
import numpy as np

# initialize class
ntc = Netica()
# create new environment
env = ntc.newenv()
# initialize environment
ntc.initenv(env)
# open net
netName = 'C:\Netica\PIPL_int_model_1_8_14simpBWVR2all.neta'
net = ntc.opennet(env,netName)
#net = ntc._readnet(netName)

# compile net
#ntc.compilenet(net)
 
# set autupdate - update beliefs when new findings entered
#ntc.setautoupdate(net, auto_update = 1)

# nodes (in future loop through all nodes)
vegtype = ntc.getnodenamed('VegetationRefined',net)
subtype = ntc.getnodenamed('VegRech',net)

# get findings - Outputs state index if finding is true, negative number is false
vegF = ntc.getnodefinding(vegtype) 
subF = ntc.getnodefinding(subtype) 

# if findings are true, get states
if (vegF >= 0) and (subF >= 0):
	vegSN = ntc.getnodestatename(vegtype,vegF)
	subSN = ntc.getnodestatename(subtype,subF)
	print "%s-%s;\n%s-%s; " %(ntc.getnodename(vegtype),vegSN,ntc.getnodename(subtype),subSN)
elif vegF >=0:
	vegSN = ntc.getnodestatename(vegtype,vegF)
	print "%s-%s;\n%s-no findings for substrate type " %(ntc.getnodename(vegtype),vegSN,ntc.getnodename(subtype))
elif subF >= 0:
	subSN = ntc.getnodestatename(subtype,subF)
	print "%s-%s;\n%s-no findings for vegetation type " %(ntc.getnodename(subtype),subSN,ntc.getnodename(vegtype))
else:
	print "no findings in net"

# clear findings
#ntc.retractnetfindings(net)

# Save net
#ntc.savenet(env,net,netName)

# close net
ntc.closeenv(env)

# Delete net
#ntc.deletenet(net)







