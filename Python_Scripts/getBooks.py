# -*- coding: utf-8 -*-
‘’’
This script iterates through an html file containing links and generates a bookmarks file readable by Chrome
’’’

dads = open("dadMarks.html","w")
dads.write('<!DOCTYPE html><html><head><meta charset="UTF-8"></head><body>\n')
with open("Bookmarks","r") as searchfile:
	for line in searchfile:
		if '"url":' in line:
			url = line.split('"url": ')[1]			
			dads.write('<a HREF="' + url[1:].strip('\r\n"') + '">' + url[1:].strip('\r\n"') + '</a><br>')
searchfile.close()
dads.write('</body></html>')
dads.close()
 
