<%

DIM toEmail,fromEmail,subject,body,urlBase,urlEnc,copy,copyTo,DecodeURL
toEmail=Request.Form("to")
fromEmail=Request.Form("from")
subject=Request.Form("subject")
body=Request.Form("body")
urlEnc=Request.Form("urlEnc")
copy=Request.Form("copy")

urlBase = "https://geoaccess.co.dutchess.ny.us/bicycleparking/"

If copy Then
	copyTo = "<string>"&fromEmail&"</string>"
Else
	copyTo = "<string></string>"
End If

DecodeURL =  Replace(Replace(Replace(urlEnc,"%3F","?"),"%2B","+"),"%3D","=")

Set oXmlHTTP = CreateObject("MSXML2.ServerXMLHTTP.3.0")

oXmlHTTP.Open "POST", "http://10.42.196.242/WebServices/SendMailWebService/v2-2/SendMail2.asmx", false
oXmlHTTP.setRequestHeader "Content-Type", "application/soap+xml; charset=utf-8" 
oXmlHTTP.setRequestHeader "SOAPAction", "10.42.196.242/SendMailWebService/SendMail2/SendMail"

SOAPRequest = _
"<?xml version='1.0' encoding='utf-8'?>" &_
"<soap12:Envelope xmlns:xsi='https://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='https://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'>" &_
	"<soap12:Body>" &_
		"<SendMail xmlns='http://www.dcny.gov/SendMailWebService/SendMail2'>" &_
  			"<sender>"&fromEmail&"</sender>" &_
	    	"<recipients>" &_
	    		"<string>"&toEmail&"</string>" &_
	        	copyTo &_
	    	"</recipients>" &_
	    	"<ccRecipients>" &_
	    		"<string></string>" &_
	     		"<string></string>" &_
	    	"</ccRecipients>" &_
	    	"<bccRecipients>" &_
	        	"<string></string>" &_
	        	"<string></string>" &_
	    	"</bccRecipients>" &_
 			"<subject>"&subject&"</subject>" &_
 			"<messageBody>"&body&urlBase&DecodeURL&"</messageBody>" &_
  			"<isBodyHtml>false</isBodyHtml>" &_
  			"<priority>Normal</priority>" &_
  			"<deliveryNotificationOptions>OnFailure</deliveryNotificationOptions>" &_
	    	"<attachments></attachments>" &_
		"</SendMail>" &_
	"</soap12:Body>" &_
"</soap12:Envelope>"

oXmlHTTP.send SOAPRequest    
Response.Write oXmlHTTP.status

%>

