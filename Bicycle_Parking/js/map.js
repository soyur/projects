// Geoaccess    
var serverURL = "https://geoaccess.co.dutchess.ny.us:6443/arcgis/rest/services/BicycleParking/MapServer";
var geometryServer = "https://geoaccess.co.dutchess.ny.us:6443/arcgis/rest/services/Utilities/Geometry/GeometryServer";
document.domain = 'geoaccess.co.dutchess.ny.us'

var profileURL = "https://elevation.arcgis.com/arcgis/rest/services/Tools/ElevationSync/GPServer";

var urlMarkerCount;
var map;
var startExtent;
var xmin_m = 623754,
    ymin_m = 948369,
    xmax_m = 780730,
    ymax_m = 1188759;

var xmin = 431350.9032258064,
    ymin = 945784.1612903225,
    xmax = 865603.8064516131,
    ymax = 1186174.1612903227;
var spr = 2260; // Spatial Reference ID
var bikeParkingSymbols = {}
var markerCount = [1, 2, 3, 4, 5, 6];
var identifyFeature;
var textGraphic;
var graphicList = [];
var loadGraphic;
// REST IDs
var municipalitiesID = 0,
    bikeParkingID = 1,
    poiID = 2,
    bikeRoutesID = 3, // [4,5,6,7]
    bikeRoutesID_local = 4,
    bikeRoutesID_railtrail = 5,
    bikeRoutesID_sharrow = 6,
    bikeRoutesID_state = 7,
    roadsID = 8, // [9,10,11,12]
    roadsID_us = 9,
    roadsID_state = 10,
    roadsID_county = 11,
    waterZoomOutID = 13,
    waterZoomInID = 14,
    parksID = 15,
    roadsOutsideID = 16,
    countiesID = 17,
    hudsonRiverID = 18,
    aerialsID = 19,
    hillshadeID = 20,
    roadsLabelID = 21,
    countyLabelsID = 22,
    addressPointsID = 23,
    structuresID = 24,
    sidewalksID = 25,
    roadEdgesID = 26,
    parkingID = 27,
    golfID = 28,
    //zoningID = 29,
    parksLabelsID = 29;

var selectableLayers = [
    bikeParkingID,
    bikeRoutesID,
    poiID
];
var zoomInFactor = 0.65,
    zoomOutFactor = 1.55;

var contentWidth;
var contentHeight;
var highPointGraphic;
var watchId;
var lineSymbol, chartOptions, profileParams;
var setComplete;
var poiCat2List = [];
var queryCat2 = [];
var poiLayerDefinitions = [];
var poiClick;
var poiHover;
var panorama;
var heading,
    headingSym,
    headingGraphic,
    manSym,
    manGraphic;
var gsvPOS; //gsv cursor position
var halfHeight;
var actives;
var epWidget;

$('#bikeLockModal').modal('show')


$("form").bind("keypress", function(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
    }
});

$('.tipLinks').click(function() {
    window.open($(this).attr("href"), '_blank')

})

$('#gsv_drag').bind(
    'touchmove',
    function(e) {
        e.preventDefault();
    }
);


require(["esri/map", "esri/geometry/Extent", "esri/SpatialReference", "esri/layers/ArcGISDynamicMapServiceLayer", "esri/tasks/query", "esri/tasks/QueryTask", "dojo/query", "esri/tasks/IdentifyTask", "esri/tasks/IdentifyParameters", "esri/graphic", "esri/symbols/SimpleMarkerSymbol", "esri/symbols/TextSymbol", "esri/symbols/Font", "esri/Color", "esri/symbols/PictureMarkerSymbol", "esri/geometry/Point", "esri/dijit/Scalebar", "esri/dijit/Popup", "dojo/dom-construct", "dojo/_base/array", "esri/InfoTemplate", "dojo/dom", "esri/geometry/Polyline", "esri/tasks/GeometryService", "esri/tasks/ProjectParameters", "esri/symbols/SimpleLineSymbol", "esri/dijit/LocateButton", "esri/dijit/ElevationProfile", "esri/units", "dojo/on", "dojox/gesture/swipe", "dojox/gesture/tap", "esri/toolbars/edit", "dojo/domReady!"], function(Map, Extent, SpatialReference, ArcGISDynamicMapServiceLayer, Query, QueryTask, query, IdentifyTask, IdentifyParameters, Graphic, SimpleMarkerSymbol, TextSymbol, Font, Color, PictureMarkerSymbol, Point, Scalebar, Popup, domConstruct, arrayUtils, InfoTemplate, dom, Polyline, GeometryService, ProjectParameters, SimpleLineSymbol, LocateButton, ElevationsProfileWidget, Units, on, swipe, tap, Edit) {

        // Get URL variables
        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("+");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
            return (false);
        }

        urlMarkerCount = getQueryVariable("MarkerCount");

        $(window).width() < 768 ? initMobile() : initDesktop();

        function initMobile() {
            startExtent = new Extent(xmin_m, ymin_m, xmax_m, ymax_m, new SpatialReference({
                wkid: spr
            }))

            $(document).ready(function() {
                hideContent()
            });
        }

        function initDesktop() {
            startExtent = new Extent(xmin, ymin, xmax, ymax, new SpatialReference({
                wkid: spr
            }));
        }

        gsvc = new GeometryService(geometryServer);

        esriConfig.defaults.geometryService = gsvc;

        // Location symbol
        var outline = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([239, 236, 225]), 2);
        var locateMarker = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 20, outline, new Color([61, 85, 119, 0.8]));

        //set content height

        $(window).resize(setContentHeight)

        setContentHeight()

        function setContentHeight() {
            if (panorama) {
                console.log(panorama)
                if (panorama.visible) {
                    toggleGSVcontent(true)
                    var navH;
                    $(window).width() < 768 ? navH = 0 : navH = $('#navbar-container').height();
                    halfHeight = ($('body').height() - navH) / 2
                    toggleGSVcontent(true)
                    $('#mapContainer').animate({
                            height: halfHeight + 'px',
                        })
                        // Initiate Panorama AFTER street view container is up or else Panorama
                        // needs to be refreshed
                    $('#streetviewContainer').animate({
                        height: halfHeight + 'px',
                    }, function() {
                        google.maps.event.trigger(panorama, 'resize');
                    })
                }
            }

            contentHeight = $('body').height() - $('#navbar-container').height() - 5;
            $('#contentContainer').height(contentHeight);
            $('#contentContainer').css({
                // 'max-height': height + 'px',
                'top': $('#navbar-container').height() + 'px',
                'max-height': contentHeight + 'px'
            });
            $('.nav-tabs').css('min-width', $('.nav-tabs').css('width'))
            $('#contentToggleContainer').css('left', $('#contentContainer').css('width'))
            $('#controlsContainer').css('left', ($('#contentContainer').width() + 60) + 'px')

        }

        var popup = new Popup({
            markerSymbol: locateMarker,
            marginTop: 60,
            visibleWhenEmpty: false
        }, domConstruct.create("div"));

        popup.on('set-features', function() {
            setProfile();
            setComplete = true
        })

        popup.on('selection-change', function() {
            if (setComplete) {
                setProfile()
            }
        })

        map = new Map("mapContainer", {
            logo: false,
            slider: false,
            infoWindow: popup
        });

        map.setExtent(startExtent)

        map.on('update-start', function() {
            $('#mapLoading').show()
        })

        map.on('update-end', function() {
            $('#mapLoading').hide()
        })

        map.on('load', function() {
            function genMarkers(x, y, i) {

                var marker = new PictureMarkerSymbol();
                marker.setHeight(55);
                marker.setWidth(35);
                marker.setUrl("graphics/graphic_" + i + ".png");
                var pt = new Point(x, y, map.spatialReference)

                loadGraphic = new Graphic(pt, marker)
                map.graphics.add(loadGraphic);

            }

            if (urlMarkerCount !== false) {
                for (var i = 1; i <= urlMarkerCount; i++) {
                    var marker = "marker" + i
                    var x = getQueryVariable(marker).split('x')[1].split('y')[0]
                    var y = getQueryVariable(marker).split('y')[1]
                    genMarkers(x, y, i)
                    createTextLabel(new Point(x, y, map.spatialReference), marker)
                }
            }
        })

        // Marker drag
        var move = new Edit(map)

        // scalebar
        scalebar = new Scalebar({
            map: map,
            // attachTo: "bottom-left",
            scalebarStyle: "line"
        }, dom.byId("scalebarContainer"));

        // geolocation
        locate = new LocateButton({
            map: map,
            scale: map.getMinScale(),
        }, dom.byId("zoomLoc"))
        locate.startup();

        /*=========================================
        =            Add layers to map            =
        =========================================*/

        aerialsLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        aerialsLayer.setVisibleLayers([aerialsID]);

        hillshadeLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        hillshadeLayer.setVisibleLayers([hillshadeID]);
        hillshadeLayer.setOpacity(0.4);
        map.addLayer(hillshadeLayer);

        countiesLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        countiesLayer.setVisibleLayers([countiesID]);
        map.addLayer(countiesLayer);

        hudsonRiverLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        hudsonRiverLayer.setVisibleLayers([hudsonRiverID]);
        map.addLayer(hudsonRiverLayer);

        parksLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        parksLayer.setVisibleLayers([parksID]);
        parksLayer.setOpacity(0.5);
        map.addLayer(parksLayer);

        golfLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        golfLayer.setVisibleLayers([golfID]);
        golfLayer.setOpacity(0.5);
        map.addLayer(golfLayer);

        waterZoomInLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        waterZoomInLayer.setVisibleLayers([waterZoomInID]);
        waterZoomInLayer.setOpacity(0.6);
        map.addLayer(waterZoomInLayer);

        waterZoomOutLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        waterZoomOutLayer.setVisibleLayers([waterZoomOutID]);
        map.addLayer(waterZoomOutLayer);

        parkingLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        parkingLayer.setVisibleLayers([parkingID]);
        map.addLayer(parkingLayer);

        roadEdgesLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        roadEdgesLayer.setVisibleLayers([roadEdgesID]);
        map.addLayer(roadEdgesLayer);

        sidewalksLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        sidewalksLayer.setVisibleLayers([sidewalksID]);
        map.addLayer(sidewalksLayer);

        roadsLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        roadsLayer.setVisibleLayers([roadsID]);
        roadsLayer.setOpacity(0.6);
        map.addLayer(roadsLayer);

        bikeRoutesLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        bikeRoutesLayer.setVisibleLayers([bikeRoutesID]);
        map.addLayer(bikeRoutesLayer);

        roadsOutsideLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        roadsOutsideLayer.setVisibleLayers([roadsOutsideID]);
        map.addLayer(roadsOutsideLayer);

        structuresLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        structuresLayer.setVisibleLayers([structuresID]);
        map.addLayer(structuresLayer);

        parksLabelsLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        parksLabelsLayer.setVisibleLayers([parksLabelsID]);
        map.addLayer(parksLabelsLayer);

        countyLabelsLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        countyLabelsLayer.setVisibleLayers([countyLabelsID]);
        map.addLayer(countyLabelsLayer);

        municipalitiesLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        municipalitiesLayer.setVisibleLayers([municipalitiesID]);
        municipalitiesLayer.setOpacity(0.5);
        map.addLayer(municipalitiesLayer);

        roadsLabelLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        roadsLabelLayer.setVisibleLayers([roadsLabelID]);
        map.addLayer(roadsLabelLayer);

        poiLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        poiLayer.setVisibleLayers([poiID]);
        poiLayerDefinitions[poiID] = "Cat2 = ''";
        poiLayer.setLayerDefinitions(poiLayerDefinitions);
        map.addLayer(poiLayer);

        bikeParkingLayer = new ArcGISDynamicMapServiceLayer(serverURL);
        bikeParkingLayer.setVisibleLayers([bikeParkingID]);
        map.addLayer(bikeParkingLayer);


        map.on('layer-add-result', function(layer) {
            if (layer.layer === aerialsLayer) {
                map.removeLayer(hillshadeLayer)
                    // map.removeLayer(zoningLayer)
                map.removeLayer(hudsonRiverLayer)
                map.removeLayer(waterZoomOutLayer)
                map.removeLayer(waterZoomInLayer)
                map.removeLayer(roadsLayer)
                map.removeLayer(parkingLayer)
                map.removeLayer(roadEdgesLayer)
                map.removeLayer(structuresLayer)
                map.removeLayer(sidewalksLayer)
            } else if (layer.layer === hillshadeLayer) {
                map.removeLayer(aerialsLayer)
                    // map.addLayer(zoningLayer, 1)
                map.addLayer(hudsonRiverLayer, 2)
                map.reorderLayer(parksLayer, 3)
                map.reorderLayer(golfLayer, 4)
                map.addLayer(waterZoomInLayer, 5)
                map.addLayer(waterZoomOutLayer, 6)
                map.addLayer(parkingLayer, 7)
                map.addLayer(roadEdgesLayer, 8)
                map.addLayer(sidewalksLayer, 9)
                map.addLayer(roadsLayer, 10)
                map.addLayer(structuresLayer, 11)
            }
        })

        /*=====  End of Add Layers to map  ======*/


        // Zoom controls
        $('#zoomIn').click(function() {
            map.centerAndZoom(map.extent.getCenter(), zoomInFactor)
        })

        // --->>> Zoom out <<<--- //
        $('#zoomOut').click(function() {
            map.centerAndZoom(map.extent.getCenter(), zoomOutFactor)
        })

        // --->>> Zoom to extent <<<--- // 
        $('#zoomToExtent').click(function() {
            map.setExtent(startExtent)
        })

        // Tap controls

        on(dom.byId('mobileMenu'), tap, function() {
            // identifyFeature.pause();
            $('#navbar-collapse-menu').collapse('toggle');
        });

        on(dom.byId('contentToggle'), tap, function() {
            $('#contentToggle').children('span').hasClass('glyphicon-chevron-left') ? hideContent() : showContent();
            $('#navbar-collapse-menu').hasClass('in') ? $('#navbar-collapse-menu').collapse('hide') : {};
        });

        on(query(".visibleButton"), tap, function() {
            var id = $(this)[0].id
            toggleLegend(id)
        });

        on(dom.byId('legendTabButton'), tap, function() {
            // identifyFeature.resume();
            $('#legendTabButton').tab('show')
            if ($(window).width() >= 768) {
                expandTab()
            }
        });

        on(dom.byId('contactTabButton'), tap, function() {
            // identifyFeature.pause();
            $('#contactTabButton').tab('show')
            if ($(window).width() >= 768) {
                expandTab()
            }
        });

        on(dom.byId('lockTabButton'), tap, function() {
            // identifyFeature.resume();
            $('#lockTabButton').tab('show')
            if ($(window).width() >= 768) {
                expandTabLG()
            }
        });

        on(dom.byId('closeTab'), tap, function() {
            hideContent()
        })

        on(query(".navbar-toggle"), tap, function() {
            $('#contentContainer').width() > 100 ? hideContent() : {};
        });


        on(dom.byId('poiToggleOff'), tap, function(event) {
            poiAll(false);
        });

        on(dom.byId('poiToggleOn'), tap, function() {
            poiAll(true)
        });

        // Aerial Toggle Control

        $('#aerialToggle').click(function() {
            var vis = false;
            var layers = map.getLayersVisibleAtScale(map.getScale());

            $.each(layers, function(i, item) {
                if (aerialsLayer.id === item.id) {
                    vis = true;
                }
            });

            vis ? map.addLayer(hillshadeLayer, 0) : map.addLayer(aerialsLayer, 0);
        })


        // Build legend
        $(document).ready(getLayerLegends())

        function getLayerLegends() {
            $.ajax({
                    url: serverURL + "/legend?f=pjson",
                    dataType: "jsonp",
                    success: function(data) {

                            for (var i = 0; i < data.layers.length; i++) {
                                var layerID = data.layers[i].layerId
                                var layerName = data.layers[i].layerName;
                                var legend = data.layers[i].legend

                                if (layerID === bikeParkingID) {
                                    for (var c = 0; c < legend.length; c++) {

                                        var image = 'data:' + legend[c].contentType + ';base64,' + legend[c].imageData;
                                        $('#bikeParkingLegend').append('<div class="legendItem"><span><img src="' + image + '"></span>' + ' ' + legend[c].label + ' ' + '</div>');
                                        bikeParkingSymbols[legend[c].label] = image
                                    } // end for each symbol of layer  
                                } else if (layerID > bikeRoutesID && layerID < bikeRoutesID + 5) {
                                    for (var c = 0; c < legend.length; c++) {
                                        var image = 'data:' + legend[c].contentType + ';base64,' + legend[c].imageData;
                                        $('#bikeRoutesLegend').append('<div class="legendItem"><span><img src="' + image + '"></span>' + ' ' + legend[c].label + ' ' + '</div>')
                                    } // end for each symbol of layer  
                                } else if (layerID > roadsID && layerID < roadsID + 5) {
                                    for (var c = 0; c < legend.length; c++) {
                                        var image = 'data:' + legend[c].contentType + ';base64,' + legend[c].imageData;
                                        $('#roadsLegend').append('<div class="legendItem"><img height="30px" width="30px" style="position:relative;left:21px;opacity:0.4;"  src="graphics/street_' + layerID + '.png"><img style="position:relative;left:-4px;" src="' + image + '"></span>' + ' ' + legend[c].label + ' ' + '</div>')
                                    } // end for each symbol of layer  
                                } else if (layerID === poiID) {
                                    for (var c = 0; c < legend.length; c++) {
                                        var image = 'data:' + legend[c].contentType + ';base64,' + legend[c].imageData;
                                        $('#poiLegend').append('<div class="legendItem"><table><tr><td><span><img src="' + image + '"></span></td><td style="padding-left:4px;">' + ' ' + legend[c].label + ' ' + '</td></tr></table></div>');
                                        poiCat2List.push(legend[c].label);

                                    } // end for each symbol of layer 
                                }
                            }
                        } // end success
                }) // end ajax request
        }; // end getLayerLegends()

        // layer visibility control
        function toggleLegend(sel) {
            lid = '#' + sel
            $(lid).toggleClass('hiddenButton');
            var layer = window[$(lid).attr('id').replace('Toggle', 'Layer')];
            var id = window[$(lid).attr('id').replace('Toggle', 'ID')];
            if ($(lid).hasClass('hiddenButton')) {
                $(lid).children('span').switchClass('glyphicon-eye-open', 'glyphicon-eye-close');
                $(lid + '> .visibleText').text('Hidden')

                hideLayer(layer, id);
                if (sel === 'poiToggle') {
                    disablePOI();
                }

            } else {
                $(lid).children('span').switchClass('glyphicon-eye-close', 'glyphicon-eye-open')
                $(lid + '> .visibleText').text('Visible')

                showLayer(layer, id);
                if (sel === 'poiToggle') {
                    enablePOI();
                }
            }
        }

        function hideLayer(layer, id) {
            layer.hide();
            if (id !== roadsID) {
                var ind = selectableLayers.indexOf(id);
                selectableLayers.splice(ind, 1);
            } else {
                roadsLabelLayer.hide();
            }

        }

        // To Preserve order of identify 
        function showLayer(layer, id) {
            layer.show()
            if (id !== roadsID) {
                if (id === bikeParkingID) {
                    selectableLayers.unshift(id)
                } else if (id === bikeRoutesID) {
                    var ind = selectableLayers.indexOf(municipalitiesID);
                    selectableLayers.splice(ind, 0, id)
                } else {
                    selectableLayers.unshift(id)
                }
            } else {
                roadsLabelLayer.show();
            }

        }

        // POI Legend

        $(window).on('load', function() {
            initPOI()
        })

        function initPOI() {


            on(query('.legendItem', 'poiLegend'), tap, function() {

                if ($(this).hasClass('nonActivePOI')) {
                    $(this).removeClass('nonActivePOI')
                }
                if ($(this).hasClass('activePOI')) {
                    $(this).removeClass('activePOI');
                    poiToggle($(this), true)
                } else {
                    $(this).addClass('activePOI');
                    poiToggle($(this), false)
                }
            })
        }

        $('#poiToggleOn').hover(function() {
            $('#poiLegend .legendItem').not('.activePOI').addClass('nonActivePOI');
        }, function() {
            $('#poiLegend .legendItem').not('.activePOI').removeClass('nonActivePOI')
        })

        $('#poiToggleOff').mouseover(function() {
            actives = $('.activePOI')
            if ($('#poiLegend .legendItem').hasClass('nonActivePOI')) {
                $('#poiLegend .legendItem').removeClass('nonActivePOI')
            }
            $('#poiLegend .legendItem').removeClass('activePOI')
        })

        $('#poiToggleOff').mouseout(function(event) {
            if (queryCat2.length !== 0) {
                actives.addClass('activePOI')
            }
        })

        function poiToggle(poi, remove) {
            var cat2 = poi.text().trim();
            if (remove) {
                var indexToRemove;
                $.each(queryCat2, function(i, item) {
                    var ind = item.indexOf(cat2)
                    if (ind > 0) {
                        indexToRemove = i
                    }
                })

                if (indexToRemove === 0 && queryCat2.length > 1) {
                    queryCat2[1] = queryCat2[1].replace('OR ', '')
                }
                queryCat2.splice(indexToRemove, 1)

            } else {
                if (queryCat2.length === 0) {
                    queryCat2.push("Cat2 = '" + cat2 + "'")
                } else {
                    queryCat2.push("OR Cat2 = '" + cat2 + "'")
                }
            }

            if (queryCat2.length === 0) {
                setLayerDef("Cat2 = ''")
            } else {
                setLayerDef(queryCat2.join(" "))
            }
        }

        function setLayerDef(ld) {
            poiLayerDefinitions[poiID] = ld;
            poiLayer.setLayerDefinitions(poiLayerDefinitions);
            poiLayer.refresh();
        }

        function poiAll(on) {
            if (on) {
                genAllCat2LayerDefinitions()
                setLayerDef(queryCat2.join(" "));
                $('#poiLegend .legendItem').addClass('activePOI')
                if ($(this).hasClass('nonActivePOI')) {
                    $(this).removeClass('nonActivePOI')
                }

            } else {
                setLayerDef("Cat2 = ''")
                queryCat2 = [];
                $('#poiLegend .legendItem').removeClass('activePOI');
                if ($(this).hasClass('nonActivePOI')) {
                    $(this).removeClass('nonActivePOI')
                }
            }
        }

        function genAllCat2LayerDefinitions() {
            queryCat2 = [];
            for (var i = 0; i < poiCat2List.length; i++) {
                if (i === 0) {
                    queryCat2.push("Cat2 = '" + poiCat2List[i] + "'")
                } else {
                    queryCat2.push(" OR Cat2 = '" + poiCat2List[i] + "'")
                }
            }
        }

        /*=======================================
        =            Content Control            =
        =======================================*/

        function expandTab() {
            $('#contentContainer').animate({
                width: contentWidth
            }, 350);

            $('#contentToggleContainer').animate({
                left: contentWidth
            }, 350);

            $('#controlsContainer').animate({
                left: contentWidth + 40
            }, 350);
        }

        function expandTabLG() {
            $('#contentContainer').animate({
                width: contentWidth + 200
            }, 350);

            $('#contentToggleContainer').animate({
                left: contentWidth + 200
            }, 350);

            $('#controlsContainer').animate({
                left: contentWidth + 240
            }, 350);
        }

        $('#contentContainer').ready(setContent)

        function setContent() {
            contentWidth = $('#contentContainer').width() + 20;
            $('#contentToggleContainer').css('left', contentWidth)
            $('#controlsContainer').css('left', contentWidth + 40)
            $('.panel,#contactTab').css('min-width', $('.panel').width() + 'px');
        }

        function hideContent() {
            $('#contentToggle').children('span').switchClass('glyphicon-chevron-left', 'glyphicon-chevron-right');
            $('.panel').fadeOut(350);
            $('#contentContainer').animate({
                width: 0
            }, 350, function() {
                $('#contentContainer').toggle()
            });
            $('#navbar-container .navbar-header  img').animate({
                height: 66
            }, 800);

            $('#contentToggleContainer').animate({
                left: 0
            }, 350);

            $('#controlsContainer').animate({
                left: 40
            }, 350);
        }

        function showContent() {
            $('#contentToggle').children('span').switchClass('glyphicon-chevron-right', 'glyphicon-chevron-left')
            $('.panel').fadeIn(350);
            $('#contentContainer').toggle()
            $('#navbar-container .navbar-header  img').animate({
                height: 50
            }, 200);


            if ($('#legendTabButton,#feedbackTabButton,#contactTabButton').parent('li').hasClass('active')) {
                expandTab();
            } else {
                if ($(window).width() >= 768) {
                    expandTabLG();
                } else {
                    expandTab();
                }
            }
        }

        /*=====  End of Content Control  ======*/


        /*===========================================
        =            Generate Route List            =
        ===========================================*/

        map.on('load', function() {
            getStateRoutes()
        })

        function getStateRoutes() {
            var queryTask = new QueryTask(serverURL + "/" + bikeRoutesID_state);
            var query = new Query();
            query.outFields = ["OBJECTID", "Route_Name"];
            query.where = "OBJECTID > -1"
            query.returnGeometry = false;
            queryTask.execute(query, function(result) {
                    for (var i = 0; i < result.features.length; i++) {
                        var name = result.features[i].attributes.Route_Name;
                        var objID = result.features[i].attributes.OBJECTID;
                        var geometry = result.features[i].geometry;
                        $('#StateBikeRoutes').append('<li><a class="bike-route" data-id="7" data-oid="' + objID + '">' + name + '</a></li>')
                    } // end feature loop
                    getLocalRoutes()
                }) // End query execute
        }

        function getLocalRoutes() {
            var queryTask = new QueryTask(serverURL + "/" + bikeRoutesID_local);
            var query = new Query();
            query.outFields = ["OBJECTID", "Route_Name"];
            query.where = "OBJECTID > -1"
            query.returnGeometry = false;
            queryTask.execute(query, function(result) {
                    for (var i = 0; i < result.features.length; i++) {
                        var name = result.features[i].attributes.Route_Name;
                        var objID = result.features[i].attributes.OBJECTID;
                        var geometry = result.features[i].geometry;
                        $('#LocalBikeRoutes').append('<li><a class="bike-route" data-id="4" data-oid="' + objID + '">' + name + '</a></li>')
                    } // end feature loop
                    getRailTrails()
                }) // End query execute
        }

        function getRailTrails() {
            var queryTask = new QueryTask(serverURL + "/" + bikeRoutesID_railtrail);
            var query = new Query();
            query.outFields = ["OBJECTID", "NAME"];
            query.where = "OBJECTID > -1"
            query.returnGeometry = false;
            queryTask.execute(query, function(result) {
                    for (var i = 0; i < result.features.length; i++) {
                        var name = result.features[i].attributes.NAME;
                        var objID = result.features[i].attributes.OBJECTID;
                        var geometry = result.features[i].geometry;
                        $('#RailTrails').append('<li><a class="bike-route" data-id="5" data-oid="' + objID + '">' + name + '</a></li>')
                    } // end feature loop
                    getSharrows()
                }) // End query execute
        }

        function getSharrows() {
            var queryTask = new QueryTask(serverURL + "/" + bikeRoutesID_sharrow);
            var query = new Query();
            query.outFields = ["OBJECTID", "STREET_NAM"];
            query.where = "OBJECTID > -1"
            query.returnGeometry = false;
            queryTask.execute(query, function(result) {
                    for (var i = 0; i < result.features.length; i++) {
                        var name = result.features[i].attributes.STREET_NAM;
                        var objID = result.features[i].attributes.OBJECTID;
                        var geometry = result.features[i].geometry;
                        $('#Sharrows').append('<li><a class="bike-route" data-id="6" data-oid="' + objID + '">' + name + '</a></li>')
                    } // end feature loop
                    $('.bike-route').click(function() {
                        getRouteGeometry($(this).attr('data-oid'), $(this).attr('data-id'));
                        $('#navbar-collapse-menu').collapse('hide');
                    })
                    initProfile()
                    $('.bike-routes-list li').hover(function() {
                        $(this).css('cursor', 'pointer');
                    })
                }) // End query execute
        }

        function getRouteGeometry(oid, id) {
            var queryTask = new QueryTask(serverURL + "/" + id);
            var query = new Query();
            query.returnGeometry = true;
            query.outFields = ["OBJECTID"];
            query.where = "OBJECTID = " + oid
            queryTask.execute(query, function(result) {
                //project(result.features[0].geometry);
                zoomToRoute(result.features[0].geometry.paths[0], id);

            });
        } // End zoomToCountyRoad(sr)

        function zoomToRoute(geom, id) {
            road = new Polyline(new SpatialReference({
                wkid: spr
            }));
            road.addPath(geom)
            map.setExtent(road.getExtent());
            var midPointID = Math.round(road.paths[0].length / 2)
            var mp = new Point(road.getPoint(0, midPointID))
            setTimeout(function() {
                executeIdentifyTask(mp, id);
            }, 1500)

        }

        /*=====  End of Generate Route List  ======*/


        /*=========================================
        =            Elevation Profile            =
        =========================================*/

        function initProfile() {
            ems = PictureMarkerSymbol('graphics/gsv_bike_v2_icon.png', 39, 37)
            chartOptions = {
                titleFontColor: "#333",
                axisFontColor: "#333",
                sourceTextColor: "#333",
                skyTopColor: "white",
                skyBottomColor: "white",
                elevationLineColor: "#273b5f",
                elevationTopColor: "#5c7095",
                elevationBottomColor: "#5c7095",
                indicatorFillColor: 'white',
                indicatorFontColor: '#333',
                mapIndicatorSymbol: ems,
            };

            profileParams = {
                map: map,
                chartOptions: chartOptions,
                profileTaskUrl: profileURL,
                scalebarUnits: Units.MILES
            };

            epWidget = new ElevationsProfileWidget(profileParams, dom.byId('profileContainer'));
            epWidget.startup();
            epWidget.on('load', function() {
                $('#profileContainer').css('visibility', 'visible').hide();
            })
        }

        $(document).on('click', '.profileOpen', function() {
            showProfile();
        });

        function showProfile() {
            if (panorama) {
                hideGSV()
            }
            if ($('#profileContainer').is(':hidden') || $('#profileContainer').width() === 0) {
                var h = contentHeight - $('#profileContainer').height()
                var w = $('#profileContainer').width()
                $('#contentContainer').animate({
                    height: h
                })
                $('#contentToggleContainer,#controlsContainer,#scalebarContainer').animate({
                    bottom: '+=220px'
                }).promise().then(function() {

                    $('#profileContainer').width(0).show().animate({
                        width: w,
                    }, 900, "linear", function() {
                        closeProfile = on(dom.byId('profileCloseContainer'), 'click', hideProfile)
                    })
                })
            }
        }

        function hideProfile() {
            $('#profileContainer').hide()
            $('#contentToggleContainer,#controlsContainer,#scalebarContainer').animate({
                bottom: '-=220px'
            })
            $('#contentContainer').animate({
                height: contentHeight
            })
            closeProfile.remove()
            epWidget.clearProfile();
        }

        function setProfile() {
            if (popup.features) {
                if (popup.features[popup.selectedIndex].geometry.type === 'polyline') {
                    epWidget.clearProfile()
                    epWidget.set("profileGeometry", popup.features[popup.selectedIndex].geometry);
                }
            }
        }

        /*=====  End of Elevation Profile  ======*/


        /*======================================
        =            Identify Layer            =
        ======================================*/

        identifyFeature = on.pausable(map, "click", executeIdentifyTask);

        function executeIdentifyTask(event, id) {
            setComplete = false
            var firstSelected = true

            identifyTask = new IdentifyTask(serverURL);
            identifyParams = new IdentifyParameters();

            identifyParams.tolerance = 8;
            identifyParams.returnGeometry = true;
            identifyParams.layerDefinitions = []
                // identifyParams.maxAllowableOffset = 1;
            identifyParams.layerIds = event.mapPoint ? selectableLayers : [id];
            identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
            identifyParams.layerDefinitions[poiID] = poiLayerDefinitions[poiID];
            identifyParams.mapExtent = map.extent;
            identifyParams.geometry = event.mapPoint ? event.mapPoint : event;

            var deferred = identifyTask
                .execute(identifyParams)
                .addCallback(function(response) {
                    var xc = identifyParams.geometry.x
                    var yc = identifyParams.geometry.y

                    var orderedResponse = response.sort(function(a, b) {
                        return Math.abs(xc - a.feature.geometry.x) - Math.abs(xc - b.feature.geometry.x)
                    })

                    // response is an array of identify result objects
                    // Let's return an array of features.
                    return arrayUtils.map(orderedResponse, function(result) {
                        var feature = result.feature;
                        var layerName = result.layerName;
                        feature.attributes.layerName = layerName;
                        if (firstSelected && result.geometryType === 'esriGeometryPoint') {
                            highlightPoint(result.feature.geometry)
                            firstSelected = false
                        }

                        if (layerName === 'BikeParking') {
                            var useLink, useAddInfo;
                            if (feature.attributes.Links === 'Null') {
                                useLink = "</table>";
                            } else {
                                useLink = "<tr><th>Link</th><td><a href='${Links}' target='_blank'>More info</a></td></tr></table>";
                            }
                            if (feature.attributes.Add__Info === 'Null') {
                                useAddInfo = "";
                            } else {
                                useAddInfo = "<tr><th>Info</th><td>${Add__Info}</td></tr>"
                            }

                            var bikeParkingTemplate = new InfoTemplate("Bike Parking",
                                "<table class='table table-striped table-condensed popupTable'><tr><td colspan='2'><img src='photos/${ImageLinkLocal}' style='width:100%;height:auto; '></td></tr>" +
                                "<tr><th>Rack Type</th><td>${RackType}</td></tr>" +
                                "<tr><th>Spaces</th><td>${Spaces}</td></tr>" +
                                "<tr><th>Condition</th><td>${Condition}</td></tr>" +
                                "<tr><th>Location</th><td>${LOCATION}</td></tr>" +
                                "<tr><th>Address</th><td>${Address}</td></tr>" +
                                "<tr><th>Municipality</th><td>${Municipality}</td></tr>" +
                                "<tr><th>Notes</th><td>${NOTES}</td></tr>" +
                                useAddInfo +
                                useLink)
                            feature.setInfoTemplate(bikeParkingTemplate);
                        } else if (layerName === 'StateBikeRoutes') {
                            var miles = Number(feature.attributes.Length_Miles).toFixed(2)
                            var profileID = layerName + "ProfileContainer"
                            var stateBikeRoutesTemplate = new InfoTemplate("State Bike Route",
                                "<table class='table table-striped table-condensed popupTable'>" +
                                "<tr><th>Route Name</th><td>${Route_Name}</td></tr>" +
                                "<tr><th>Description</th><td>${Route_Desc}</td></tr>" +
                                "<tr><th>Use</th><td>${Route_Use}</td></tr>" +
                                "<tr><th>Surface</th><td>${Surface}</td></tr>" +
                                "<tr><th>Route Type</th><td>${Route_Type}</td></tr>" +
                                "<tr><th>Length (Miles)</th><td>" + miles + "</td></tr>" +
                                "<tr><td colspan='2' style='text-align:center;'><b >Elevation Profile</b><br><div class='profileOpen' ><img src='graphics/profile_state_${OBJECTID}.PNG'></div></td></tr></table>");
                            feature.setInfoTemplate(stateBikeRoutesTemplate);
                        } else if (layerName === 'LocalBikeRoutes') {
                            if ($('#profileContainer').is(':visible')) {
                                hideProfile()
                            }

                            var miles = Number(feature.attributes.Length_Miles).toFixed(2)
                            var profileID = layerName + "ProfileContainer"
                            var localBikeRoutesTemplate = new InfoTemplate("Local Bike Route",
                                "<table class='table table-striped table-condensed popupTable'>" +
                                "<tr><th>Municipality</th><td>${Municipality}</td></tr>" +
                                "<tr><th>Route Name</th><td>${Route_Name}</td></tr>" +
                                "<tr><th>Route Type</th><td>${Route_Type}</td></tr>" +
                                "<tr><th>Length (Miles)</th><td>" + miles + "</td></tr></table>");
                            feature.setInfoTemplate(localBikeRoutesTemplate);
                        } else if (layerName === 'RailTrails') {
                            var miles = Number(feature.attributes.Length_Miles).toFixed(2)
                            var name = result.feature.attributes.NAME
                            var prof;
                            name === 'Walkway Over the Hudson' ? prof = "</table>" : prof = "<tr><td colspan='2' style='text-align:center;'><b >Elevation Profile</b><br><div class='profileOpen' ><img src='graphics/profile_rt_${OBJECTID}.PNG'></div></td></tr></table>"

                            var RailTrailBikeRoutesTemplate = new InfoTemplate("Rail Trail",
                                "<table class='table table-striped table-condensed popupTable'>" +
                                "<tr><th>Trail Name</th><td>${NAME}</td></tr>" +
                                "<tr><th>Surface</th><td>${SURFACE}</td></tr>" +
                                "<tr><th>Length (Miles)</th><td>" + miles + "</td></tr>" +
                                prof);

                            feature.setInfoTemplate(RailTrailBikeRoutesTemplate);
                        } else if (layerName === 'Sharrows') {
                            if ($('#profileContainer').is(':visible')) {
                                hideProfile()
                            }
                            var miles = Number(feature.attributes.Length_Miles).toFixed(2)
                            var profileID = layerName + "ProfileContainer"
                            var localBikeRoutesTemplate = new InfoTemplate("Sharrow",
                                "<table class='table table-striped table-condensed popupTable'>" +
                                "<tr><th>Street</th><td>${STREET_NAM}</td></tr>" +
                                "<tr><th>Municipality</th><td>${municipality}</td></tr>" +
                                "<tr><th>Length (Miles)</th><td>" + miles + "</td></tr></table>");
                            feature.setInfoTemplate(localBikeRoutesTemplate);
                        } else if (layerName === 'POIzoomOut') {
                            var poiTemplate = new InfoTemplate("Points of Interest",
                                "<table class='table table-striped table-condensed popupTable'>" +
                                "<tr><th>Name</th><td>${Feature_Name}</td></tr>" +
                                "<tr><th>Address</th><td>${Feature_Address}</td></tr>" +
                                "<tr><th>Latitude</th><td>${Latitude}</td></tr>" +
                                "<tr><th>Longitude</th><td>${Longitude}</td></tr>" +
                                "<tr><th>Link</th><td><a href='${WebLink}' target='_blank'>More info</a></td></tr></table>");
                            feature.setInfoTemplate(poiTemplate);
                        } else if (layerName === 'Municipalities') {
                            var poiTemplate = new InfoTemplate("Municipalities",
                                "<table class='table table-striped table-condensed popupTable'>" +
                                "<tr><th>Name</th><td>${NAME}</td></tr>" +
                                "<tr><th>Link</th><td><a href='${WEBLINK}' target='_blank'>More info</a></td></tr></table>");
                            feature.setInfoTemplate(poiTemplate);
                        } else if (layerName === 'POI') {
                            var poiTemplate = new InfoTemplate("${Cat2}",
                                "<table class='table table-striped table-condensed popupTable'>" +
                                "<tr><th>Name</th><td>${Feature_Name}</td></tr>" +
                                "<tr><th>Address</th><td>${Feature_Address}</td></tr>" +
                                "<tr><th>Link</th><td><a href='${WebLink}' target='_blank'>More info</a></td></tr></table>");
                            feature.setInfoTemplate(poiTemplate);
                        } else {
                            var infoTemplate = new InfoTemplate();
                            feature.setInfoTemplate(infoTemplate);
                        }
                        return feature;
                    });
                });

            popup.setFeatures([deferred]);
            popup.show(event.mapPoint ? event.mapPoint : event)
            if ($(window).width() < 768) {
                popup.maximize();
            }

            // Highlight point feature
            $('.titleButton.prev,.titleButton.next').click(function() {
                if (popup.features[popup.selectedIndex].geometry.type === 'point') {
                    highlightPoint(popup.getSelectedFeature().geometry)
                } else {
                    highPointGraphic.hide()
                }
            })

            $('.titleButton.close').click(function() {
                highPointGraphic.hide()
            })

        } // end executeIdentifyTask

        /*=====  End of Identify Layer  ======*/



        /*==============================
        =            Search            =
        ==============================*/

        function highlightPoint(geometry) {
            if (highPointGraphic) {
                highPointGraphic.hide()
            }

            var highPoint = new Point(geometry, new SpatialReference({
                wkid: spr
            }))

            var highPointMarker = new SimpleMarkerSymbol();
            highPointMarker.setColor(new Color([0, 255, 255, 1]));
            highPointMarker.setSize(7, 7);
            highPointGraphic = new Graphic(highPoint, highPointMarker);
            map.graphics.add(highPointGraphic);
        }

        function searchString(request, response) {
            var addressList = [];
            var poiList = [];
            searchPOI();

            function searchPOI() {
                $.ajax({
                    url: serverURL + "/" + poiID + "/query",
                    dataType: "jsonp", // with this dataType, cache is set to false by default
                    data: {
                        where: "Feature_Name LIKE '%" + request.term.replace(/\'/g, '\'\'') + "%'", //makes single quotes into double for sql  
                        outFields: "Feature_Name,Feature_Address",
                        returnGeometry: false,
                        f: "pjson"
                    },
                    success: function(data) {
                        $('#autocomplete').removeClass('ui-autocomplete-loading');
                        if (data.features) {
                            for (var i = 0; i < data.features.length; i++) {
                                var ind = [data.features[i].attributes.Feature_Name, data.features[i].attributes.Feature_Address, poiID]
                                if (poiList.join().indexOf(ind.join().toString()) === -1) {
                                    poiList.push(ind)
                                }
                            }

                            poiList = poiList.slice(0, 50) // only display first 100
                                // sorted alphabetically
                            poiList.sort(function(a, b) {
                                var indA, indB;
                                var search = request.term.replace(/\'/g, '\'\'')
                                if (search.match(/\d+/g) != null) {
                                    indA = a[1].indexOf(search);
                                    indB = b[1].indexOf(search);
                                } else {
                                    var newA = a[1].replace(/\d/g, "");
                                    indA = newA.indexOf(search.toUpperCase());
                                    var newB = b[1].replace(/\d/g, "");
                                    indB = newB.indexOf(search.toUpperCase());
                                }
                                return indA - indB
                            })
                        }
                    },
                    complete: searchAddresses
                })
            } // End searchPOI()

            function searchAddresses() {
                $.ajax({
                    url: serverURL + "/" + addressPointsID + "/query",
                    dataType: "jsonp", // with this dataType, cache is set to false by default
                    data: {
                        where: "FULLADDRESS LIKE '%" + request.term.replace(/\'/g, '\'\'') + "%'", //makes single quotes into double for sql  
                        outFields: "FULLADDRESS, MUNICIPALITY",
                        returnGeometry: false,
                        f: "pjson"
                    },
                    success: function(data) {
                        $('#autocomplete').removeClass('ui-autocomplete-loading');
                        if (data.features) {
                            for (var i = 0; i < data.features.length; i++) {
                                var ind = [data.features[i].attributes.FULLADDRESS, data.features[i].attributes.MUNICIPALITY, addressPointsID]
                                if (addressList.join().indexOf(ind.join().toString()) === -1) {
                                    addressList.push(ind)
                                }
                            }
                            addressList = addressList.slice(0, 50) // only display first 100
                                // sorted alphabetically
                            addressList.sort(function(a, b) {
                                var indA, indB;
                                var search = request.term.replace(/\'/g, '\'\'')
                                if (search.match(/\d+/g) != null) {
                                    indA = a[1].indexOf(search);
                                    indB = b[1].indexOf(search);
                                } else {
                                    var newA = a[1].replace(/\d/g, "");
                                    indA = newA.indexOf(search.toUpperCase());
                                    var newB = b[1].replace(/\d/g, "");
                                    indB = newB.indexOf(search.toUpperCase());
                                }
                                return indA - indB
                            })
                        }
                    },
                    complete: endSearch
                })
            } // End searchAddresses()

            function endSearch(request, status) {
                var searchList = poiList.concat(addressList);
                response($.map(searchList, function(item) {
                    return {
                        label: "<span>" + item[0] + "</span>" + "<br>" + "<span class='address'>" + item[1] + "</span>",
                        value: [item[0], item[1], item[2]]
                    }
                }))
            }

        } // End searchString searchString

        $('#autocomplete').autocomplete({
            source: searchString,
            minLength: 1, /// Minimum search value
            focus: function(event, ui) {
                var focus = event.type
                getSearchLocation(focus, ui) // Results focus event
            },
            select: function(event, ui) { // Results select event
                $('#navbar-collapse-menu').collapse('hide');
                getSearchLocation(event, ui);

            },
            change: function(event, ui) { // Results change event
                map.graphics.clear();
            },
            close: function(event, ui) {
                map.graphics.clear();
            },
            error: function(error) { // error log
                console.log(error)
            },

        });

        // Highlight search string in search results and apply style
        $.ui.autocomplete.prototype._renderItem = function(ul, item) {
            item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append(item.label)
                .appendTo(ul);
        };

        function getSearchLocation(event, ui) {
            var query = new Query();
            query.returnGeometry = true;
            var ID = ui.item.value[2]
            if (ID === addressPointsID) {
                var sql = "FULLADDRESS = '" + ui.item.value[0] + "' AND MUNICIPALITY = '" + ui.item.value[1] + "'";
                var queryTask = new QueryTask(serverURL + "/" + ID);
            } else {
                var sql = "Feature_Name = '" + ui.item.value[0] + "' AND Feature_Address = '" + ui.item.value[1] + "'";
                var queryTask = new QueryTask(serverURL + "/" + ID);
            }

            query.where = sql;
            queryTask.execute(query, function(result) {
                map.graphics.clear()
                var point = new Point(result.features[0].geometry, new SpatialReference({
                    wkid: spr
                }))

                var addressMarker = new PictureMarkerSymbol('./graphics/pinAPF.png', 35, 43);
                var searchGraphic = new Graphic(point, addressMarker);
                map.graphics.add(searchGraphic);
                if (event !== 'autocompletefocus') {
                    zoomToPoint(point)
                }
            });
        }

        function zoomToPoint(point) {
            var dY = 800 // North/South Extent in feet
            var widthR = map.extent.xmax - map.extent.xmin
            var heightR = map.extent.ymax - map.extent.ymin
            var ratio = widthR / heightR
            var dX = dY * ratio
            var yminS = point.y - 400
            var ymaxS = point.y + 400
            var xminS = point.x - (dX / 2)
            var xmaxS = point.x + (dX / 2)

            // Zoom to point - using Extent
            map.setExtent(new Extent(xminS, yminS, xmaxS, ymaxS, new SpatialReference({
                wkid: spr
            })));
        }


        /*=====  End of Search  ======*/




        /*===================================
        =            Street View           =
        ===================================*/

        $('#gsv_drag').hover(function(event) {
            $('#gsv_drag').attr('src', './StreetView/images/icon_glyph_gold.png');
        }, function() {
            $('#gsv_drag').attr('src', './StreetView/images/icon_glyph_white.png');
        })

        $('#gsv_text').click(function() {
            $(this).empty()
        })

        // Drag gsv icon
        $('#gsv_drag').draggable({
                containment: "#mapContainer",
                appendTo: '#mapContainer',
                scroll: false,
                cursor: "move",
                cursorAt: {
                    left: 20,
                    top: -4
                },
                zIndex: 9999999,
                helper: function() {
                    $copy = $(this).clone();
                    $copy[0].src = "./StreetView/images/flying_gsv_man_w.png"
                    $copy[0].style.width = '40px'
                    return $copy;
                },
                start: function(event, ui) {
                    mX = 0;
                    $('#gsv').children('img').attr('src', '')

                    ui.previousPosition = ui.position;
                    gsvPOS = []
                },
                drag: function(event, ui) {

                    ui.helper[0].src = "./StreetView/images/flying_gsv_man_w.png"
                    if (event.pageX < gsvPOS[gsvPOS.length - 1]) {
                        ui.helper[0].src = "./StreetView/images/flying_gsv_man_w.png"
                    } else {
                        ui.helper[0].src = "./StreetView/images/flying_gsv_man_e.png"
                    }
                    gsvPOS.push(event.pageX)
                },
                stop: function(event, ui) {
                    $('#gsv').children('img').attr('src', './StreetView/images/icon_glyph_white.png')
                    cursorToStatePlane(event, true)
                }
            }) // End draggable

        // Convert cursor position to State Plane Coordinates
        function cursorToStatePlane(cursor, isGSV) {
            var cursorTop = $('#mapContainer').height() - cursor.clientY;
            var mapDeltaX = map.extent.xmax - map.extent.xmin;
            var mapDeltaY = map.extent.ymax - map.extent.ymin;
            var xL = (mapDeltaX * cursor.clientX) / $('#mapContainer').width();
            var yT = (mapDeltaY * cursorTop) / $('#mapContainer').height();
            var x = map.extent.xmin + xL;
            var y = map.extent.ymin + yT
            var point = new Point(x, y, new SpatialReference({
                wkid: spr
            }));
            if (isGSV) {
                toLatLon(point)
            }
            return x + ' ' + y
        }

        // Convert State Plane to Lat-Lon
        function toLatLon(gsvSP) {
            var outSR = new SpatialReference(4326);
            var params = new ProjectParameters();
            params.geometries = [gsvSP];
            params.outSR = outSR;
            gsvc.project(params, function(projectPoint) {
                gsv_test_latlon(projectPoint[0], gsvSP)
            });;
        }

        // Test if Lat-Lon coordinates have available gsv 
        function gsv_test_latlon(gsvLatLon, gsvSP) {
            var streetViewService = new google.maps.StreetViewService();
            var STREETVIEW_MAX_DISTANCE = 70;
            var latLng = new google.maps.LatLng(gsvLatLon.y, gsvLatLon.x);
            streetViewService.getPanoramaByLocation(latLng, STREETVIEW_MAX_DISTANCE, function(streetViewPanoramaData, status) {
                if (status === google.maps.StreetViewStatus.OK) {
                    viewStreet(gsvLatLon)
                    gsv_move(gsvSP)
                    $('#gsv_text').empty();
                } else {
                    $('#gsv_text').text('Steetview unavailable in this area')
                }
            });
        }

        // Open streetview at Lat-Lon coordinates
        function viewStreet(gsvLatLon) {
            if ($('#profileContainer').is(':visible')) {
                hideProfile()
            }
            if (panorama) {
                panorama.setVisible(true);
            }

            var navH;
            $(window).width() < 768 ? navH = 0 : navH = $('#navbar-container').height();
            halfHeight = ($('body').height() - navH) / 2
            toggleGSVcontent(true)
            $('#mapContainer').animate({
                    height: halfHeight + 'px',
                })
                // Initiate Panorama AFTER street view container is up or else Panorama
                // needs to be refreshed
            $('#streetviewContainer').animate({
                height: halfHeight + 'px',
            }, function() {
                $('#gsv_Close').show();
                initPanorama(gsvLatLon)

            })

            // Enable gsv close button after panorama initiates
            setTimeout(gsv_close, 600)
        }

        function toggleGSVcontent(tog) {
            if (tog) {

                if ($(window).width() < 768) {
                    $('#mapLoading').animate({
                        bottom: '51%'
                    });
                    $('#contentContainer').animate({
                        height: (halfHeight - $('#navbar-container').height()) + 'px',
                    })


                    $('#contentToggleContainer').animate({
                        bottom: '54%'
                    })


                    $('#controlsContainer,#scalebarContainer').animate({
                        bottom: '53%'
                    })
                } else {
                    $('#mapLoading').animate({
                        bottom: '49%'
                    });
                    $('#contentContainer').animate({
                        height: halfHeight + 'px',
                    })

                    $('#contentToggleContainer').animate({
                        bottom: '51%'
                    })

                    $('#controlsContainer,#scalebarContainer').animate({
                        bottom: '50%'
                    })
                }

            } else {
                $('#mapLoading').animate({
                    bottom: '10px'
                });
                $('#contentToggleContainer').animate({
                    bottom: '33px'
                })


                $('#controlsContainer,#scalebarContainer').animate({
                    bottom: '27px'
                })

                $('#contentContainer').animate({
                    height: contentHeight
                })
            }
        }


        function initPanorama(gsvLatLon) {
            panorama = new google.maps.StreetViewPanorama(
                document.getElementById('streetview'), {
                    position: {
                        lat: gsvLatLon.y,
                        lng: gsvLatLon.x
                    },
                    pov: {
                        heading: 0,
                        pitch: 0
                    },
                    zoom: 1,
                    linksControl: true,
                    panControl: false,
                    addressControl: false,
                    enableCloseButton: false,
                    imageDateControl: true,
                    enableCloseButton: false,
                });

            // Move gsv icon as street view position changes
            panorama.addListener('position_changed', function() {
                var lat = panorama.getPosition().lat()
                var lng = panorama.getPosition().lng()
                var point = new Point(lng, lat, new SpatialReference({
                    wkid: 4269
                }));
                toStatePlane(point)
            });

            // Move heading icon has heading changes
            panorama.addListener('pov_changed', function() {
                heading = panorama.getPov().heading;
                //var pitch = panorama.getPov().pitch;
                var zoom = panorama.getPov().zoom;
                var zooms = {
                        0: (zoom < 0.9),
                        1: (zoom >= 0.9 && zoom < 1.9),
                        2: (zoom >= 1.9 && zoom < 2.9),
                        3: (zoom >= 2.9 && zoom < 3.9),
                        4: (zoom >= 3.9)
                    }
                    // Set heading icon based off zoom level (FOV)
                for (var i = 0; i < Object.keys(zooms).length; i++) {
                    if (zooms[i]) {
                        headingSym.setUrl('./Streetview/images/heading_' + i + '.png')
                    }
                }
                headingSym.setAngle(heading);
                headingGraphic.draw();
            });
        } // End initPanorama()

        function gsv_close() {
            $('#gsv_Close').click(hideGSV)
        }

        function hideGSV() {
            map.graphics.clear();
            $('#mapContainer').animate({
                height: contentHeight + 50 + 'px',
            })
            $('#streetviewContainer').animate({
                height: '0%',
            }, function() {
                $('#gsv_Close').hide();
            })
            toggleGSVcontent(false)
            panorama.setVisible(false);
        }

        // Convert Lat-Lon to State Plane
        function toStatePlane(point) {
            var params = new ProjectParameters();
            params.geometries = [point];
            params.inSR = 4269;
            params.outSR = map.spatialReference;
            gsvc.project(params, function(projectPoint) {
                var gsv_sp = projectPoint[0]
                gsv_move(gsv_sp)
            }, function(error) {
                console.log(error)
            });;
        }

        // Move gsv icon with gsv navigation
        function gsv_move(gsv_man) {
            map.graphics.clear()
            headingSym = new PictureMarkerSymbol('./Streetview/images/heading_1.png', 65, 65)
            if (heading) {
                headingSym.setAngle(heading)
            }
            headingGraphic = new Graphic(gsv_man, headingSym);
            manSym = new PictureMarkerSymbol('./Streetview/images/gsv_bike_v2_icon.png', 39, 37)
            manGraphic = new Graphic(gsv_man, manSym);
            map.graphics.add(headingGraphic)
            map.graphics.add(manGraphic)
            map.centerAt(gsv_man);
        }

        /*=====  End of Street View  ======*/



        /*====================================
        =            Contact Form            =
        ====================================*/

        on(dom.byId('addMarker'), tap, function(event) {
            addMarkers();
        });

        function addMarkers() {
            $('#markerDescriptions').show()
            if (textGraphic) {
                textGraphic.hide()
            }
            if (loadGraphic) {
                loadGraphic.hide();
            }
            $('.removeMarker').unbind('click')
            if (markerCount.length > 0) {
                var marker = 'marker' + markerCount[0]
                window[marker] = new PictureMarkerSymbol();
                window['marker' + markerCount[0]].setHeight(55);
                window['marker' + markerCount[0]].setWidth(35);
                window['marker' + markerCount[0]].setUrl("graphics/graphic_" + markerCount[0] + ".png");
                var graphic = 'graphic' + markerCount[0]
                $('#markerList').append('<li data-mc="' + markerCount[0] + '"id="' + graphic + '"style="text-align:center;" class="list-group-item form-inline" ><div class="form-group"><img style="margin-right:20px;" height="25px" width="auto" src="' + window[marker].url + '"><select class="form-control markerInputs" id="sel_' + graphic + '" style="display:inline-block;width:134px;"><option value="Missing">Missing</option><option value="Damaged">Damaged</option><option value="New">New Location</option><option value="Other">Other</option></select><span title="Remove Marker" id="rm_' + graphic + '" class="removeMarker glyphicon glyphicon-remove"></span></div></li>')

                window[graphic] = new Graphic(map.extent.getCenter(), window[marker])
                map.graphics.add(window[graphic]);
                graphicList.push(window[graphic])
                move.activate(Edit.MOVE, window[graphic])
                move.on('graphic-move-start', function() {
                    textGraphic.hide()
                })
                markerCount.splice(0, 1)
                createTextLabel(map.extent.getCenter(), "Drag to Location")
            } else {
                $('#addMarker').addClass('disabled');
                $('#markerDisclaimer').css('display', 'block');
            }

            query(".removeMarker").forEach(function(item) {
                on(dom.byId($(item).attr("id")), tap, function(e) {
                    removeMarker(e)
                });
            });

            function removeMarker(node) {
                var li = $("#" + node.target.id).closest('li')
                if (typeof window[li.attr('id')] !== "undefined") {
                    textGraphic.hide()
                    $('#markerDisclaimer').css('display', 'none')
                    $('#addMarker').removeClass('disabled')
                    window[li.attr('id')].hide()
                    markerCount.push(parseInt(li.attr('data-mc')))
                    graphicList.splice(graphicList.indexOf(window[li.attr('id')]), 1)
                    li.remove()
                    if (markerCount.length === 6) {
                        $('#markerDescriptions').hide()
                    }
                }
            }
        }

        function createTextLabel(geom, text) {
            var font = new Font();
            font.setWeight(Font.WEIGHT_BOLD);
            font.setSize(16);
            var textSym = new TextSymbol();
            textSym.setFont(font);
            textSym.setHorizontalAlignment("left");
            textSym.setOffset(15, -20);
            textSym.setText(text);
            textSym.setHaloColor(new Color([255, 255, 255]));
            textSym.setHaloSize(1);
            textGraphic = new Graphic(geom, textSym)
            map.graphics.add(textGraphic);
        }

        on(dom.byId('submit'), tap, function(e) {
            submitForm(e)
        });

        function submitForm(event) {

            dojo.stopEvent(event);
            event.preventDefault();

            var body = "";
            var url;
            if (graphicList.length > 0) {
                url = "?MarkerCount=" + graphicList.length
                for (var i = 0; i < graphicList.length; i++) {
                    url = url + '+marker' + (i + 1) + '=x' + graphicList[i].geometry.x + 'y' + graphicList[i].geometry.y
                    body = body + '\r\n' + "Marker " + (i + 1) + '\r\n' + "Comment: " + $('#markerList option').eq(i).val() + '\r\n'
                }
            } else {
                url = ""
            }

            var fname = $('#fname').val();
            var lname = $('#lname').val();
            var copy = $('#sendToSelf > input').is(':checked') ? true : false;
            var msgSubject = 'Bicycle Parking Application';
            var msgTo = 'dctc@dutchessny.gov';
            //var msgTo = 'pdctc@dutchessny.gov';
            //var msgTo = 'sstippa@dutchessny.gov';
            var msgFrom = $('#email').val();
            var messageBody = 'Message from: ' + fname + ' ' + lname + '\r\n' + '\r\n' + $('#message').val() + '\r\n' + body + '\r\n';
            var encodeURL = encodeURIComponent(url)

            $.ajax({
                type: "POST",
                url: "sendEmail.asp",
                data: {
                    to: msgTo,
                    from: msgFrom,
                    copy: copy,
                    subject: msgSubject,
                    body: messageBody,
                    urlEnc: encodeURL
                },
                dataType: "json",
                success: function(response) {
                    $('#feedbackForm').slideUp();
                    if (response === 200) {
                        $('#contactTab').append('<p class="formSubmitResponse bg-success text-center"><span class="glyphicon glyphicon glyphicon-ok text-success"></span> Message sent successfully. Thank you. </p><p><small><a class="resetForm" style="cursor:pointer;">Click here to send another message</a></small></p>')
                    } else if (response === 500) {
                        $('#contactTab').append('<p class="bg-danger text-center"><span class="glyphicon glyphicon-warning-sign "></span> Unable to send. Something went wrong on our end. Please try again later.</p>')
                    } else {
                        $('#contactTab').append('<p class="bg-danger text-center"><span class="glyphicon glyphicon-warning-sign "></span> Unfortunately, an unknown error has occured. Please refresh the page and try again.</p>')
                    }
                    $('.resetForm').click(function() {
                        $('.formSubmitResponse').remove();
                        $(this).remove();
                        $('#feedbackForm').slideDown();
                    })
                },
                failure: function(response) {
                    console.log('AJAX post request failed')
                }
            });
        }

        /*=====  End of Contact Form  ======*/


        /*=============================
        =          Simple Print       =
        =============================*/

        on(dom.byId('print'), tap, function(e) {
            printContent($('#mapContainer_container'))
        });

        function printContent(el) {

            var mywindow = window.open('', 'PRINT', 'height=600,width=800');
            var printcontent = el.clone();
            printcontent.css('z-index', '-1')
            mywindow.document.write('<html><head><title>Print Bicycle Parking</title><link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"><link rel="stylesheet" type="text/css" href="./styles.css">');
            mywindow.document.write('</head><body>');

            if ($('.esriPopup').hasClass('esriPopupVisible')) {

                var popupcontent = $('.esriPopupWrapper .content').clone();
                mywindow.document.write(printcontent.html());
                mywindow.document.write('<div style="background-color:white;width:300px;position:absolute;left:10px;top:10px;">' + popupcontent.html() + '</div>');

            } else {
                mywindow.document.write(printcontent.html());
            }

            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            setTimeout(function() {
                mywindow.print();
                mywindow.close();
            }, 1000)
        }
    }) // End require