var pano;
var panoConfig = {}
var serverURL = "http://gistest2:6080/arcgis/rest/services/masterVR/MapServer";
var clusterLayer;
var slider;
var data = [];
var graphic;
var config;
var scenes = {};
var selectGraphic;
var placeName = 'BOWDOIN';
var firstScene = 'BOWDOIN24';
var titleName = 'Bowdoin Park';


require(["esri/map", "esri/geometry/Extent", "esri/SpatialReference", "esri/layers/ArcGISDynamicMapServiceLayer", "esri/tasks/query", "esri/tasks/QueryTask", "esri/tasks/IdentifyTask", "esri/tasks/IdentifyParameters", "esri/symbols/Symbol", "esri/graphic", "esri/symbols/SimpleMarkerSymbol", "esri/Color", "esri/renderers/ClassBreaksRenderer", "extras/ClusterLayer", "esri/symbols/PictureMarkerSymbol", "dojo/_base/array", "esri/geometry/Point", "esri/geometry/Multipoint", "esri/dijit/Scalebar"], function(Map, Extent, SpatialReference, ArcGISDynamicMapServiceLayer, Query, QueryTask, IdentifyTask, IdentifyParameters, Symbol, Graphic, SimpleMarkerSymbol, Color, ClassBreaksRenderer, ClusterLayer, PictureMarkerSymbol, arrayUtils, Point, Multipoint, Scalebar) {


        //set content height
        if ($(window).width() > 768) {
            $('#contentContainer').height($(window).height() - $('#navbarContainer').height() - 50)
        } else {
            //$('#contentContainer').height($('body').height() - $('#navbarContainer').height() - 50);
            // $('#leftColContainer').css('padding', '0');
            $('#imageContainer').css('height', '30%');
            $('#rightColContainer').css('height', '55%');
        }


        var selectIcon = new PictureMarkerSymbol("graphics/markerRedFullBlue.png", 50, 50).setOffset(0, 9);
        var icon = new PictureMarkerSymbol("graphics/markerRedFullDropShadow.png", 50, 55).setOffset(0, 9);
        var clusterIcon = new PictureMarkerSymbol("graphics/markerRedFullWhiteDropShadow.png", 50, 55).setOffset(0, 9)

        BOWDOINextent = new Extent(642127.2838762631, 1006003.2643034572, 648960.6172095968, 1011711.2669489603, new SpatialReference({
            wkid: 2260
        }));

        WILCOXextent = new Extent(699429.6648286475, 1134391.8744697548, 706262.9981619817, 1140562.7928371024, new SpatialReference({
            wkid: 2260
        }));

        QUIETCOVEextent = new Extent(645450.1471850098, 1056262.2550245132, 647158.4805183435, 1057804.9846163501, new SpatialReference({
            wkid: 2260
        }));

        DUTCHESSSTADIUMextent = new Extent(639364.8182625036, 981613.3805374027, 640218.984929171, 982384.7453333217, new SpatialReference({
            wkid: 2260
        }));

        map = new Map("mapContainer", {
            logo: false,
            slider: false,
        });

        map.setExtent(BOWDOINextent)

        map.on('click', function() {
            console.log(map.extent)
        })

        var scalebar = new Scalebar({
            map: map,
            attachTo: "bottom-center",
            scalebarStyle: "line"
        });



        layers = new ArcGISDynamicMapServiceLayer(serverURL);
        layers.setVisibleLayers([1, 2, 3, 4, 5])
        map.addLayer(layers)

        masterVR = new ArcGISDynamicMapServiceLayer(serverURL);
        masterVR.setVisibleLayers([0])
            //map.addLayer(masterVR)



        // map.on('mouse-drag', function(){
        // 	console.log(map.extent)
        // })		   

        map.on('zoom', function() {
            try {
                map.graphics.remove(selectGraphic);
            } catch (err) {
                console.log(err)
            }
        })

        $(document).ready(function() {
            initImages();
            getOptions();
        })




        function initImages() {
            var media, desc, x, y;
            var queryTask = new QueryTask(serverURL + "/" + 0);
            var query = new Query();
            query.outFields = ["MEDIA", "DESCRIPTION"];
            // query.where = "PLACE = '" + placeName + "'"
            query.where = "PLACE LIKE '%'"
            query.returnGeometry = true;
            queryTask.execute(query, function(result) {
                    for (var i = 0; i < result.features.length; i++) {

                        desc = result.features[i].attributes.DESCRIPTION
                        media = result.features[i].attributes.MEDIA.split(".JPG")[0]
                        x = result.features[i].geometry.x
                        y = result.features[i].geometry.y
                        data[i] = {
                            "name": desc,
                            "media": media,
                            "x": x,
                            "y": y,
                        }

                    } // end feature loop

                    initPan(data, firstScene)
                    initCluster(data);

                    $('.lslide').hover(function() {

                        var media = $(this).children('img').attr('src').split("icons/")[1].split(".png")[0]
                            //console.log(media)
                            // $(this).children('img')
                        highlightCluster(false, media)
                    }, function() {

                        map.graphics.remove(graphic)
                    })

                    getOptions('BOWDOIN');
                }) // End query execute
        } // End initImages


        function getOptions(location) {
            var descList = [];

            var queryTask = new QueryTask(serverURL + "/" + 0);
            var query = new Query();
            query.outFields = ["DESCRIPTION"];
            // query.where = "PLACE = '" + placeName + "'"
            query.where = "PLACE = '" + location + "'"

            queryTask.execute(query, function(result) {
                    for (var i = 0; i < result.features.length; i++) {

                        desc = result.features[i].attributes.DESCRIPTION

                        if (descList.indexOf(desc) < 0) {
                            descList.push(desc)
                        }

                    } // end feature loop





                    // Add description options
                    $.each(descList.sort(), function(i, item) {
                        $('.descWrapper').append('<div class="descOptions">' + item + '</div>')
                    })

                    $('.descOptions').click(function() {
                        map.graphics.remove(descGraphic);
                        var selected = $(this)

                        var desc = selected.text()

                        var media
                        var first = true;
                        if (desc === "VIEW ALL") {
                            map.setExtent(window[placeName + 'extent']);
                        } else {
                            $.each(data, function(i, item) {
                                if (item.name === desc) {
                                    if (first) {
                                        media = item.media
                                        newPan(media, item.name);
                                        first = false;
                                    }
                                }
                            })
                            zoomToExtent(desc);
                            setTimeout(function() {
                                highlightCluster(true, media)
                            }, 500)
                        }
                        // Highlight Place
                        $('.descOptions').removeClass('decsOptionSelected')
                        $('.descOptions').each(function(i, item) {
                            if ($(this)[0] === selected[0]) {
                                selected.addClass('decsOptionSelected')
                            }
                        })
                    })

                    $('.descOptions').hover(function() {
                        highlightCluster(false, $(this).text(), true)
                    }, function() {
                        map.graphics.remove(descGraphic);
                    })
                }) // End query execute
        } // End initImages

        function initCluster(data) {
            clusterLayer = new ClusterLayer({
                "data": data,
                "distance": 90,
                "id": "clusters",
                "labelColor": "black",
                "labelOffset": 9,
                "resolution": map.extent.getWidth() / map.width,
                "singleColor": "#888",
            });

            var defaultSym = new SimpleMarkerSymbol().setSize(4);
            var renderer = new ClassBreaksRenderer(defaultSym, "clusterCount");

            renderer.addBreak(0, 1, icon);
            renderer.addBreak(2, 100, clusterIcon);

            clusterLayer.setRenderer(renderer);
            map.addLayer(clusterLayer);

            clusterLayer.on('click', function(event) {
                var zoomGeom = event.graphic.geometry
                var cID = event.graphic.attributes.clusterId
                var cCount = event.graphic.attributes.clusterCount
                zoomToCluster(zoomGeom, cID, cCount);
                markerHighlight(cCount, cID);


            })

            clusterLayer.on('mouse-over', function(event) {
                map.setMapCursor("pointer");
                //console.log(event.graphic.attributes.clusterId)
                //          var zoomGeom = event.graphic.geometry

                //          var cID = event.graphic.attributes.clusterId
                //          var cCount = event.graphic.attributes.clusterCount



                //          $.each(clusterLayer._clusterData,function(count,item){
                //  if (cID === item.attributes.clusterId){

                //      var media = item.media 
                //      console.log(media)
                //      highlightCluster(media)
                //  }
                // })



            })

            clusterLayer.on('mouse-out', function(event) {
                map.setMapCursor("default");
                // map.graphics.remove(graphic)
            })

            map.infoWindow.set("popupWindow", false);

        }


        $('#navbarContainer li a').click(function(evt) {
            $('#navbarContainer li').removeClass('active')
            $(this).parent('li').addClass('active')

            placeName = $(this).attr('data-place');
            console.log(placeName)
            if (placeName === 'BOWDOIN') {
                firstScene = 'BOWDOIN24';
                // titleName = 'Bowdoin Park';
            } else if (placeName === 'WILCOX') {
                firstScene = 'WILCOX1';
                // titleName = 'Wilcox Park';
            } else if (placeName === 'DUTCHESSSTADIUM') {
                firstScene = 'DUTCHESSSTADIUM1';
                // titleName = 'Dutchess Stadium';
            } else if (placeName === 'QUIETCOVE') {
                firstScene = 'QUIETCOVE1';
                // titleName = 'Quiet Cove';
            } else {}


            // $('#titleName').text(titleName)
            // $('.thumbContainer').empty();
            $('.descWrapper').empty().append('<div class="descOptions decsOptionSelected">VIEW ALL</div>')

            getOptions(placeName);
            zoomToLocation(placeName)

        })

        function zoomToLocation(location) {
            var loc = location + 'extent'
            map.setExtent(window[loc])
        }


        function zoomToExtent(title) {
            var extents = []

            $.each(clusterLayer._clusterData, function(count, item) {
                if (item.name === title) {
                    extents.push({
                        "x": item.x,
                        "y": item.y
                    })
                }
            })

            var xmin = Math.min.apply(this, $.map(extents, function(o) {
                return o.x;
            }));
            var ymin = Math.min.apply(this, $.map(extents, function(o) {
                return o.y;
            }));
            var xmax = Math.max.apply(this, $.map(extents, function(o) {
                return o.x;
            }));
            var ymax = Math.max.apply(this, $.map(extents, function(o) {
                return o.y;
            }));

            var extent = new Extent(xmin - 100, ymin - 100, xmax + 100, ymax + 100, new SpatialReference({
                wkid: 2260
            }));
            map.setExtent(extent)
                //setTimeout(function(){map.centerAndZoom(map.extent.getCenter(),2)},500)
        }

        function zoomToCluster(zoomGeom, cID, cCount) {
            if (cCount > 1) {

                map.centerAndZoom({
                    "x": zoomGeom.x,
                    "y": zoomGeom.y
                }, 0.5)
            } else {
                $.each(clusterLayer._clusterData, function(count, item) {
                    if (cID === item.attributes.clusterId) {

                        var media = item.media
                        var title = item.name
                        newPan(media, title)

                        // Highlight Place
                        $('.descOptions').removeClass('decsOptionSelected')
                        $('.descOptions').each(function(i, item) {
                            if ($(this).text() === title) {
                                $(this).addClass('decsOptionSelected')
                            }
                        })
                    }
                })
            }

        }


        //create identify tasks and setup parameters

        map.on("click", executeIdentifyTask);

        function executeIdentifyTask(event) {
            identifyTask = new IdentifyTask(serverURL);
            identifyParams = new IdentifyParameters();
            identifyParams.tolerance = 1;
            identifyParams.returnGeometry = true;
            identifyParams.layerIds = [0]
            identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
            identifyParams.mapExtent = map.extent;
            identifyParams.geometry = event.mapPoint;
            identifyParams.mapExtent = map.extent;
            identifyTask.execute(identifyParams, function(obj) {
                console.log(obj[0].feature)
                var pan = obj[0].feature.attributes.MEDIA.strip('.JPG')[0]
                var title = obj[0].feature.attributes.DESCRIPTION
                var xc = obj[0].feature.geometry.x
                var yc = obj[0].feature.geometry.y
                newPan(pan, title)
                setThumb(title)
                highlight(xc, yc)
            })
        }

        function setThumb(desc) {
            $('.thumbDesc').each(function(i, f) {
                if ($(this).text() === desc) {
                    $('.lslide').removeClass('lslideSelect')
                    $(this).parent('.lslide').addClass('lslideSelect')
                    slider.goToSlide(i);
                }
            })
        }

        function highlight(x, y) {
            var symbol = new Symbol.SimpleMarkerSymbol({
                "color": [0, 0, 128, 128],
                "size": 18,
                "angle": 0,
                "xoffset": 0,
                "yoffset": 0,
                "type": "esriSMS",
                "style": "esriSMSSquare",
                "outline": {
                    "color": [0, 0, 128, 255],
                    "width": 1,
                    "type": "esriSLS",
                    "style": "esriSLSSolid"
                }
            })
            var graphic = new Graphic([x, y], symbol)
            map.addGraphic(graphic)
        }








        function highlightCluster(stay, media, desc) {

            var list = []
            $.each(clusterLayer._clusterData, function(count, item) {

                if (media === item.media) {
                    var cID = item.attributes.clusterId
                    var ID = cID - 1
                    var x = clusterLayer._clusters[ID].x
                    var y = clusterLayer._clusters[ID].y
                    if (stay) {
                        markerHighlight(1, cID)
                    } else {
                        graphicCL(x, y, 'thumb')
                    }
                } else if (media === item.name) {

                    var cID = item.attributes.clusterId
                    var ID = cID - 1

                    var x = clusterLayer._clusters[ID].x
                    var y = clusterLayer._clusters[ID].y
                    list.push([x, y])

                    // graphicCL(x, y, media)



                }
            })

            if (desc) {
                var mpJson = {
                    "points": list,
                    "spatialReference": ({
                        " wkid": 2260
                    })
                };
                var multipoint = new Multipoint(mpJson);
                descGraphic = new Graphic(multipoint, selectIcon)
                map.graphics.add(descGraphic);
            }

        }

        function graphicCL(x, y, id) {
            // try {
            //     map.graphics.remove(graphic);
            // } catch (err) {
            //     console.log(err)
            // }

            var point = new Point([x, y], new SpatialReference({
                wkid: 2260
            }))


            graphic = new Graphic(point, selectIcon, {
                'name': id
            })
            map.graphics.add(graphic);
        }

        function markerHighlight(cCount, cID) {
            if (cCount === 1) {
                try {
                    map.graphics.remove(selectGraphic);
                } catch (err) {
                    console.log(err)
                }
                var ID = cID - 1
                var x = clusterLayer._clusters[ID].x
                var y = clusterLayer._clusters[ID].y

                var point = new Point([x, y], new SpatialReference({
                    wkid: 2260
                }))
                selectGraphic = new Graphic(point, selectIcon)

                map.graphics.add(selectGraphic);

            }
        }

        // function getVR(opt1,opt2) {
        //    	var query = new Query();
        //    	var sql = "PLACE = '" + opt1 + "' AND DESCRIPTION = '" + opt2 + "'";
        //        var queryTask = new QueryTask(serverURL + "/" + 0);
        //        query.where = sql;
        //        query.outFields = ["CODEBLOCK"];
        //        queryTask.execute(query, function(result) {
        //        	var code = result.features[0].attributes.CODEBLOCK
        //        	$('#vr').empty();
        //        	$('#vr').append(code)
        //        	$('#code').text(code)
        //    		$('#outputContainer').show();
        //        })
        //       }

        $('#placeSelect').on('change', function() {
            console.log($(this).val())
            getDesc($(this).val())
        })

        $('#build').click(function() {
            var height = $('input[type=number][name=height]').val();
            var width = $('input[type=number][name=width]').val();

            $('#vr').height(height);
            $('#vr').width(width);
            var opt1 = $("#placeSelect option:selected").text();
            var opt2 = $("#descSelect option:selected").text();

            getVR(opt1, opt2)

        })


    }) // End require

$(document).ready(function() {

    // Init tabs
    $("#tabs").tabs({
        heightStyle: "content"
    });

});


function initPan(data, firstScene) {
    panoConfig['default'] = {
        "firstScene": firstScene,
        "hotSpotDebug": true,
        "autoLoad": true,
        "autoRotate": -1,
        "hfov": 120,
        "basePath": "./images/",
    }

    $.each(data, function(i, item) {
        var media = item.media
        var scene = {
            // "title": item.name,
            "panorama": item.media + ".JPG"
        }
        scenes[media] = scene
    })

    panoConfig["scenes"] = scenes;
    var panoDIV = 'panoContainer'
    pano = pannellum.viewer(panoDIV, panoConfig);
}


function newPan(pan, title) {
    pano.loadScene(pan)
}

$('.container').click(function(event) {

    console.log(pano.mouseEventToCoords(event))
    getCoords(pano.mouseEventToCoords(event))
})


function getCoords(coords) {
    var pitch = coords[0];
    var yaw = coords[1]
    $('input[type=number][name=pitch]').val(pitch);
    $('input[type=number][name=yaw]').val(yaw);
}


$(document).ready(function() {
    $('#reserve').click(function() {
        var site = $('.decsOptionSelected').text();
        var id = 10;
        var start = "8%2f17%2f2016";
        var end = "8%2f17%2f2016"
        var string = "https://test.dcny.gov/ParkReservations/public/SiteDetails.aspx?s=" + id + "&cidate=" + start + "&codate=" + end;
        window.open(string)
    })







})


// $(document).ready(function() {
// $('.lSNext').click(function(){rotate(-360)})
// $('.lSPrev').click(function(){rotate(360)})

// function rotate(rot){
// 	$({deg: 0}).animate(
// 	{deg: rot}, 
// 	{duration: 300,
// 		step: function(now, fx){
//      			$('img').css('box-shadow',"none");
//          		$('img').css({transform: "rotate(" + now + "deg)"});
//      		}},{
//       	complete: function(){
//  				$('img').css('box-shadow',"0px 66px 54px -31px black")
//  			}})
// }
// })