
% NE CSC implementation
addpath c:\Users\sstippa\Documents\MATLAB\Matlab_Net\bayesTools06Sep2013
addpath c:\Users\sstippa\Documents\MATLAB\Matlab_Net\interpTools
addpath c:\Users\sstippa\Documents\MATLAB\Matlab_Net\interpTools\scalec

% pick an analyiss date: 2020/2030/2050/2080 
yearIn = '2050'
% NED Error - 1/3 1.25 1/9 0.42 crm 1.0
DEM_ERROR = 0.42;

%% choose net
cd I:\NECSC\toMatlab

netName = 'NECSC_v4_w2080_ignorantPrior2_dem_sl75to2_over5m_train2region2crm_beach.neta' % change table built from expert input and train the priors and make sure the change bins are discrete! and code change -1,1
[netstuff] = netInfo(netName)
nodeNames = {netstuff.name}
%     'sealevel2020or2050'    'demMHW_m'    'VLM2020or2050'    'inundation'    'landclassCode'    'change'

%% choose data , savepaths
%data = f4_19_v2;
datafilename = 'forsythe_training.csv'; % pick one
shpName = 'forsythe_NED19_y50'; % shapefile name

% Save paths
savepath = 'I:\NECSC\fromMatlab\predictions' %predictions
shapepath = 'cd I:\NECSC\fromMatlab\shapefiles' %shapefiles

% import data
%data = importdata(datafilename); 
data2 = importdata(datafilename);
data = data2.data;

% 
% % % Clip to extent and remove landclass "0"
% dem = data2.data(:,21);
% demID = find(dem >= -10 & dem <= 5);
% data4 = data3(demID,:);
% lc = data4(:,19);
% lcID = find(lc > 0);
% data = data4(lcID,[4:15,17,19:22,24:end]);

%data = data2.data(demID,:);

N = length(data(:,1))
%dataNames = {'POINTID','grid_code','SL_1020_cm','SL_1030_cm','SL_1050_cm','SL_1080_cm','SL_2520_cm','SL_2530_cm','SL_2550_cm','SL_2580_cm', ...
%            'SL_7520_cm','SL_7530_cm','SL_7550_cm','SL_7580_cm','SL_9020_cm','SL_9030_cm','SL_9050_cm','SL_9080_cm','VLM_mmYr','dem19','dem13', ...
%            'esmplus','Lat','Lon','error','demMHW_m','landclass'}
%dataNames = {'POINTID','grid_code','SL_1020_cm','SL_1030_cm','SL_1050_cm','SL_1080_cm','SL_2520_cm','SL_2530_cm','SL_2550_cm','SL_2580_cm', ...
%            'SL_7520_cm','SL_7530_cm','SL_7550_cm','SL_7580_cm','SL_9020_cm','SL_9030_cm','SL_9050_cm','SL_9080_cm','VLM_mmYr','demMHW_m', ...
%            'error','landclass1','Lat','Lon','landclass','ESMPlus_080414'}
% dataNames = {'POINTID','SL_1020_cm','SL_1030_cm','SL_1050_cm','SL_1080_cm','SL_2520_cm','SL_2530_cm','SL_2550_cm','SL_2580_cm', ...
%             'SL_7520_cm','SL_7530_cm','SL_7550_cm','SL_7580_cm','SL_9020_cm','SL_9030_cm','SL_9050_cm','SL_9080_cm','VLM_mmYr', ...
%             'landclass','Lat','Lon','demMHW_m','error'}
dataNames = {'POINTID','SL_1020_cm','SL_1030_cm','SL_1050_cm','SL_1080_cm','SL_2520_cm','SL_2530_cm','SL_2550_cm','SL_2580_cm', ...
    'SL_7520_cm','SL_7530_cm','SL_7550_cm','SL_7580_cm','SL_9020_cm','SL_9030_cm','SL_9050_cm','SL_9080_cm','VLM_mmYr', ...
    'landclass','Lat','Lon','ned19mhw','ned13mhw','crm','coned_navd88','nedMHW','add2_88_4mhw','conedMHW'}
    % change elevation to demMHW_m!!!
%% Test extent
% plot extent, check out data, make sure no glitches
if(0)
    idx = strmatch('Lon', dataNames);
    idy = strmatch('Lat', dataNames);
    plot(data(:,idx), data(:,idy),'.')
    
    % inspect
    for i=1:6
        x=[-10:1:2];
        subplot(6,1,i)
        id=find(data(:,end)==i); mean(data(id,1))
        [n,x]=hist(data(id,1),x)
        bar(x,n/sum(n))
        ylabel(sprintf('landclass=%d',i))
        xlim([-10 2])
    end
    xlabel('dem elevation, m')
end


%% parse the sea-levels
%  year
y = [20, 30, 50, 80]; % 2020 and 2030 and 2050 and 2080
% pctiles
pct = [10,25,75,90];
SLPTILES2020 = zeros(N,length(pct))*nan;
SLPTILES2030 = zeros(N,length(pct))*nan;
SLPTILES2050 = zeros(N,length(pct))*nan;
SLPTILES2080 = zeros(N,length(pct))*nan;
for i=1:length(pct)
    % get values for year 2020
    id = strmatch(sprintf('SL_%d20_cm',pct(i)), dataNames);
    SLPTILES2020(:,i) = data(:,id);
    % get values for year 2030
    id = strmatch(sprintf('SL_%d30_cm',pct(i)), dataNames);
    SLPTILES2030(:,i) = data(:,id);
    % 2050
    id = strmatch(sprintf('SL_%d50_cm',pct(i)), dataNames);
    SLPTILES2050(:,i) = data(:,id);
     % 2080
    id = strmatch(sprintf('SL_%d80_cm',pct(i)), dataNames);
    SLPTILES2080(:,i) = data(:,id);
end
% make sure we get the full range
slmax = max([SLPTILES2020;SLPTILES2080])
slmin = min([SLPTILES2020;SLPTILES2080])

% convert pctiles to pdf
cdf = [0, pct, 100]; % pctiles defined on this input
% interp to these "bins", these can be Netica's bins
id = strmatch('sealevel2020or2050or2080', nodeNames) % also includes 2030
zi = netstuff(id).levels; zi=zi(:)';% we interpolate to bayes pdf!
tic
switch yearIn
    case '2020'
        cdfi2020 = zeros(N,length(zi));
        pdfi2020 = zeros(N,length(zi)-1);
    case '2030'
        cdfi2030 = zeros(N,length(zi));
        pdfi2030 = zeros(N,length(zi)-1);
    case '2050'
        cdfi2050 = zeros(N,length(zi));
        pdfi2050 = zeros(N,length(zi)-1);
    case '2080'
        cdfi2080 = zeros(N,length(zi));
        pdfi2080 = zeros(N,length(zi)-1);
end

for i=1:N
    % interp pctiles to bins
    % convert to meters
    switch yearIn
        case '2020'
            % 2020
            cdfi2020(i,:) = interp1([0, SLPTILES2020(i,:), max(slmax)*2]/100, cdf, zi);
            pdfi2020(i,:) = diff(cdfi2020(i,:));
        case '2030'
            % 2030
            cdfi2030(i,:) = interp1([0, SLPTILES2030(i,:), max(slmax)*2]/100, cdf, zi);
            pdfi2030(i,:) = diff(cdfi2030(i,:));
        case '2050'
            % 2050
            cdfi2050(i,:) = interp1([0, SLPTILES2050(i,:), max(slmax)*2]/100, cdf, zi);
            pdfi2050(i,:) = diff(cdfi2050(i,:));
        case '2080'
            % 2080
            cdfi2080(i,:) = interp1([0, SLPTILES2080(i,:), max(slmax)*2]/100, cdf, zi);
            pdfi2080(i,:) = diff(cdfi2080(i,:));
    end
end
toc
if(0)
    % check for errors
    switch yearIn
        case '2020'
            id = find(isnan(cdfi2020)); if(length(id)>0), error('nanalert!'), end
            id = find(isnan(pdfi2020)); if(length(id)>0), error('nanalert!'), end
        case '2030'    
            id = find(isnan(cdfi2030)); if(length(id)>0), error('nanalert!'), end
            id = find(isnan(pdfi2030)); if(length(id)>0), error('nanalert!'), end
        case '2050'
            id = find(isnan(cdfi2050)); if(length(id)>0), error('nanalert!'), end
            id = find(isnan(pdfi2050)); if(length(id)>0), error('nanalert!'), end
        case '2080'
            id = find(isnan(cdfi2080)); if(length(id)>0), error('nanalert!'), end
            id = find(isnan(pdfi2080)); if(length(id)>0), error('nanalert!'), end
    end
end

%% parse landmovement
id = strmatch('VLM_mmYr', dataNames)
year0 = 2010% start now-ish
% convert rate to elevation, meters
vlm2020 = (2020-year0)*data(:,id)/1000;
vlm2030 = (2030-year0)*data(:,id)/1000;
vlm2050 = (2050-year0)*data(:,id)/1000;
vlm2080 = (2080-year0)*data(:,id)/1000;
vlmmax=max([vlm2020;vlm2080])
vlmmin=min([vlm2020;vlm2080])

%% range of DEM 
id = strmatch('demMHW_m', dataNames)
demmax=max(data(:,id))
demmin=min(data(:,id))

%% range of landclassCode 
id = strmatch('landclass', dataNames)
if(0) % Histogram?
    hist(data(:,id))
end
%% convert inputs to pdfs
% 'sealevel2020or2050'    'demMHW_m'    'VLM2020or2050'    'inundation'    'landclassCode'    'change'
nodeNamesIn = {'sealevel2020or2050or2080'    'demMHW_m'    'VLM2020or2050or2080'    'landclass'}
nodeNamesOut = {'inundation'    'change'}
pdfIn = struct; % put the input pdfs here
for i=1:length(nodeNamesIn)
    j = strmatch(nodeNamesIn{i}, nodeNames)
    pdfRanges =  netstuff(j).levels; % get bin ranges
    switch nodeNamesIn{i}
        case 'sealevel2020or2050or2080' % also includes 2030
            % depends on year
            switch yearIn
                case '2020'
                    pdf = pdfi2020/100; % convert to prob
                case '2030'
                    pdf = pdfi2030/100; % convert to prob
                case '2050'
                    pdf = pdfi2050/100;
                case '2080'
                    pdf = pdfi2080/100;
            end
        case 'VLM2020or2050or2080' % also includes 2030
            VLM_ERROR = 0.001; % 1MM VLM ERROR
            % depends on year
            switch yearIn
                case '2020'
                    pdf = makeInputPdf(pdfRanges, 'normal',[vlm2020, VLM_ERROR+0*vlm2020], 'continuous');
                case '2030'
                    pdf = makeInputPdf(pdfRanges, 'normal',[vlm2030, VLM_ERROR+0*vlm2030], 'continuous');
                case '2050'
                    pdf = makeInputPdf(pdfRanges, 'normal',[vlm2050, VLM_ERROR+0*vlm2050], 'continuous');
                case '2080'
                    pdf = makeInputPdf(pdfRanges, 'normal',[vlm2080, VLM_ERROR+0*vlm2080], 'continuous');
            end
        case 'demMHW_m'
            % DEM_ERROR = 0.42; % 25CM DEM ERROR is nice and low. 0.42 is what Dean G says ----- now at top
            % get from file
            id = strmatch('demMHW_m', dataNames)
            pdf = makeInputPdf(pdfRanges, 'normal',[data(:,id),DEM_ERROR+0*data(:,id)], 'continuous');
            
        case 'landclass'
            landclassERROR = eps;% zero erro
            % get from file
            id = strmatch('landclass', dataNames)
            pdf = makeInputPdf(pdfRanges, 'normal',[data(:,id),landclassERROR+0*data(:,id)], 'discrete');
    end
    % stuff the pdf in the right place
    pdfIn.(nodeNamesIn{i}) = pdf;
end

% check
if(0)
    nn = 'demMHW_m'
    %nn = 'VLM2020or2050'
    id = strmatch(nn, dataNames)
    j = strmatch(nn, nodeNames)
    pdfRanges =  netstuff(j).levels % get bin ranges
    [stats] = PDF2Stats(pdfRanges, pdfIn.(nodeNames{j}), 0.5);
    plot(stats.bMostProb(1:N), data(:,id),'.')
end

%% build a casefile to train the priors
if(0) % need only do this once!
    nodeNamesIn = {'sealevel2020or2050or2080'    'demMHW_m'    'VLM2020or2050or2080'    'landclass'}
    % convert pdfs back to values
    allStats = struct;
    for i=1:length(nodeNamesIn)
        % convert from pdf
        j = strmatch(nodeNamesIn{i}, nodeNames)
        pdfRanges =  netstuff(j).levels; % get bin ranges
        pdf = pdfIn.(nodeNamesIn{i});
        switch nodeNamesIn{i}
            case {'sealevel2020or2050or2080'  'VLM2020or2050or2080' 'demMHW_m'}
                % deal with intrinsic pdfs
                allStats.(nodeNamesIn{i}) = PDF2Stats(pdfRanges, pdf, 0.5);
            case {'landclass'}
                % deal direct data
                j= strmatch(nodeNamesIn{i}, dataNames);
                allStats.(nodeNamesIn{i}).bMean = data(:,id);
        end
        % and show this
        nodeNamesIn{i}
        pdfRanges'
        mean(pdf)
    end
    % check plot(allStats.demMHW_m.bMean(1:N), data(:,1),'.')
    % populate priors with these:
    % sealevel2020or2050
    %          0    0.2500    0.5000    1.0000
    %     0.8913    0.0280    0.0391
    % demMHW_m
    %    -10    -1     0     1     2
    %    0.3456    0.3515    0.2113    0.0916
    % VLM2020or2050
    %    -0.1000         0    0.0500    0.1000
    %     0.9424    0.0576         0
    % landclassCode
    %      1     2     3     4     5     6
    %     0.5431    0.2561    0.0486    0.0025    0.0858    0.0639
    % spit out case file!
    fid = fopen([datafilename,'_',yearIn,'.cas'], 'wt');
    % header
    for i=1:length(nodeNamesIn)
        fprintf(fid,'%s, ', nodeNamesIn{i});
    end
    fprintf(fid, '\n');
    for j=1:N
        for i=1:length(nodeNamesIn)
            fprintf(fid, '%.2f, ', allStats.(nodeNamesIn{i}).bMean(j));
        end
        fprintf(fid, '\n');
    end
    fclose(fid);
    % now open net and train the inputs
end

%% predict 
% implement subsample on the pdfs
did = [1:1:N]'; length(did)
goingIn1 = did;
pdfInTMP = struct;
pdfSum = [];
for i=1:length(nodeNamesIn)
    pdfInTMP.(nodeNamesIn{i}) = pdfIn.(nodeNamesIn{i})(did,:);
    pdfSum(:,i) = sum(pdfInTMP.(nodeNamesIn{i}),2);
end

% % exclude cases that did not get pdfs (e.g.,very deep areas)
% did = did(find(all(pdfSum'>0))); length(did)
% goingIn2 = did;
% pdfInTMP = struct;
% pdfSum = [];
for i=1:length(nodeNamesIn)
    pdfInTMP.(nodeNamesIn{i}) = pdfIn.(nodeNamesIn{i})(did,:);
    pdfSum(:,i) = sum(pdfInTMP.(nodeNamesIn{i}),2);
end
find(any(pdfSum'==0));

% if length(goingIn1) ~= length(goingIn2);     
%     disp('Error, different size inputs. Press Key to Continue')
%     pause
% else;
%     disp('Good to Go')
% end

% do it - *predictBayes_v2dev edited
pred = predictBayes_v2dev(netName, nodeNamesIn, nodeNamesOut, pdfInTMP, nan*ones(length(did),length(nodeNamesOut)));
savename = sprintf('%s_%s_%s.mat', netName,datafilename,yearIn)
%savepath = 'G:\NECSC\matlab_outputs\predictions' %%% now up at top
save(fullfile(savepath,savename), 'netName', 'nodeNamesIn', 'nodeNamesOut', 'did', 'pred')

%% Find Most likely outcomes. Create Shapefile 

idx = strmatch('Lon', dataNames);
idy = strmatch('Lat', dataNames);

inun = pred.pdf.inundation;
change = pred.pdf.change;
[L,m] = size(inun);
inunProb = zeros(L,1);
inunBin = zeros(L,1);
for i = 1:length(data(:,1)); % For each row
	for k = 1:5; % For each column
		if ((inun(i,k) > inun(i,k+1:end)) & (inun(i,k) ~= inun(i,k+1:end)));
			inunProb(i,1) = inun(i,k);
			inunBin(i,1) = k;
			break % Next row
		elseif (inun(i,k) >= inun(i,k+1:end));
			inunProb(i,1) = NaN;
			inunBin(i,1) = NaN;
			break % Next row
		else
			% Next column
		end	
	end
end

region = horzcat(inun,change,inunProb,inunBin,data(:,idy),data(:,idx));
struct_array = struct('Bin1',region(:,1),'Bin2',region(:,2),'Bin3',region(:,3),'Bin4',region(:,4),'Bin5',region(:,5),'Dyn',region(:,6),'Inun',region(:,7), ...
 'PH',region(:,8),'BH',region(:,9),'Lat',region(:,10),'Lon',region(:,11));

gp = geopoint(struct_array);
eval(shapepath)

%shapename = sprintf('%s_%s', datafilename,yearIn)
%shapewrite(gp,shapename);
shapewrite(gp,shpName);

cd I:\NECSC\toMatlab



%% Old
% idx = strmatch('Lon', dataNames);
% idy = strmatch('Lat', dataNames);
% 
% % 2020-----------------------------------------------------------------------------------------------2020
% if yearIn == '2020';
%     inun2020 = pred.pdf.inundation;
%     change2020 = pred.pdf.change;
%     [L,m] = size(inun2020);
%     inun2020prob = zeros(L,1);
%     inun2020bin = zeros(L,1);
%     for i = 1:length(data(:,1));
%         if (inun2020(i,1) > inun2020(i,2)) & (inun2020(i,1) > inun2020(i,3)) & (inun2020(i,1) > inun2020(i,4)) & (inun2020(i,1) > inun2020(i,5));
%             inun2020prob(i,1) = inun2020(i,1);
%             inun2020bin(i,1) = 1;
%         elseif (inun2020(i,2) > inun2020(i,1)) & (inun2020(i,2) > inun2020(i,3)) & (inun2020(i,2) > inun2020(i,4)) & (inun2020(i,2) > inun2020(i,5));
%             inun2020prob(i,1) = inun2020(i,2);
%             inun2020bin(i,1) = 2;
%         elseif (inun2020(i,3) > inun2020(i,1)) & (inun2020(i,3) > inun2020(i,2)) & (inun2020(i,3) > inun2020(i,4)) & (inun2020(i,3) > inun2020(i,5));
%             inun2020prob(i,1) = inun2020(i,3);
%             inun2020bin(i,1) = 3;
%         elseif (inun2020(i,4) > inun2020(i,1)) & (inun2020(i,4) > inun2020(i,2)) & (inun2020(i,4) > inun2020(i,3)) & (inun2020(i,4) > inun2020(i,5));
%             inun2020prob(i,1) = inun2020(i,4);
%             inun2020bin(i,1) = 4;
%         elseif (inun2020(i,5) > inun2020(i,1)) & (inun2020(i,5) > inun2020(i,2)) & (inun2020(i,5) > inun2020(i,3)) & (inun2020(i,5) > inun2020(i,4));
%             inun2020prob(i,1) = inun2020(i,5);
%             inun2020bin(i,1) = 5; 
%         elseif (((inun2020(i,1) == inun2020(i,2)) & (inun2020(i,1) > inun2020(i,3)) & (inun2020(i,1) > inun2020(i,4)) & (inun2020(i,1) > inun2020(i,5))) | ...
%                 ((inun2020(i,1) == inun2020(i,3)) & (inun2020(i,1) > inun2020(i,2)) & (inun2020(i,1) > inun2020(i,4)) & (inun2020(i,1) > inun2020(i,5))) | ...
%                 ((inun2020(i,1) == inun2020(i,4)) & (inun2020(i,1) > inun2020(i,2)) & (inun2020(i,1) > inun2020(i,3)) & (inun2020(i,1) > inun2020(i,5))) | ...
%               ((inun2020(i,1) == inun2020(i,5)) & (inun2020(i,1) > inun2020(i,2)) & (inun2020(i,1) > inun2020(i,3)) & (inun2020(i,1) > inun2020(i,4))) | ...
%                 ((inun2020(i,2) == inun2020(i,3)) & (inun2020(i,2) > inun2020(i,1)) & (inun2020(i,2) > inun2020(i,4)) & (inun2020(i,2) > inun2020(i,5))) | ...
%                 ((inun2020(i,2) == inun2020(i,4)) & (inun2020(i,2) > inun2020(i,1)) & (inun2020(i,2) > inun2020(i,3)) & (inun2020(i,2) > inun2020(i,5))) | ...
%               ((inun2020(i,2) == inun2020(i,5)) & (inun2020(i,2) > inun2020(i,1)) & (inun2020(i,2) > inun2020(i,3)) & (inun2020(i,2) > inun2020(i,4))) | ...
%                 ((inun2020(i,3) == inun2020(i,4)) & (inun2020(i,3) > inun2020(i,1)) & (inun2020(i,3) > inun2020(i,2)) & (inun2020(i,3) > inun2020(i,5))) | ...
%               ((inun2020(i,3) == inun2020(i,5)) & (inun2020(i,3) > inun2020(i,1)) & (inun2020(i,3) > inun2020(i,2)) & (inun2020(i,3) > inun2020(i,4))) | ...
%               ((inun2020(i,4) == inun2020(i,5)) & (inun2020(i,4) > inun2020(i,1)) & (inun2020(i,4) > inun2020(i,2)) & (inun2020(i,4) > inun2020(i,3))));
%             inun2020prob(i,1) = NaN;
%             inun2020bin(i,1) = NaN;
%         else
%             disp('Error');
% 
%         end
%     end
%     region = horzcat(inun2020,change2020,inun2020prob,inun2020bin,data(:,idy),data(:,idx));
%     struct_array = struct('B1y20',region(:,1),'B2y20',region(:,2),'B3y20',region(:,3),'B4y20',region(:,4),'B5y20',region(:,5),'D20',region(:,6),'I20',region(:,7), ...
%      'PH20',region(:,8),'BH20',region(:,9),'Lat',region(:,10),'Lon',region(:,11));
% 
% % 2030-----------------------------------------------------------------------------------------------2030
% elseif yearIn == '2030';
%     inun2030 = pred.pdf.inundation;
%     change2030 = pred.pdf.change;
%     [L,m] = size(inun2030);
%     inun2030prob = zeros(L,1);
%     inun2030bin = zeros(L,1);
%     for i = 1:length(data(:,1));
%         if (inun2030(i,1) > inun2030(i,2)) & (inun2030(i,1) > inun2030(i,3)) & (inun2030(i,1) > inun2030(i,4)) & (inun2030(i,1) > inun2030(i,5));
%             inun2030prob(i,1) = inun2030(i,1);
%             inun2030bin(i,1) = 1;
%         elseif (inun2030(i,2) > inun2030(i,1)) & (inun2030(i,2) > inun2030(i,3)) & (inun2030(i,2) > inun2030(i,4))& (inun2030(i,2) > inun2030(i,5));
%             inun2030prob(i,1) = inun2030(i,2);
%             inun2030bin(i,1) = 2;
%         elseif (inun2030(i,3) > inun2030(i,1)) & (inun2030(i,3) > inun2030(i,2)) & (inun2030(i,3) > inun2030(i,4)) & (inun2030(i,3) > inun2030(i,5));
%             inun2030prob(i,1) = inun2030(i,3);
%             inun2030bin(i,1) = 3;
%         elseif (inun2030(i,4) > inun2030(i,1)) & (inun2030(i,4) > inun2030(i,2)) & (inun2030(i,4) > inun2030(i,3)) & (inun2030(i,4) > inun2030(i,5));
%             inun2030prob(i,1) = inun2030(i,4);
%             inun2030bin(i,1) = 4;
%         elseif (inun2030(i,5) > inun2030(i,1)) & (inun2030(i,5) > inun2030(i,2)) & (inun2030(i,5) > inun2030(i,3)) & (inun2030(i,5) > inun2030(i,4));
%             inun2030prob(i,1) = inun2030(i,5);
%             inun2030bin(i,1) = 5; 
%         elseif (((inun2030(i,1) == inun2030(i,2)) & (inun2030(i,1) > inun2030(i,3)) & (inun2030(i,1) > inun2030(i,4)) & (inun2030(i,1) > inun2030(i,5))) | ...
%                 ((inun2030(i,1) == inun2030(i,3)) & (inun2030(i,1) > inun2030(i,2)) & (inun2030(i,1) > inun2030(i,4)) & (inun2030(i,1) > inun2030(i,5))) | ...
%                 ((inun2030(i,1) == inun2030(i,4)) & (inun2030(i,1) > inun2030(i,2)) & (inun2030(i,1) > inun2030(i,3)) & (inun2030(i,1) > inun2030(i,5))) | ...
%                 ((inun2030(i,1) == inun2030(i,5)) & (inun2030(i,1) > inun2030(i,2)) & (inun2030(i,1) > inun2030(i,3)) & (inun2030(i,1) > inun2030(i,4))) | ...
%                 ((inun2030(i,2) == inun2030(i,3)) & (inun2030(i,2) > inun2030(i,1)) & (inun2030(i,2) > inun2030(i,4)) & (inun2030(i,2) > inun2030(i,5))) | ...
%                 ((inun2030(i,2) == inun2030(i,4)) & (inun2030(i,2) > inun2030(i,1)) & (inun2030(i,2) > inun2030(i,3)) & (inun2030(i,2) > inun2030(i,5))) | ...
%                 ((inun2030(i,2) == inun2030(i,5)) & (inun2030(i,2) > inun2030(i,1)) & (inun2030(i,2) > inun2030(i,3)) & (inun2030(i,2) > inun2030(i,4))) | ...
%                 ((inun2030(i,3) == inun2030(i,4)) & (inun2030(i,3) > inun2030(i,1)) & (inun2030(i,3) > inun2030(i,2)) & (inun2030(i,3) > inun2030(i,5))) | ...
%                 ((inun2030(i,3) == inun2030(i,5)) & (inun2030(i,3) > inun2030(i,1)) & (inun2030(i,3) > inun2030(i,2)) & (inun2030(i,3) > inun2030(i,4))) | ...
%                 ((inun2030(i,4) == inun2030(i,5)) & (inun2030(i,4) > inun2030(i,1)) & (inun2030(i,4) > inun2030(i,2)) & (inun2030(i,4) > inun2030(i,3))));
%             inun2030prob(i,1) = NaN;
%             inun2030bin(i,1) = NaN;
%         else
%             disp('Error');
%         end
%     end
%     region = horzcat(inun2030,change2030,inun2030prob,inun2030bin,data(:,idy),data(:,idx));
%     struct_array = struct('B1y30',region(:,1),'B2y30',region(:,2),'B3y30',region(:,3),'B4y30',region(:,4),'B5y30',region(:,5),'D30',region(:,6),'I30',region(:,7), ...
%      'PH30',region(:,8),'BH30',region(:,9),'Lat',region(:,10),'Lon',region(:,11));
% 
% % 2050-----------------------------------------------------------------------------------------------2050
% elseif yearIn == '2050';
%     inun2050 = pred.pdf.inundation;
%     change2050 = pred.pdf.change;
%     [L,m] = size(inun2050);
%     inun2050prob = zeros(L,1);
%     inun2050bin = zeros(L,1);
%     for i = 1:length(data(:,1));
%         if (inun2050(i,1) > inun2050(i,2)) & (inun2050(i,1) > inun2050(i,3)) & (inun2050(i,1) > inun2050(i,4)) & (inun2050(i,1) > inun2050(i,5));
%             inun2050prob(i,1) = inun2050(i,1);
%             inun2050bin(i,1) = 1;
%         elseif (inun2050(i,2) > inun2050(i,1)) & (inun2050(i,2) > inun2050(i,3)) & (inun2050(i,2) > inun2050(i,4)) & (inun2050(i,2) > inun2050(i,5));
%             inun2050prob(i,1) = inun2050(i,2);
%             inun2050bin(i,1) = 2;
%         elseif (inun2050(i,3) > inun2050(i,1)) & (inun2050(i,3) > inun2050(i,2)) & (inun2050(i,3) > inun2050(i,4)) & (inun2050(i,3) > inun2050(i,5));
%             inun2050prob(i,1) = inun2050(i,3);
%             inun2050bin(i,1) = 3;
%         elseif (inun2050(i,4) > inun2050(i,1)) & (inun2050(i,4) > inun2050(i,2)) & (inun2050(i,4) > inun2050(i,3)) & (inun2050(i,4) > inun2050(i,5));
%             inun2050prob(i,1) = inun2050(i,4);
%             inun2050bin(i,1) = 4;
%         elseif (inun2050(i,5) > inun2050(i,1)) & (inun2050(i,5) > inun2050(i,2)) & (inun2050(i,5) > inun2050(i,3)) & (inun2050(i,5) > inun2050(i,4));
%             inun2050prob(i,1) = inun2050(i,5);
%             inun2050bin(i,1) = 5; 
%         elseif (((inun2050(i,1) == inun2050(i,2)) & (inun2050(i,1) > inun2050(i,3)) & (inun2050(i,1) > inun2050(i,4)) & (inun2050(i,1) > inun2050(i,5))) | ...
%                 ((inun2050(i,1) == inun2050(i,3)) & (inun2050(i,1) > inun2050(i,2)) & (inun2050(i,1) > inun2050(i,4)) & (inun2050(i,1) > inun2050(i,5))) | ...
%                 ((inun2050(i,1) == inun2050(i,4)) & (inun2050(i,1) > inun2050(i,2)) & (inun2050(i,1) > inun2050(i,3)) & (inun2050(i,1) > inun2050(i,5))) | ...
%               ((inun2050(i,1) == inun2050(i,5)) & (inun2050(i,1) > inun2050(i,2)) & (inun2050(i,1) > inun2050(i,3)) & (inun2050(i,1) > inun2050(i,4))) | ...
%                 ((inun2050(i,2) == inun2050(i,3)) & (inun2050(i,2) > inun2050(i,1)) & (inun2050(i,2) > inun2050(i,4)) & (inun2050(i,2) > inun2050(i,5))) | ...
%                 ((inun2050(i,2) == inun2050(i,4)) & (inun2050(i,2) > inun2050(i,1)) & (inun2050(i,2) > inun2050(i,3)) & (inun2050(i,2) > inun2050(i,5))) | ...
%               ((inun2050(i,2) == inun2050(i,5)) & (inun2050(i,2) > inun2050(i,1)) & (inun2050(i,2) > inun2050(i,3)) & (inun2050(i,2) > inun2050(i,4))) | ...
%                 ((inun2050(i,3) == inun2050(i,4)) & (inun2050(i,3) > inun2050(i,1)) & (inun2050(i,3) > inun2050(i,2)) & (inun2050(i,3) > inun2050(i,5))) | ...
%               ((inun2050(i,3) == inun2050(i,5)) & (inun2050(i,3) > inun2050(i,1)) & (inun2050(i,3) > inun2050(i,2)) & (inun2050(i,3) > inun2050(i,4))) | ...
%               ((inun2050(i,4) == inun2050(i,5)) & (inun2050(i,4) > inun2050(i,1)) & (inun2050(i,4) > inun2050(i,2)) & (inun2050(i,4) > inun2050(i,3))));
%             inun2050prob(i,1) = NaN;
%             inun2050bin(i,1) = NaN;
%         else
%             disp('Error');
% 
%         end
%     end
%     region = horzcat(inun2050,change2050,inun2050prob,inun2050bin,data(:,idy),data(:,idx));
%     struct_array = struct('B1y50',region(:,1),'B2y50',region(:,2),'B3y50',region(:,3),'B4y50',region(:,4),'B5y50',region(:,5),'D50',region(:,6),'I50',region(:,7), ...
%      'PH50',region(:,8),'BH50',region(:,9),'Lat',region(:,10),'Lon',region(:,11));
% 
% % 2080-----------------------------------------------------------------------------------------------2080 
% elseif yearIn == '2080';
%     inun2080 = pred.pdf.inundation;
%     change2080 = pred.pdf.change;
%     [L,m] = size(inun2080);
%     inun2080prob = zeros(L,1);
%     inun2080bin = zeros(L,1);
%     for i = 1:length(data(:,1));
%         if (inun2080(i,1) > inun2080(i,2)) & (inun2080(i,1) > inun2080(i,3)) & (inun2080(i,1) > inun2080(i,4)) & (inun2080(i,1) > inun2080(i,5));
%             inun2080prob(i,1) = inun2080(i,1);
%             inun2080bin(i,1) = 1;
%         elseif (inun2080(i,2) > inun2080(i,1)) & (inun2080(i,2) > inun2080(i,3)) & (inun2080(i,2) > inun2080(i,4))& (inun2080(i,2) > inun2080(i,5));
%             inun2080prob(i,1) = inun2080(i,2);
%             inun2080bin(i,1) = 2;
%         elseif (inun2080(i,3) > inun2080(i,1)) & (inun2080(i,3) > inun2080(i,2)) & (inun2080(i,3) > inun2080(i,4)) & (inun2080(i,3) > inun2080(i,5));
%             inun2080prob(i,1) = inun2080(i,3);
%             inun2080bin(i,1) = 3;
%         elseif (inun2080(i,4) > inun2080(i,1)) & (inun2080(i,4) > inun2080(i,2)) & (inun2080(i,4) > inun2080(i,3)) & (inun2080(i,4) > inun2080(i,5));
%             inun2080prob(i,1) = inun2080(i,4);
%             inun2080bin(i,1) = 4;
%         elseif (inun2080(i,5) > inun2080(i,1)) & (inun2080(i,5) > inun2080(i,2)) & (inun2080(i,5) > inun2080(i,3)) & (inun2080(i,5) > inun2080(i,4));
%             inun2080prob(i,1) = inun2080(i,5);
%             inun2080bin(i,1) = 5; 
%         elseif (((inun2080(i,1) == inun2080(i,2)) & (inun2080(i,1) > inun2080(i,3)) & (inun2080(i,1) > inun2080(i,4)) & (inun2080(i,1) > inun2080(i,5))) | ...
%               ((inun2080(i,1) == inun2080(i,3)) & (inun2080(i,1) > inun2080(i,2)) & (inun2080(i,1) > inun2080(i,4)) & (inun2080(i,1) > inun2080(i,5))) | ...
%               ((inun2080(i,1) == inun2080(i,4)) & (inun2080(i,1) > inun2080(i,2)) & (inun2080(i,1) > inun2080(i,3)) & (inun2080(i,1) > inun2080(i,5))) | ...
%               ((inun2080(i,1) == inun2080(i,5)) & (inun2080(i,1) > inun2080(i,2)) & (inun2080(i,1) > inun2080(i,3)) & (inun2080(i,1) > inun2080(i,4))) | ...
%               ((inun2080(i,2) == inun2080(i,3)) & (inun2080(i,2) > inun2080(i,1)) & (inun2080(i,2) > inun2080(i,4)) & (inun2080(i,2) > inun2080(i,5))) | ...
%               ((inun2080(i,2) == inun2080(i,4)) & (inun2080(i,2) > inun2080(i,1)) & (inun2080(i,2) > inun2080(i,3)) & (inun2080(i,2) > inun2080(i,5))) | ...
%               ((inun2080(i,2) == inun2080(i,5)) & (inun2080(i,2) > inun2080(i,1)) & (inun2080(i,2) > inun2080(i,3)) & (inun2080(i,2) > inun2080(i,4))) | ...
%               ((inun2080(i,3) == inun2080(i,4)) & (inun2080(i,3) > inun2080(i,1)) & (inun2080(i,3) > inun2080(i,2)) & (inun2080(i,3) > inun2080(i,5))) | ...
%               ((inun2080(i,3) == inun2080(i,5)) & (inun2080(i,3) > inun2080(i,1)) & (inun2080(i,3) > inun2080(i,2)) & (inun2080(i,3) > inun2080(i,4))) | ...
%               ((inun2080(i,4) == inun2080(i,5)) & (inun2080(i,4) > inun2080(i,1)) & (inun2080(i,4) > inun2080(i,2)) & (inun2080(i,4) > inun2080(i,3))));
%             inun2080prob(i,1) = NaN;
%             inun2080bin(i,1) = NaN;
%         else
%             disp('Error');
%         end
%     end
%     region = horzcat(inun2080,change2080,inun2080prob,inun2080bin,data(:,idy),data(:,idx));
%     struct_array = struct('B1y80',region(:,1),'B2y80',region(:,2),'B3y80',region(:,3),'B4y80',region(:,4),'B5y80',region(:,5),'D80',region(:,6),'I80',region(:,7), ...
%      'PH80',region(:,8),'BH80',region(:,9),'Lat',region(:,10),'Lon',region(:,11));
% 
% % end of years --------------------------------------------------------
% else
%     disp('Error with yearIn')
% end
% 
% gp = geopoint(struct_array);
% eval(shapepath)
% 
% %shapename = sprintf('%s_%s', datafilename,yearIn)
% %shapewrite(gp,shapename);
% shapewrite(gp,shpName);
% 
% cd I:\NECSC\toMatlab


% %% Output region with headers % Create Table (Need to adjust precision first!!!)
% outfile = 'I:\NECSC\fromMatlab\randAll.txt'; % Output path
% headers = {'Bin1','Bin2','Bin3','Bin4','Bin5','Dynamic','Inundate','Year','ID'}; %fields
% %data2 = horzcat(inun,change,latlon);
% fid = fopen(outfile, 'w');
% if fid == -1; error('Cannot open file: %s', outfile); end
% fprintf(fid, '%s,', headers{:});
% fprintf(fid, '\n');
% fclose(fid);
% dlmwrite(outfile,randAll,'delimiter',',','-append');


%% post process
% for j=1:N
%     for i=1:length(nodeNamesOut)
%         pdf = pred.pdf.(nodeNamesOut{i})(j,:);
%         pdfRanges = pred.ranges.(nodeNamesOut{i});
%         subplot(length(nodeNamesOut),1,i)
%         hp = plotPDF2(pdfRanges,pdf,1,'r');
%         if(i==1)
%             title(sprintf('location %02d', j))
%         end
%         switch nodeNamesOut{i}
%             case 'change'
%                 xlabel('Dynamic response --- OR --- Inundate')
%             otherwise
%         xlabel(nodeNamesOut{i}, 'interpreter','none')
%         ylabel('prob')
%         end
%     end
%     print('-dpng', sprintf('predict_loc%02d_%s.png', j, yearIn))
%     clf
% end
% 
% % want to see inputs too
% for j=1:N
%     for i=1:length(nodeNamesIn)
%         pdf = pred.pdf.(nodeNamesIn{i})(j,:);
%         pdfRanges = pred.ranges.(nodeNamesIn{i});
%         subplot(length(nodeNamesIn),1,i)
%         hp = plotPDF2(pdfRanges,pdf,1,'r');
%         if(i==1)
%             title(sprintf('location %02d', j))
%         end
%         switch nodeNamesIn{i}
%             case 'change'
%                 xlabel('Dynamic response --- OR --- Inundate')
%             otherwise
%         xlabel(nodeNamesIn{i}, 'interpreter','none')
%         ylabel('prob')
%         end
%     end
%     print('-dpng', sprintf('predict_loc%02d_%s_input.png', j, yearIn))
%     clf
% end
