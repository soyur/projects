/*

Contact:Sawyer Stippa
email:sawyer.stippa@gmail.com
date: 1/1/16

*/

var map;
var measurement, borehole, fossil, rock; //Tables
var bedrock, surficial; //wms layers
var keyworth = [-1.1028078 + 0.0155, 52.8745503 - 0.005]; // Map center (with offset)
var zoomLevel = 14; // Home zoom level
// WMS Layer
var theServer = "https://map.bgs.ac.uk";
var ghServer = "3d.bgs.ac.uk";
var lCount = 0; // Content load counter

require([
"esri/map",
"esri/layers/ArcGISDynamicMapServiceLayer",
"esri/layers/CSVLayer",
"esri/dijit/Search",
"esri/geometry/Point",
"esri/config",
"esri/SpatialReference",
"esri/Color",
"esri/symbols/SimpleMarkerSymbol",
"esri/renderers/SimpleRenderer",
"esri/InfoTemplate",
"esri/layers/LabelClass",
"esri/dijit/BasemapGallery",
"dojo/domReady!"
],
	function (
		Map,
		ArcGISDynamicMapServiceLayer,
		CSVLayer,
		Search,
		Point,
		esriConfig,
		SpatialReference,
		Color,
		SimpleMarkerSymbol,
		SimpleRenderer,
		InfoTemplate,
		LabelClass,
		BasemapGallery
	) {

		//esriConfig.defaults.io.proxyUrl = "/proxy/";
		//esriConfig.defaults.io.alwaysUseProxy = true;

		// Adjust zoom for mobile view
		if ($(window).width() < 420) {
			zoomLevel = 13;
		}

		// Init map
		map = new Map("map", {
			basemap: "topo",
			center: keyworth,
			zoom: zoomLevel,
			logo: false,
			slider: false,
			showLabels: true
		});

		//  Init search widget
		var search = new Search({
			map: map,
			maxSuggestions: 3
		}, "search");
		search.startup();
		search.on('load', function () {
			console.log('search initialized');
			lCount++
		});

		// Init basemap gallery widget
		var basemapGallery = new BasemapGallery({
			showArcGISBasemaps: true,
			map: map
		}, "basemapGallery");
		basemapGallery.startup();
		// Remove unwanted Basemaps (ie low resolution)
		basemapGallery.on('load', function () {
			$('#galleryNode_basemap_0').remove();
			$('#galleryNode_basemap_1').remove();
			$('#galleryNode_basemap_3').remove();
			$('#galleryNode_basemap_4').remove();
			$('#galleryNode_basemap_5').remove();
			$('.esriBasemapGalleryNode').click(function () {
				var noneSRC = $('.esriBasemapGallerySelectedNode >a > img').attr('src');
				console.log(noneSRC)
				$('ul.dropdown-menu.layer > li:last > img').attr('src', noneSRC)
			});
			$('#basemapGallery').mouseleave(function () {
				//$(this).scrollTop(1);
				$(this).animate({
					scrollTop: 0
				}, 300);
			});
			console.log('basemap gallery loaded');
			lCount++
		});

		// Zoom events
		$('.glyphicon-zoom-in').parent().click(function () {
			map.centerAndZoom(map.extent.getCenter(), map.getZoom() + 1)
		});

		$('.glyphicon-zoom-out').parent().click(function () {
			map.centerAndZoom(map.extent.getCenter(), map.getZoom() - 1)
		});

		$('.glyphicon-globe').parent().click(function () {
			map.centerAndZoom(keyworth, zoomLevel)
		});


		//Geology layers
		bedrock = new ArcGISDynamicMapServiceLayer("https://map.bgs.ac.uk/arcgis/rest/services/GeologyOfBritain/GeologyViewer_Bedrock/MapServer?", {
			opacity: 0.5,
		});
		bedrock.on('load', function () {
			console.log('bedrock layer loaded');
			lCount++
		})

		surficial = new ArcGISDynamicMapServiceLayer("https://map.bgs.ac.uk/arcgis/rest/services/GeologyOfBritain/GeologyViewer_Superficial/MapServer", {
			opacity: 0.5
		});
		surficial.on('load', function () {
			console.log('surficial layer loaded');
			lCount++
		})


		// Init Data Labels
		var json = {
			"labelExpressionInfo": {
				"value": "{Name}",
			},
			"labelPlacement": 'above-right',
		};
		var labelClass = new LabelClass(json);

		// ---> Measurement <---
		//Name	Type	Latitude (WGS84)	Longitude (WGS84)	Z	Date	Time	Recorded by	Porosity

		measurement = new CSVLayer("tables/measurement.txt", {
			latitudeFieldName: "Latitude (WGS84)",
			longitudeFieldName: "Longitude (WGS84)",
			columnDelimiter: "\t",
			showLabel: true,
			outFields: ['*']
		});

		var mMarker = new SimpleMarkerSymbol();
		mMarker.setColor(new Color([0, 255, 0, 1]));
		mMarker.setSize(10);
		mMarker.setOutline(null);
		measurement.setRenderer(new SimpleRenderer(mMarker));

		var mTemplate = new InfoTemplate("Measurement ${Name}",
			"<center><b>Porosity:</b> ${Porosity}</center><br>" +
			"<b>Latitude:</b> ${Latitude (WGS84)}<br>" +
			"<b>Longitude:</b> ${Longitude (WGS84)}<br>" +
			"<b>Elevation:</b>  ${Z}<br>" +
			"<b>Date:</b> ${Date}<br>" +
			"<b>Time:</b>  ${Time}<br>");

		measurement.setInfoTemplate(mTemplate);
		measurement.setLabelingInfo([labelClass]);

		measurement.on('load', function () {
			map.addLayer(measurement);
			parseTables(measurement);
			console.log('measurement csv loaded');
			lCount++;
		});

		// ---> Fossil <---
		//Name	Type	Latitude (WGS84)	Longitude (WGS84)	Z	Date	Time	Recorded by	Species	Image
		fossil = new CSVLayer("tables/fossil.txt", {
			latitudeFieldName: "Latitude (WGS84)",
			longitudeFieldName: "Longitude (WGS84)",
			columnDelimiter: "\t",
			showLabel: true,
			outFields: ['*']
		});

		var fMarker = new SimpleMarkerSymbol();
		fMarker.setColor(new Color([0, 112, 255, 1]));
		fMarker.setSize(10);
		fMarker.setOutline(null);
		fossil.setRenderer(new SimpleRenderer(fMarker));

		var fTemplate = new InfoTemplate("Fossil ${Name}",
			"<center><b>Species:</b>  ${Species}<br><br>" +
			"<img src='images/${Image}' width='200px' /></center><br>" +
			"<b>Latitude:</b> ${Latitude (WGS84)}<br>" +
			"<b>Longitude:</b> ${Longitude (WGS84)}<br>" +
			"<b>Elevation:</b>  ${Z}<br>" +
			"<b>Date:</b> ${Date}<br>" +
			"<b>Time:</b>  ${Time}<br>")
		fossil.setInfoTemplate(fTemplate);
		fossil.setLabelingInfo([labelClass]);

		fossil.on('load', function () {
			map.addLayer(fossil);
			parseTables(fossil);
			console.log('fossil csv loaded');
			lCount++
		});


		// ---> Borehole <---
		//Name	Type	Latitude (WGS84)	Longitude (WGS84)	Z	Date	Time	Recorded by	Drilled depth (m)
		borehole = new CSVLayer("tables/borehole.txt", {
			latitudeFieldName: "Latitude (WGS84)",
			longitudeFieldName: "Longitude (WGS84)",
			columnDelimiter: "\t",
			showLabel: true,
			outFields: ['*']
		});

		var bMarker = new SimpleMarkerSymbol();
		bMarker.setColor(new Color([255, 0, 0, 1]));
		bMarker.setSize(10);
		bMarker.setOutline(null);
		borehole.setRenderer(new SimpleRenderer(bMarker));

		var bTemplate = new InfoTemplate("Borehole ${Name}",
			"<center><b>Drill Depth (m):</b>  ${Drilled depth (m)}</center><br>" +
			"<b>Latitude:</b> ${Latitude (WGS84)}<br>" +
			"<b>Longitude:</b> ${Longitude (WGS84)}<br>" +
			"<b>Elevation:</b>  ${Z}<br>" +
			"<b>Date:</b> ${Date}<br>" +
			"<b>Time:</b>  ${Time}<br>");
		borehole.setInfoTemplate(bTemplate);
		borehole.setLabelingInfo([labelClass]);

		borehole.on('load', function () {
			map.addLayer(borehole);
			parseTables(borehole);
			console.log('borehole csv loaded');
			lCount++
		});


		// ---> Rock <---
		//Name	Type	Latitude (WGS84)	Longitude (WGS84)	Z	Date	Time	Recorded by	Rock name	Image
		rock = new CSVLayer("tables/rock.txt", {
			latitudeFieldName: "Latitude (WGS84)",
			longitudeFieldName: "Longitude (WGS84)",
			columnDelimiter: "\t",
			showLabel: true,
			outFields: ['*']
		});

		var rMarker = new SimpleMarkerSymbol();
		rMarker.setColor(new Color([255, 255, 0, 1]));
		rMarker.setSize(10);
		rMarker.setOutline(null);
		rock.setRenderer(new SimpleRenderer(rMarker));

		var rTemplate = new InfoTemplate("Rock ${Name}",
			"<center><b>Rock:</b>  ${Rock name}<br><br>" +
			"<img src='images/${Image}' width='200px'/></center><br>" +
			"<b>Latitude:</b> ${Latitude (WGS84)}<br>" +
			"<b>Longitude:</b> ${Longitude (WGS84)}<br>" +
			"<b>Elevation:</b>  ${Z}<br>" +
			"<b>Date:</b> ${Date}<br>" +
			"<b>Time:</b>  ${Time}<br>");
		rock.setInfoTemplate(rTemplate);
		rock.setLabelingInfo([labelClass]);

		rock.on('load', function () {
			map.addLayer(rock);
			parseTables(rock);
			console.log('rock csv loaded');
			lCount++
		});

		// Generate dropup tables for nav bar

		function parseTables(layer) {
			var id = layer.graphics[0].attributes['Type'].toLowerCase();
			// Append Headers
			$('#' + id + 'DATA').append('<tr></tr>')
			for (var a = 1; a < Object.keys(layer.graphics[0].attributes).length; a++) {
				$('#' + id + 'DATA > tr:last').append('<th>' + layer.fields[a].name + '</th>');
			}
			// Append Data
			for (var i = 0; i < 4; i++) {
				var row = layer.graphics[i].attributes;
				$('#' + id + 'DATA').append('<tr></tr>')
				for (var a = 1; a < Object.keys(row).length; a++) {
					if (layer.fields[a].name === 'Name') {
						$('#' + id + 'DATA > tr:last').append('<td id=' + row[layer.fields[a].name] + '>' + row[layer.fields[a].name] + '</td>');
					} else {
						$('#' + id + 'DATA > tr:last').append('<td>' + row[layer.fields[a].name] + '</td>');
					}
				}
			}

			$('#' + id + 'DATA > tr').click(function () {
				var dataPoint = $(this).children('td').eq(0).html();
				for (var g = 0; g < 4; g++) {
					if (dataPoint === layer.graphics[g].attributes['Name']) {
						map.centerAndZoom(layer.graphics[g].geometry, 19)
					}
				}
			})
		}

		$('.dataTable > tr').click(function () {
			var dataPoint = $(this).children('td').eq(0).html();
			var dataList = [borehole, measurement, fossil, rock];
			for (var d = 0; d < 4; d++) {
				for (var g = 0; g < 4; g++) {
					if (dataPoint === dataList[d].graphics[g].attributes['Name']) {
						map.centerAndZoom(dataList[d].graphics[g].geometry, 19)
					}
				}
			}
		})

		// Nav Colors 

		// select object
		$('.nav.navbar-nav > li').click(function () {
			$(".nav").find(".active").removeClass("active");
			$(this).addClass("active");

		});

		var returnColor = function () {
			$(this).css('color', '#9d9d9d')
		};

		// Borehole
		$('.nav.navbar-nav').find('a').eq(0).hover(
			function () {
				$(this).css('color', 'rgb(255, 0, 0)');
			}, returnColor
		);

		// Measurement
		$('.nav.navbar-nav').find('a').eq(1).hover(
			function () {
				$(this).css('color', 'rgb(85, 255, 0)');
			}, returnColor
		);

		// Fossil
		$('.nav.navbar-nav').find('a').eq(2).hover(
			function () {
				$(this).css('color', 'rgb(0, 112, 255)');
			}, returnColor
		);

		// Rock
		$('.nav.navbar-nav').find('a').eq(3).hover(
			function () {
				$(this).css('color', 'rgb(255, 255, 0)');
			}, returnColor
		);


		// Geology dropdown events

		// Hover
		$(".dropdown-menu.layer > li > img").hover(function () {
			if ($(this).attr('src') === 'icons/bedrock.png') {
				$('#dropdownMenu1').html('Bedrock')
			} else if ($(this).attr('src') === 'icons/surficial.png') {
				$('#dropdownMenu1').html('Surficial')
			} else {
				$('#dropdownMenu1').html('None')
			}
		}, function () {
			$('#dropdownMenu1').html('Geology');
		})

		// Click
		$(".dropdown-menu.layer > li > img").click(function () {
			$(this).off('mouseleave');
			if ($(this).attr('src') === 'icons/bedrock.png') {
				map.centerAndZoom(keyworth, 14)
				map.removeLayer(surficial)
				map.addLayer(bedrock)
				$('#dropdownMenu1').html('Bedrock <span class="caret"></span>')
			} else if ($(this).attr('src') === 'icons/surficial.png') {
				map.centerAndZoom(keyworth, 14)
				map.removeLayer(bedrock)
				map.addLayer(surficial)
				$('#dropdownMenu1').html('Surficial <span class="caret"></span>')
			} else {
				map.removeLayer(bedrock)
				map.removeLayer(surficial)
				$('#dropdownMenu1').html('Geology <span class="caret"></span>')
			}
		})

		// Content load test 
		$(window).load(function () {
			setTimeout(function () {
				if (lCount != 8) {
					alert('Error loading content. Please try again.')
				}
			}, 4000)
		});


	}); // End Require




/*

//// Openlayer option

$(document).ready(init);

function init() {
var keyworth = [-1.1028078, 52.8745503]
var measurement = new ol.layer.Vector({
source: new ol.source.Vector({
		url: 'data/measurement.geojson',
		format: new ol.format.GeoJSON(),
})
})

var borehole = new ol.layer.Vector({
source: new ol.source.Vector({
		url: 'data/borehole.geojson',
		format: new ol.format.GeoJSON(),
		projection: 'EPSG:3857',

})
})

var fossil = new ol.layer.Vector({
source: new ol.source.Vector({
		url: 'data/fossil.geojson',
		format: new ol.format.GeoJSON()
})
})

var rock = new ol.layer.Vector({
source: new ol.source.Vector({
		url: 'data/rock.geojson',
		format: new ol.format.GeoJSON(),
})
})
var map = new ol.Map({
controls: ol.control.defaults,
target: 'map',
layers: [
		new ol.layer.Tile({
				source: new ol.source.OSM()
		}),
		measurement,
		borehole,
		fossil,
		rock
],
view: new ol.View({
		center: ol.proj.fromLonLat(keyworth),
		zoom: 13
}),
});
}
*/