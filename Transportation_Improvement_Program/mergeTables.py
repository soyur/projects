"""
File name: mergeTables.py
Description:
    - Generates master TIP table for use in TIP Application
Inputs: (SDE.gis_prod.planning)
    (1) TIP_PointsData
    (2) TIP_LinesData
    (3) TIP_ProjectData (no geometry)
Outputs: (\\gisprod2\APPLICATIONS\TIP_v2\TIP.gdb)
    (1) TIP Table
 
Author: Sawyer Stippa sstippa@dutchessny.gov
created: 4/11/17
Modified: 5/10/17
"""
import arcpy

def genMasterTIP():
	root = r'\\gisprod2\ARCGISSERVER\APPLICATIONS\TIP_v2'
	
	ws = root + '\TIP.gdb' # Where Master TIP resides
	arcpy.env.workspace = ws
	arcpy.env.overwriteOutput = True
	
	# Pre-compiled SDE Connection to Planning DB - as DCGIS user
	SDE = root + '\connection_planning.sde'

	points = SDE + '\gis_planning.DCGIS.TIP_PointsData' # TIP Lines in SDE
	lines =  SDE + '\gis_planning.DCGIS.TIP_LinesData' # TIP Points in SDE
	none = SDE + '\gis_planning.DCGIS.TIP_ProjectData' # TIP Table in SDE

	# Convert FC's to tables to remove geometry and merge
	print 'Converting Point and Line Feature Classes to Tables'
	arcpy.TableToTable_conversion(points,ws,'points_table')
	arcpy.TableToTable_conversion(lines,ws,'lines_table')

	# Merge all tables - 1. Lines 2. Points 3. Table 
	print 'Merging Tables'
	arcpy.Merge_management([none,'lines_table','points_table'], 'TIP')
	
	#Add Field Alias 
	fields = {"PIN":"PORJECT ID","LOCATION_MUNICIPALITY":"LOCATION","PROJECT_TYPE":"TYPE","CONSTRUCTION_YEAR":"CONSTRUCTION YEAR"}
	for field in fields:
		arcpy.AlterField_management('TIP', field, new_field_alias=fields[field])

	# Remove Temp Tables
	print 'Removing Temp Tables'
	for rem in ['lines_table','points_table']:
		arcpy.Delete_management(rem)
	
	print 'Processing Complete'
	
# run script from file
if __name__ == '__main__':
	genMasterTIP()