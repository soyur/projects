var map, searchWidgetNav, searchWidgetPanel, activeView;
var gsvc; //Geometry Service
var desktop = 1200 //Desktop Width break in pixels
var mobile = 480 //Mobile (phone) Width break in pixels
var my_date = new Date();
var printer;
var webversion = 'Intranet';
var identifyTask, identifyParams; //infoTemplate Gen
var firstResult;
var selectedItem;
var zoomInFactor = 0.65,
    zoomOutFactor = 1.55
var dragCheck = false; // Disable click in drag
var Defaults = [38, 11, 3, 4, 43, 44, 53],
    Shields = 52,
    RoadLabels = 1,
    CountyRoads = 2,
    Counties = 5,
    Signs = 6,
    DMIPoints = 7,
    Bridges = 8,
    TrafficSignals = 9,
    StormSewerDischarge = 10,
    Parcels = 11,
    Classifications = 12,
    PassingZones = 13,
    Culverts = 49,
    Road = [16, 15, 2], //Roads = 15, RoadArea = 16,  2 = County Road
    Soils = 17,
    Terrain = [19, 20, 21, 22, 23, 24, 25],
    Hydro = [27, 28, 29, 30, 31, 32, 33, 34],
    Planimetric = [36, 37, /*38,*/ 39, 40, /*41,*/ 42, 43, 44, 45], // Structures added to Defaults, parks hidden
    Aerials = 46,
    Intersections = 47,
    AddressPoints = 48,
    Traffic = 50,
    SpeedLimits = 51,
    Inspection = 68,
    nonSelect = [19, 20, 22, 25, 29, 30, 31, 34, 42, 43, 44, 45, 46, 53], //Contour 25ft, Hillshades, wetlands, streams/hydro, forests
    selectableLayers = [6, 7, 8, 9, 10, 11, 12, 13];

var options = []; //Init layer select
var visibleLayerIds = []; // List of visible layers (toggle)
var serverURL = "http://gisprod2:6080/arcgis/rest/services/RIMS/MapServer";
var printService = "http://gisprod2:6080/arcgis/rest/services/PrintServiceDutchess/GPServer/Export%20Web%20Map";
var geometryServer = "http://gisprod2:6080/arcgis/rest/services/Utilities/Geometry/GeometryServer";
var loading; // Layer loding gif
var popup,
    popOnSelect,
    popOnClear,
    popOnSet,
    selectFeature,
    initSelectFeature;
var legend;
var distance;
var geodesic = false; //geodesic measurement
//county extents at full scale
var xmin = 623754,
    ymin = 948369,
    xmax = 780730,
    ymax = 1188759;
var spr = 2260; // Spatial Reference ID
// Zoom factors
var panorama;
var heading,
    headingSym,
    headingGraphic,
    manSym,
    manGraphic;
var gsvPOS; //gsv cursor position
var zoomInFactor = 0.65,
    zoomOutFactor = 1.55
var searchScale = 1253.4820393975292;
var startExtent; // Initial Map Extent object
require(["esri/map",
    "esri/geometry/Extent",
    "esri/SpatialReference",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/dijit/Search",
    "dojo/query",
    "esri/tasks/query",
    "esri/tasks/QueryTask",
    "esri/geometry/Polyline",
    "esri/geometry/Point",
    "esri/InfoTemplate",
    "esri/dijit/InfoWindow",
    "esri/tasks/IdentifyTask",
    "esri/tasks/IdentifyParameters",
    "esri/dijit/Popup",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/PictureMarkerSymbol",
    "esri/Color",
    "dojo/dom-construct",
    "dojo/_base/array",
    "dojo/on",
    "dojo/_base/connect",
    "dojo/dom",
    "dijit/registry",
    "esri/domUtils",
    "esri/SnappingManager",
    "esri/dijit/Measurement",
    "esri/tasks/GeometryService",
    "esri/sniff",
    "dojo/keys",
    "esri/layers/JoinDataSource",
    "esri/layers/TableDataSource",
    "esri/graphic",
    "esri/layers/GraphicsLayer",
    "esri/geometry/Multipoint",
    "dojo/dom-style",
    "esri/dijit/Legend",
    "esri/layers/FeatureLayer",
    "esri/renderers/SimpleRenderer",
    "esri/tasks/ProjectParameters",
    "esri/geometry/webMercatorUtils",
    "dojo/parser",
    "esri/tasks/DistanceParameters",
    "esri/dijit/Print",
    "esri/tasks/PrintTemplate",
    "dijit/focus",

    "bootstrap/Collapse",
    "bootstrap/Dropdown",
    "bootstrap/Tab",
    "bootstrap/Tooltip",
    "calcite-maps/calcitemaps",
    "dojo/domReady!"
], function(
    Map,
    Extent,
    SpatialReference,
    ArcGISDynamicMapServiceLayer,
    Search,
    query,
    Query,
    QueryTask,
    Polyline,
    Point,
    InfoTemplate,
    InfoWindow,
    IdentifyTask,
    IdentifyParameters,
    Popup,
    SimpleFillSymbol,
    SimpleLineSymbol,
    SimpleMarkerSymbol,
    PictureMarkerSymbol,
    Color,
    domConstruct,
    arrayUtils,
    on,
    connect,
    dom,
    registry,
    domUtils,
    SnappingManager,
    Measurement,
    GeometryService,
    has,
    keys,
    JoinDataSource,
    TableDataSource,
    Graphic,
    GraphicsLayer,
    Multipoint,
    domStyle,
    Legend,
    FeatureLayer,
    SimpleRenderer,
    ProjectParameters,
    webMercatorUtils,
    parser,
    DistanceParameters,
    Print,
    PrintTemplate,
    focusUtil) {

    /*==============================
    =            Config            =
    ==============================*/

    startExtent = new Extent(xmin, ymin, xmax, ymax, new SpatialReference({
        wkid: spr
    }));

    gsvc = new GeometryService(geometryServer);

    esriConfig.defaults.geometryService = gsvc;
    //esriConfig.defaults.io.corsDetection = false;

    /*===========================
   =            Map            =
   ===========================*/

    map = new Map("mapViewDiv", {
        logo: false,
        slider: false,
    });

    map.setExtent(startExtent);

    map.on('mouse-move', function(event) {
        var x = Math.round(event.mapPoint.x * 1000) / 1000
        var y = Math.round(event.mapPoint.y * 1000) / 1000
        $('#cursorLocation').text('X: ' + x.toFixed(3) + ' Y: ' + y.toFixed(3));
    });

    map.on('click', function(event) {
        // for measuring distance
        start = event.mapPoint;
    })


    /*==================================
    =            Add Layers            =
    ==================================*/


    counties = new ArcGISDynamicMapServiceLayer(serverURL);
    counties.setVisibleLayers([Counties]);
    map.addLayer(counties);

    defaultLayer = new ArcGISDynamicMapServiceLayer(serverURL);
    defaultLayer.setVisibleLayers(Defaults);
    map.addLayer(defaultLayer);

    roadLayer = new ArcGISDynamicMapServiceLayer(serverURL);
    roadLayer.setVisibleLayers(Road);
    map.addLayer(roadLayer)

    aerialLayer = new ArcGISDynamicMapServiceLayer(serverURL);
    aerialLayer.setVisibleLayers([Aerials]);

    hydroLayer = new ArcGISDynamicMapServiceLayer(serverURL);
    hydroLayer.setVisibleLayers(Hydro);

    terrainLayer = new ArcGISDynamicMapServiceLayer(serverURL);
    terrainLayer.setVisibleLayers(Terrain);

    soilsLayer = new ArcGISDynamicMapServiceLayer(serverURL);
    soilsLayer.setVisibleLayers([Soils]);

    planimetricLayer = new ArcGISDynamicMapServiceLayer(serverURL);
    planimetricLayer.setVisibleLayers(Planimetric);

    layers = new ArcGISDynamicMapServiceLayer(serverURL);
    layers.setVisibleLayers(visibleLayerIds);
    map.addLayer(layers);

    roadLabels = new ArcGISDynamicMapServiceLayer(serverURL);
    roadLabels.setVisibleLayers([RoadLabels]);
    map.addLayer(roadLabels);

    crLabels = new ArcGISDynamicMapServiceLayer(serverURL);
    crLabels.setVisibleLayers([Shields]);
    map.addLayer(crLabels);


    /*================================
    =            Controls            =
    ================================*/

    // --->>> Zoom in <<<--- //
    $('#zoomIn').click(function() {
        map.centerAndZoom(map.extent.getCenter(), zoomInFactor)
    })

    // --->>> Zoom out <<<--- //
    $('#zoomOut').click(function() {
        map.centerAndZoom(map.extent.getCenter(), zoomOutFactor)
    })

    // --->>> Zoom to extent <<<--- // 
    $('#zoomToExtent').click(function() {
        map.setExtent(startExtent)
    })

    // --->>> Panel control event <<<--- // 
    $(document).ready(function() {

        // Show panels if desktop
        if ($('.navbar-toggle').css('display') === 'none') {
            $('.panel.panel-default').each(function(count, item) {
                var id = '#' + item.id
                query(id).collapse('show');
            })
        }

        query('#collapseLayers').collapse('show');

        $('.control,.panel-heading').mouseup(togglePanels);


        $('.panel-expand').click(function() {

            var usableHeight = $('body').height() - 85
            var currentHeight = $('#panelAccordion').height()
            var addHeight = usableHeight - currentHeight
            var maxHeight = parseInt($('#infoContainer').css('max-height'), 10)
            var newMax = maxHeight + addHeight
                //expand()

            $('#infoContainer').css('max-height') === '180px' ? expand() : collapse();

            function expand() {
                $('.panel-container').css({
                    'top': '65px',
                    'right': '0px',
                    'left': 'auto'
                })
                $('#infoContainer').css({
                    'max-height': newMax + 'px',
                    //'height': 'auto'
                })
            }

            function collapse() {
                $('#infoContainer').css({
                    'max-height': '180px',
                    //'height': 'auto'
                })
            }
        })

        $('.clearInfo').click(function() {

            popup.clearFeatures();
            //map.graphics.remove(highPointGraphic);
            map.graphics.clear();
        })
    })


    // --->>> Tool controls <<<--- // 
    function togglePanels(evt, showInfo) {

        // toggle tool on click event, not drag
        // Collapse.js DefaultOptions Toggle: false
        if (dragCheck == false) {
            // Panel hides/shows everything
            // Collapses hides/shows content but keeps header
            var panel, collapse;

            if (showInfo) {
                panel = '#panelInfo';
                collapse = '#collapseInfo';
                panelIn();
                $('.panel-collapse').each(function(count, item) {
                    '#' + $(this).attr('id') === collapse ? query('#' + $(this).attr('id')).collapse('show') : query('#' + $(this).attr('id')).collapse('hide');
                })
            } else {
                panel = '#panel' + $(this).val();
                collapse = '#collapse' + $(this).val();
                panelIn();
                $('.panel-collapse').each(function(count, item) {
                    '#' + $(this).attr('id') === collapse ? query('#' + $(this).attr('id')).collapse('toggle') : query('#' + $(this).attr('id')).collapse('hide');
                })
            }

            function panelIn() {

                if (!$(panel).hasClass('in')) {
                    $(panel).addClass('in').css('height', 'auto')
                    query(collapse).collapse('show');
                }
            }
        }

        $('#infoContainer').css({
            'max-height': '180px'
        })
    }


    // GSV control hover
    $('#gcsCTR').hover(function() {
        $('#gcsCTR img').attr('src', './StreetView/images/icon.png')
    }, function() {
        $('#gcsCTR img').attr('src', './StreetView/images/icon_glyph.png')
    })

    // Measure control hover
    $('#measureCTR').hover(function() {
        $('#measureCTR img').attr('src', './images/ruler_icon_white.png')
    }, function() {
        $('#measureCTR img').attr('src', './images/ruler_icon.png')
    })

    /*==========================================
    =            Get Search Results            =
    ==========================================*/

    function searchString(request, response) {
        var addressList = [];
        var roadList = [];
        var bridgeList = [];
        var culvertList = [];
        var sewerDischargeList = [];
        searchAddresses();

        function searchAddresses() {
            $.ajax({
                url: serverURL + "/" + AddressPoints + "/query",
                dataType: "jsonp", // with this dataType, cache is set to false by default
                data: {
                    where: "FULLADDRESS LIKE '%" + request.term.replace(/\'/g, '\'\'') + "%'", //makes single quotes into double for sql  
                    outFields: "FULLADDRESS, MUNICIPALITY",
                    returnGeometry: false,
                    f: "pjson"
                },
                success: function(data) {
                    $('#autocomplete').removeClass('ui-autocomplete-loading');
                    if (data.features) {

                        for (var i = 0; i < data.features.length; i++) {
                            var ind = ['./images/latlong-useAdd.png', data.features[i].attributes.FULLADDRESS, data.features[i].attributes.MUNICIPALITY, AddressPoints]

                            if (addressList.join().indexOf(ind.join().toString()) === -1) {
                                addressList.push(ind)
                            }
                        }

                        addressList = addressList.slice(0, 100) // only display first 100
                            // sorted alphabetically
                        addressList.sort(function(a, b) {
                            var indA, indB;
                            var search = request.term.replace(/\'/g, '\'\'')
                            if (search.match(/\d+/g) != null) {
                                indA = a[1].indexOf(search);
                                indB = b[1].indexOf(search);
                            } else {
                                var newA = a[1].replace(/\d/g, "");
                                indA = newA.indexOf(search.toUpperCase());
                                var newB = b[1].replace(/\d/g, "");
                                indB = newB.indexOf(search.toUpperCase());
                            }
                            return indA - indB
                        })
                    }
                },
                complete: searchRoads
            })
        } // End searchAddresses()

        function searchRoads() {
            $.ajax({
                url: serverURL + "/" + RoadLabels + "/query",
                dataType: "jsonp", // with this dataType, cache is set to false by default
                data: {
                    where: "LABEL LIKE '%" + request.term.replace(/\'/g, '\'\'') + "%'", //makes single quotes into double for sql  
                    outFields: "LABEL, L_MUNICIPALITY",
                    returnGeometry: false,
                    f: "pjson"
                },
                success: function(data) {
                    $('#autocomplete').removeClass('ui-autocomplete-loading');
                    if (data.features) {

                        for (var i = 0; i < data.features.length; i++) {
                            var ind = ['./images/draw_fhline.png', data.features[i].attributes.LABEL, data.features[i].attributes.L_MUNICIPALITY, RoadLabels]

                            if (roadList.join().indexOf(ind.join().toString()) === -1) {
                                roadList.push(ind)
                            }
                        }
                        roadList = roadList.slice(0, 100) //only display 100
                    }
                },
                complete: searchBridges
            })
        }

        function searchBridges() {
            $.ajax({
                url: serverURL + "/" + Bridges + "/query",
                dataType: "jsonp", // with this dataType, cache is set to false by default
                data: {
                    where: "NAME LIKE '%" + request.term.replace(/\'/g, '\'\'') + "%' OR BIN LIKE'%" + request.term.replace(/\'/g, '\'\'') + "%'", //makes single quotes into double for sql  
                    outFields: "NAME,BIN, CARRIED,COUNTY_OWNED",
                    returnGeometry: false,
                    f: "pjson"
                },
                success: function(data) {
                    $('#autocomplete').removeClass('ui-autocomplete-loading');
                    if (data.features) {
                        for (var i = 0; i < data.features.length; i++) {
                            var image;
                            if (data.features[i].attributes.COUNTY_OWNED === "N") {
                                image = './images/bridgeNO.png'
                            } else {
                                image = './images/bridgeYES.png'
                            }
                            var ind = [image, data.features[i].attributes.BIN, data.features[i].attributes.NAME, /*data.features[i].attributes.CARRIED,*/ Bridges]

                            if (bridgeList.join().indexOf(ind.join().toString()) === -1) {
                                bridgeList.push(ind)
                            }
                        }
                        bridgeList = bridgeList.slice(0, 10) //only display 10
                    }
                },
                // Merge search lists
                complete: searchCulverts
            })
        }

        function searchCulverts() {

            $.ajax({
                url: serverURL + "/" + Culverts + "/query",
                dataType: "jsonp", // with this dataType, cache is set to false by default
                data: {
                    where: "gis_rims.dcgis.Inspection.CULVERT_NUMBER LIKE '%" + request.term.replace(/\'/g, '\'\'') + "%'", //makes single quotes into double for sql  
                    outFields: "gis_rims.dcgis.Inspection.CULVERT_NUMBER, gis_rims.dcgis.Inspection.STRUCTURAL_RATING,gis_rims.dcgis.Geographic_Features.ROAD_NAME",
                    returnGeometry: false,
                    f: "pjson"
                },
                success: function(data) {

                    $('#autocomplete').removeClass('ui-autocomplete-loading');
                    if (data.features) {
                        for (var i = 0; i < data.features.length; i++) {
                            var sr = 'gis_rims.dcgis.Inspection.STRUCTURAL_RATING';
                            var cn = 'gis_rims.dcgis.Inspection.CULVERT_NUMBER';
                            var rn = 'gis_rims.dcgis.Geographic_Features.ROAD_NAME'
                            var image = './images/culvert' + data.features[i].attributes[sr] + '.png'

                            var ind = [image, data.features[i].attributes[cn], data.features[i].attributes[rn], Culverts]

                            if (culvertList.join().indexOf(ind.join().toString()) === -1) {
                                culvertList.push(ind)
                            }
                        }
                        culvertList = culvertList.slice(0, 10) //only display 10
                    }
                },
                // Merge search lists
                complete: searchSewerDischarge
            })
        }

        function searchSewerDischarge() {
            $.ajax({
                url: serverURL + "/" + StormSewerDischarge + "/query",
                dataType: "jsonp", // with this dataType, cache is set to false by default
                data: {
                    where: "stodcrg_id LIKE '%" + request.term.replace(/\'/g, '\'\'') + "%'", //makes single quotes into double for sql  
                    outFields: "stodcrg_id,MS4RECEIVE",
                    returnGeometry: false,
                    f: "pjson"
                },
                success: function(data) {
                    $('#autocomplete').removeClass('ui-autocomplete-loading');
                    if (data.features) {

                        for (var i = 0; i < data.features.length; i++) {
                            var ind = ['./images/ssdSELECT.png', data.features[i].attributes.stodcrg_id, data.features[i].attributes.MS4RECEIVE, StormSewerDischarge]

                            if (sewerDischargeList.join().indexOf(ind.join().toString()) === -1) {
                                sewerDischargeList.push(ind)
                            }
                        }
                        sewerDischargeList = sewerDischargeList.slice(0, 10) //only display 10
                    }
                },
                complete: endSearch
            })
        }

        function endSearch(request, status) {
            var searchList = addressList.concat(roadList).concat(bridgeList).concat(culvertList).concat(sewerDischargeList);
            response($.map(searchList, function(item) {
                return {
                    label: '<img src="' + item[0] + '" height="14px">' + item[1] + ' <small><i>' + item[2] + '</i></small>',
                    value: [item[1], item[2], item[3]]
                }
            }))
        }
    } // End searchString searchString

    /*===========================================
    =            Search Autocomplete            =
    ===========================================*/

    $('#autocomplete').autocomplete({
        source: searchString,
        minLength: 1, /// Minimum search value
        focus: function(event, ui) {
            var focus = event.type
            getSearchLocation(focus, ui) // Results focus event
        },
        select: function(event, ui) { // Results select event
            getSearchLocation(event, ui);
        },
        change: function(event, ui) { // Results change event
            map.graphics.clear();
        },
        close: function(event, ui) {
            map.graphics.clear();
        },
        error: function(error) { // error log
            console.log(error)
        }
    });

    // Highlight search string in search results
    $.ui.autocomplete.prototype._renderItem = function(ul, item) {
        item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
        return $("<li></li>")
            .data("item.autocomplete", item)
            .append(item.label)
            .appendTo(ul);
    };

    // Toggle search clear
    $('#autocomplete').keydown(function(event) {
        $('#clear').css('visibility', 'visible')
    });

    // Search clear click event
    $("#clear").click(function(evt) {
        $(this).css('visibility', 'hidden')
        evt.preventDefault();
        $("#autocomplete").val("").focus();

    });

    /*===============================================
    =            Highlight Search Result            =
    ===============================================*/

    function getSearchLocation(event, ui) {

        var query = new Query();
        query.returnGeometry = true;

        if (ui.item.value[2] === RoadLabels) {
            var sql = "LABEL = '" + ui.item.value[0] + "' AND L_MUNICIPALITY = '" + ui.item.value[1] + "'";
            var queryTask = new QueryTask(serverURL + "/" + ui.item.value[2]);
        }

        if (ui.item.value[2] === AddressPoints) {
            var sql = "FULLADDRESS = '" + ui.item.value[0] + "' AND MUNICIPALITY = '" + ui.item.value[1] + "'";
            var queryTask = new QueryTask(serverURL + "/" + ui.item.value[2]);
        }

        if (ui.item.value[2] === Bridges) {
            var sql = "NAME = '" + ui.item.value[1] + "' AND BIN = '" + ui.item.value[0] + "'";

            // var sql = "NAME = '" + ui.item.value[0] + "' AND CARRIED = '" + ui.item.value[1] + "'";
            var queryTask = new QueryTask(serverURL + "/" + ui.item.value[2]);
            query.outFields = ["COUNTY_OWNED"]
        }

        if (ui.item.value[2] === Culverts) {
            var sql = "gis_rims.dcgis.Inspection.CULVERT_NUMBER = '" + ui.item.value[0] + "'";

            // var sql = "NAME = '" + ui.item.value[0] + "' AND CARRIED = '" + ui.item.value[1] + "'";
            var queryTask = new QueryTask(serverURL + "/" + ui.item.value[2]);
            query.outFields = ["gis_rims.dcgis.Inspection.STRUCTURAL_RATING"]
        }

        if (ui.item.value[2] === StormSewerDischarge) {
            var sql = "stodcrg_id = '" + ui.item.value[0] + "'";
            var queryTask = new QueryTask(serverURL + "/" + ui.item.value[2]);
            query.outFields = ["stodcrg_id"]
        }

        query.where = sql;
        queryTask.execute(query, function(result) {
            map.graphics.clear()
            if (ui.item.value[2] === RoadLabels) {
                var polyline = new Polyline(new SpatialReference({
                    wkid: spr
                }))

                for (var i = 0; i < result.features.length; i++) {
                    polyline.addPath(result.features[i].geometry.paths[0])
                }
                var sls = new SimpleLineSymbol(
                    SimpleLineSymbol.STYLE_SOLID,
                    new Color([255, 0, 0]),
                    3
                )

                var searchGraphic = new Graphic(polyline, sls);
                map.graphics.add(searchGraphic);
                if (event !== 'autocompletefocus') {
                    map.setExtent(polyline.getExtent());
                }
            } else if (ui.item.value[2] === AddressPoints) {
                var point = new Point(result.features[0].geometry, new SpatialReference({
                    wkid: spr
                }))

                var addressMarker = new PictureMarkerSymbol('./images/pinAPF.png', 35, 43);

                var searchGraphic = new Graphic(point, addressMarker);
                map.graphics.add(searchGraphic);
                if (event !== 'autocompletefocus') {
                    zoomToPoint(point)
                }
            } else if (ui.item.value[2] === Bridges) {
                var point = new Point(result.features[0].geometry, new SpatialReference({
                    wkid: spr
                }))
                var addressMarker;
                if (result.features[0].attributes.COUNTY_OWNED === 'Y') {
                    addressMarker = new PictureMarkerSymbol('./images/bridgeYES.png', 26, 19);
                } else {
                    addressMarker = new PictureMarkerSymbol('./images/bridgeNO.png', 26, 19);
                }

                var searchGraphic = new Graphic(point, addressMarker);
                map.graphics.add(searchGraphic);
                if (event !== 'autocompletefocus') {
                    zoomToPoint(point)

                }
            } else if (ui.item.value[2] === Culverts) {
                var point = new Point(result.features[0].geometry, new SpatialReference({
                    wkid: spr
                }))

                var addressMarker = new PictureMarkerSymbol('./images/culvert' + result.features[0].attributes["gis_rims.dcgis.Inspection.STRUCTURAL_RATING"] + '.png', 25, 25)

                var searchGraphic = new Graphic(point, addressMarker);
                map.graphics.add(searchGraphic);
                if (event !== 'autocompletefocus') {
                    zoomToPoint(point)

                }
            } else if (ui.item.value[2] === StormSewerDischarge) {
                var point = new Point(result.features[0].geometry, new SpatialReference({
                    wkid: spr
                }))
                var addressMarker;

                addressMarker = new PictureMarkerSymbol('./images/ssdSELECT.png', 15, 15);

                var searchGraphic = new Graphic(point, addressMarker);
                map.graphics.add(searchGraphic);
                if (event !== 'autocompletefocus') {
                    zoomToPoint(point)

                }
            } else { /* More labels*/ }
        });
    }

    /*=====  End of search  ======*/


    /*=====================================
    =            Zoom to Point            =
    =====================================*/

    function zoomToPoint(point) {
        var dY = 800 // North/South Extent in feet
        var widthR = map.extent.xmax - map.extent.xmin
        var heightR = map.extent.ymax - map.extent.ymin
        var ratio = widthR / heightR
        var dX = dY * ratio
        var yminS = point.y - 400
        var ymaxS = point.y + 400
        var xminS = point.x - (dX / 2)
        var xmaxS = point.x + (dX / 2)

        // Zoom to point - using Extent
        map.setExtent(new Extent(xminS, yminS, xmaxS, ymaxS, new SpatialReference({
            wkid: spr
        })));
    }


    /*=======================================
    =            Draggable Panel            =
    =======================================*/

    $(document).ready(function() {
        $(".panel-container").draggable({
            //cursor: "crosshair",
            handle: ".panel-heading",
            containment: "document",
            start: function(event, ui) {
                $(".panel-container").css('opacity', '0.9')
            },
            drag: function() {
                dragCheck = true;
            },
            stop: function(event, ui) {
                $(".panel-container").css('opacity', '1');
                dragCheck = false;
            }
        });
    });


    /*==============================================
    =            Initialize Info window            =
    ==============================================*/

    map.on('load', initIdentify);

    function initIdentify() {

        popup = map.infoWindow;
        popup.set("popupWindow", false); //Prevent the popup from displaying

        //when the selection changes update the side panel to display the popup info for the 
        //currently selected feature. 
        popOnSelect = connect.connect(popup, "onSelectionChange", function() {
            displayPopupContent(popup.getSelectedFeature());
        });
        //when the selection is cleared remove the popup content from the side panel. 
        popOnClear = connect.connect(popup, "onClearFeatures", function() {
            //$('#featureCount').text('Click to select feature(s)')
            //$('#infoContainer').css('height', 'auto')
            $('#featureCount').text('Click to select feature(s)');
            dom.byId("infoContainer").innerHTML = '';
            $('#pager').hide();

        });

        //Update the sidebar with the new content. 
        popOnSet = connect.connect(popup, "onSetFeatures", updatePopupContent)
    } // End initIdentify


    function displayPopupContent(feature) {
        if (feature) {
			 dom.byId("infoContainer").innerHTML = feature.getContent();
        } 
    }

    function updatePopupContent() {

        if (!popup.features) {
            console.log('no features selected')
        } else {
            $('#featureCount').empty();
            var names = []
                // Get feature names from info template
            for (var i = 0; i < popup.count; i++) {
                var name = popup.features[i].infoTemplate.title;
                names.push(name)
            }

            var counts = {};
            // Get feature counts
            $.each(names, function(key, value) {
                if (!counts.hasOwnProperty(value)) {
                    counts[value] = 1;
                } else {
                    counts[value]++;
                }
            });

            // For each feature, generate count
            for (var i = 0; i < Object.keys(counts).length; i++) {
                // For first feature only
                if (i === 0) {
                    $('#featureCount').append('<span value="' + Object.keys(counts)[i] + '" class="selectedLayers " id="selectedFeature"><span>' + Object.keys(counts)[i] + '</span><p>(' + counts[Object.keys(counts)[i]] + ')</p></span>&nbsp');
                    displayPopupContent(popup.getSelectedFeature());
                } else {
                    $('#featureCount').append('<span value="' + Object.keys(counts)[i] + '" class="selectedLayers"><span>' + Object.keys(counts)[i] + '</span><p>(' + counts[Object.keys(counts)[i]] + ')</p></span>&nbsp')
                }
            }

            // Selected feature button
            $('.selectedLayers').click(function(evt) {
                map.graphics.clear();
                $('.selectedLayers').each(function(e) {
                    // remove style from all buttons
                    if ($(this).attr('id') === 'selectedFeature') {
                        $(this).attr('id', '')
                    }
                })

                // add style to selected feature button
                $(this).attr('id', 'selectedFeature');

                // Highlight feature according to index in feature list
                var fc = $(this)[0].attributes.value.value
                var index = names.indexOf(fc)
                popup.select(index)
                displayPopupContent(popup.features[index]);
            })
            $('#pager').show()
        }
    };

    /*===============================================
    =            Feature Select Controls            =
    ===============================================*/

    $('#previous').click(function() {
        map.graphics.clear();
        popup.selectPrevious();
        featureTitle();
    })

    $('#next').click(function() {
        map.graphics.clear();
        popup.selectNext();
        featureTitle()
    })

    // Highlight point feature
    $('#previous,#next').click(function() {
        if (popup.features[popup.selectedIndex].geometry.type === 'point') {
            highlightPoint(popup.features[popup.selectedIndex].geometry)
        }
    })

    // update feature button style
    function featureTitle() {
        var feature = popup.getSelectedFeature().infoTemplate.title
        $('.selectedLayers').each(function(e) {
            var fc = $(this)[0].attributes.value.value
            if (fc === feature) {
                $(this).attr('id', 'selectedFeature');
            } else {
                $(this).attr('id', '');
            }
        })
    }

    /*=====================================
    =            Identify Tool            =
    =====================================*/

    $(document).ready(function() {
        identifyParameters();
        selectFeature = map.on("click", executeIdentifyTask);
    })

    function identifyParameters() {
        // Merge selected layers with default layers
        // var identifyIds = visibleLayerIds.concat(Defaults);
        //create identify tasks and setup parameters
        identifyTask = new IdentifyTask(serverURL);
        identifyParams = new IdentifyParameters();
        identifyParams.tolerance = 3;
        identifyParams.returnGeometry = true;
        identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
        identifyParams.width = map.width;
        identifyParams.height = map.height;
    }

    function executeIdentifyTask(event) {
        map.graphics.clear();
        firstResult = true;
        togglePanels(null, true);
        var identifyIds = visibleLayerIds.concat([2, 15]).concat(Defaults);
        // Remove undesired layers from indentify
        $.each(nonSelect, function(i, item) {
            var ind = identifyIds.indexOf(item);
            if (ind > -1) {
                identifyIds.splice(ind, 1);
            }

        })
        identifyParams.layerIds = identifyIds
        identifyParams.geometry = event.mapPoint;
        identifyParams.mapExtent = map.extent;

        var deferred = identifyTask
            .execute(identifyParams)
            .addCallback(function(response) {
                // response is an array of identify result objects
                // Let's return an array of features.
                return arrayUtils.map(response, function(result) {
                    var feature = result.feature;
                    var layerName = result.layerName;
                    var infoTemplate;
                    var culvertNumber;

                    // Highlight point
                    if (result.geometryType === 'esriGeometryPoint') {
                        // only highlight first result
                        if (firstResult) {
                            highlightPoint(result.feature.geometry)
                            firstResult = false;
                        }
                    }

					if (layerName === 'StormSewerDischarge') {
                        infoTemplate = new InfoTemplate("", '<b>' + layerName + '</b><br>' +
                            "PKStodcrgID: <span  class='link' id='ssdLink'>${PKStodcrgID}</span> <br/>" +
                            "MS4Receive: ${MS4Receive} <br/>" +
                            "Material: ${MatDOM} <br/>" +
                            "Size: ${SizeDOM} <br/>" +
                            "Apprx Location: ${Approx_Loc} <br/>" +
                            // "Narrative: ${Narrative}"
                            // "Watershed: ${WATSHD3EMC} <br/>" +
                            "Latitude: ${Latitude} <br/>" +
                            "Longitude: ${Longitude} <br/>")
                    } else if (layerName === 'Parcels') {
                        var infoTemplate = new InfoTemplate("", '<b>' + layerName + '</b><br>' +
                            "Parcel Number: <span  class='link' id='parcelLink'>${Parcelgrid}</span><br/>" +
                            "Parcel Address: ${loc_st_nbr} ${loc_post_dir} ${loc_st_name} ${loc_unit_name} ${loc_unit_nbr} ${Loc_mail_st_suff} ${loc_muni_name} ${loc_zip}<br/>" +
                            "Owner: ${owner_first_name} ${owner_last_name} ${owner_name_suffix}<br/>" +
                            // "PO Box: ${po_box}<br/>" +
                            "Mailing Address: ${mail_st_nbr} ${prefix_dir} ${mail_st_rt} ${own_mail_st_suff} ${own_unit_name} ${own_unit_nbr} ${mail_city} ${owner_mail_state} ${mail_zip} ${mail_country}<br/>" +
                            "Lot Size: ${descript_1}<br/>" +
                            "Land Use: ${prop_class_desc}");
                    } else if (layerName === 'Culverts') {
                        var dir = 'http://gis.dcny.gov/';
                        var doc = dir + feature.attributes.PICTURES.split('VOL2\\')[1].split('#')[0];

                        var folder = doc.split('.DOC')[0]
                        var inlet = folder + '/Inlet.JPG';
                        var outlet = folder + '/Outlet.JPG';
                        var inletStream = folder + '/Inlet Stream.JPG';
                        var outletStream = folder + '/Outlet Stream.JPG';
                        // These change
                        var eastApp = folder + '/Eastbound approach.JPG';
                        var westApp = folder + '/Westbound approach.JPG';
                        var northApp = folder + '/Northbound approach.JPG';
                        var southApp = folder + '/Southbound approach.JPG';

                        culvertNumber = feature.attributes.CULVERT_NUMBER
                        hs.align = 'center'; // align fullscreen culvert pics to center
                        hs.graphicsDir = "./highslide/graphics/";
                        hs.dimmingOpacity = 0.5;
                        hs.allowMultipleInstances = false;
                        hs.showCredits = false;
                        // Add the slideshow controller
                        hs.addSlideshow({
                            slideshowGroup: 'group' + culvertNumber,

                            repeat: true,
                            useControls: true,
                            fixedControls: 'fit',
                            overlayOptions: {
                                // className: 'large-dark',
                                opacity: 0.75,
                                position: 'bottom center',
                                offsetX: 0,
                                offsetY: -10,
                                hideOnMouseOut: true
                            }
                        });


                        var infoTemplate = new InfoTemplate("", '<b>' + layerName + '</b><br>' +
                            "Culvert Number: <span class='link' id='culvertLink'>${CULVERT_NUMBER}</span><br/>" +
                            "County Route: ${CR}<br/>" +
                            "Road: ${ROAD_NAME}<br/>" +
                            "Rating: ${STRUCTURAL_RATING} <br/>" +
                            "Pictures: <a class='link' href='" + doc + "'>Download all images (.doc)</a> <br> <a href='" + inlet + "' class='highslide' onclick='return hs.expand(this,{slideshowGroup: &quot;group" + culvertNumber + "&quot;})'><img alt='inlet' class='culvertImage' src='" + inlet + "'></a><div class='highslide-caption'>" + culvertNumber + " Inlet</div><a href='" + outlet + "' class='highslide' onclick='return hs.expand(this,{slideshowGroup: &quot;group" + culvertNumber + "&quot;})'><img alt='outlet' class='culvertImage' src='" + outlet + "'></a><div class='highslide-caption'>" + culvertNumber + " Outlet</div><a href='" + inletStream + "' class='highslide' onclick='return hs.expand(this,{slideshowGroup: &quot;group" + culvertNumber + "&quot;})'><img alt='inlet stream' class='culvertImage' src='" + inletStream + "'></a><div class='highslide-caption'>" + culvertNumber + " Inlet Stream</div><a href='" + outletStream + "' class='highslide' onclick='return hs.expand(this,{slideshowGroup: &quot;group" + culvertNumber + "&quot;})'><img alt='outlet stream' class='culvertImage' src='" + outletStream + "'></a><div class='highslide-caption'>" + culvertNumber + " Outlet Stream</div>"
                        );

                        getCulvert(culvertNumber)

                    } else if (layerName === 'Municipalities') {
                        var infoTemplate = new InfoTemplate("", '<b>' + layerName + '</b><br>' +
                            "NAME:  ${NAME} <br/>" +
                            "NAME 911:  ${NAME_911} <br/>" +
                            "SWIS CODE:  ${SWIS} <br/>" +
                            "WEBLINK:  <span  class='link' id='muniLink'>${WEBLINK}</span>");
                    } else if (layerName === 'DEM') {
                        var elev = result.feature.attributes['Pixel Value'];
                        var infoTemplate = new InfoTemplate("", '<b>' + layerName + '</b><br>' +
                            "Elevation (ft): " + Math.round(elev));
                    } else if (layerName === 'CountyRoads') {
                        var ft = result.feature.attributes.len;
                        var mi = result.feature.attributes.len / 5280;
                        var infoTemplate = new InfoTemplate("", '<b>' + layerName + '</b><br>' +
                            "County Route: ${CO_RT1}<br/>" +
                            "SEGMENTID: ${SEGMENTID} <br/>" +
                            "Length (ft): " + Math.round(ft) + '<br/>' +
                            "Length (mi): " + mi.toFixed(2));
                    } else {
                        var fields = []
                            // For each feature attribute
                        for (var i = 0; i < Object.keys(feature.attributes).length; i++) {
                            //Test for OBJECTID field
                            var objectid = /OBJECTID/i.test(Object.keys(feature.attributes)[i]);
                            // Test for Shape field
                            var shapeid = /Shape/i.test(Object.keys(feature.attributes)[i]);
                            // Test for GlobalID field
                            var globalid = /GlobalID/i.test(Object.keys(feature.attributes)[i]);
                            // Skip fields
                            if (!shapeid && !objectid && !globalid) {
                                if (/Report/i.test(Object.keys(feature.attributes)[i])) {
                                    fields.push(Object.keys(feature.attributes)[i] + ": <span class='link' id='trafficLink' >${" + Object.keys(feature.attributes)[i] + "} </span><br>")
                                } else {
                                    fields.push(Object.keys(feature.attributes)[i] + ": ${" + Object.keys(feature.attributes)[i] + "}<br>")
                                }
                            }
                        }
                        infoTemplate = new InfoTemplate("", '<b>' + layerName + '</b><br>' + fields.join(" "));
                    }
                    infoTemplate.setTitle(layerName)
                    feature.setInfoTemplate(infoTemplate);

                    return feature
                });
            });
        popup.setFeatures([deferred]);
        popup.show(event.mapPoint);
    }

    function highlightPoint(geometry) {
        var highPoint = new Point(geometry, new SpatialReference({
            wkid: spr
        }))
        var line = new SimpleLineSymbol();
        line.setStyle(SimpleLineSymbol.STYLE_NULL);
        var highPointMarker = new SimpleMarkerSymbol();
        highPointMarker.setColor(new Color([0, 255, 255, 1]));
        highPointMarker.setSize(7, 7);
        highPointMarker.setOutline(line);
        var highPointGraphic = new Graphic(highPoint, highPointMarker);
        map.graphics.add(highPointGraphic);
    }

    /*====================================
    =            Basemap Tool            =
    ====================================*/

	$('#basemaps td').click(function(thumb) {
		$('#basemaps td').each(function() {
			if ($(this).hasClass('selectedBasemap')) {
				$(this).removeClass('selectedBasemap')
			}
		})
		$(this).addClass('selectedBasemap');

		var value = window[$(this).attr('data-value')]
		var basemaps = [aerialLayer, soilsLayer, terrainLayer, hydroLayer, planimetricLayer]
		for (var i = 0; i < basemaps.length; i++) {
			if (basemaps[i] === value) {
				map.addLayer(value);
				map.reorderLayer(value, 0);
				//map.reorderLayer(Counties, 1);
				

				if (basemaps[i] === aerialLayer) {
					//roadLayer.hide();
					defaultLayer.setVisibleLayers([11, 3, 4, 53])
					roadLayer.setVisibleLayers([2])
					$('#legendDiv').html("Aerial")
				} else if (basemaps[i] === soilsLayer) {
					//roadLayer.hide();
					defaultLayer.setVisibleLayers([38, 11, 3, 4])
					roadLayer.setVisibleLayers([2])
					$('#legendDiv').html("Soil")
				} else {
					defaultLayer.setVisibleLayers(Defaults)
					roadLayer.setVisibleLayers(Road)
				}
				
				map.reorderLayer(defaultLayer, 10);
				map.reorderLayer(roadLayer, 11);
				map.reorderLayer(layers, 12);
				map.reorderLayer(roadLabels, 13);
				map.reorderLayer(crLabels, 14);
				
				
				$.merge(visibleLayerIds, basemaps[i].visibleLayers);
				
			} else {
				map.removeLayer(basemaps[i]);
				// Remove basemap layer ids from visible layers list
				var baseIndexStart = visibleLayerIds.indexOf(basemaps[i].visibleLayers[0]);
				if (baseIndexStart !== -1) {
					visibleLayerIds.splice(baseIndexStart, basemaps[i].visibleLayers.length);
				}
			}
		} // End for each basemap

		if ($(this).attr('data-value') === 'defaultLayer') {

			defaultLayer.setVisibleLayers(Defaults)
			roadLayer.setVisibleLayers(Road)

			//legend.destroyRecursive(true);
			$('#legendDiv').empty();
			// initLegend();
		}
		updateprint();
	})

    /*===================================
    =            Layers Tool            =
    ===================================*/

    $(document).ready(function() {
        $('.checkbox label').off('click')
    })

    $('.checkbox').on('click', function(event) {

        // Clear list of selectable layers
        for (var i = 0; i < selectableLayers.length; i++) {
            var idv = visibleLayerIds.indexOf(selectableLayers[i]);
            visibleLayerIds.splice(idv, 1);
        }

        var idx;
        var $target = $(event.currentTarget),
            val = $target.find('label').text(),
            $inp = $target.find('input'),
            $layer = $target.find('p'),
            idx = options.indexOf(val);

        // Build list of selectable layers 
        if (idx > -1) { // Turn layer off if on
            options.splice(idx, 1);
            setTimeout(function() {
                $layer.removeClass('checkboxSelected');
                $layer.parent(".checkbox").css({
                    "border": "1px solid transparent",
                    "background-color": "transparent",
                })
            }, 0);

        } else { // Turn layer on if off
            options.push(val);
            setTimeout(function() {
                $layer.addClass('checkboxSelected');
                $layer.parent(".checkbox").css({
                    "border": "1px solid rgba(61,85,119,0.2)",
                    "background-color": "#f8f8f8",
                })
            }, 0);

        }

        // Add list of selectable layer IDs to visibleLayerIds
        $(event.target).blur();
        for (var i = 0; i < options.length; i++) {
            visibleLayerIds.push(window[options[i]])
        }

        // Show visible layers on map
        layers.setVisibleLayers(visibleLayerIds);
    });

    $('.checkbox').hover(function() {
        $(this).children('.layerLegend').css('opacity', '1');
    }, function() {
        $(this).children('.layerLegend').css('opacity', '0.2');
    })


    /*======================================
    =            Layer's Legend            =
    ======================================*/

    $(document).ready(getLayerLegends())

    function getLayerLegends() {
        $.ajax({
                url: serverURL + "/legend?f=pjson",
                dataType: "jsonp",
                success: function(data) {
                        for (var i = 0; i < data.layers.length; i++) {
                            var layerName = data.layers[i].layerName;
                            var legend = data.layers[i].legend
                            if (legend.length <= 10) { // 10 = max legend symbols
                                $('.layerLegend').each(function(evt) {
                                        var label = $(this).parents('span').children('label')
                                        var layer = label.text();
                                        if (layer === layerName) {
                                            if (layer !== 'PassingZones') {

                                                for (var c = 0; c < legend.length; c++) {
                                                    var image = 'data:' + legend[c].contentType + ';base64,' + legend[c].imageData;
                                                    $(this).append('<div class="legendSymbols"><span><img src="' + image + '"></span>' + ' ' + legend[c].label + ' ' + '<span></span></div>')
                                                } // end for each symbol of layer

                                            } else {
                                                for (var c = 0; c < 2; c++) {
                                                    var image = 'data:' + legend[c].contentType + ';base64,' + legend[c].imageData;
                                                    $(this).append('<div class="legendSymbols"><span><img src="' + image + '"></span>' + legend[c].label + '<span></span></div>')
                                                }
                                            } // end for passing zones
                                        } // end match legend to layer
                                    }) // end for each layer

                            } // end if layer symbology less than 10
                            else if (layerName === 'Signs') {
                                var signs = [legend[32], legend[426], legend[135], legend[222]]
                                for (var s = 0; s < signs.length; s++) {
                                    var image = 'data:' + signs[s].contentType + ';base64,' + signs[s].imageData;
                                    $('.layerLegend.signs').append('<div class="legendSymbols"><span><img src="' + image + '"></span></div>')
                                }

                            } else if (layerName === 'StormSewerDischarge') {
                                var ssd = [legend[2], legend[15], legend[7], legend[8], legend[0], legend[6], legend[12], legend[3]]
                                for (var s = 0; s < ssd.length; s++) {
                                    var image = 'data:' + ssd[s].contentType + ';base64,' + ssd[s].imageData;
                                    if (s < 4) {
                                        $('.layerLegend.ssd').append('<div class="legendSymbols"><span><img src="' + image + '"></span>' + ssd[s].label + '<span></span></div>')
                                    } else {
                                        $('.layerLegend.ssd').append('<div class="legendSymbols"><span><img src="' + image + '"></span></div>')
                                    }
                                }
                                $('.layerLegend.ssd').append('<div class="legendSymbols"><span>Local</span></div>')
                            }
                        } // end for each layer
                    } // end success
            }) // end ajax request
    }; // end getLayerLegends()


    /*====================================
    =            Muni weblink            =
    ====================================*/
    $(document).on('click', '#muniLink', function(e) {

        window.open($(this).text(), "_blank");
    })

    /*=====================================================
    =            Storm Sewere Discharge Report            =
    =====================================================*/

    $(document).on('click', '#ssdLink', function(e) {
        var id = $(this).text();
        var report = 'http://gis.dcny.gov/arcstudio/outfallreport.asp?id=' + id
        window.open(report, "_blank");
    })

    /*=====================================
    =            Parcel Report            =
    =====================================*/

    $(document).on('click', '#parcelLink', function(e) {
        var id = $(this).text();
        var report = 'http://gis.dcny.gov/parcelaccess/parcelaccess_map.htm?grid=' + id
        window.open(report, "_blank");
    })


    /*======================================
    =            Traffic Report            =
    ======================================*/

    $(document).on('click', '#trafficLink', function(e) {
        window.open('http://www.dutchessny.gov/PlnRoadCnts/RoadCntsPDFs/' + $(this).text(), "_blank");
    })


    /*=====================================================
    =            Generate list of County Roads            =
    =====================================================*/

    $(function() {
            $.ajax({
                url: serverURL + "/" + Intersections + "/query",
                async: false,
                dataType: "jsonp", // with this dataType, cache is set to false by default
                data: {
                    select: "DISTINCT",
                    where: "ROUTENAME LIKE '%'",
                    outFields: "ROUTENAME,SEGMENTID ",
                    returnGeometry: false,
                    returnDistinctValues: true,
                    f: "pjson"
                },
                success: function(data) {
                    var routeList = [];
                    for (var i = 0; i < data.features.length; i++) {
                        var routeName = data.features[i].attributes.ROUTENAME
                        var segment = data.features[i].attributes.SEGMENTID
                        var elem = '<li data-cr="' + segment + '">' + routeName + '</li>';
                        $('.county-roads-list').append(elem);
                        routeList.push(routeName)
                    }
                },
            });
        }) // end getCountyRoads

    /*============================================
    =            Zoom to County Roads            =
    ============================================*/

    function zoomToCountyRoad(cr) {
        var queryTask = new QueryTask(serverURL + "/" + CountyRoads);
        var query = new Query();
        query.returnGeometry = true;
        query.outFields = ["CO_RT1"];
        query.where = "CO_RT1 = '" + cr + "'",
            queryTask.execute(query, function(result) {
                var factor = searchScale / map.getScale();
                countyRoad = new Polyline(result.features[0].geometry);
                map.setExtent(countyRoad.getExtent());
                //map.setExtent(result.features[0].geometry);

            });
        $('.dropdown.intersections').css('display', 'block');
        $('.dropdown.intersections > a').html('Select Intersection<span class="caret"></span>');
        intersections(cr)
    } // End zoomToCountyRoad(cr)

    // County roads click event
    $(document).on("click", '.county-roads-list li', function(event) {
        $(this).click(zoomToCountyRoad($(this).attr('data-cr')));
        $('.dropdown.roads > a').html($(this).text() + '<span class="caret"></span>');
    });


    /*=========================================
     =            Sort County Roads            =
     =========================================*/

    $(document).ready(sorted)

    function sorted() {
        $('.sort').val('abc');
        $('.sort').click(function() {
            if ($(this).val() === 'abc') { // Sort by number
                $('.sort').val('123');
                $('.sort span').switchClass('glyphicon-sort-by-order', 'glyphicon-sort-by-alphabet');
                $('.county-roads-list').html(
                    $('.county-roads-list').children('li').sort(function(a, b) {
                        return parseInt($(a).attr('data-cr')) - parseInt($(b).attr('data-cr'));
                    })
                )
            } else { // Sort alphabetically
                $('.sort').val('abc');
                $('.sort span').switchClass('glyphicon-sort-by-alphabet', 'glyphicon-sort-by-order')
                $('.county-roads-list').html(
                    $('.county-roads-list').children('li').sort(function(a, b) {

                        return $(a).text().toUpperCase().localeCompare(
                            $(b).text().toUpperCase());
                    })
                )
            }
        })
    } // End sorted()

    // Keep county menu open on sort - removed data-toggle from inline element
    $('body').on('click', function(e) {
        if ($('li.dropdown.roads a').is(e.target)) {
            $('li.dropdown.roads').toggleClass('open');
        } else if (!$('.sort').is(e.target) && $('.sort').has(e.target).length === 0) {
            $('li.dropdown.roads').removeClass('open');
        } else {}
    });
	
	
	 /*======================================
    =            Nav roads list            =
    ======================================*/

    // Zoom to first letter of road list on keydown

    $(document).keydown(function(event) {

        selectedItem ? selectedItem.removeClass('firstLetter') : console.log('nothing');

        // If County road list is showing
        if ($('.dropdown.roads').hasClass('open')) {
            var keyChar

            var pos;
            $.isNumeric(event.key) ? keyChar = event.key : keyChar = event.key.toUpperCase();
            // If list is alphabetized
            if ($('.county-roads span').hasClass('glyphicon-sort-by-order')) {
                selectedItem = $('.county-roads-list').find('li').filter(function() {
                    return $(this).text().indexOf(keyChar) === 0;
                }).first();
                pos = $('.county-roads-list').scrollTop() + selectedItem.position().top

                selectedItem.addClass('firstLetter')
                selectedItem.css('display', 'block')

            } else {
                selectedItem = $('.county-roads-list').find('li').filter(function() {
                    return $(this).text().indexOf(keyChar) > -1;
                }).first();
                pos = $('.county-roads-list').scrollTop() + selectedItem.position().top
            }

            $('.county-roads-list').animate({
                scrollTop: pos,
            }, 'slow');
        }
    })


    /*==================================================================
    =            Generate list of county road intersections            =
    ==================================================================*/

    function intersections(id) {
        $('.intersections').removeClass('disabled');
        $('.dropdown-menu.intersections').html('');
        $.ajax({ // Set search icons
            url: serverURL + "/" + Intersections + "/query",
            async: false,
            dataType: "jsonp", // with this dataType, cache is set to false by default
            data: {
                where: "SEGMENTID = '" + id + "'",
                outFields: "SEGMENTID,STREET",
                returnGeometry: true,
                f: "pjson"
            },
            success: function(data) {

                for (var i = 0; i < data.features.length; i++) {
                    var street = data.features[i].attributes.STREET
                    var segment = data.features[i].attributes.SEGMENTID
                    var elem = '<li data-int="' + segment + '">' + street + '</li>';
                    $('.dropdown-menu.intersections').append(elem);
                }

            }
        });
    } // End gen Intersections


    /*============================================
    =            Zoom to Intersection            =
    ============================================*/

    function zoomToIntersection(seg, rd) {
        var queryTask = new QueryTask(serverURL + "/" + Intersections);
        var query = new Query();
        query.returnGeometry = true;
        query.outFields = ["SEGMENTID"]
        query.where = "SEGMENTID = '" + seg + "' AND STREET = '" + rd + "'",
            queryTask.execute(query, function(result) {
                var factor = searchScale / map.getScale();
                intersection = new Point(result.features[0].geometry);
                map.centerAndZoom(intersection, factor)
            });
    } // end zoom to intersection

    // Zoom to intersection click event
    $(document).on("click", '.intersections li', function(event) {
        $(this).click(zoomToIntersection($(this).attr('data-int'), $(this).text()));
        $('.dropdown.intersections > a').html($(this).text() + '<span class="caret"></span>');
    });

    /*====================================
    =            Measure Tool            =
    ====================================*/

    var measurement = new Measurement({
        map: map,
        advancedLocationUnits: false,
    }, dom.byId("measure-container"));
    measurement.startup();
    // Only show these units
    $.extend(measurement, {
        _areaUnitStrings: ["Acres", 'Sq Miles', "Sq Yards", "Sq Feet"],
        _areaUnitStringsLong: "esriAcres esriSquareMiles esriSquareYards esriSquareFeet".split(" "),
        _distanceUnitStrings: ["Miles", 'Feet', "Yards"],
        _distanceUnitStringsLong: "esriMiles esriFeet esriYards".split(" ")
    })
    initMeasure();

    // Measure text init
    //$('#measureCTR, #headingMeasure').one('click', initMeasure);

    function initMeasure() {
        $('#measure-container')
            .prepend('<span class="toolDesc" span><div class="toolName">Area</div><div class="toolName">Distance</div><div class="toolName">Location</div></span')

        $('.dijit.dijitReset.dijitInline.esriToggleButton')
            .css('text-align', 'center')
            .prepend('<span id="selectTool" class="toolDesc">Select tool from above</span')
           
    }

    // Change measure cursor on tool click
    measurement.on('tool-change', function(evt) {

        if (distance) {
            distance.remove();
        }
        $('#selectTool').empty();
        $('#measureDescription').empty();
        $('#length').empty();


        if (evt.toolName) {
            map.setMapCursor("wait");
            setTimeout(function() {
                map.setMapCursor("crosshair");
            }, 1000);
        } else {
            map.setMapCursor("default");
            $('#measureDescription').empty();
        }

        // Change tool directions 
        if (evt.toolName === 'area') {
            $('#measureDescription').append('<b>Directions: </b>Click to add vertices. Double click to complete polygon. Press CTRL to enable snapping.')
        } else if (evt.toolName === 'distance') {
            $('#measureDescription').append('<b>Directions: </b>Click to add path(s). Double click to complete path. Press CTRL to enable snapping.')
        } else if (evt.toolName === 'location') {
            $('.esriMeasurementTableRow img').eq(0).attr({
                'src': './images/crosshair.png',
                'width': '30px',
                'height': '30px'
            })
            $('#measureDescription').append('<b>Directions: </b>Click to add location.')
        }
    })

    // Add cursor distance measurement
    $('.esriMeasurementResultValue').append('<span id="length"></span>');

    // Calculate distance while moving cursor
    measurement.on("measure-start", function(tool, geometry) {
        if (tool.toolName === 'distance') {
            distance = map.on('mouse-move', function(event) {
                current = event.mapPoint
                var conversions = {
                        'Feet': 1,
                        'Miles': 0.000189394,
                        'Kilometers': 0.0003048,
                        'Meters': 0.3048,
                        'Yards': 0.333333,
                        'Nautical Miles': 0.000164579
                    }
                    // Geodesic or Euclidean distance
                var distParams = new DistanceParameters();
                distParams.distanceUnit = gsvc.UNIT_FOOT;
                distParams.geometry1 = start;
                distParams.geometry2 = current;
                distParams.geodesic = geodesic;
                gsvc.distance(distParams, function(distance) {
                    var d = distance * conversions[tool.unitName];
                    $('#length').text('+ ' + d.toFixed(2) + ' ' + tool.unitName)
                });
            })
        }
    });

    measurement.on("measure-end", function(tool, geometry) {
        $('#length').empty();
        if (distance) {
            distance.remove();
        }
    });
    /*=====  End of Measure Tool  ======*/


    /*===================================
    =            Select Tool            =
    ===================================*/


    $('.panel-close').click(function() {
        if ($(this).attr('data-target') === '#panelInfo') {
            //clearInfo();

            if (map.cursor === "help") {
                map.setMapCursor("default");
            }
        } else if ($(this).attr('data-target') === '#panelMeasure') {
            clearMeasure();
            if (map.cursor === "target") {
                map.setMapCursor("default");
            }
        }
    });

    // clear info panel
    function clearInfo() {
        selectFeature.remove();
    }

    // clear measure panel
    function clearMeasure() {
        measurement.clearResult();
        measurement.setTool("area", false);
        measurement.setTool("distance", false);
        measurement.setTool("location", false);
        measurement.hide();
        //map.setMapCursor("default");
    }

    on(dom.byId('collapseMeasure'), 'show.bs.collapse', function() {
        measurement.show();
        map.setMapCursor("default");
    })

    on(dom.byId('collapseMeasure'), 'shown.bs.collapse', function() {
        clearInfo()
    })

    on(dom.byId('collapseInfo'), 'hide.bs.collapse', function() {
        map.setMapCursor("default");
    })

    on(dom.byId('collapseMeasure'), 'hide.bs.collapse', function() {
        selectFeature = map.on("click", executeIdentifyTask);
        map.setMapCursor("default");
    })

    on(dom.byId('collapseMeasure'), 'hidden.bs.collapse', function() {
        clearMeasure();
    })


    /*===================================
    =            Street View           =
    ===================================*/

    $('#gsv_icon').hover(function(event) {
        $(this).css('background-image', 'url("./StreetView/css/images/gsv_button_over.png")');
    }, function() {
        $(this).css('background-image', 'url("./StreetView/css/images/gsv_button_norm.png")');
    })

    // Drag gsv icon
    $('#gsv_drag').draggable({
            containment: "#mapViewDiv",
            appendTo: '#mapViewDiv',
            scroll: false,
            cursorAt: {
                left: 20,
                top: 35
            },
            //revert: 'invalid',
            zIndex: 9000,
            // helper: 'clone',

            helper: function() {
                $copy = $(this).clone();
                $copy[0].src = "./StreetView/images/flying_gsv_man_w.png"
                return $copy;
            },

            start: function(event, ui) {
                mX = 0;
                $('#gsv_icon')
                    .css({
                        'background-image': 'url("./StreetView/css/images/gsv_button_down.png")',
                    })
                    //.unbind('mouseenter mouseleave')
                ui.previousPosition = ui.position;
                gsvPOS = []
            },
            drag: function(event, ui) {
                ui.helper[0].src = "./StreetView/images/flying_gsv_man_w.png"
                if (event.pageX < gsvPOS[gsvPOS.length - 1]) {
                    ui.helper[0].src = "./StreetView/images/flying_gsv_man_w.png"
                } else {
                    ui.helper[0].src = "./StreetView/images/flying_gsv_man_e.png"
                }
                gsvPOS.push(event.pageX)
            },
            stop: function(event, ui) {
                cursorToStatePlane(event, true)
            }
        }) // End draggable

    // Convert cursor position to State Plane Coordinates
    function cursorToStatePlane(cursor, isGSV) {
        var cursorTop = $('#mapViewDiv').height() - cursor.clientY;
        var mapDeltaX = map.extent.xmax - map.extent.xmin;
        var mapDeltaY = map.extent.ymax - map.extent.ymin;
        var xL = (mapDeltaX * cursor.clientX) / $('#mapViewDiv').width();
        var yT = (mapDeltaY * cursorTop) / $('#mapViewDiv').height();
        var x = map.extent.xmin + xL;
        var y = map.extent.ymin + yT
        var point = new Point(x, y, new SpatialReference({
            wkid: spr
        }));
        if (isGSV) {
            toLatLon(point)
        }
        return x + ' ' + y
    }

    // Convert State Plane to Lat-Lon
    function toLatLon(gsvSP) {
        var outSR = new SpatialReference(4326);
        var params = new ProjectParameters();
        params.geometries = [gsvSP];
        params.outSR = outSR;
        //params.transformation = transformation;

        gsvc.project(params, function(projectPoint) {
            gsv_test_latlon(projectPoint[0], gsvSP)
        });;
    }

    // Test if Lat-Lon coordinates have available gsv 
    function gsv_test_latlon(gsvLatLon, gsvSP) {
        var streetViewService = new google.maps.StreetViewService();
        var STREETVIEW_MAX_DISTANCE = 80;
        var latLng = new google.maps.LatLng(gsvLatLon.y, gsvLatLon.x);
        streetViewService.getPanoramaByLocation(latLng, STREETVIEW_MAX_DISTANCE, function(streetViewPanoramaData, status) {
            if (status === google.maps.StreetViewStatus.OK) {
                viewStreet(gsvLatLon)
                gsv_move(gsvSP)
                $('#gsv_status').empty();
            } else {
                $('#gsv_status').text('Steetview unavailable in this area')
            }
        });
    }

    // Open streetview at Lat-Lon coordinates
    function viewStreet(gsvLatLon) {
        if (panorama) {
            panorama.setVisible(true);
        }

        var bot = $('body').height() / 38

        $('#controlsContainer').animate({
            bottom: '48.5%',
        })

        $('#footer').animate({
                bottom: '50%',
                height: '5px'
            })
        $('.map-container').animate({
                height: '50%',
            })
            // Initiate Panorama AFTER street view container is up or else Panorama
            // needs to be refreshed
        $('#streetviewContainer').animate({
            height: '50%',
        }, initPanorama)

        function initPanorama() {

            panorama = new google.maps.StreetViewPanorama(
                document.getElementById('streetview'), {
                    position: {
                        lat: gsvLatLon.y,
                        lng: gsvLatLon.x
                    },
                    pov: {
                        heading: 0,
                        pitch: 0
                    },
                    zoom: 1,
                    linksControl: true,
                    panControl: false,
                    addressControl: false,
                    enableCloseButton: false,
                    imageDateControl: true,
                    enableCloseButton: false,
                });
				
            // Move gsv icon as street view position changes
            panorama.addListener('position_changed', function() {
                var lat = panorama.getPosition().lat()
                var lng = panorama.getPosition().lng()
                var point = new Point(lng, lat, new SpatialReference({
                    wkid: 4269
                }));
                toStatePlane(point)

                //console.log(panorama.streetViewDataProviders)
            });

            // Move heading icon has heading changes
            panorama.addListener('pov_changed', function() {
                heading = panorama.getPov().heading;
                //var pitch = panorama.getPov().pitch;
                var zoom = panorama.getPov().zoom;

                var zooms = {
                        0: (zoom < 1),
                        1: (zoom >= 1 && zoom < 2),
                        2: (zoom >= 2 && zoom < 3),
                        3: (zoom >= 3 && zoom < 4),
                        4: (zoom >= 4)
                    }
                    // Set heading icon based off zoom level (FOV)
                for (var i = 0; i < Object.keys(zooms).length; i++) {
                    if (zooms[i]) {
                        headingSym.setUrl('./Streetview/images/heading_' + i + '.png')
                    }
                }
                headingSym.setAngle(heading);
                headingGraphic.draw();
            });
        } // End initPanorama()

        // Enable gsv close button after panorama initiates
        setTimeout(gsv_close, 600)

        function gsv_close() {
            $('#gsv_Close').click(function() {
                map.graphics.clear();

                $('#controlsContainer').animate({
                    bottom: '0.5%',
                })

                $('#footer').animate({
                    bottom: '0',
                    height: '20px'
                }, function() {
                    setTimeout(function() {
                        $('#footer').off("mouseenter mouseleave")
                    }, 100)
                })

                $('.map-container').animate({
                    height: '100%',
                })

                $('#streetviewContainer').animate({
                    height: '0%',
                })
                panorama.setVisible(false);
            })
        }

    } // End viewStreet(gsvLatLon)

    // Convert Lat-Lon to State Plane
    function toStatePlane(point) {

        var params = new ProjectParameters();
        params.geometries = [point];
        params.inSR = 4269;
        params.outSR = map.spatialReference;
        gsvc.project(params, function(projectPoint) {
            var gsv_sp = projectPoint[0]
            gsv_move(gsv_sp)
        }, function(error) {
            console.log(error)
        });;
    }

    // Move gsv icon with gsv navigation
    function gsv_move(gsv_man) {
        map.graphics.clear()
        headingSym = new PictureMarkerSymbol('./Streetview/images/heading_1.png', 65, 65)
        if (heading) {
            headingSym.setAngle(heading)
        }
        headingGraphic = new Graphic(gsv_man, headingSym);
        manSym = new PictureMarkerSymbol('./Streetview/images/SVM2.png', 65, 65)
        manGraphic = new Graphic(gsv_man, manSym);
        map.graphics.add(headingGraphic)
        map.graphics.add(manGraphic)
        map.centerAt(gsv_man);
    }

    /*=====  End of Street View  ======*/

    /*==========================================
    =            Culvert Inspection            =
    ==========================================*/

    function getCulvert(culNum) {

        var queryTask = new QueryTask(serverURL + "/" + Inspection);
        var query = new Query();
        query.where = "CULVERT_NUMBER LIKE '%" + culNum + "%'";
        query.outFields = ['CULVERT_NUMBER', 'DATE_', 'INSPECTOR', 'INLET_HEADWALL_SCOUR', 'INLET_HEADWALL_CRACKS', 'INLET_HEADWALL_SPALLING', 'INLET_HEADWALL_STABILITY', 'INLET_WINGWALL_SCOUR', 'INLET_WINGWALL_CRACKS', 'INLET_WINGWALL_SPALLING', 'INLET_WINGWALL_STABILITY', 'INLET_OPENING', 'INLET_STREAM_BANK_PROTECTION', 'INLET_STREAM_OBSTRUCTIONS', 'INLET_STREAM_SILTATION', 'OUTLET_HEADWALL_SCOUR', 'OUTLET_HEADWALL_CRACKS', 'OUTLET_HEADWALL_SPALLING', 'OUTLET_HEADWALL_STABILITY', 'OUTLET_WINGWALL_SCOUR', 'OUTLET_WINGWALL_CRACKS', 'OUTLET_WINGWALL_SPALLING', 'OUTLET_WINGWALL_STABILITY', 'OUTLET_OPENING', 'OUTLET_STREAM_BANK_PROTECTION', 'OUTLET_STREAM_OBSTRUCTIONS', 'OUTLET_STREAM_SILTATION', 'BARREL_SCOUR', 'BARREL_CRACKS', 'BARREL_SPALLING', 'BARREL_CORROSION', 'BARREL_SILTATION', 'SEPARATED_JOINTS', 'BARREL_OBSTRUCTIONS', 'BARREL_STABILITY', 'PAVEMENT_SETTLEMENT', 'PAVEMENT_DRAINAGE', 'SHOULDER_DRAINAGE', 'GUIDE_RAIL', 'STRUCTURAL_RATING', 'PICTURES', 'INSPECTION_COMMENTS', 'MAINTENANCE_LOG', 'FEET_OF_COVER', 'POTENTIAL_POLLUTION']; // If indexed then ['*']

        queryTask.execute(query, function(result) {
            $('#culvertLink').click(function() {
                culvertCard(result.features[0].attributes);
            })
        });

    } 

    function culvertCard(data) {
        var params = $.param(data);
        window.open("culverts.html?" + params, "_blank", "width=880,height=920,menubar=no,resizable=yes,scrollbars=yes");
    }


    /*=============================
    =            Print            =
    =============================*/

    // Init print
    $(document).ready(updateprint);

    // Update print settings
    $('#printContainer input').blur(updateprint);
    $('#scaleUnit').change(updateprint);

    function updateprint() {

        if (printer) {
            printer.destroy();
            // $('.dijitPopup ').remove();
        }

        mydatestring = "'" + (my_date.getMonth() + 1) + "/" + my_date.getDate() + "/" + my_date.getFullYear() + "'";
        var mapDate = mydatestring.replace(/'/g, "");
        var authortext = "Produced with \n ParcelAccess \n Dutchess County, \n New York";

        layouts = [{
                "layout": "Portrait_Dutchess_Rims",
                "label": "Portrait 8.5x11 (PDF)",
                "format": "PDF",
                "layoutOptions": {
                    "legendLayers": [], // empty array means no legend
                    "scalebarUnit": dom.byId('scaleUnit').value,
                    "customTextElements": [{
                        "mapsubtext": dom.byId("mapsubtext").value
                    }, {
                        "maptitle": dom.byId("maptitle").value
                    }, {
                        "additionalinfo": dom.byId("additionalinfo").value
                    }, {
                        "date": mapDate
                    }, {
                        "webversion": webversion
                    }]
                }
            }, {
                "layout": "Landscape_Dutchess_Rims",
                "label": "Landscape 11x8.5 (PDF)",
                "format": "PDF",
                "layoutOptions": {
                    "legendLayers": [], // empty array means no legend
                    "scalebarUnit": dom.byId('scaleUnit').value,
                    "customTextElements": [{
                        "mapsubtext": dom.byId("mapsubtext").value
                    }, {
                        "maptitle": dom.byId("maptitle").value
                    }, {
                        "additionalinfo": dom.byId("additionalinfo").value
                    }, {
                        "date": mapDate
                    }, {
                        "webversion": webversion
                    }]
                }
            },

            {
                "layout": "MAP_ONLY",
                "label": "Map Only (gif)",
                "format": "gif",
                "layoutOptions": {}
            }
        ];

        var templates = arrayUtils.map(layouts, function(lo) {
            var t = new PrintTemplate();
            t.layout = lo.layout;
            t.label = lo.label;
            t.format = lo.format;
            t.layoutOptions = lo.layoutOptions;
            return t
        });

        printer = new Print({
            map: map,
            url: printService,
            templates: templates
        }, dom.byId("printButton"));

        printer.startup();
    } // End updatePrint

    // Hide footer with header
    $('.navbar-brand').click(function() {
        $('#footer').toggle();
        $('#locationContainer').css('bottom') === '0px' ? $('#locationContainer').css('bottom', '20px') : $('#locationContainer').css('bottom', '0px')
    })

}); // end require
