//function to get url variables out of url.... just call it and request what you are looking for... ie getQueryVariable("parcelnum")
function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

//FUNCTIONS THAT CHANGE THE MAP CONTENTS BASED ON YEAR SELECTED BY THE USER.

function setuserchoiceswipelayer(photoyear1){
 	map.removeLayer(swipelayer);
	//swipelayer = new esri.layers.ArcGISDynamicMapServiceLayer("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_"+photoyear1.value+"/MapServer", {
    //               "id": photoyear1.value });
    
    layers.setVisibleLayers([14]); 
					
	//disable the matching year in the drop down lists so the user can not select the same year for base and swipe.				
	for (var i = 0; i< document.getElementById("layerlistbase").options.length; i++){
		document.getElementById("layerlistbase").options[i].disabled = false;
	}
	for (var i = 0; i< document.getElementById("layerlistbase").options.length; i++){
		if (document.getElementById("layerlistbase").options[i].value == photoyear1.value){
			document.getElementById("layerlistbase").options[i].disabled = true;
		}
	}

  	updatelayers();	

}
 
 
function setuserchoicebaselayer(photoyear){
 	//if (photoyear.value == "-"){
	//	document.getElementById("startswipebutton").disabled = true;
	//}
	//else{document.getElementById("startswipebutton").disabled = false; }

    layers.setVisibleLayers([14]); 
	map.removeLayer(layers);
																
	//baselayer = new esri.layers.ArcGISDynamicMapServiceLayer("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_"+photoyear.value+"/MapServer", {"id": photoyear.value });
		
	for (var i = 0; i< document.getElementById("layerlistswipe").options.length; i++){
		document.getElementById("layerlistswipe").options[i].disabled = false;
	}
	for (var i = 0; i< document.getElementById("layerlistswipe").options.length; i++){
		if (document.getElementById("layerlistswipe").options[i].value == photoyear.value){
			document.getElementById("layerlistswipe").options[i].disabled = true;
		}
	}

	updatelayers();	
}
 
  
 
function updatelayers(){
	map.removeAllLayers();
    layers.setVisibleLayers([13,14]); 
    map.addLayer(layers)
	//map.addLayer(baselayer);
	//map.addLayer(swipelayer);
	if(document.getElementById("refmapbutton").checked){
			map.removeLayer(reference);
			map.addLayer(reference);
	}
			
		if(document.getElementById("layerlistbase").options[0].selected != true) {
		startswipe();
	}
	else{ 
		stopswipe();
	}
	
}
  
function extentHistoryChangeHandler(){
    dijit.byId("zoomprev").disabled = navToolbar.isFirstExtent();
    dijit.byId("zoomnext").disabled = navToolbar.isLastExtent();
}
			
			
function whatstate(){
    layers.setVisibleLayers([0,2,3,4,5]); 
	map.removeLayer(layers);
	if(document.getElementById("refmapbutton").checked==false){
		map.addLayer(layers);
	}
}


 
function startswipe(){
	   dom.byId("sliderdiv").style.display = "none";
	
		//if(swipeconnect) dojo.disconnect(swipeconnect);
	   if(swipeconnect) {swipeconnect.remove();}
    
		swipelayerid = dom.byId("layerlistswipe").value;
 		
        swipeslider = new Moveable("sliderdiv", { area: "content",within: true });
 
		//swipeslider.node.style.top = "0px";
		swipeslider.node.style.left = (map.width - map.width/2) + "px";
				//swipeslider.node.style.color = "red";
		clipval = (map.width - map.width/2);
 
		//Just a check, should not call this function here
		if (swipediv != null) clearClip();
		
		var t=setTimeout("cliplayer(swipelayerid)",500);
		//cliplayer(swipelayerid);
}
 
function cliplayer(layerid){
        swipediv = null;

        if (layerid == map.graphics.id) {
			swipediv = dom.byId(map.container.id + "_gc");
			//alert(swipdiv);
        }
		else 
		{
	        var layer = map.getLayer(layerid);
			//alert("my layer" + layerid);
			if (layer == null || layer == undefined) {
			//layer = map.getLayer('2009');
                alert("Swipe layer not defined");
				return;
            }
            swipediv = layer._div; //node
						
        }
 
        if (swipediv === undefined) {
            alert("unable to initialize swipe tool, try again");
            return;
        }
 
        if (swipediv.style === undefined) {
			alert("Cannot swipe on this layer!");
			return;
		}
				
		//Initial swipe slider location
		swipe(dom.byId("sliderdiv").style.left);
 
		//Make the slider visible
		//alert(dom.byId("sliderdiv").style.display);
		dom.byId("sliderdiv").style.display = "";
 
		on(swipeslider,"onMoveStart",function(args){
			this.node.style.opacity = "0.5";
		});
		
        swipeconnect = on(swipeslider, 'onMove', function(e){  //onMove
		//alert(this.node.style.left);
			this.node.style.top = "0px";	//needed to avoid offset
            clipval = this.node.style.left;
	//alert(clipval);
			swipe(clipval);
        });
		on(swipeslider,"onMoveStop",function(args){
			this.node.style.opacity = "1.0";
		});
		panconnect = on(map,'onPanEnd',function(args){
		//clipval=200;
			//alert(clipval);
			swipe(clipval);
		});
}
 
function clearClip() {
		if (swipediv != null) {
			swipediv.style.clip = dojo.isIE ? "rect(auto auto auto auto)" : "";
		}
}
 
function swipe(val){
//alert(val);
        if (swipediv != null) {

		//where is the div swipe
			offset_left = parseFloat(swipediv.style.left);
			offset_top = parseFloat(swipediv.style.top);
			
  //alert(offset_left);
			//var rightval, leftval, topval, bottomval;
 
			if (offset_left > 0) {
			//rightval = parseFloat(val);
				rightval = parseFloat(val) - Math.abs(offset_left);
				leftval = -(offset_left);
			}
			else if (offset_left < 0){
				leftval = 0;
				rightval = parseFloat(val) + Math.abs(offset_left);
						} else {
				leftval = 0;
				rightval = parseFloat(val);
			}
			if (offset_top > 0) {
				topval = -(offset_top);
				bottomval = map.height - offset_top;
			} else if (offset_top < 0) {
				topval = 0;
				bottomval = map.height + Math.abs(offset_top);
			} else {
		//	alert("else");
				topval = 0;
				bottomval = map.height;
			}
 
            //Syntax for clip "rect(top,right,bottom,left)"
			var clipstring = "rect(" + topval + "px " + rightval + "px " + bottomval + "px " + leftval +"px)";
       //var clipstring = "rect(0px 300px 300px 0px)";
			swipediv.style.clip = clipstring;
        }
}
 
            //This is called when "Stop Swipe" button is clicked.
function stopswipe(){
			
		
        swipeslider = null;
		
        dom.byId("sliderdiv").style.display = "none";
        //if(swipeconnect) dojo.disconnect(swipeconnect);
		if(swipeconnect) swipeconnect.remove();
		//if(panconnect) dojo.disconnect(panconnect);
        if(panconnect) panconnect.remove();
		if (swipediv != null) {
			swipediv.style.clip = dojo.isIE? "rect(auto auto auto auto)" : "";
			swipediv = null;
		}
 }
 	
			
 		//set the new swipe layer from drop down layer list
function updateSwipeLayer(o /*dom node*/) {
				//swipelayerid = o.value;
			}
//END OF FUNCTIONS THAT CHANGE THE MAP CONTENTS BASED ON YEAR SELECTED BY THE USER.		
		

function setActiveButton(tool) {
//alert(tool);
	var btnArray = new Array("zoomin", "zoomout", "pan");
	var offArray = new Array("zoominIcon", "zoomoutIcon", "panIcon");
	var onArray = new Array("zoominIconActive", "zoomoutIconActive", "panIconActive");
	//debugger;
	for (var i = 0; i < btnArray.length; i++) {
		var btn = dom.byId(btnArray[i]);
		if (tool == btnArray[i]) {
			//if iconClass is currently off - remove it
			if (! dojo.hasClass(btn.firstChild, onArray[i])) {
				dojo.removeClass(btn.firstChild, offArray[i]);
				dojo.addClass(btn.firstChild, onArray[i]);
			}
		} else {
			//if iconClass is currently on - remove it
			if (! dojo.hasClass(btn.firstChild, offArray[i])) {
				dojo.removeClass(btn.firstChild, onArray[i]);
				dojo.addClass(btn.firstChild, offArray[i]);                  
			}
		}					
	}
}

// ZOOM TO MUNI EXTENT
function loadJSon(url) {  
	var headID = document.getElementsByTagName("head")[0];           
	var newScript = document.createElement('script');      
	newScript.type = 'text/javascript';      
	newScript.src = url;  
	headID.appendChild(newScript);
}
function SetURLExtent(){
//alert("zoom to area");
	xmini = getQueryVariable("xmin");
	ymini = getQueryVariable("ymin");
	xmaxi = getQueryVariable("xmax");
	ymaxi = getQueryVariable("ymax");
	//var theok = confirm("will zoom to extent");
	//if (theok){
	  //map.setExtent(new esri.geometry.Extent(xmini, ymini, xmaxi, ymaxi, new esri.SpatialReference({wkid:2260})));
	//}
	//setuserchoiceswipelayer(2009);
}



function SetTownExtent(passaswis){
	aSWIS=passaswis.value;
//alert(aSWIS);
	if (aSWIS == 1){
		map.setExtent(new esri.geometry.Extent(576722, 955115, 832381, 1182936, new esri.SpatialReference({wkid:2260}) ));
	}
	else {
		var url = SERVERURL +"/arcgis/rest/services/parcelaccess_full/MapServer/2/query?where=swis='" + aSWIS + "'&f=json&returnGeometry=true&callback=pushtownextent";
		loadJSon(url)
	}
}
	
function pushtownextent(muni){
	var myfeature = muni.features[0];

//to get the extent of a polygon after you do a query...
//1 assign a geometry object.
	var munigeometry = new esri.geometry.Polygon();
//2 for a p0lygone loop through the rings of the feature in question
	var rings;
	rings = myfeature.geometry.rings;
//for (var r=0, rl=rings.length; r<rl; r++) {
					//add ring to combined polygon
	munigeometry.addRing(rings[0]);
//3 do a getextent
	var munifeatureextent = munigeometry.getExtent();
			//zoom to map
	map.setExtent(new esri.geometry.Extent(munifeatureextent.xmin,munifeatureextent.ymin,munifeatureextent.xmax,munifeatureextent.ymax, new esri.SpatialReference({wkid:2260}) ));
}	

function getmapextents(){
	xmin = map.extent.xmin;  //county extents at full scale
	ymin = map.extent.ymin;
	xmax = map.extent.xmax;
	ymax = map.extent.ymax; 
}

function getImgURLswipe(Jsonresults){picimage1 = Jsonresults.href;	}
function getImgURLbase(Jsonresults){picimage2 = Jsonresults.href;	}
function getImgURL2014(Jsonresults){picimage2014 = Jsonresults.href;}
function getImgURL2009(Jsonresults){picimage2009 = Jsonresults.href;}
function getImgURL2004(Jsonresults){picimage2004 = Jsonresults.href;}
function getImgURL2000(Jsonresults){picimage2000 = Jsonresults.href;}		
function getImgURL1995(Jsonresults){picimage1995 = Jsonresults.href;}
function getImgURL1990(Jsonresults){picimage1990 = Jsonresults.href;}
function getImgURL1980(Jsonresults){picimage1980 = Jsonresults.href;}
function getImgURL1970(Jsonresults){picimage1970 = Jsonresults.href;}
function getImgURL1955(Jsonresults){picimage1955 = Jsonresults.href;}
function getImgURL1940s(Jsonresults){picimage1940s = Jsonresults.href;}		
function getImgURL1936(Jsonresults){picimage1936 = Jsonresults.href;}	




function popup_all(){
getmapextents();
bboxCoords = xmin + "," + ymin + "," + xmax + "," + ymax;
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_2014/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL2014");
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_2009/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL2009");
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_2004/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL2004");
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_2000/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL2000");
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_1995/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL1995");
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_1990/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL1990");
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_1980/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL1980");
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_1970/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL1970");
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_1955/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL1955");
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_1940s/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL1940s");
loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_1936/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURL1936");
var t=setTimeout("popupwriteall()",3000);
}
	
	
function popup_ss(){
	getmapextents();
	secondyear = 0;
	bboxCoords = xmin + "," + ymin + "," + xmax + "," + ymax;

		for (var i = 0; i< document.getElementById("layerlistswipe").options.length; i++){
			if (document.getElementById("layerlistswipe").options[i].selected == true){
			firstyear = document.getElementById("layerlistswipe").options[i].value;
			}
		}
		for (var i = 0; i< document.getElementById("layerlistbase").options.length; i++){
			if (document.getElementById("layerlistbase").options[i].selected == true){
			secondyear = document.getElementById("layerlistbase").options[i].value;
			}
		}

	if (secondyear != "0"){ 
	loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_"+firstyear+"/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURLswipe");
	}
	else{
	loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_"+firstyear+"/MapServer/export?bbox="+bboxCoords+"&f=json&size=750,750&callback=getImgURLswipe");
	}
		
	if (secondyear != "0"){
		loadJSon("http://gisprod2:6080/arcgis/rest/services/Aerials/Aerials_"+secondyear+"/MapServer/export?bbox="+bboxCoords+"&f=json&size=425,425&callback=getImgURLbase");
	}
	var t=setTimeout("popupwrite()",1000);
}
	
		
function popupwrite() {
	if (secondyear == "0"){
		newwindow2=window.open('','name','height=920,width=800');
		var tmp = newwindow2.document;
		tmp.write('<html><head><title>AerialAccess </title>');
		tmp.write('<link rel="stylesheet" href="js.css">');
		tmp.write('</head><body>');
		tmp.write('<center><table width=750px><tr><td><img valign=top src="images/aerialaccessbanner.png"><tr><td align=center><img id="firstmap" src="')
		tmp.write(picimage1);
		tmp.write('"></tr>');
		tmp.write('<tr><td>')
		tmp.write("<h3 align=center>"+firstyear+"</h3>");
		tmp.write('</tr><tr><td>');
		
	}	
	else{
		newwindow2=window.open('','name','height=600,width=900');
		var tmp = newwindow2.document;
		tmp.write('<html><head><title>AerialAccess </title>');
		tmp.write('<link rel="stylesheet" href="js.css">');
		tmp.write('</head><body>');
		tmp.write('<center><table><tr><td colspan=2><div  style="background-image:url(\'images/aerialaccessbanner_extend.png\')"  ><img valign=top src="images/aerialaccessbanner.png"></div><tr><td><img id="firstmap" src="')
		tmp.write(picimage1);
		tmp.write('"><td><img id="secondmap" src="');
		tmp.write(picimage2);
		tmp.write('"></tr>');
		tmp.write('<tr><td>')
		tmp.write("<h3  align=center>"+firstyear+"</h3>");
		tmp.write('<td>')
		tmp.write("<h3  align=center>"+secondyear+"</h3>");
		tmp.write('</tr><tr><td colspan=2>');
	}

	
	tmp.write('<div style="background-color:#3d5577; "><a style="color:white; position:relative; right:-399px">AerialAccess is provided by Dutchess County, NY. </a><br></div></td></tr>');
	tmp.write('</table></center><a href="javascript:window.print()" style=\'position:absolute; top:25px; right:50px\'><img border=0 src=\'images/print_panel.png\'></a></body></html>');
	tmp.close();
}
function popupwriteall() {
newwindow2=window.open('','name','height=4000,width=900,scrollbars=yes');
		var tmp = newwindow2.document;
		tmp.write('<html><head><title>AerialAccess </title>');
		tmp.write('<link rel="stylesheet" href="js.css">');
		tmp.write('</head><body>');
	 
		tmp.write('<table><tr><td colspan=2><div  style="background-image:url(\'images/aerialaccessbanner_extend.png\')"  ><img valign=top src="images/aerialaccessbanner.png"></div>')
			//row1
		tmp.write('<tr><td><img src="')
		tmp.write(picimage2014);
		tmp.write('"><td><img src="');
		tmp.write();
		tmp.write('"></tr><tr><td><h3 align=center>2014</h3><td><h3 align=center></h3></tr>');
			

		tmp.write('<tr><td><img src="')
		tmp.write(picimage2009);
		tmp.write('"><td><img src="');
		tmp.write(picimage2004); 
		tmp.write('"></tr><tr><td><h3 align=center>2009</h3><td><h3 align=center>2004</h3></tr>');
		
	//row2
		tmp.write('<tr><td><img src="')
		tmp.write(picimage2000);
		tmp.write('"><td><img src="');
		tmp.write(picimage1995);
		tmp.write('"></tr><tr><td><h3 align=center>2000</h3><td><h3 align=center>1995</h3></tr>');
		
		//row3
		tmp.write('<tr><td><img src="')
		tmp.write(picimage1990);
		tmp.write('"><td><img src="');
		tmp.write(picimage1980);
		tmp.write('"></tr><tr><td><h3 align=center>1990</h3><td><h3 align=center>1980</h3></tr>');
		//row4
		tmp.write('<tr><td><img src="')
		tmp.write(picimage1970);
		tmp.write('"><td><img src="');
		tmp.write(picimage1955);
		tmp.write('"></tr><tr><td><h3 align=center>1970</h3><td><h3 align=center>1955</h3></tr>');
		//row5
		tmp.write('<tr><td><img src="')
		tmp.write(picimage1940s);
		tmp.write('"><td><img src="');
		tmp.write(picimage1936);
		tmp.write('"></tr><tr><td><h3 align=center>1940s</h3><td><h3 align=center>1936</h3></tr></table>');
	
		
		
		tmp.write('<div style="background-color:#3d5577; "><a style="color:white; position:relative; right:-399px">AerialAccess is provided by Dutchess County, NY. </a><br></div></td></tr>');
	tmp.write('</center><a href="javascript:window.print()" style=\'position:absolute; top:25px; right:50px\'><img border=0 src=\'images/print_panel.png\'></a></body></html>');
		
		tmp.close();
		
}
	
//END OF ZOOM TO MUNI EXTENT
	