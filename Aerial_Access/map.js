var map; // Main map object
var gsvc; // Esri conversion service
var graphic; // Geolocation and POI locator
var searchGraphic; // Search results graphic
var desktop = 1200 //Desktop Width break in pixels
var mobile = 480 //Mobile (phone) Width break in pixels
var activeTL = true //Time-lapse tab focus indicator
  // Server path
var serverURL = "http://geoaccess.co.dutchess.ny.us/webadaptor/rest/services/Aerial_Access_v2/MapServer";
//var serverURL = "http://gistest2:6080/arcgis/rest/services/Aerial_Access_v2/MapServer";
var POI_APF = '24' // Dutchess POI APF view layer
var loading; // Layer loding gif
//county extents at full scale
var xmin = 623754,
  ymin = 948369,
  xmax = 780730,
  ymax = 1188759,
  xmini,
  ymini,
  xmaxi,
  ymaxi;
var spr = 2260; // Spatial Reference ID
// Zoom factors
var zoomInFactor = 0.65,
  zoomOutFactor = 1.55
  //width (px) of print images, height (px) of print images
var picWidth = 380,
  picHeight = 260;
var startExtent; // Initial Map Extent object
var overviewMapDijit; // Map overview object
var bboxCoords; // Current Map Extent
var swipeWidget; // Swipe object
var listPOI = []; // array of POI and Address points
var searchResultCount = 100, // Number of search results
  minSearch = 3; // Min search string match
var searchScale = 1253.4820393975292; // Search zoom to scale
var watchId; // Geolocation ID
var mapStop; // layer change event
var carSpeed = 4000; // Init carousel speed
var play = true; // Initiate play button
// Object of layers and Rest Index
var layerLeft, layerRight, layers, counties; //Dynamic Map Serivce Layers
// Explore objects
var turfObj, castleObj, suburbanObj, damObj, airportObj, bridgeObj, shadowObj, quarryObj;
var objects; // List of explore objects
var thumbList = []; // List of Thumb urls
// REST IDs
var yearList = {
  "1936 BW": 8,
  "1940 BW": [9, 10, 11, 12],
  "1955 BW": 13,
  "1970 BW": 14,
  "1980 BW": 15,
  "1990 BW": 16,
  "1995 CIR": 17,
  "2000 TC": 18,
  "2004 TC": 19,
  "2009 TC": 20,
  "2009 CIR": 21,
  "2014 TC": 22,
  "2014 CIR": 23
};
/*
var yearList = {
  "1936 BW": 8,
  "1940 BW": [9, 10, 11, 12],
  "1955 BW": 13,
  "1970 BW": 14,
  "1980 BW": 15,
  "1990 BW": 16,
  "1995 CIR": 17,
  "2000 TC": 18,
  "2004 TC": 19,
  "2004 CIR": 20,
  "2009 TC": 21,
  "2009 CIR": 22,
  "2014 TC": 23,
  "2014 CIR": 24
};
*/
// Initial print values
var leftYear = 8;
var rightYear = 22;
var leftYearText = "1936 BW";
var rightYearText = "2014 TC";

require([
"esri/map",
"esri/config",
"esri/dijit/LayerSwipe",
"dojo/parser",
"dojo/dom",
"esri/geometry/Extent",
"dijit/Toolbar",
"esri/dijit/OverviewMap",
"esri/dijit/Scalebar",
"esri/layers/ArcGISDynamicMapServiceLayer",
"dojo/on",
"dojox/gesture/swipe",
"dojox/gesture/tap",
"esri/SpatialReference",
"dijit/registry",
"esri/tasks/query",
"esri/tasks/QueryTask",
"esri/geometry/Point",
"esri/tasks/GeometryService",
"esri/graphic",
"esri/Color",
"esri/symbols/SimpleMarkerSymbol",
"esri/symbols/SimpleLineSymbol",
"esri/symbols/PictureMarkerSymbol",
"esri/dijit/Search",
"esri/layers/FeatureLayer",
// Non-var
"dijit/layout/TabContainer",
"dijit/layout/ContentPane",
"dijit/layout/BorderContainer",
"dijit/form/Button",
"dojox/image/LightboxNano",
"dijit/dijit", // optimize: load dijit layer
"dojo/domReady!"
], function (
  map,
  esriConfig,
  LayerSwipe,
  parser,
  dom,
  Extent,
  Toolbar,
  OverviewMap,
  Scalebar,
  ArcGISDynamicMapServiceLayer,
  on,
  swipe,
  tap,
  SpatialReference,
  registry,
  Query,
  QueryTask,
  Point,
  GeometryService,
  Graphic,
  Color,
  SimpleMarkerSymbol,
  SimpleLineSymbol,
  PictureMarkerSymbol,
  Search,
  FeatureLayer) {
  parser.parse();

  startExtent = new Extent(xmin, ymin, xmax, ymax, new SpatialReference({
    wkid: spr
  }));


xmini = getQueryVariable("xmin");
	if (xmini != false){
		xmini = parseInt(getQueryVariable("xmin"));
		ymini = parseInt(getQueryVariable("ymin"));
		xmaxi = parseInt(getQueryVariable("xmax"));
		ymaxi = parseInt(getQueryVariable("ymax"));
		
		startExtent = new Extent(xmini, ymini, xmaxi, ymaxi, new SpatialReference({wkid:spr}));
	}

//function to get url variables out of url.... just call it and request what you are looking for... ie getQueryVariable("parcelnum")
	function getQueryVariable(variable){
       var query = window.location.search.substring(1);
	   console.log(query);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}




  // Zoom speeds
  esriConfig.defaults.map.zoomDuration = 200;
  esriConfig.defaults.map.zoomRate = 2;

  map = new map("mapDiv", {
    logo: false,
    navigationMode: "classic",
    slider: false,
    extent: startExtent,
  });


  // Location symbol
  var outline = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([239, 236, 225]), 2);
  //var searchMarker = new PictureMarkerSymbol('./Images/mapIcons/pin-target.png', 40, 40);
  var locateMarker = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 20, outline, new Color([61, 85, 119, 0.8]));
  //var locateMarker = new PictureMarkerSymbol('./Images/mapIcons/pin-target.png', 40, 40);

  // --->>> Loading gif <<<--- // 
  loading = dom.byId("loadingImg");
  on(map, "update-start", function () {
    loading.style.display = 'inline-block';
  });
  on(map, "update-end", function () {
    loading.style.display = 'none';
  });

  // --->>> load dynamic map layers <<<--- // 

  layerRight = new ArcGISDynamicMapServiceLayer(serverURL);
  layerRight.setImageTransparency(false);
  //layerRight.setImageFormat('png24');
  layerRight.setVisibleLayers([22]);
  map.addLayer(layerRight);

  layerLeft = new ArcGISDynamicMapServiceLayer(serverURL);
  layerLeft.setImageTransparency(false);
  //layerLeft.setImageFormat('png24');
  layerLeft.setVisibleLayers([8]);
  map.addLayer(layerLeft);

  counties = new ArcGISDynamicMapServiceLayer(serverURL, {
    "opacity": 0.8,
  });
  counties.setVisibleLayers([5]);
  map.addLayer(counties);

  layers = new ArcGISDynamicMapServiceLayer(serverURL);
  layers.setVisibleLayers([0, 1, 2, 3, 4, 6, 7]);
  map.addLayer(layers);


  // --->>> Scalebar widget <<<--- // 
  var scalebar = new Scalebar({
    map: map,
    scalebarUnit: 'english',
    attachTo: "bottom-right"
  });

  // --->>> Overview widget <<<--- // 
  var overviewMapDijit = new OverviewMap({
    map: map,
    attachTo: "bottom-left",
    visible: false,
    expandFactor: "4",
    id: 'overviewDiv'
  });
  overviewMapDijit.startup();

  // --->>> Swipe Widget <<<--- // 
  map.on('load', swipeGen);

  function swipeGen() {
    scope = 0;
    var vertStart = (document.getElementById("mapDiv").offsetWidth / 2) - 3;
    swipeWidget = new LayerSwipe({
      type: "vertical",
      map: map,
      left: vertStart,
      layers: [layerLeft],
    }, 'swipeDiv');
    swipeWidget.startup();
    swipeWidget.on('swipe', setYearLocation);
    swipeWidget.on('load', infoUp)
  }

  //--->>> Add list of aerials to dropdown menu <<<---//
  map.on('load', aerials);

  function aerials() {
    var yearDowns = $(".dropdown");
    var keys = Object.keys(yearList);
    for (var i = 0; i < yearDowns.length; i++) {
      for (var y = 0; y < keys.length; y++) {
        var id = yearList[keys[y]];
        var elem = '<li class="' + keys[y] + '" value="' + y + '">' + keys[y] + '</li>';
        $('.dropdown > ul').eq(i).append(elem);
      }
    }

    if ($('#mapDiv').outerWidth() > desktop) { // Desktop
      // Generate thumb urls
      function getThumbs() {
        for (var i = 0; i < Object.keys(yearList).length; i++) {
          thumbList[i] = serverURL + '/export?transparent=false&format=jpg&layers=show%3A' + yearList[Object.keys(yearList)[i]] + '&bbox=' + bboxCoords + '&bboxSR=2260&imageSR=2260&size=100%2C100&f=image';
        }
        preloadImages(thumbList);
      }

      // Cache Thumbs
      function preloadImages(srcs) {
        if (!preloadImages.cache) {
          preloadImages.cache = [];
        }
        var img;
        for (var i = 0; i < srcs.length; i++) {
          img = new Image();
          img.src = srcs[i];
          preloadImages.cache.push(img);
        }
      }

      map.on('update-start', getThumbs)
      map.on('extent-change', getThumbs)

      // Display thumbs on hover
      $('.dropdown-menu li').hover(function () {
        var val = parseInt($(this).attr('value'));
        var thumbTop = ($(this).offset().top - 62) + 'px';
        var thumb = $(this).closest('.yearDropDown').find('.yearThumb');
        var thumbURL = 'url("' + thumbList[val] + '")'
        thumb.css({
          'top': thumbTop,
          'background-image': thumbURL
        });
        thumb.toggle();
      })
    }

    // Toggle scrollbars for dropdown-menus
    var dropdownHeight = $('.dropdown-menu').height();
    var mapHeight = $('#mapDiv').height();
    var headerHeight = $('#infoHeader').height();
    if (dropdownHeight > (mapHeight - headerHeight)) {
      $('.dropdown-menu').css({
        'max-height': '200px',
        'overflow-y': 'auto'
      });
    }
  }

  // Change Aerials on click
  map.on('load', function () {
    $('.dropdown > ul > li').on('click', changeAerial)
      //$('.dropdown > ul > li').bind('touchstart', changeAerial)
    function changeAerial() {
      if ($(this).closest('#dL').length) {
        leftYear = [yearList[$(this).html()]];
        leftYearText = $(this).html();
        layerLeft.setVisibleLayers(leftYear);
        $('#sel1 > .activeYear').html($(this).html())
      } else if ($(this).closest('#dR').length) {
        rightYear = [yearList[$(this).html()]];
        rightYearText = $(this).html();
        layerRight.setVisibleLayers(rightYear);
        $('#sel2 > .activeYear').html($(this).html());

      } else {}
    };
  })


  // --->>> zoom buttons <<<--- // 

  map.hideZoomSlider();

  // --->>> Zoom in <<<--- //
  $('.zoomin').click(zoomIn);

  function zoomIn() {
    map.centerAndZoom(map.extent.getCenter(), zoomInFactor)
  }
  // --->>> Zoom out <<<--- //
  $('.zoomout').click(zoomOut);

  function zoomOut() {
    map.centerAndZoom(map.extent.getCenter(), zoomOutFactor)
  }
  // --->>> Zoom top extent <<<--- // 
  $('.zoomext').click(zoomExt);

  function zoomExt() {
    map.setExtent(startExtent)
    if ($('.toggleID > span').hasClass('glyphicon-eye-close')) {
      toggle()
    }
  }

  // --->>> Zoom too location<<<--- // 

  gsvc = new GeometryService("http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");
  $('.zoomloc').click(location);

  function location() {
    navigator.geolocation.clearWatch(watchId);
    map.graphics.clear()
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(zoomToLocation, locationError);
      watchId = navigator.geolocation.watchPosition(showLocation, locationError);
    } else {
      alert("Browser doesn't support Geolocation. Visit http://caniuse.com to see browser support for the Geolocation API.");
    }
  }

  function locationError(error) {
    //error occurred so stop watchPosition
    if (navigator.geolocation) {
      navigator.geolocation.clearWatch(watchId);
    }
    switch (error.code) {
    case error.PERMISSION_DENIED:
      alert("Location not provided");
      break;

    case error.POSITION_UNAVAILABLE:
      alert("Current location not available");
      break;

    case error.TIMEOUT:
      alert("Timeout");
      break;

    default:
      alert("unknown error");
      break;
    }
  }

  //initial zoom to the users location 
  function zoomToLocation(location) {
    var point = new Point(location.coords.longitude, location.coords.latitude);
    gsvc.project([point], new SpatialReference({
      wkid: spr
    }), function (projectedPoints) {
      var pt = projectedPoints[0];
      addGraphic(pt);
      map.centerAndZoom(pt, searchScale / map.getScale());
    })
  }

  // existing zoom to the users location
  function showLocation(location) {

    var point = new Point(location.coords.longitude, location.coords.latitude);
    gsvc.project([point], new SpatialReference({
      wkid: spr
    }), function (projectedPoints) {
      var pt = projectedPoints[0];
      if (!graphic) {
        addGraphic(pt);
      } else { // move the graphic if it already exists
        graphic.setGeometry(pt);
      }
      //map.centerAt(pt);
    })
  }

  // Add location graphic
  function addGraphic(pt) {
    graphic = new Graphic(pt, locateMarker);
    map.graphics.add(graphic);
  }

  // Clear location when user searches
  function stopLocation() {
    navigator.geolocation.clearWatch(watchId);
    map.graphics.clear();
  }

  // If user leaves tab, clear location 
  $(window).blur(function () {
    navigator.geolocation.clearWatch(watchId);
  });

  // --->>> Toggle vector layers <<<--- // 
  $('.toggleID').click(toggle);

  function toggle() {
    if ($('.toggleID > span').hasClass('glyphicon-eye-open')) {
      $('.toggleID > span').switchClass('glyphicon-eye-open', 'glyphicon-eye-close')
      map.removeLayer(layers);
      //map.graphics.hide();
    } else {
      $('.toggleID > span').switchClass('glyphicon-eye-close', 'glyphicon-eye-open')
      map.addLayer(layers);
      //map.graphics.show();
    }
  }

  // --->>> Init Time-lapse play button <<<--- // 

  // Highlight play button on first click
  function initPlay(init) {
    if (init) {
      function highlight() {
        $('.playCar').animate({
          backgroundColor: "rgb(61,85,119)"
        }, 200, function () {
          $('.playCar').animate({
            backgroundColor: "rgb(239, 236, 225)"
          }, 200);
        });
      }
      var pulseCount = 4;
      for (var i = 0; i < pulseCount; i++) {
        setTimeout(highlight, i * 500);
      }
    }
  }
  // Time-lapse control
  function ctrTL() {
    if ($('.playCar > span').hasClass('glyphicon-play')) {
      $('.playCar > span').switchClass('glyphicon-play', 'glyphicon-pause');
      $('.playCar').attr('title', 'pause');
      if ($("#info").height() === 250 || $("#infoBar").height() === '100%') {
        infoUp()
      }
      playTL();
    } else {
      stopTL();
    }
  }
  // Time-lapse stop
  function stopTL() {
    $('.playCar > span').switchClass('glyphicon-pause', 'glyphicon-play');
    $('.playCar').attr('title', 'play');
    $(".progress-bar").stop();
    //$(".progress-bar").finish();
    $(".progress-bar").css('width', '0%')
    mapStop.remove();
  }
  // Time-lapse play
  function playTL() {
    mapStop = on(layerRight, "update-end", function () {
      progressBar();
    });
    progressBar();
  }
  // Progress bar control
  function progressBar() {
    $(".progress-bar").animate({
      width: "100%",
      easing: 'linear'
    }, carSpeed, function () {
      $('.progress-bar').width('0%');
      $('#carouselTL').carousel('next');
    });
  }

  //--->>> Explore and Terminology navigation tabs) <<<---//
  $("#terminologyTab,#exploreTab ").click(nonTLTab);

 function nonTLTab() {
    if (activeTL) {} else {
      console.log($(this).hasClass('active'))
      if ($('.playCar > span').hasClass('glyphicon-pause')) {
        stopTL()
      }
      $(".playCar").off("click")
      map.addLayer(layerLeft)
      map.reorderLayer(counties, 3);
      map.reorderLayer(layers, 4);
      swipeWidget.enable();
      //swipeWidget.set("type", "vertical");
    

      $('#sel2 > .activeYear').html($('.item.active > .year > h5').html());

      activeTL = true;
    }
	  if ($("#info").height() === 0 || $("#infoBar").height() === 50) {
        infoUp()
      }
  };

  //--->>> Time-Lapse navigation Tab <<<---//
  $("#Time-LapseTab").click(TLTab)

  function TLTab() {

    initPlay(play);
    play = false;
    if (activeTL) {
      $(".playCar").click(ctrTL);
      layerRight.setVisibleLayers([8]);
      setTimeout(function () {
        swipeWidget.disable();
        map.removeLayer(layerLeft);
      }, 50);
      $(".carousel-indicators > li,#carItems > .item").each(function () {
        $(this).removeClass('active')
      })
      setTimeout(function () {
        $('#carDefault').addClass('active');
        $('#carItems').children('div').first().addClass('active');
        $('#yearTitle > span').text('1936 BW');
      }, 1)
      map.on("update-end", function () {
        $('#yearTitle > span').css({
          'transition': 'none',
          'margin-left': '0%',
          'margin-right': '0%',
          'opacity': '1'
        })
        $('#yearTitle > span').html($('.item.active > .year > h5').html());
        if (document.getElementById('mapDiv').offsetWidth < mobile) {
          $('.arrow').css('display', 'inline-block');
        }
      });
    }
    if ($("#info").height() === 0 || $("#infoBar").height() === 50) {
      infoUp()
    }


    activeTL = false;
  };

  //--->>> Get map extent coordinates <<<---//
  map.on('extent-change', getMapCoord)

  function getMapCoord() {
    bboxCoords = map.extent.xmin + "%2C" + map.extent.ymin + "%2C" + map.extent.xmax + "%2C" + map.extent.ymax;
    //console.log(bboxCoords)
    //console.log("map current extent =\n " + " xmin: " + map.extent.xmin + " ymin: " + map.extent.ymin + " xmax: " + map.extent.xmax + " ymax: " + map.extent.ymax);
  }

  //--->>> Get map scale <<<---//
  map.on('extent-change', getScaleRange)

  function getScaleRange() {
    function addCommas(intNum) {
      return (intNum + '').replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    var scale = Math.round(map.getScale() / 12) //ft
    var scaleComma = addCommas(scale);
    $('#scaleRange').html(' Scale 1 in : ' + scaleComma + " ft");
  }

  $('.esriScalebar').hover(function () {
    $('#scaleRange').toggle();
  })

  //--->>> Search Bar Mobile View <<<---//
  $(document).ready(function () {
    if (document.getElementById('mapDiv').offsetWidth < mobile) {
      $('#searchClear').click(function () {
        if ($(this).hasClass('glyphicon-search')) {
          if ($('.search').width() < 40) {
            $('.search').animate({
              width: "100%",
            });
            $('#autocomplete').animate({
              "opacity": "1",
              height: "44px"
            });
            $('#searchClear').animate({
              "color": "gray",
              "top": "17px"
            })
            setTimeout(function () {
              $('#searchClear').css('backgroundColor', 'white')
            }, 500)
          } else {
            $('.search').animate({
              width: "0px",
            });
            $('#autocomplete').animate({
              "opacity": "0",
              height: "28px"

            });
            $('#searchClear').animate({
              "color": "white",
              "top": "10px"
            })
            $('#searchClear').css('backgroundColor', 'transparent')
          }
        }
      })
    }
  })



  layers.on('load', function () {
    //--->>> Search bar <<<---// 
    function getSearchLocation(event, ui) {
      var icon;
      var street = ui.item.value.split(', ')[0]
      var city = ui.item.value.split(', ')[1]
      var iconURL = './Images/mapIcons/pin-' + city.toLowerCase() + '.png';
      $.ajax({ // Set search icons
        url: iconURL,
        async: false,
        success: function (data) {
          icon = iconURL;
        },
        error: function () {
          console.log('Using default icon');
          icon = './Images/mapIcons/pin-APF.png';
        }
      });
      var addressMarker = new PictureMarkerSymbol(icon, 35, 43);
      var sql = "TopItem = '" + street.replace(/\'/g, '\'\'') + "' AND BottomItem = '" + city.replace(/\'/g, '\'\'') + "'";
      var queryTask = new QueryTask(serverURL + "/" + POI_APF);
      var query = new Query();
      query.returnGeometry = false;
      query.outFields = ["northing", "easting"]
      query.where = sql;
      queryTask.execute(query, function (result) {
        if (searchGraphic) {
          map.graphics.remove(searchGraphic);
        }
        var easting = result.features[0].attributes.easting
        var northing = result.features[0].attributes.northing
        searchPoint = new Point(easting, northing, new SpatialReference({
          wkid: spr
        }));
        searchGraphic = new Graphic(searchPoint, addressMarker);
        map.graphics.add(searchGraphic);
        if (event.type === "autocompleteselect") {
          var factor = searchScale / map.getScale();
          map.centerAndZoom(searchPoint, factor);
        }
      });
    }
    // Search results
    $('#autocomplete').autocomplete({
      source: function (request, response) { // Source - POI APF
        $.ajax({
          url: serverURL + "/" + POI_APF + "/query",
          dataType: "jsonp", // with this dataType, cache is set to false by default
          data: {
            where: "TopItem LIKE '%" + request.term.replace(/\'/g, '\'\'') + "%' OR BottomItem LIKE  '%" + request.term.replace(/\'/g, '\'\'') + "%'", //makes single quotes into double for sql  
            outFields: "TopItem, BottomItem",
            returnGeometry: false,
            f: "pjson"
          },
          success: function (data) {
            $('#autocomplete').removeClass('ui-autocomplete-loading');
            if (data.features) {
              response($.map(data.features.slice(0, searchResultCount), function (item) { //only display first 25  
                return {
                  label: ' ' + item.attributes.TopItem + ', ' + item.attributes.BottomItem,
                  value: item.attributes.TopItem + ', ' + item.attributes.BottomItem
                }
              }));
            }
          }
        })
      },
      minLength: minSearch, /// Minimum search value
      focus: function (event, ui) { // Results focus event
        getSearchLocation(event, ui);
      },
      select: function (event, ui) { // Results select event
        this.blur();
        map.graphics.clear();
        $('#searchClear').switchClass('glyphicon-search', 'glyphicon-remove');
        $('#searchClear').hover(function () {
          $(this).css('cursor', 'pointer')
        });
        getSearchLocation(event, ui);

      },
      change: function (event, ui) { // Results change event
        map.graphics.clear();
      },
      error: function (error) { // error log
        console.log(error)
      }
    });
    $('#searchClear').click(function () { // Clear search text and graphics
        if ($('#searchClear').hasClass('glyphicon-remove')) {
          map.graphics.remove(searchGraphic);
          //stopLocation();
          $('#searchClear ').switchClass('glyphicon-remove', 'glyphicon-search');
          $('#searchClear').hover(function () {
            $(this).css('cursor', 'default')
          });
          $('#autocomplete').val('');
        }



      })
      // Match Suggestions String and add icon
    $.ui.autocomplete.prototype._renderItem = function (ul, item) {
      var poi = item.value.split(', ')[1].toLowerCase()
      var icon;
      var iconURL = './Images/mapIcons/' + poi + '.png';
      var img = new Image();
      $.ajax({
        url: iconURL,
        error: function () {
          console.log('Using default icon')
          img = null;
        },
        success: function (data) {
          img.src = iconURL;
        }
      });
      item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
      return $("<li></li>")
        .data("item.autocomplete", item)
        .append(img)
        .append("<a style='text-align:left'>" + item.label + "</a>")
        .appendTo(ul);
    };
  });


  //--->>> Initialize explore <<<---// 
  map.on('load', initExplore)

  function initExplore() {
    var exploreTable = document.getElementById('exploreTable');
    for (var r = 0; r < exploreTable.rows.length; r++) {
      for (var c = 0; c < exploreTable.rows[r].cells.length; c++) {
        on(exploreTable.rows[r].cells[c], 'click', exploreExtent)
      }
    }
  }

  function exploreExtent(POI) {
    var POI = this;
    var spRef = new SpatialReference({
      wkid: spr
    });

    $("#activeExplore").css({
      'opacity': '0',
      'transition': 'all 0.5s'
    });

    // Hide Layers when exploring
    if ($('.toggleID > span').hasClass('glyphicon-eye-open')) {
      toggle()
    }

    // Explore Extents
    var turfExtent = new Extent(653972.0605627173, 1038230.1698834004, 657628.0498442932, 1040108.2167584003, spRef);
    var castleExtent = new Extent(630726.6740991405, 954446.3341173367, 633811.4150554707, 956030.9361681181, spRef);
    var suburbanExtent = new Extent(641006.3557177, 1070860.7064804072, 651974.3235624277, 1076494.8471054072, spRef);
    var damExtent = new Extent(650591.0237627745, 1101986.8198639536, 665214.9808890781, 1109499.0073639536, spRef);
    var airportExtent = new Extent(653220.9884041559, 1013305.3548797376, 667844.9455304595, 1020817.5423797376, spRef);
    var bridgeExtent = new Extent(640933.982298456, 1142773.6101055052, 648245.9608616078, 1146529.7038555052, spRef);
    var shadowExtent = new Extent(673575.5209506486, 1047513.7263371802, 674147.9300126954, 1047807.7674208388, spRef);
    var quarryExtent = new Extent(673770.2655823197, 1065141.2650712754, 685615.6708546273, 1071226.1369462763, spRef);
    var treeExtent = new Extent(710523.0352965525, 1083908.8181081621, 716007.0192189167, 1086725.8884206621, spRef);

    turfObj = {
      extent: turfExtent,
      lYear: 23,
      rYear: 22,
      lIndex: 12,
      rIndex: 11,
      title: 'Turf',
      text: 'Unlike vegetation, artificial material such as astroturf absorbs large amounts of infrared light. Slide over the track and watch how the field does not change.',
      left: '<b>2014</b> Color Infrared - ',
      right: '<b>2014</b> True Color'
    }

    castleObj = {
      extent: castleExtent,
      lYear: 8,
      rYear: 22,
      lIndex: 0,
      rIndex: 11,
      title: 'Bannerman Castle',
      text: 'The ruins of Bannerman Castle continue to fall into disrepair. Coupled with a steady rise in sea level, Pollepel Island and the old ruins are rapidly disappearing.',
      left: '<b>1936</b> Black & White -',
      right: '<b>2014</b> True Color'
    }

    suburbanObj = {
      extent: suburbanExtent,
      lYear: 13,
      rYear: [9, 10, 11, 12],
      lIndex: 2,
      rIndex: 1,
      title: 'Suburban Growth',
      text: 'As veterans returned home from World War II, they settled into suburban areas to start families. During the 1950s, you can see rapid development and expansion of the suburbs and the subsequent beginning of the "baby boom." Use the slider to show the suburban expansion of Hyde Park.',
      left: '<b>1955</b> Black & White - ',
      right: '<b>1950</b> Black & White'
    }

    damObj = {
      extent: damExtent,
      lYear: 22,
      rYear: 13,
      lIndex: 11,
      rIndex: 2,
      title: 'DeFlora Brothers Dam',
      text: 'Building of the DeFlora Brothers Dam formed what is now called Apple Lake. Use the slider to show the the area before and after the dam was constructed.',
      left: '<b>2014</b> True Color - ',
      right: '<b>1955</b> Black & White'
    }

    airportObj = {
      extent: airportExtent,
      lYear: 22,
      rYear: [9, 10, 11, 12],
      lIndex: 11,
      rIndex: 1,
      title: 'Dutchess County Airport',
      text: 'Since its construction in the 1930s, the Dutchess County Airport has gone through several changes. Most noticeably, the main runway was lengthened to 5,001 (ft) to accommodate larger aircraft.',
      left: '<b>2014</b> True Color - ',
      right: '<b>1940</b> Black & White'
    }

    bridgeObj = {
      extent: bridgeExtent,
      lYear: 22,
      rYear: 13,
      lIndex: 11,
      rIndex: 2,
      title: 'Kingston-Rhinecliff Bridge',
      text: 'Construction began on the "continuous under-deck truss" style bridge in the mid 1950s, replacing the sporadic and often unreliable ferry service.',
      left: '<b>2014</b> True Color - ',
      right: '<b>1955</b> Black & White'
    }

    shadowObj = {
      extent: shadowExtent,
      lYear: 21, //21
      rYear: 23, //19
      lIndex: 10,
      rIndex: 12,
      title: 'Shadows',
      text: 'Shadows are often an unwanted side-affect in aerial photography, primarily because they make land classification more difficult. However, using more advanced photo-interpretation techniques it is possible to calculate time of day and even the height of structures using the size and position of shadows. ',
      left: '<b>2009</b> True Color - ',
      right: '<b>2004</b> True Color'
    }

    quarryObj = {
      extent: quarryExtent,
      lYear: 22,
      rYear: 13,
      lIndex: 11,
      rIndex: 2,
      title: 'Quarry',
      text: 'The site of Dutchess Quarry & Supply Co. Inc.  has expanded steadily over the last century. It now operates as a subsidiary of Peckham Industries, Inc. ',
      left: '<b>2014</b> True Color - ',
      right: '<b>1955</b> Black & White'
    }

    treeObj = {
      extent: treeExtent,
      lYear: 19,
      rYear: 23,
      lIndex: 8,
      rIndex: 12,
      title: 'Tree Farm',
      text: 'The Tree Farm north of the Village of Millbrooke',
      left: '<b>2004</b> Color Infrared - ',
      right: '<b>2014</b> Color Infrared'
    }

    function delayContentExplore(place) {
      $('#exploreTitle').html(place.title);
      $('#exploreText').html(place.text);
      $("#activeExplore > span").eq(0).html(place.left);
      $("#activeExplore > span").eq(1).html(place.right);
      $("#activeExplore").css('opacity', '1')
    }

    function layerExplore(place) {
      map.setExtent(place.extent);
      layerLeft.setVisibleLayers([place.lYear]);
      layerRight.setVisibleLayers([place.rYear]);
      leftYear = [place.lYear];
      rightYear = [place.rYear];
      leftYearText = Object.keys(yearList)[place.lIndex];
      rightYearText = Object.keys(yearList)[place.rIndex];
      $('#sel1 > .activeYear').html(Object.keys(yearList)[place.lIndex]);
      $('#sel2 > .activeYear').html(Object.keys(yearList)[place.rIndex])
    }

    objects = [turfObj, castleObj, suburbanObj, damObj, airportObj, bridgeObj, shadowObj, quarryObj];

    for (var i = 0; i < objects.length; i++) {
      if (this.innerHTML === objects[i].title) {
        setTimeout(delayContentExplore, 500, objects[i]);
        layerExplore(objects[i]);
      }
    }

    clearExploreTable(sel2, POI);
  }

  // Explore Selection
  function clearExploreTable(sel2, POI) {
    $('#exploreTable td').each(function () {
      console.log(POI.innerHTML)
      if ($(this).html() === POI.innerHTML) {
        POI.className = 'activeTD'
      } else {
        $(this).removeClass('activeTD');
      }
    })
  };


  /*
      // Mobile info swipe
      
    on(dom.byId('infoHeader'), swipe, function (e) {
      if (Math.abs(e.dy) > Math.abs(e.dx)) {
        infoUp();
      }
    });
    on(dom.byId('exploreTab'), tap, function () {
      viewInfo(this)
    })

    on(dom.byId('Time-LapseTab'), tap, function () {
      viewInfo(this)
    })

    on(dom.byId('terminologyTab'), tap, function () {
      viewInfo(this)
    })
  */

})


//////////////////////////////////////////////////////////////////////////////////////////////
//                               <--- Start Functions --->
//////////////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////
// --->>> Navigation tabs <<<--- //
/////////////////////////////////////////////

function viewInfo(tag) {
  $('.yearDropDown').css("transition", 'opacity 0.5s');
  $('#yearTitle').css("transition", 'opacity 0.5s');
  $('.playCar').css("transition", 'opacity 0.5s');
  $('.nav.nav-tabs > li > a').removeClass();
  if (tag.innerHTML === 'Time-Lapse') {
    $('.yearDropDown').css("opacity", '0');
    $('#yearTitle').css({
      "visibility": 'visible',
      "opacity": '1'
    });
    $('.playCar').css({
      "opacity": '1.0 ',
      "cursor": 'pointer',
    });
    $('.playCar').hover(function () {
      $('.playCar').attr('style', 'padding: 0; background-color:rgb(61,85,119) !important;color:rgb(239, 236, 225);')
    }, function () {
      $('.playCar').attr('style', 'padding: 0; background-color: "inherit" !important;color:"inherit";');
    })
    $('.playCar').attr('title', 'play');
    setTimeout(function () {
      $('.yearDropDown').css("visibility", 'hidden');
    }, 500)
  } else {
    $('.yearDropDown').css({
      "visibility": 'visible',
      "opacity": '1'
    });
    $('#yearTitle').css("opacity", '0.0');
    $('.playCar').unbind('mouseenter mouseleave');
    $('.playCar').css({
      "opacity": '0.4',
      "cursor": 'default'
    });
    $('.playCar').attr('title', 'Time-Lapse');
    setTimeout(function () {
      $('#yearTitle').css("visibility", 'hidden');
    }, 500)
  }
  tag.className = 'focused';
}


///////////////////////////////////
// --->>> Info Tab open close<<<--- 
///////////////////////////////////

function infoUp() {
  $("#info").css('transition', 'height 0.5s')
  $("#infoBar").css('transition', 'height 0.5s')
  $('.handle').eq(0).css('transition', 'top 0.5s')
  $('.scalebar_bottom-right').eq(0).css('transition', 'bottom 0.5s')
    //$('#infoView').css('transition', 'bottom 0.5s');
  $('#scaleRange').css('transition', 'bottom 0.5s');
  $('#yearTitle').css("transition", 'opacity 0.5s');
  if ($('#mapDiv').outerWidth() > mobile) { // If not phone
    if ($("#info").height() === 0 || $("#infoBar").height() === 50) { // If Down
      $("#info").css('display', 'inherit');
      if ($('.active').has('#Time-LapseTab')) {
        $('#yearTitle').css("opacity", '0.0');
      }
      setTimeout(function () {
        $("#info").height('250');
        $('.handle').eq(0).css('top', '35%');
        $('.scalebar_bottom-right').eq(0).css('bottom', '253px');
        $('#scaleRange').css('bottom', '288px');
        //$('#yearTitle').css("visibility", 'hidden');
      }, 1);
    } else {
      $("#info").height('0');
      setTimeout(function () {
        $("#info").css('display', 'none')
      }, 500);
      $('.handle').eq(0).css('top', '50%');
      $('.scalebar_bottom-right').eq(0).css('bottom', '3px')
      $('#scaleRange').css('bottom', '38px');
      if ($('.active').has('#Time-LapseTab')) {
        $('#yearTitle').css("opacity", '1.0');
      }
    }
  } else { // Mobile 
    //console.log('mobile')
    if ($("#infoBar").css('height') === '50px') {
      //$("#info").css('display', 'inherit');
      $("#infoBar").css('height', '100%');
    } else { //if up
      $("#infoBar").css('height', '50px');
    }
  }
}


/////////////////////////////////////////////////////////
//--->>> Adjust horizontal position of dropdowns <<<---//
/////////////////////////////////////////////////////////

function setYearLocation() {
  var vertLoc = parseInt(document.getElementsByClassName("vertical")[0].style.left)
  var yL = document.getElementById('yearLeftContainer');
  var yR = document.getElementById('yearRightContainer');
  var mapWidth = document.getElementById('mapDiv').offsetWidth;
  var yld = document.getElementById('yearLeftDIV');
  var yrd = document.getElementById('yearRightDIV');
  yL.style.left = vertLoc - (yL.offsetWidth + 10) + 'px'
  yR.style.left = vertLoc + 18 + 'px'
  yld.style.width = vertLoc + 'px';
  yrd.style.width = mapWidth - vertLoc - 8 + 'px';
}


/////////////////////////////////////////////////////
//--->>>Add slide animation to dropdown menus <<<---/
////////////////////////////////////////////////////

$(window).load(function () {
  // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
  $('.dropdown').on('show.bs.dropdown', function (e) {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown({
      "duration": 250
    });
  });

  // ADD SLIDEUP ANIMATION TO DROPDOWN //
  $('.dropdown').on('hide.bs.dropdown', function (e) {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp({
      "duration": 250
    });
  });

});



/////////////////////////////////////
//--->>> Initialize Carousel <<<---//
/////////////////////////////////////

$(document).ready(function () {
  console.log('Initializing Carousel')
    //$('#carouselTL').bind('slide.bs.carousel',
  $('#carouselTL').bind('slid.bs.carousel', function () {
    var $year = $('.item.active > .year > h5').html();
    $('#sel2 > .activeYear').html($year);
    for (var i = 0; i < Object.keys(yearList).length; i++) {
      if ($year === Object.keys(yearList)[i]) {
        layerRight.setVisibleLayers([yearList[Object.keys(yearList)[i]]]);
        //map.addLayer(layerRight);
      }
    }
  });
})


///////////////////////
//--->>> Print <<<---//
///////////////////////

function print() {
  var aTab = document.getElementsByClassName('tab-pane')
  for (var i = 0; i < aTab.length; i++) {
    if (aTab[i].className.indexOf('active') > 0) {
      if (aTab[i].id == 'terminologyTabContent' || aTab[i].id == 'exploreTabContent') { // Print Aerials
        var leftRightPrint = {};
        leftRightPrint[leftYearText] = [leftYear];
        leftRightPrint[rightYearText] = [rightYear];
        console.log(leftRightPrint)
        console.log(yearList)
        printPicList(leftRightPrint)
          //printAerials()
      } else if (aTab[i].id == 'timelapseTabContent') { // Print Time-Lapse
        printPicList(yearList)
      } else {}
    }
  }
}

function printPicList(printYears) {
  var printList = [];
  for (var i = 0; i < Object.keys(printYears).length; i++) {
    var pic = serverURL + "/export?dpi=96&transparent=true&format=png8&layers=show%3A" + printYears[Object.keys(printYears)[i]] + "&bbox=" + bboxCoords + "&bboxSR=2260&imageSR=2260&size=" + picWidth + "%2C" + picHeight + "&f=image";
    console.log(pic)
    printList.push(pic)
  }
  printWindow(printList, printYears)
}

//Generate Print window2
function printWindow(printList, printYears) {

  newwindow2 = window.open('', 'Aerial Access Export', 'height=800,width=900,scrollbars=yes,resizable=yes');
  var tmp = newwindow2.document;
  tmp.write('<html><head><title>Aerial Access</title>');
  tmp.write('<script src="https://code.jquery.com/jquery-2.1.4.js"></script>');
  tmp.write('<script src="bootstrap/js/bootstrap.js"></script>');
  tmp.write('<link href="bootstrap/css/bootstrap.css" rel="stylesheet">')
  tmp.write('<style>body{width: 800px;margin: auto;page-break-inside:avoid;}')
  tmp.write('.closeOut{position:absolute; top: -11px; right: -11px; z-index:9999999; color:gray; cursor:pointer; border-radius:12px; background-color:white; padding:3px; text-align:center;line-height:50%;}')
  tmp.write('.closeOut:hover{color: black;}')
  tmp.write('h3{margin-top:4px;margin-bottom:20px;}')
  tmp.write('h6{margin-bottom:0px;}')
  tmp.write('@media print {.closeOut{display:none;}#printIMG{display:none;}}')
  tmp.write('#printIMG{position:absolute;top:5px;right:25px;}')
  tmp.write('.printYear{display:inline-block;height:280px;position:relative;margin:5px;margin-bottom:20px;page-break-after:avoid;}')
  tmp.write('#footer{background-color: #3d5577;text-align: right; padding:2px; color:white; width:100%;}')
  tmp.write('</style></head><body>')
  tmp.write('<div style="position:relative; background-image:' + 'url("images/aerialaccessbanner_extend.png");' + '">')
  tmp.write('<img valign=top src="images/aerialaccessbanner.png" style="float:left;margin-bottom:25px">');
  tmp.write('<img id="printIMG" style="z-index:99999" border=0 src="images/print_panel.png" onclick="window.print()" ></div>');
  for (var i = 0; i < printList.length; i++) {
    tmp.write('<div class="printYear">')
    tmp.write('<div class="closeOut" onclick="$(this).parent().remove();onlyOne()">' +
      '<span class="glyphicon glyphicon-remove" style="top: 0 px;"></span></div>');
    tmp.write('<img src="' + printList[i] + '">');
    tmp.write('<h3 align=center>' + Object.keys(printYears)[i] + ' </h3></div>')
  }
  tmp.write('<h6>BW=Black and White, TC=True Color, CIR=Color Infrared</h6>');
  tmp.write('<div id="footer">  Aerial Access is provided by Dutchess County, NY.</div>');
  tmp.write('<script>function onlyOne() { var yearList =' + JSON.stringify(printYears) + '; var $pic1 = $(".printYear");if( $pic1.length === 1){ var name = $.trim($pic1.children("h3").html()); var picLarge = "' + serverURL + '/export?dpi=96&transparent=true&format=png8&layers=show%3A"+yearList[name]+"&bbox=' + bboxCoords + '&bboxSR=2260&imageSR=2260&size=760%2C520&f=image";$pic1.children("img").attr("src",picLarge); $pic1.css({"display":"block","height":"auto"});$(".closeOut").remove();}} </script>');
  tmp.write('</body></html> ');
  tmp.close();
  newwindow2.focus();
}


//////////////////////////////////////
//--->>> #infoHeader infoUp() <<<---//
//////////////////////////////////////

$(document).ready(viewBar)

function viewBar() {
  //$('#infoHeader').hover($(this).css('box-shadow', '0px 0px 20px black'))
  $('#infoHeader').on('click', function (e) {
    if (e.target === this) {
      infoUp();
    }
  });
}

/////////////////////////////////////
//--->>> Mobile Orientation <<<---//
/////////////////////////////////////

$(window).on("orientationchange", function (event) {
  location.reload();
});


///////////////////////////////////////////////////
//--->>> Initiate Time Lapse Swipe for Mobile<<<---//
///////////////////////////////////////////////////

$(document).ready(function () {
  $("#carouselTL, #yearTitle").swiperight(function () {
    $("#carouselTL").carousel('prev');
    $('#yearTitle > span').css({
      'transition': 'all 0.5s',
      'margin-left': '100%',
      'opacity': '0'
    })
    $('.arrow').css('display', 'none')
  });
  $("#carouselTL, #yearTitle").swipeleft(function () {
    $("#carouselTL").carousel('next');
    $('#yearTitle > span').css({
      'transition': 'all 0.5s',
      'margin-right': '100%',
      'opacity': '0'
    })
    $('.arrow').css('display', 'none')
  });

  //$('#yearTitle').on('swiperight,swipeleft'){
  //  ('.arrow').css('')
  //}

});

//////////////////////////////////////////
//--->>> Left/Right Year Hide/Show<<<---//
//////////////////////////////////////////

$(document).ready(function () {
  if (document.getElementById('mapDiv').offsetWidth >= desktop) { /* desktop */
    var selIn = {
      'opacity': '0.5',
      'zIndex': '1'
    };
    var selOut = {
      'opacity': '0',
      'zIndex': '-1'
    }
    $('#sel1, #dL').hover(
      function () {
        $('#yearLeftDIV').css(selIn)
      },
      function () {
        $('#yearLeftDIV').css(selOut)
      }
    )
    $('#sel2, #dR').hover(
      function () {
        $('#yearRightDIV').css(selIn)
      },
      function () {
        $('#yearRightDIV').css(selOut)
      }
    )
  }
})



$(window).on('load', function () {
  $('#pageLoading').css('display', 'none');
  $('#contentPane').css('opacity', '1');
  //infoUp();
  $('#loadingImg').css('visibility', 'visible');
});


// END