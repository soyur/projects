British Geological Survey Assignment for Geospatial applications developer.
August 2, 2016
By Sawyer Stippa

A simple, responsive web application displaying four sets of data: Boreholes, Fossils, Measurements, and Rocks. 

JS API:
1. ArcGIS JS API 3.17 https://developers.arcgis.com/javascript/3/
2. JQuery https://jquery.com/
3. Materialize http://materializecss.com/

Notes:
-Data tables were converted to JSON objects to be more accessible.
-Projected from British National Grid to WGS84 for 2 of the 4 datasets.
-Data displayed as graphics (markers on map) and as table.
-Responsive design allows for mobile use (Using materialize framework)
-Bedrock and Surficial geology layers added via dynamic map service.
-Address search widget added, although map graphic needs additional work.
-Borehole and measurement data represented by animations
-Fossil and Rock data represented by images.
-Porosity animation shown by series of circles (particles) resizing depending on actual porosity values.
-Borehole depth animation shown by resizing DIVs based off actual drilled depths.




