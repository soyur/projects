require(["esri/map",
    "esri/graphic",
    "esri/tasks/GeometryService",
    "esri/SpatialReference",
    "esri/geometry/Point",
    "esri/symbols/PictureMarkerSymbol",
    "esri/tasks/ProjectParameters",
    "dojo/Deferred",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/Color",
    "esri/geometry/Extent",
		"esri/dijit/Search",
		"esri/layers/ArcGISDynamicMapServiceLayer",
    // Non-var
    "dojo/domReady!"
], function (
	Map,
	Graphic,
	GeometryService,
	SpatialReference,
	Point,
	PictureMarkerSymbol,
	ProjectParameters,
	Deferred,
	SimpleMarkerSymbol,
	SimpleLineSymbol,
	Color,
	Extent,
	Search,
	ArcGISDynamicMapServiceLayer) {

	var graphicList = [];
	var outSR = new SpatialReference(102100);
	var xmin, ymin, xmax, ymax;
	var visible;

	// Projection service (for British National Grid)
	gsvc = new GeometryService("https://utility.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");

	// Get data name
	var dataName = $('table').attr('id');

	// Create map
	var map = new Map("mapDIV", {
		//		center: [-1.086, 52.875],
		logo: false,
		//zoom: 14,
		basemap: "topo",
	});




	// Initial Extents
	if (dataName === 'rock') {
		xmax = -111405.22086560928;
		xmin = -129750.10765402687;
		ymax = 6963435.349204159;
		ymin = 6954778.855750875;
	} else if (dataName === 'measurement') {
		xmax = -116237.47424738132
		xmin = -123116.8067932284
		ymax = 6962762.942221038
		ymin = 6959251.616234096
	} else if (dataName === 'borehole') {
		xmax = -117309.98130053595
		xmin = -124189.31384638303
		ymax = 6961328.553612086
		ymin = 6957817.227625144
	} else if (dataName === 'fossil') {
		xmax = -118934.26815163872;
		xmin = -122373.93442456228;
		ymax = 6960718.251714356;
		ymin = 6958962.588720884;
	} else {
		xmax = -107002.92576783389;
		xmin = -134520.25595046027;
		ymax = 6969063.025411635;
		ymin = 6953680.0734692635;
	}

	var startExtent = new Extent(xmin, ymin, xmax, ymax, new SpatialReference({
		"wkid": 102100
	}))

	map.setExtent(startExtent)

	// Get active extent ()
//	map.on('mouse-drag', function (evt) {
			//		console.log(map.extent)
			//	})

	//map.on('mouse-move', function (evt) {})

	// Init Search widget
	var search = new Search({
		map: map,
	}, "search");
	search.startup();

	bedrock = new ArcGISDynamicMapServiceLayer("https://map.bgs.ac.uk/arcgis/rest/services/GeologyOfBritain/GeologyViewer_Bedrock/MapServer?", {
		opacity: 0.5,
		visible: false
	});
	map.addLayer(bedrock);


	surficial = new ArcGISDynamicMapServiceLayer("https://map.bgs.ac.uk/arcgis/rest/services/GeologyOfBritain/GeologyViewer_Superficial/MapServer", {
		opacity: 0.5,
		visible: false
	});
	map.addLayer(surficial);

	/******************
			Initialize
	******************/

	map.on('load', init)

	function init() {
		// Load Tables
		getData(dataName);
		$('.dropdown-button').dropdown({
			inDuration: 300,
			outDuration: 225,
			constrain_width: false, // Does not change width of dropdown to that of the activator
			hover: true, // Activate on hover
			gutter: 0, // Spacing from edge
			belowOrigin: false, // Displays dropdown below the button
			alignment: 'right' // Displays dropdown with edge aligned to the left of button
		});

		// Disable navigation if index page
		if (!dataName) {
			map.disableMapNavigation()
			map.hideZoomSlider();
		}


	}


	/*************************************
		Fetch json objects and build table
	*************************************/

	function getData(jsons) {
		$.getJSON({
				url: "https://dl.dropboxusercontent.com/u/17388891/BGS_2nd/data/" + jsons + ".json",
				success: function (data) {

						var json = data;
						var depths = []
						getLocation(data)

						// Get table field names (th)
						$.each(json.fields, function (count, item) {
							var append = '<th data-field="' + item.name + '">' + item.name + '</th>'
							$('table thead tr').append(append)
						})

						// Get features (tr)
						$.each(json.features, function (count, item) {
								var tr = "<tr id='" + json.features[count].attributes.Name + "'></tr>"
								$('table tbody').append(tr);
								fCount = 1;

								// Get attribbutes (td)
								$.each(json.features[count].attributes, function (i, it) {

										var string = '<td>' + it + '</td>'
										$('table tbody tr').eq(count).append(string);

										if (i === "Image") {
											var id = count + 1
											var string = '#f' + id;
											var path = "https://dl.dropboxusercontent.com/u/17388891/BGS_2nd/images/";
											$(string).attr('src', path + it)
										}


										if (i === "Drilled depth (m)") {

											depths.push(it)
										}
									}) // End get attributes (td)
							}) // End get features (tr)
						getRow();
						boreholeDepth(depths);
					} // End Success
			}) // End getJSON
	} // End getData


	/********************
		Get data location
	********************/

	function getLocation(data) {
		// For each feature (row)
		var points = [];
		var attrs = [];
		var initProj;
		for (var f = 0; f < data.features.length; f++) {
			var x = data.features[f].geometry.x;
			var y = data.features[f].geometry.y;
			var z = data.features[f].geometry.z;
			var attr = data.features[f].attributes;
			var sr = data.spatialReference.wkid;

			if (sr !== 4326) { // If British National Grid
				initProj = true;
				var outSR = new SpatialReference(102100);
				var point = new Point({
					"x": x,
					"y": y,
					"spatialReference": {
						"wkid": sr
					}
				})

				points.push(point);
				attrs.push(attr);


			} else { // If WGS84
				addGraphic(x, y, z, sr, attr, attr.Type)
			}
		}

		if (initProj) {
			project(points, attrs)
		}
	}


	/******************
				Project 
	******************/

	function project(points, attrs) {
		var deferred = gsvc.project(points, outSR, function (projectedPoints) {
			//console.log(projectedPoints)
			var pt = projectedPoints;
			for (var i = 0; i < pt.length; i++) {
				addGraphic(pt[i].x, pt[i].y, attrs[i].Z, outSR, attrs[i], attrs[i].Type)
			}
		});
	}


	/******************
		Add data to map
	******************/

	function addGraphic(px, py, pz, spRef, attrs, name) {

		// Gen symbol
		var line = new SimpleLineSymbol();
		//line.setStyle(SimpleLineSymbol.STYLE_NULL);
		line.setWidth(1);
		var marker = new SimpleMarkerSymbol();
		marker.setSize(40);
		marker.setColor(new Color([255, 138, 101, 1]));
		marker.setOutline(line);
		marker.setOffset(0, 200)
			//marker.setPath("M16,3.5c-4.142,0-7.5,3.358-7.5,7.5c0,4.143,7.5,18.121,7.5,18.121S23.5,15.143,23.5,11C23.5,6.858,20.143,3.5,16,3.5z M16,14.584c-1.979,0-3.584-1.604-3.584-3.584S14.021,7.416,16,7.416S19.584,9.021,19.584,11S17.979,14.584,16,14.584z");
		marker.setPath("M16,3.5c-4.142,0-7.5,3.358-7.5,7.5c0,4.143,7.5,18.121,7.5,18.121S23.5,15.143,23.5,11C23.5,6.858,20.143,3.5,16,3.5z M16,14.584c-1.979");
		marker.setStyle(SimpleMarkerSymbol.STYLE_PATH);

		// Gen graphic
		var myPoint = {
			"geometry": {
				"x": px,
				"y": py,
				"z": pz,
				"spatialReference": {
					"wkid": spRef
				}
			},
			"attributes": attrs,
			"symbol": marker.toJson(),
		};

		// Add graphic
		var graphic = new Graphic(myPoint)
		map.graphics.add(graphic)

		// Animate graphic drop
		drop(graphic)

		// Add to list of graphics
		graphicList.push(graphic)

	} // End addGraphic


	/************************
		Animate graphic drop
	************************/

	function drop(graphicMark) {
		time = 1

		function dropGO(mark, offset, time) {
			setTimeout(function () {
				mark.symbol.setOffset(0, offset);
				mark.draw();
			}, time)
		}
		// from marker offset (200) to half marker height (20) 
		for (var i = 199; i > 19; i--) {
			dropGO(graphicMark, i, time)
			time = time + 1
		}
	}


	/************************
		Toggle Table
	************************/

	// Toggle table (header)
	$('thead').click(function () {
		$('#tableContainer').slideToggle(500);
		$('#tableToggleContainer').fadeIn(500);
	})

	// Toggle table (button)
	$('#tableToggleContainer').click(function () {
		$('#tableContainer').slideToggle(500);
		$('#tableToggleContainer').fadeOut(500)
	})


	/******************
		Toggle Geology
	*******************/

	$('#geologyToggleContainer').click(function () {
		var $this = $(this).children('a');
		var visible;
		if ($this.text() === "Geology") {
			bedrock.setVisibility(true);
			$this.text('Bedrock');
			if (!bedrock.isVisibleAtScale(map.getScale())) {
				$('#scaleAlert').fadeIn();
			}
		} else if ($this.text() === "Bedrock") {
			bedrock.setVisibility(false)
			surficial.setVisibility(true)
			$this.text('Surficial');
			if (!surficial.isVisibleAtScale(map.getScale())) {
				$('#scaleAlert').fadeIn();
			}

		} else {
			surficial.setVisibility(false);
			$this.text('Geology');
			$('#scaleAlert').fadeOut();
		}
	})

	// Scale Alert toggle
	map.on('extent-change', function () {
		if ($('#geologyToggleContainer a').text() !== 'Geology') {
			var b = bedrock.isVisibleAtScale(map.getScale())
			var s = surficial.isVisibleAtScale(map.getScale())
			if (b || s) {
				$('#scaleAlert').fadeOut();
			} else {
				$('#scaleAlert').fadeIn();
			}
		}
	})

	/************************
			Show popup
	************************/

	function showInfo(evt, row) {
		var graphic;

		// If hovering table row or map graphic
		if (row) {
			map.infoWindow.show(evt.geometry);
			graphic = evt
		} else {
			map.infoWindow.show(evt.mapPoint);
			graphic = evt.graphic;
		}

		var type = graphic.attributes.Type

		// Popup content depending on data type
		if (type === 'Fossil') {
			map.infoWindow.setTitle(graphic.attributes.Name + ' ' + graphic.attributes.Species);
			var image = 'https://dl.dropboxusercontent.com/u/17388891/BGS_2nd/images/' + graphic.attributes.Image
			var string = '<img src="' + image + '" width="100%" height="100%">'
			map.infoWindow.setContent(string);

		} else if (type === 'Rock') {
			map.infoWindow.setTitle(graphic.attributes.Name + ' ' + graphic.attributes['Rock name']);
			var image = 'https://dl.dropboxusercontent.com/u/17388891/BGS_2nd/images/' + graphic.attributes.Image
			var string = '<img src="' + image + '" width="100%" height="100%">'
			map.infoWindow.setContent(string);

		} else if (type === 'Measurement') {
			map.infoWindow.setTitle(graphic.attributes.Name + ' Porosity: ' + graphic.attributes.Porosity);
			map.infoWindow.setContent($('#measurementContainer').html());
			animateMeasurements(graphic);

		} else if (type === 'Borehole') {
			map.infoWindow.setTitle(graphic.attributes.Name + ' Drilled Depth: ' + graphic.attributes['Drilled depth (m)'] + '(m)');

			map.infoWindow.setContent($('#boreholeContainer').html());
			animateBoreholes(graphic);

		} else {
			console.log('error')
		}
	} // End showInfo


	/******************
		On graphic click
	******************/

	map.on('click', function (evt) {
		if (evt.graphic) {
			showInfo(evt)
		}
	})


	/******************
		On graphic hover
	******************/

	map.on('mouse-move', function (evt) {

		// If hovering over graphic
		if (evt.graphic) {
			// If popup is not already showing for graphic
			if (!map.infoWindow.isShowing) {
				showInfo(evt);
			}

			//map.setMapCursor('pointer');

			//evt.graphic.symbol.setSize(80);
			evt.graphic.symbol.setColor(new Color([0, 172, 193, 1]));
			evt.graphic.draw();

		} else { // Else not hovering over graphic
			map.infoWindow.hide()
				//map.setMapCursor('default')
			$.each(graphicList, function (i, it) {

				//it.symbol.setSize(40)
				it.symbol.setColor(new Color([255, 138, 101, 1]));
				it.draw();

			})
		}
	})


	/******************
		 On row hover
	******************/

	function getRow() {
		$('table tbody tr').hover(function () {
				var id = $(this).attr('id')
				console.log(id)
				$.each(graphicList, function (i, it) {
					console.log(it.attributes.Name)
					if (id === it.attributes.Name) {
						showInfo(it, true)
					}
				})
			},
			function () {
				map.infoWindow.hide()
			})
	}


	/***********************
		 Get borehole depths
	***********************/
	function boreholeDepth(depths) {
		for (var i = 0; i < depths.length; i++) {
			var bh = "#b" + (i + 1);
			$(bh).height(depths[i] * 8);
			$(bh).next('.drillLabel').text('B' + (i + 1) + ' ' + depths[i])
		}
	}


	/***************************
		 Animate borehole depth
	***************************/

	function animateBoreholes(bg) {
		var speed = 600;

		var name = bg.attributes.Name.toLowerCase()
		var id = '#' + name;

		$('#' + name).children('.drill').css('background-color', '#ff8a65')
		$('#' + name).children('.drillHead').css('background', 'repeating-linear-gradient(45deg, #ff8a65, white 10px)')

		$('#b1').slideDown(speed)
		$('#b2').slideDown(speed)
		$('#b3').slideDown(speed)
		$('#b4').slideDown(speed)

	} // End animateBoreholes


	/*********************
		 Animate Porosity
	*********************/

	function animateMeasurements(mg) {
		var por = mg.attributes.Porosity
		$.each($('circle'), function (count, item) {
			var r = $(this).attr('r');
			var newR = r - (por * 100)

			// Change circle size
			$('circle').animate({
				"r": newR.toString()
			}, 1000)
		})
	} // End animateMeasurements


}); // End require